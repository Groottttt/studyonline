/*
 Navicat Premium Data Transfer

 Source Server         : 达知网校
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : 118.25.131.131:3306
 Source Schema         : dzwx_base

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 13/12/2021 09:45:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_collection
-- ----------------------------
DROP TABLE IF EXISTS `t_collection`;
CREATE TABLE `t_collection`  (
  `user_id` bigint(20) NOT NULL,
  `obj_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`, `obj_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_collection
-- ----------------------------
INSERT INTO `t_collection` VALUES (21, 15);

-- ----------------------------
-- Table structure for t_course
-- ----------------------------
DROP TABLE IF EXISTS `t_course`;
CREATE TABLE `t_course`  (
  `COURSE_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '课程ID',
  `COURSE_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '课程名称',
  `COURSE_TITLE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程标题',
  `SUBJECT` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '科目',
  `STATUS` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态 0锁定 1有效',
  `CREATE_TIME` datetime(0) NOT NULL COMMENT '创建时间',
  `MODIFY_TIME` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `DESCRIPTION` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `THUMBNAIL` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '缩略图',
  `VIDEOURL` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '视频URL',
  `VIDEOSIZE` int(20) NULL DEFAULT 0 COMMENT '视频大小',
  PRIMARY KEY (`COURSE_ID`) USING BTREE,
  INDEX `t_user_username`(`COURSE_NAME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '课程表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_course
-- ----------------------------
INSERT INTO `t_course` VALUES (14, '第一节', NULL, '15', '1', '2021-09-25 09:54:35', NULL, '砌筑工入门', '\\upload\\course\\images\\2021-09-25\\69d5440f-d05c-42c0-98ce-5c4992fec0a8.jpg', '\\upload\\course\\videos\\2021-10-29\\a879de40-b784-4f46-9380-fcfcffcb6b82.mp4', 173715807);
INSERT INTO `t_course` VALUES (22, '第二节', NULL, '15', '1', '2021-10-29 10:12:27', NULL, '', '', '\\upload\\course\\videos\\2021-10-29\\dee53df6-86f1-41f5-91fa-3b4fbcbd6863.mp4', 1130018);
INSERT INTO `t_course` VALUES (23, '第三节', NULL, '15', '1', '2021-11-17 12:43:00', NULL, '', '', '', 0);
INSERT INTO `t_course` VALUES (24, '第四节', NULL, '15', '1', '2021-11-17 12:43:23', NULL, '', '', '', 0);
INSERT INTO `t_course` VALUES (25, '第五节', NULL, '15', '1', '2021-11-17 12:44:14', NULL, '', '', '', 0);
INSERT INTO `t_course` VALUES (26, '第六', NULL, '15', '1', '2021-11-17 12:44:32', NULL, '', '', '', 0);
INSERT INTO `t_course` VALUES (27, 'di7', NULL, '15', '1', '2021-11-17 12:51:53', NULL, '', '', '', 0);
INSERT INTO `t_course` VALUES (28, 'di8', NULL, '15', '1', '2021-11-17 12:52:03', NULL, '', '', '', 0);
INSERT INTO `t_course` VALUES (29, 'di9', NULL, '15', '1', '2021-11-17 12:52:13', NULL, '', '', '', 0);
INSERT INTO `t_course` VALUES (30, '1-----', NULL, '32', '1', '2021-12-08 14:05:13', NULL, '', '', '', 0);
INSERT INTO `t_course` VALUES (31, '2-------', NULL, '32', '1', '2021-12-08 14:05:26', NULL, '', '', '', 0);
INSERT INTO `t_course` VALUES (32, '3-------', NULL, '32', '1', '2021-12-08 14:05:33', NULL, '', '', '', 0);
INSERT INTO `t_course` VALUES (33, '4-------', NULL, '32', '1', '2021-12-08 14:05:40', NULL, '', '', '', 0);

-- ----------------------------
-- Table structure for t_course_type
-- ----------------------------
DROP TABLE IF EXISTS `t_course_type`;
CREATE TABLE `t_course_type`  (
  `CTYPE_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '课程类型ID',
  `PARENT_ID` bigint(20) NOT NULL COMMENT '上级课程类型ID',
  `CTYPE_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '课程类型名称',
  `ORDER_NUM` bigint(20) NULL DEFAULT NULL COMMENT '排序',
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `MODIFY_TIME` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`CTYPE_ID`) USING BTREE,
  INDEX `t_dept_parent_id`(`PARENT_ID`) USING BTREE,
  INDEX `t_dept_dept_name`(`CTYPE_NAME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_course_type
-- ----------------------------
INSERT INTO `t_course_type` VALUES (1, 0, '游戏开发', 1, '2021-08-25 15:41:27', NULL);

-- ----------------------------
-- Table structure for t_dept
-- ----------------------------
DROP TABLE IF EXISTS `t_dept`;
CREATE TABLE `t_dept`  (
  `DEPT_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门ID',
  `PARENT_ID` bigint(20) NOT NULL COMMENT '上级部门ID',
  `DEPT_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门名称',
  `ORDER_NUM` bigint(20) NULL DEFAULT NULL COMMENT '排序',
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `MODIFY_TIME` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`DEPT_ID`) USING BTREE,
  INDEX `t_dept_parent_id`(`PARENT_ID`) USING BTREE,
  INDEX `t_dept_dept_name`(`DEPT_NAME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_dept
-- ----------------------------
INSERT INTO `t_dept` VALUES (10, 0, '系统部', 1, '2019-06-14 21:01:31', '2021-10-13 09:25:25');

-- ----------------------------
-- Table structure for t_dict
-- ----------------------------
DROP TABLE IF EXISTS `t_dict`;
CREATE TABLE `t_dict`  (
  `version` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `url` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_dict
-- ----------------------------
INSERT INTO `t_dict` VALUES ('1', '/upload/apk/dzwx.apk', '2021-10-26 10:16:08');

-- ----------------------------
-- Table structure for t_eximport
-- ----------------------------
DROP TABLE IF EXISTS `t_eximport`;
CREATE TABLE `t_eximport`  (
  `FIELD1` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字段1',
  `FIELD2` int(11) NOT NULL COMMENT '字段2',
  `FIELD3` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字段3',
  `CREATE_TIME` datetime(0) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Excel导入导出测试' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_eximport
-- ----------------------------
INSERT INTO `t_eximport` VALUES ('字段1', 1, 'mrbird0@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 2, 'mrbird1@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 3, 'mrbird2@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 4, 'mrbird3@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 5, 'mrbird4@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 6, 'mrbird5@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 7, 'mrbird6@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 8, 'mrbird7@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 9, 'mrbird8@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 10, 'mrbird9@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 11, 'mrbird10@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 12, 'mrbird11@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 13, 'mrbird12@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 14, 'mrbird13@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 15, 'mrbird14@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 16, 'mrbird15@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 17, 'mrbird16@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 18, 'mrbird17@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 19, 'mrbird18@gmail.com', '2019-06-13 03:14:06');
INSERT INTO `t_eximport` VALUES ('字段1', 20, 'mrbird19@gmail.com', '2019-06-13 03:14:06');

-- ----------------------------
-- Table structure for t_face_detect
-- ----------------------------
DROP TABLE IF EXISTS `t_face_detect`;
CREATE TABLE `t_face_detect`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `face_detect_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '播放时长',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_face_detect
-- ----------------------------
INSERT INTO `t_face_detect` VALUES (1, 20, 14, 'qwertyuyyrterweetytreytr', 30000, '2021-10-11 13:46:38');
INSERT INTO `t_face_detect` VALUES (2, 21, 14, '/upload/face_detect/21/0f2efba4-b779-4061-b81a-cea80235da85.jpg', 127016, '2021-10-11 13:58:53');
INSERT INTO `t_face_detect` VALUES (3, 21, 14, '/upload/face_detect/21/ea8c50a8-23ff-48ec-aa44-e9dc7b52468a.jpg', 13359, '2021-10-11 14:24:05');
INSERT INTO `t_face_detect` VALUES (4, 21, 14, '/upload/face_detect/21/5e06b79e-931b-4e5c-beaf-2fe00e66fee9.jpg', 14716, '2021-10-12 09:27:49');
INSERT INTO `t_face_detect` VALUES (5, 21, 14, '/upload/face_detect/21/33a9dd0e-dc99-49a9-aa9a-18e2ee366fc1.jpg', 448396, '2021-10-14 16:39:32');

-- ----------------------------
-- Table structure for t_generator_config
-- ----------------------------
DROP TABLE IF EXISTS `t_generator_config`;
CREATE TABLE `t_generator_config`  (
  `id` int(11) NOT NULL COMMENT '主键',
  `author` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '作者',
  `base_package` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '基础包名',
  `entity_package` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'entity文件存放路径',
  `mapper_package` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'mapper文件存放路径',
  `mapper_xml_package` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'mapper xml文件存放路径',
  `service_package` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'servcie文件存放路径',
  `service_impl_package` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'serviceImpl文件存放路径',
  `controller_package` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'controller文件存放路径',
  `is_trim` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否去除前缀 1是 0否',
  `trim_value` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '前缀内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_generator_config
-- ----------------------------
INSERT INTO `t_generator_config` VALUES (1, 'MrBird', 'com.dzwx.gen', 'entity', 'mapper', 'mapper', 'service', 'service.impl', 'controller', '1', 't_');

-- ----------------------------
-- Table structure for t_job
-- ----------------------------
DROP TABLE IF EXISTS `t_job`;
CREATE TABLE `t_job`  (
  `JOB_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `BEAN_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'spring bean名称',
  `METHOD_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '方法名',
  `PARAMS` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `CRON_EXPRESSION` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'cron表达式',
  `STATUS` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务状态  0：正常  1：暂停',
  `REMARK` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`JOB_ID`) USING BTREE,
  INDEX `t_job_create_time`(`CREATE_TIME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_job
-- ----------------------------
INSERT INTO `t_job` VALUES (2, 'testTask', 'test1', NULL, '0/10 * * * * ?', '1', '无参任务调度测试', '2018-02-24 17:06:23');
INSERT INTO `t_job` VALUES (3, 'testTask', 'test', 'hello world', '0/1 * * * * ?', '1', '有参任务调度测试,每隔一秒触发', '2018-02-26 09:28:26');
INSERT INTO `t_job` VALUES (12, 'dbBackupTask', 'dbBackup', '', '0 0 0 * * ? ', '1', '数据库备份每天0点执行，保留七天', '2021-09-16 16:46:33');

-- ----------------------------
-- Table structure for t_job_log
-- ----------------------------
DROP TABLE IF EXISTS `t_job_log`;
CREATE TABLE `t_job_log`  (
  `LOG_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志id',
  `JOB_ID` bigint(20) NOT NULL COMMENT '任务id',
  `BEAN_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'spring bean名称',
  `METHOD_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '方法名',
  `PARAMS` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `STATUS` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `ERROR` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '失败信息',
  `TIMES` decimal(11, 0) NULL DEFAULT NULL COMMENT '耗时(单位：毫秒)',
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`LOG_ID`) USING BTREE,
  INDEX `t_job_log_create_time`(`CREATE_TIME`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4248 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_job_log
-- ----------------------------
INSERT INTO `t_job_log` VALUES (2782, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-09-16 17:42:19');
INSERT INTO `t_job_log` VALUES (2783, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-09-16 17:42:22');
INSERT INTO `t_job_log` VALUES (2784, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-09-16 17:42:25');
INSERT INTO `t_job_log` VALUES (2785, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-09-16 17:42:29');
INSERT INTO `t_job_log` VALUES (2786, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 3, '2021-09-22 17:28:18');
INSERT INTO `t_job_log` VALUES (2787, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 5, '2021-09-22 17:33:45');
INSERT INTO `t_job_log` VALUES (2788, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:33:49');
INSERT INTO `t_job_log` VALUES (2789, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:33:51');
INSERT INTO `t_job_log` VALUES (2790, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 0, '2021-09-22 17:33:52');
INSERT INTO `t_job_log` VALUES (2791, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-09-22 17:33:53');
INSERT INTO `t_job_log` VALUES (2792, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:33:54');
INSERT INTO `t_job_log` VALUES (2793, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 10, '2021-09-22 17:33:55');
INSERT INTO `t_job_log` VALUES (2794, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:33:56');
INSERT INTO `t_job_log` VALUES (2795, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:33:57');
INSERT INTO `t_job_log` VALUES (2796, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 0, '2021-09-22 17:33:58');
INSERT INTO `t_job_log` VALUES (2797, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:33:59');
INSERT INTO `t_job_log` VALUES (2798, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:34:01');
INSERT INTO `t_job_log` VALUES (2799, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:34:02');
INSERT INTO `t_job_log` VALUES (2800, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:34:03');
INSERT INTO `t_job_log` VALUES (2801, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:34:04');
INSERT INTO `t_job_log` VALUES (2802, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-09-22 17:34:05');
INSERT INTO `t_job_log` VALUES (2803, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:34:06');
INSERT INTO `t_job_log` VALUES (2804, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:34:07');
INSERT INTO `t_job_log` VALUES (2805, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 0, '2021-09-22 17:34:09');
INSERT INTO `t_job_log` VALUES (2806, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:34:10');
INSERT INTO `t_job_log` VALUES (2807, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:34:11');
INSERT INTO `t_job_log` VALUES (2808, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 0, '2021-09-22 17:34:12');
INSERT INTO `t_job_log` VALUES (2809, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:34:13');
INSERT INTO `t_job_log` VALUES (2810, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 0, '2021-09-22 17:34:15');
INSERT INTO `t_job_log` VALUES (2811, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:34:16');
INSERT INTO `t_job_log` VALUES (2812, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:34:17');
INSERT INTO `t_job_log` VALUES (2813, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-09-22 17:34:18');
INSERT INTO `t_job_log` VALUES (2814, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:34:19');
INSERT INTO `t_job_log` VALUES (2815, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-09-22 17:34:52');
INSERT INTO `t_job_log` VALUES (2816, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-09-22 17:34:53');
INSERT INTO `t_job_log` VALUES (2817, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:35:00');
INSERT INTO `t_job_log` VALUES (2818, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-09-22 17:35:01');
INSERT INTO `t_job_log` VALUES (2819, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:35:03');
INSERT INTO `t_job_log` VALUES (2820, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 0, '2021-09-22 17:35:04');
INSERT INTO `t_job_log` VALUES (2821, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:35:05');
INSERT INTO `t_job_log` VALUES (2822, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:35:38');
INSERT INTO `t_job_log` VALUES (2823, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:35:39');
INSERT INTO `t_job_log` VALUES (2824, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:35:40');
INSERT INTO `t_job_log` VALUES (2825, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:35:41');
INSERT INTO `t_job_log` VALUES (2826, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:35:43');
INSERT INTO `t_job_log` VALUES (2827, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:35:44');
INSERT INTO `t_job_log` VALUES (2828, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:35:45');
INSERT INTO `t_job_log` VALUES (2829, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:35:46');
INSERT INTO `t_job_log` VALUES (2830, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:35:47');
INSERT INTO `t_job_log` VALUES (2831, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:35:48');
INSERT INTO `t_job_log` VALUES (2832, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:35:49');
INSERT INTO `t_job_log` VALUES (2833, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:35:51');
INSERT INTO `t_job_log` VALUES (2834, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:35:52');
INSERT INTO `t_job_log` VALUES (2835, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:35:53');
INSERT INTO `t_job_log` VALUES (2836, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 5, '2021-09-22 17:35:54');
INSERT INTO `t_job_log` VALUES (2837, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:35:55');
INSERT INTO `t_job_log` VALUES (2838, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:35:56');
INSERT INTO `t_job_log` VALUES (2839, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 5, '2021-09-22 17:35:58');
INSERT INTO `t_job_log` VALUES (2840, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:35:59');
INSERT INTO `t_job_log` VALUES (2841, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:00');
INSERT INTO `t_job_log` VALUES (2842, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:36:01');
INSERT INTO `t_job_log` VALUES (2843, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 0, '2021-09-22 17:36:02');
INSERT INTO `t_job_log` VALUES (2844, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:03');
INSERT INTO `t_job_log` VALUES (2845, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 0, '2021-09-22 17:36:04');
INSERT INTO `t_job_log` VALUES (2846, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:05');
INSERT INTO `t_job_log` VALUES (2847, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:36:06');
INSERT INTO `t_job_log` VALUES (2848, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:08');
INSERT INTO `t_job_log` VALUES (2849, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:09');
INSERT INTO `t_job_log` VALUES (2850, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:36:10');
INSERT INTO `t_job_log` VALUES (2851, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:11');
INSERT INTO `t_job_log` VALUES (2852, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 0, '2021-09-22 17:36:12');
INSERT INTO `t_job_log` VALUES (2853, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:13');
INSERT INTO `t_job_log` VALUES (2854, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:15');
INSERT INTO `t_job_log` VALUES (2855, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:16');
INSERT INTO `t_job_log` VALUES (2856, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:17');
INSERT INTO `t_job_log` VALUES (2857, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:18');
INSERT INTO `t_job_log` VALUES (2858, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:19');
INSERT INTO `t_job_log` VALUES (2859, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:20');
INSERT INTO `t_job_log` VALUES (2860, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:22');
INSERT INTO `t_job_log` VALUES (2861, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:23');
INSERT INTO `t_job_log` VALUES (2862, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:36:24');
INSERT INTO `t_job_log` VALUES (2863, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:25');
INSERT INTO `t_job_log` VALUES (2864, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:26');
INSERT INTO `t_job_log` VALUES (2865, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:28');
INSERT INTO `t_job_log` VALUES (2866, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:29');
INSERT INTO `t_job_log` VALUES (2867, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:31');
INSERT INTO `t_job_log` VALUES (2868, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:32');
INSERT INTO `t_job_log` VALUES (2869, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:36:33');
INSERT INTO `t_job_log` VALUES (2870, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:36:34');
INSERT INTO `t_job_log` VALUES (2871, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:35');
INSERT INTO `t_job_log` VALUES (2872, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:36');
INSERT INTO `t_job_log` VALUES (2873, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:36:37');
INSERT INTO `t_job_log` VALUES (2874, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 3, '2021-09-22 17:38:23');
INSERT INTO `t_job_log` VALUES (2875, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:38:24');
INSERT INTO `t_job_log` VALUES (2876, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:38:25');
INSERT INTO `t_job_log` VALUES (2877, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 2, '2021-09-22 17:38:27');
INSERT INTO `t_job_log` VALUES (2878, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:38:29');
INSERT INTO `t_job_log` VALUES (2879, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:38:30');
INSERT INTO `t_job_log` VALUES (2880, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 1, '2021-09-22 17:38:31');
INSERT INTO `t_job_log` VALUES (2881, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 863, '2021-09-22 17:40:09');
INSERT INTO `t_job_log` VALUES (2882, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 466, '2021-09-22 17:40:11');
INSERT INTO `t_job_log` VALUES (2883, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 413, '2021-09-22 17:40:12');
INSERT INTO `t_job_log` VALUES (2884, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 442, '2021-09-22 17:40:13');
INSERT INTO `t_job_log` VALUES (2885, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 428, '2021-09-22 17:40:14');
INSERT INTO `t_job_log` VALUES (2886, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 423, '2021-09-22 17:40:16');
INSERT INTO `t_job_log` VALUES (2887, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 419, '2021-09-22 17:40:17');
INSERT INTO `t_job_log` VALUES (2888, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 428, '2021-09-22 17:40:18');
INSERT INTO `t_job_log` VALUES (2889, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 511, '2021-09-22 17:40:19');
INSERT INTO `t_job_log` VALUES (2890, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 670, '2021-09-22 17:40:21');
INSERT INTO `t_job_log` VALUES (2891, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 464, '2021-09-22 17:40:22');
INSERT INTO `t_job_log` VALUES (2892, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 424, '2021-09-22 17:40:23');
INSERT INTO `t_job_log` VALUES (2893, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 417, '2021-09-22 17:40:24');
INSERT INTO `t_job_log` VALUES (2894, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 443, '2021-09-22 17:40:25');
INSERT INTO `t_job_log` VALUES (2895, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 419, '2021-09-22 17:40:27');
INSERT INTO `t_job_log` VALUES (2896, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 496, '2021-09-22 17:40:28');
INSERT INTO `t_job_log` VALUES (2897, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 426, '2021-09-22 17:40:29');
INSERT INTO `t_job_log` VALUES (2898, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 474, '2021-09-22 17:40:30');
INSERT INTO `t_job_log` VALUES (2899, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 431, '2021-09-22 17:41:24');
INSERT INTO `t_job_log` VALUES (2900, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 966, '2021-09-22 17:46:31');
INSERT INTO `t_job_log` VALUES (2901, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 983, '2021-09-22 17:49:10');
INSERT INTO `t_job_log` VALUES (2902, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 16043, '2021-09-22 17:50:00');
INSERT INTO `t_job_log` VALUES (2903, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 90673, '2021-09-24 17:54:46');
INSERT INTO `t_job_log` VALUES (2904, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-09-26 14:18:31');
INSERT INTO `t_job_log` VALUES (2905, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-09-26 14:18:41');
INSERT INTO `t_job_log` VALUES (2906, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-09-26 14:18:51');
INSERT INTO `t_job_log` VALUES (2907, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-09-26 14:19:01');
INSERT INTO `t_job_log` VALUES (2908, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-09-26 14:19:11');
INSERT INTO `t_job_log` VALUES (2909, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-09-26 14:19:21');
INSERT INTO `t_job_log` VALUES (2910, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-09-26 14:19:31');
INSERT INTO `t_job_log` VALUES (2911, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-09-26 14:19:41');
INSERT INTO `t_job_log` VALUES (2912, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-09-26 14:19:51');
INSERT INTO `t_job_log` VALUES (2913, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-09-26 14:20:01');
INSERT INTO `t_job_log` VALUES (2914, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-09-26 14:20:11');
INSERT INTO `t_job_log` VALUES (2915, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-09-26 14:20:21');
INSERT INTO `t_job_log` VALUES (2916, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-09-26 14:20:31');
INSERT INTO `t_job_log` VALUES (2917, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-09-26 14:20:41');
INSERT INTO `t_job_log` VALUES (2918, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-09-26 14:20:51');
INSERT INTO `t_job_log` VALUES (2919, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-09-26 14:21:01');
INSERT INTO `t_job_log` VALUES (2920, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-09-26 14:21:11');
INSERT INTO `t_job_log` VALUES (2921, 2, 'testTask', 'test1', NULL, '0', NULL, 7, '2021-10-12 13:50:30');
INSERT INTO `t_job_log` VALUES (2922, 2, 'testTask', 'test1', NULL, '0', NULL, 4, '2021-10-12 13:50:40');
INSERT INTO `t_job_log` VALUES (2923, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-10-12 13:50:50');
INSERT INTO `t_job_log` VALUES (2924, 2, 'testTask', 'test1', NULL, '0', NULL, 4, '2021-10-12 13:51:00');
INSERT INTO `t_job_log` VALUES (2925, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-10-12 13:51:10');
INSERT INTO `t_job_log` VALUES (2926, 2, 'testTask', 'test1', NULL, '0', NULL, 2, '2021-10-12 13:51:20');
INSERT INTO `t_job_log` VALUES (2927, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-10-12 13:51:30');
INSERT INTO `t_job_log` VALUES (2928, 2, 'testTask', 'test1', NULL, '0', NULL, 4, '2021-10-12 13:51:40');
INSERT INTO `t_job_log` VALUES (2929, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-10-12 13:51:50');
INSERT INTO `t_job_log` VALUES (2930, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-10-12 13:52:00');
INSERT INTO `t_job_log` VALUES (2931, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-10-12 13:52:10');
INSERT INTO `t_job_log` VALUES (2932, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-10-12 13:52:20');
INSERT INTO `t_job_log` VALUES (2933, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-10-12 13:52:30');
INSERT INTO `t_job_log` VALUES (2934, 2, 'testTask', 'test1', NULL, '0', NULL, 2, '2021-10-12 13:52:40');
INSERT INTO `t_job_log` VALUES (2935, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-10-12 13:52:50');
INSERT INTO `t_job_log` VALUES (2936, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-10-12 13:53:00');
INSERT INTO `t_job_log` VALUES (2937, 2, 'testTask', 'test1', NULL, '0', NULL, 2, '2021-10-12 13:53:10');
INSERT INTO `t_job_log` VALUES (2938, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-10-12 13:53:20');
INSERT INTO `t_job_log` VALUES (2939, 2, 'testTask', 'test1', NULL, '0', NULL, 2, '2021-10-12 13:53:30');
INSERT INTO `t_job_log` VALUES (2940, 2, 'testTask', 'test1', NULL, '0', NULL, 4, '2021-10-12 13:53:40');
INSERT INTO `t_job_log` VALUES (2941, 2, 'testTask', 'test1', NULL, '0', NULL, 2, '2021-10-12 13:53:50');
INSERT INTO `t_job_log` VALUES (2942, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-10-12 13:54:00');
INSERT INTO `t_job_log` VALUES (2943, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-10-12 13:54:10');
INSERT INTO `t_job_log` VALUES (2944, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-10-12 13:54:20');
INSERT INTO `t_job_log` VALUES (2945, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:27');
INSERT INTO `t_job_log` VALUES (2946, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:27');
INSERT INTO `t_job_log` VALUES (2947, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:28');
INSERT INTO `t_job_log` VALUES (2948, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:29');
INSERT INTO `t_job_log` VALUES (2949, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-12 16:14:30');
INSERT INTO `t_job_log` VALUES (2950, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-12 16:14:31');
INSERT INTO `t_job_log` VALUES (2951, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-12 16:14:32');
INSERT INTO `t_job_log` VALUES (2952, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-12 16:14:33');
INSERT INTO `t_job_log` VALUES (2953, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:34');
INSERT INTO `t_job_log` VALUES (2954, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:35');
INSERT INTO `t_job_log` VALUES (2955, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:36');
INSERT INTO `t_job_log` VALUES (2956, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:37');
INSERT INTO `t_job_log` VALUES (2957, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-12 16:14:38');
INSERT INTO `t_job_log` VALUES (2958, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:39');
INSERT INTO `t_job_log` VALUES (2959, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-12 16:14:40');
INSERT INTO `t_job_log` VALUES (2960, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-12 16:14:41');
INSERT INTO `t_job_log` VALUES (2961, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:42');
INSERT INTO `t_job_log` VALUES (2962, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-12 16:14:43');
INSERT INTO `t_job_log` VALUES (2963, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-12 16:14:44');
INSERT INTO `t_job_log` VALUES (2964, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-12 16:14:45');
INSERT INTO `t_job_log` VALUES (2965, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:46');
INSERT INTO `t_job_log` VALUES (2966, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:47');
INSERT INTO `t_job_log` VALUES (2967, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:48');
INSERT INTO `t_job_log` VALUES (2968, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:49');
INSERT INTO `t_job_log` VALUES (2969, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:50');
INSERT INTO `t_job_log` VALUES (2970, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-12 16:14:51');
INSERT INTO `t_job_log` VALUES (2971, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:52');
INSERT INTO `t_job_log` VALUES (2972, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:53');
INSERT INTO `t_job_log` VALUES (2973, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:54');
INSERT INTO `t_job_log` VALUES (2974, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:55');
INSERT INTO `t_job_log` VALUES (2975, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:56');
INSERT INTO `t_job_log` VALUES (2976, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-12 16:14:57');
INSERT INTO `t_job_log` VALUES (2977, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:58');
INSERT INTO `t_job_log` VALUES (2978, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:14:59');
INSERT INTO `t_job_log` VALUES (2979, 3, 'testTask', 'test', 'hello world', '0', NULL, 6, '2021-10-12 16:15:00');
INSERT INTO `t_job_log` VALUES (2980, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-12 16:15:01');
INSERT INTO `t_job_log` VALUES (2981, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:15:02');
INSERT INTO `t_job_log` VALUES (2982, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-12 16:15:03');
INSERT INTO `t_job_log` VALUES (2983, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-12 16:15:04');
INSERT INTO `t_job_log` VALUES (2984, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:15:05');
INSERT INTO `t_job_log` VALUES (2985, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:15:06');
INSERT INTO `t_job_log` VALUES (2986, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:15:07');
INSERT INTO `t_job_log` VALUES (2987, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:15:08');
INSERT INTO `t_job_log` VALUES (2988, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:15:09');
INSERT INTO `t_job_log` VALUES (2989, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-12 16:15:10');
INSERT INTO `t_job_log` VALUES (2990, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:15:11');
INSERT INTO `t_job_log` VALUES (2991, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-12 16:15:12');
INSERT INTO `t_job_log` VALUES (2992, 3, 'testTask', 'test', 'hello world', '0', NULL, 5, '2021-10-13 08:43:32');
INSERT INTO `t_job_log` VALUES (2993, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:43:33');
INSERT INTO `t_job_log` VALUES (2994, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:43:34');
INSERT INTO `t_job_log` VALUES (2995, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:35');
INSERT INTO `t_job_log` VALUES (2996, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:36');
INSERT INTO `t_job_log` VALUES (2997, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:43:37');
INSERT INTO `t_job_log` VALUES (2998, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:43:38');
INSERT INTO `t_job_log` VALUES (2999, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:39');
INSERT INTO `t_job_log` VALUES (3000, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:40');
INSERT INTO `t_job_log` VALUES (3001, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:41');
INSERT INTO `t_job_log` VALUES (3002, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:43:42');
INSERT INTO `t_job_log` VALUES (3003, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:43:43');
INSERT INTO `t_job_log` VALUES (3004, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:43:44');
INSERT INTO `t_job_log` VALUES (3005, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:43:45');
INSERT INTO `t_job_log` VALUES (3006, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:43:46');
INSERT INTO `t_job_log` VALUES (3007, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:47');
INSERT INTO `t_job_log` VALUES (3008, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:48');
INSERT INTO `t_job_log` VALUES (3009, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:49');
INSERT INTO `t_job_log` VALUES (3010, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:43:50');
INSERT INTO `t_job_log` VALUES (3011, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:51');
INSERT INTO `t_job_log` VALUES (3012, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:52');
INSERT INTO `t_job_log` VALUES (3013, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:53');
INSERT INTO `t_job_log` VALUES (3014, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:54');
INSERT INTO `t_job_log` VALUES (3015, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:55');
INSERT INTO `t_job_log` VALUES (3016, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:56');
INSERT INTO `t_job_log` VALUES (3017, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:57');
INSERT INTO `t_job_log` VALUES (3018, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:43:58');
INSERT INTO `t_job_log` VALUES (3019, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:43:59');
INSERT INTO `t_job_log` VALUES (3020, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:00');
INSERT INTO `t_job_log` VALUES (3021, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:01');
INSERT INTO `t_job_log` VALUES (3022, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:02');
INSERT INTO `t_job_log` VALUES (3023, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:03');
INSERT INTO `t_job_log` VALUES (3024, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:04');
INSERT INTO `t_job_log` VALUES (3025, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:05');
INSERT INTO `t_job_log` VALUES (3026, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:06');
INSERT INTO `t_job_log` VALUES (3027, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:07');
INSERT INTO `t_job_log` VALUES (3028, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:08');
INSERT INTO `t_job_log` VALUES (3029, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:09');
INSERT INTO `t_job_log` VALUES (3030, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:10');
INSERT INTO `t_job_log` VALUES (3031, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:11');
INSERT INTO `t_job_log` VALUES (3032, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:12');
INSERT INTO `t_job_log` VALUES (3033, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:13');
INSERT INTO `t_job_log` VALUES (3034, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:14');
INSERT INTO `t_job_log` VALUES (3035, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:15');
INSERT INTO `t_job_log` VALUES (3036, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:16');
INSERT INTO `t_job_log` VALUES (3037, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:17');
INSERT INTO `t_job_log` VALUES (3038, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:18');
INSERT INTO `t_job_log` VALUES (3039, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:19');
INSERT INTO `t_job_log` VALUES (3040, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:20');
INSERT INTO `t_job_log` VALUES (3041, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:21');
INSERT INTO `t_job_log` VALUES (3042, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:22');
INSERT INTO `t_job_log` VALUES (3043, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:23');
INSERT INTO `t_job_log` VALUES (3044, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:44:24');
INSERT INTO `t_job_log` VALUES (3045, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:25');
INSERT INTO `t_job_log` VALUES (3046, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:26');
INSERT INTO `t_job_log` VALUES (3047, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:27');
INSERT INTO `t_job_log` VALUES (3048, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:28');
INSERT INTO `t_job_log` VALUES (3049, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:29');
INSERT INTO `t_job_log` VALUES (3050, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:30');
INSERT INTO `t_job_log` VALUES (3051, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:31');
INSERT INTO `t_job_log` VALUES (3052, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:32');
INSERT INTO `t_job_log` VALUES (3053, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:33');
INSERT INTO `t_job_log` VALUES (3054, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:34');
INSERT INTO `t_job_log` VALUES (3055, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:35');
INSERT INTO `t_job_log` VALUES (3056, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:36');
INSERT INTO `t_job_log` VALUES (3057, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:37');
INSERT INTO `t_job_log` VALUES (3058, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:38');
INSERT INTO `t_job_log` VALUES (3059, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:39');
INSERT INTO `t_job_log` VALUES (3060, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:40');
INSERT INTO `t_job_log` VALUES (3061, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:41');
INSERT INTO `t_job_log` VALUES (3062, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:42');
INSERT INTO `t_job_log` VALUES (3063, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:43');
INSERT INTO `t_job_log` VALUES (3064, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:44');
INSERT INTO `t_job_log` VALUES (3065, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:45');
INSERT INTO `t_job_log` VALUES (3066, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:46');
INSERT INTO `t_job_log` VALUES (3067, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:47');
INSERT INTO `t_job_log` VALUES (3068, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:48');
INSERT INTO `t_job_log` VALUES (3069, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:49');
INSERT INTO `t_job_log` VALUES (3070, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:50');
INSERT INTO `t_job_log` VALUES (3071, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:51');
INSERT INTO `t_job_log` VALUES (3072, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:52');
INSERT INTO `t_job_log` VALUES (3073, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:53');
INSERT INTO `t_job_log` VALUES (3074, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:54');
INSERT INTO `t_job_log` VALUES (3075, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:55');
INSERT INTO `t_job_log` VALUES (3076, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:56');
INSERT INTO `t_job_log` VALUES (3077, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:57');
INSERT INTO `t_job_log` VALUES (3078, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:44:58');
INSERT INTO `t_job_log` VALUES (3079, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:44:59');
INSERT INTO `t_job_log` VALUES (3080, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:00');
INSERT INTO `t_job_log` VALUES (3081, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:45:01');
INSERT INTO `t_job_log` VALUES (3082, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:02');
INSERT INTO `t_job_log` VALUES (3083, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:03');
INSERT INTO `t_job_log` VALUES (3084, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:04');
INSERT INTO `t_job_log` VALUES (3085, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:45:05');
INSERT INTO `t_job_log` VALUES (3086, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:06');
INSERT INTO `t_job_log` VALUES (3087, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:07');
INSERT INTO `t_job_log` VALUES (3088, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:08');
INSERT INTO `t_job_log` VALUES (3089, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:45:09');
INSERT INTO `t_job_log` VALUES (3090, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:10');
INSERT INTO `t_job_log` VALUES (3091, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:11');
INSERT INTO `t_job_log` VALUES (3092, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:12');
INSERT INTO `t_job_log` VALUES (3093, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:13');
INSERT INTO `t_job_log` VALUES (3094, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:45:14');
INSERT INTO `t_job_log` VALUES (3095, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:45:15');
INSERT INTO `t_job_log` VALUES (3096, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:16');
INSERT INTO `t_job_log` VALUES (3097, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:17');
INSERT INTO `t_job_log` VALUES (3098, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:18');
INSERT INTO `t_job_log` VALUES (3099, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:19');
INSERT INTO `t_job_log` VALUES (3100, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:20');
INSERT INTO `t_job_log` VALUES (3101, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:45:21');
INSERT INTO `t_job_log` VALUES (3102, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:45:22');
INSERT INTO `t_job_log` VALUES (3103, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:23');
INSERT INTO `t_job_log` VALUES (3104, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:24');
INSERT INTO `t_job_log` VALUES (3105, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:25');
INSERT INTO `t_job_log` VALUES (3106, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:26');
INSERT INTO `t_job_log` VALUES (3107, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:27');
INSERT INTO `t_job_log` VALUES (3108, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:28');
INSERT INTO `t_job_log` VALUES (3109, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:29');
INSERT INTO `t_job_log` VALUES (3110, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:30');
INSERT INTO `t_job_log` VALUES (3111, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:31');
INSERT INTO `t_job_log` VALUES (3112, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:45:32');
INSERT INTO `t_job_log` VALUES (3113, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:33');
INSERT INTO `t_job_log` VALUES (3114, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:34');
INSERT INTO `t_job_log` VALUES (3115, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:35');
INSERT INTO `t_job_log` VALUES (3116, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:36');
INSERT INTO `t_job_log` VALUES (3117, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:37');
INSERT INTO `t_job_log` VALUES (3118, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:38');
INSERT INTO `t_job_log` VALUES (3119, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:39');
INSERT INTO `t_job_log` VALUES (3120, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:40');
INSERT INTO `t_job_log` VALUES (3121, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:45:41');
INSERT INTO `t_job_log` VALUES (3122, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:45:42');
INSERT INTO `t_job_log` VALUES (3123, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:43');
INSERT INTO `t_job_log` VALUES (3124, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:45:44');
INSERT INTO `t_job_log` VALUES (3125, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:45');
INSERT INTO `t_job_log` VALUES (3126, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:45:46');
INSERT INTO `t_job_log` VALUES (3127, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:45:47');
INSERT INTO `t_job_log` VALUES (3128, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:45:48');
INSERT INTO `t_job_log` VALUES (3129, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:49');
INSERT INTO `t_job_log` VALUES (3130, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:50');
INSERT INTO `t_job_log` VALUES (3131, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:51');
INSERT INTO `t_job_log` VALUES (3132, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:45:52');
INSERT INTO `t_job_log` VALUES (3133, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:53');
INSERT INTO `t_job_log` VALUES (3134, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:54');
INSERT INTO `t_job_log` VALUES (3135, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:55');
INSERT INTO `t_job_log` VALUES (3136, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:56');
INSERT INTO `t_job_log` VALUES (3137, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:57');
INSERT INTO `t_job_log` VALUES (3138, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:58');
INSERT INTO `t_job_log` VALUES (3139, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:45:59');
INSERT INTO `t_job_log` VALUES (3140, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:00');
INSERT INTO `t_job_log` VALUES (3141, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:01');
INSERT INTO `t_job_log` VALUES (3142, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:02');
INSERT INTO `t_job_log` VALUES (3143, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:03');
INSERT INTO `t_job_log` VALUES (3144, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:04');
INSERT INTO `t_job_log` VALUES (3145, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:05');
INSERT INTO `t_job_log` VALUES (3146, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:06');
INSERT INTO `t_job_log` VALUES (3147, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:07');
INSERT INTO `t_job_log` VALUES (3148, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:08');
INSERT INTO `t_job_log` VALUES (3149, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:09');
INSERT INTO `t_job_log` VALUES (3150, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:10');
INSERT INTO `t_job_log` VALUES (3151, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:11');
INSERT INTO `t_job_log` VALUES (3152, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:12');
INSERT INTO `t_job_log` VALUES (3153, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:13');
INSERT INTO `t_job_log` VALUES (3154, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:14');
INSERT INTO `t_job_log` VALUES (3155, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:15');
INSERT INTO `t_job_log` VALUES (3156, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:16');
INSERT INTO `t_job_log` VALUES (3157, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:17');
INSERT INTO `t_job_log` VALUES (3158, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:18');
INSERT INTO `t_job_log` VALUES (3159, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:19');
INSERT INTO `t_job_log` VALUES (3160, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:20');
INSERT INTO `t_job_log` VALUES (3161, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:21');
INSERT INTO `t_job_log` VALUES (3162, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:22');
INSERT INTO `t_job_log` VALUES (3163, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:23');
INSERT INTO `t_job_log` VALUES (3164, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:24');
INSERT INTO `t_job_log` VALUES (3165, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:25');
INSERT INTO `t_job_log` VALUES (3166, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:26');
INSERT INTO `t_job_log` VALUES (3167, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:27');
INSERT INTO `t_job_log` VALUES (3168, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:28');
INSERT INTO `t_job_log` VALUES (3169, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:29');
INSERT INTO `t_job_log` VALUES (3170, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:30');
INSERT INTO `t_job_log` VALUES (3171, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:31');
INSERT INTO `t_job_log` VALUES (3172, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:32');
INSERT INTO `t_job_log` VALUES (3173, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:33');
INSERT INTO `t_job_log` VALUES (3174, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:34');
INSERT INTO `t_job_log` VALUES (3175, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:35');
INSERT INTO `t_job_log` VALUES (3176, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:36');
INSERT INTO `t_job_log` VALUES (3177, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:37');
INSERT INTO `t_job_log` VALUES (3178, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:38');
INSERT INTO `t_job_log` VALUES (3179, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:39');
INSERT INTO `t_job_log` VALUES (3180, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:40');
INSERT INTO `t_job_log` VALUES (3181, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:41');
INSERT INTO `t_job_log` VALUES (3182, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:42');
INSERT INTO `t_job_log` VALUES (3183, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:43');
INSERT INTO `t_job_log` VALUES (3184, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:46:44');
INSERT INTO `t_job_log` VALUES (3185, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:46:45');
INSERT INTO `t_job_log` VALUES (3186, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:46');
INSERT INTO `t_job_log` VALUES (3187, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:47');
INSERT INTO `t_job_log` VALUES (3188, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:48');
INSERT INTO `t_job_log` VALUES (3189, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:49');
INSERT INTO `t_job_log` VALUES (3190, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:50');
INSERT INTO `t_job_log` VALUES (3191, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:51');
INSERT INTO `t_job_log` VALUES (3192, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:52');
INSERT INTO `t_job_log` VALUES (3193, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:53');
INSERT INTO `t_job_log` VALUES (3194, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:54');
INSERT INTO `t_job_log` VALUES (3195, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:55');
INSERT INTO `t_job_log` VALUES (3196, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:56');
INSERT INTO `t_job_log` VALUES (3197, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:57');
INSERT INTO `t_job_log` VALUES (3198, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:46:58');
INSERT INTO `t_job_log` VALUES (3199, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:46:59');
INSERT INTO `t_job_log` VALUES (3200, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:00');
INSERT INTO `t_job_log` VALUES (3201, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:01');
INSERT INTO `t_job_log` VALUES (3202, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:02');
INSERT INTO `t_job_log` VALUES (3203, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:03');
INSERT INTO `t_job_log` VALUES (3204, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:04');
INSERT INTO `t_job_log` VALUES (3205, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:05');
INSERT INTO `t_job_log` VALUES (3206, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:47:06');
INSERT INTO `t_job_log` VALUES (3207, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:47:07');
INSERT INTO `t_job_log` VALUES (3208, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:08');
INSERT INTO `t_job_log` VALUES (3209, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:09');
INSERT INTO `t_job_log` VALUES (3210, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:10');
INSERT INTO `t_job_log` VALUES (3211, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:47:11');
INSERT INTO `t_job_log` VALUES (3212, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:12');
INSERT INTO `t_job_log` VALUES (3213, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:13');
INSERT INTO `t_job_log` VALUES (3214, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:14');
INSERT INTO `t_job_log` VALUES (3215, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:15');
INSERT INTO `t_job_log` VALUES (3216, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:16');
INSERT INTO `t_job_log` VALUES (3217, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:17');
INSERT INTO `t_job_log` VALUES (3218, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:47:19');
INSERT INTO `t_job_log` VALUES (3219, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:19');
INSERT INTO `t_job_log` VALUES (3220, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:20');
INSERT INTO `t_job_log` VALUES (3221, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:21');
INSERT INTO `t_job_log` VALUES (3222, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:22');
INSERT INTO `t_job_log` VALUES (3223, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:47:23');
INSERT INTO `t_job_log` VALUES (3224, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:24');
INSERT INTO `t_job_log` VALUES (3225, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:25');
INSERT INTO `t_job_log` VALUES (3226, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:47:26');
INSERT INTO `t_job_log` VALUES (3227, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:47:27');
INSERT INTO `t_job_log` VALUES (3228, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:28');
INSERT INTO `t_job_log` VALUES (3229, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:29');
INSERT INTO `t_job_log` VALUES (3230, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:30');
INSERT INTO `t_job_log` VALUES (3231, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:31');
INSERT INTO `t_job_log` VALUES (3232, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:32');
INSERT INTO `t_job_log` VALUES (3233, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:33');
INSERT INTO `t_job_log` VALUES (3234, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:34');
INSERT INTO `t_job_log` VALUES (3235, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:35');
INSERT INTO `t_job_log` VALUES (3236, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:36');
INSERT INTO `t_job_log` VALUES (3237, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:37');
INSERT INTO `t_job_log` VALUES (3238, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:38');
INSERT INTO `t_job_log` VALUES (3239, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:47:39');
INSERT INTO `t_job_log` VALUES (3240, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:40');
INSERT INTO `t_job_log` VALUES (3241, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:41');
INSERT INTO `t_job_log` VALUES (3242, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:47:42');
INSERT INTO `t_job_log` VALUES (3243, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:47:43');
INSERT INTO `t_job_log` VALUES (3244, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:47:44');
INSERT INTO `t_job_log` VALUES (3245, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:47:45');
INSERT INTO `t_job_log` VALUES (3246, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:46');
INSERT INTO `t_job_log` VALUES (3247, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:47:47');
INSERT INTO `t_job_log` VALUES (3248, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:47:48');
INSERT INTO `t_job_log` VALUES (3249, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:49');
INSERT INTO `t_job_log` VALUES (3250, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:50');
INSERT INTO `t_job_log` VALUES (3251, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:51');
INSERT INTO `t_job_log` VALUES (3252, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:52');
INSERT INTO `t_job_log` VALUES (3253, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:47:53');
INSERT INTO `t_job_log` VALUES (3254, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:54');
INSERT INTO `t_job_log` VALUES (3255, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:47:55');
INSERT INTO `t_job_log` VALUES (3256, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:56');
INSERT INTO `t_job_log` VALUES (3257, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:57');
INSERT INTO `t_job_log` VALUES (3258, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:58');
INSERT INTO `t_job_log` VALUES (3259, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:47:59');
INSERT INTO `t_job_log` VALUES (3260, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:48:00');
INSERT INTO `t_job_log` VALUES (3261, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:48:01');
INSERT INTO `t_job_log` VALUES (3262, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:48:02');
INSERT INTO `t_job_log` VALUES (3263, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:48:03');
INSERT INTO `t_job_log` VALUES (3264, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:48:04');
INSERT INTO `t_job_log` VALUES (3265, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:48:05');
INSERT INTO `t_job_log` VALUES (3266, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:48:06');
INSERT INTO `t_job_log` VALUES (3267, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:48:07');
INSERT INTO `t_job_log` VALUES (3268, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:48:08');
INSERT INTO `t_job_log` VALUES (3269, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:48:09');
INSERT INTO `t_job_log` VALUES (3270, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:02');
INSERT INTO `t_job_log` VALUES (3271, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:02');
INSERT INTO `t_job_log` VALUES (3272, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:03');
INSERT INTO `t_job_log` VALUES (3273, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:04');
INSERT INTO `t_job_log` VALUES (3274, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:05');
INSERT INTO `t_job_log` VALUES (3275, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:06');
INSERT INTO `t_job_log` VALUES (3276, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:07');
INSERT INTO `t_job_log` VALUES (3277, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:08');
INSERT INTO `t_job_log` VALUES (3278, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:09');
INSERT INTO `t_job_log` VALUES (3279, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:10');
INSERT INTO `t_job_log` VALUES (3280, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:11');
INSERT INTO `t_job_log` VALUES (3281, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:12');
INSERT INTO `t_job_log` VALUES (3282, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:13');
INSERT INTO `t_job_log` VALUES (3283, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:14');
INSERT INTO `t_job_log` VALUES (3284, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:15');
INSERT INTO `t_job_log` VALUES (3285, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:16');
INSERT INTO `t_job_log` VALUES (3286, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:17');
INSERT INTO `t_job_log` VALUES (3287, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:18');
INSERT INTO `t_job_log` VALUES (3288, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:19');
INSERT INTO `t_job_log` VALUES (3289, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:20');
INSERT INTO `t_job_log` VALUES (3290, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:21');
INSERT INTO `t_job_log` VALUES (3291, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:22');
INSERT INTO `t_job_log` VALUES (3292, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:23');
INSERT INTO `t_job_log` VALUES (3293, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:24');
INSERT INTO `t_job_log` VALUES (3294, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:25');
INSERT INTO `t_job_log` VALUES (3295, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:26');
INSERT INTO `t_job_log` VALUES (3296, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:27');
INSERT INTO `t_job_log` VALUES (3297, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:28');
INSERT INTO `t_job_log` VALUES (3298, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:29');
INSERT INTO `t_job_log` VALUES (3299, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:30');
INSERT INTO `t_job_log` VALUES (3300, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:31');
INSERT INTO `t_job_log` VALUES (3301, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:32');
INSERT INTO `t_job_log` VALUES (3302, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:33');
INSERT INTO `t_job_log` VALUES (3303, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:34');
INSERT INTO `t_job_log` VALUES (3304, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:35');
INSERT INTO `t_job_log` VALUES (3305, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:36');
INSERT INTO `t_job_log` VALUES (3306, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:37');
INSERT INTO `t_job_log` VALUES (3307, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:38');
INSERT INTO `t_job_log` VALUES (3308, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:39');
INSERT INTO `t_job_log` VALUES (3309, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:40');
INSERT INTO `t_job_log` VALUES (3310, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:41');
INSERT INTO `t_job_log` VALUES (3311, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:42');
INSERT INTO `t_job_log` VALUES (3312, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:43');
INSERT INTO `t_job_log` VALUES (3313, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:44');
INSERT INTO `t_job_log` VALUES (3314, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:45');
INSERT INTO `t_job_log` VALUES (3315, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:46');
INSERT INTO `t_job_log` VALUES (3316, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:47');
INSERT INTO `t_job_log` VALUES (3317, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:48');
INSERT INTO `t_job_log` VALUES (3318, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:49');
INSERT INTO `t_job_log` VALUES (3319, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:50');
INSERT INTO `t_job_log` VALUES (3320, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:51');
INSERT INTO `t_job_log` VALUES (3321, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:52');
INSERT INTO `t_job_log` VALUES (3322, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:53');
INSERT INTO `t_job_log` VALUES (3323, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-13 08:50:55');
INSERT INTO `t_job_log` VALUES (3324, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:50:55');
INSERT INTO `t_job_log` VALUES (3325, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:56');
INSERT INTO `t_job_log` VALUES (3326, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:57');
INSERT INTO `t_job_log` VALUES (3327, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:50:58');
INSERT INTO `t_job_log` VALUES (3328, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:50:59');
INSERT INTO `t_job_log` VALUES (3329, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:00');
INSERT INTO `t_job_log` VALUES (3330, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:01');
INSERT INTO `t_job_log` VALUES (3331, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:02');
INSERT INTO `t_job_log` VALUES (3332, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:03');
INSERT INTO `t_job_log` VALUES (3333, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:04');
INSERT INTO `t_job_log` VALUES (3334, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:05');
INSERT INTO `t_job_log` VALUES (3335, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:06');
INSERT INTO `t_job_log` VALUES (3336, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:07');
INSERT INTO `t_job_log` VALUES (3337, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:08');
INSERT INTO `t_job_log` VALUES (3338, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:09');
INSERT INTO `t_job_log` VALUES (3339, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:10');
INSERT INTO `t_job_log` VALUES (3340, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:11');
INSERT INTO `t_job_log` VALUES (3341, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:12');
INSERT INTO `t_job_log` VALUES (3342, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:13');
INSERT INTO `t_job_log` VALUES (3343, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:14');
INSERT INTO `t_job_log` VALUES (3344, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:15');
INSERT INTO `t_job_log` VALUES (3345, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:16');
INSERT INTO `t_job_log` VALUES (3346, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:17');
INSERT INTO `t_job_log` VALUES (3347, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:18');
INSERT INTO `t_job_log` VALUES (3348, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:19');
INSERT INTO `t_job_log` VALUES (3349, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:20');
INSERT INTO `t_job_log` VALUES (3350, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:21');
INSERT INTO `t_job_log` VALUES (3351, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:22');
INSERT INTO `t_job_log` VALUES (3352, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:23');
INSERT INTO `t_job_log` VALUES (3353, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:24');
INSERT INTO `t_job_log` VALUES (3354, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:26');
INSERT INTO `t_job_log` VALUES (3355, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:26');
INSERT INTO `t_job_log` VALUES (3356, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:27');
INSERT INTO `t_job_log` VALUES (3357, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:28');
INSERT INTO `t_job_log` VALUES (3358, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:29');
INSERT INTO `t_job_log` VALUES (3359, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:30');
INSERT INTO `t_job_log` VALUES (3360, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:31');
INSERT INTO `t_job_log` VALUES (3361, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:32');
INSERT INTO `t_job_log` VALUES (3362, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:33');
INSERT INTO `t_job_log` VALUES (3363, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:34');
INSERT INTO `t_job_log` VALUES (3364, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:35');
INSERT INTO `t_job_log` VALUES (3365, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:36');
INSERT INTO `t_job_log` VALUES (3366, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:37');
INSERT INTO `t_job_log` VALUES (3367, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:38');
INSERT INTO `t_job_log` VALUES (3368, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:39');
INSERT INTO `t_job_log` VALUES (3369, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:40');
INSERT INTO `t_job_log` VALUES (3370, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:41');
INSERT INTO `t_job_log` VALUES (3371, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:42');
INSERT INTO `t_job_log` VALUES (3372, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:43');
INSERT INTO `t_job_log` VALUES (3373, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:44');
INSERT INTO `t_job_log` VALUES (3374, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:45');
INSERT INTO `t_job_log` VALUES (3375, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:46');
INSERT INTO `t_job_log` VALUES (3376, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:47');
INSERT INTO `t_job_log` VALUES (3377, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:48');
INSERT INTO `t_job_log` VALUES (3378, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:49');
INSERT INTO `t_job_log` VALUES (3379, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:50');
INSERT INTO `t_job_log` VALUES (3380, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:51');
INSERT INTO `t_job_log` VALUES (3381, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:52');
INSERT INTO `t_job_log` VALUES (3382, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:53');
INSERT INTO `t_job_log` VALUES (3383, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:55');
INSERT INTO `t_job_log` VALUES (3384, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:55');
INSERT INTO `t_job_log` VALUES (3385, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:51:56');
INSERT INTO `t_job_log` VALUES (3386, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:57');
INSERT INTO `t_job_log` VALUES (3387, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:51:58');
INSERT INTO `t_job_log` VALUES (3388, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:51:59');
INSERT INTO `t_job_log` VALUES (3389, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:00');
INSERT INTO `t_job_log` VALUES (3390, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:01');
INSERT INTO `t_job_log` VALUES (3391, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:02');
INSERT INTO `t_job_log` VALUES (3392, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:03');
INSERT INTO `t_job_log` VALUES (3393, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:04');
INSERT INTO `t_job_log` VALUES (3394, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:05');
INSERT INTO `t_job_log` VALUES (3395, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:06');
INSERT INTO `t_job_log` VALUES (3396, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:07');
INSERT INTO `t_job_log` VALUES (3397, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:08');
INSERT INTO `t_job_log` VALUES (3398, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:09');
INSERT INTO `t_job_log` VALUES (3399, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:10');
INSERT INTO `t_job_log` VALUES (3400, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:11');
INSERT INTO `t_job_log` VALUES (3401, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:12');
INSERT INTO `t_job_log` VALUES (3402, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:13');
INSERT INTO `t_job_log` VALUES (3403, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:14');
INSERT INTO `t_job_log` VALUES (3404, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:15');
INSERT INTO `t_job_log` VALUES (3405, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:16');
INSERT INTO `t_job_log` VALUES (3406, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:17');
INSERT INTO `t_job_log` VALUES (3407, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:18');
INSERT INTO `t_job_log` VALUES (3408, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:19');
INSERT INTO `t_job_log` VALUES (3409, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:20');
INSERT INTO `t_job_log` VALUES (3410, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:21');
INSERT INTO `t_job_log` VALUES (3411, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:22');
INSERT INTO `t_job_log` VALUES (3412, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:23');
INSERT INTO `t_job_log` VALUES (3413, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:24');
INSERT INTO `t_job_log` VALUES (3414, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:25');
INSERT INTO `t_job_log` VALUES (3415, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:26');
INSERT INTO `t_job_log` VALUES (3416, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:27');
INSERT INTO `t_job_log` VALUES (3417, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:28');
INSERT INTO `t_job_log` VALUES (3418, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:29');
INSERT INTO `t_job_log` VALUES (3419, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:30');
INSERT INTO `t_job_log` VALUES (3420, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:31');
INSERT INTO `t_job_log` VALUES (3421, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:32');
INSERT INTO `t_job_log` VALUES (3422, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:33');
INSERT INTO `t_job_log` VALUES (3423, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:34');
INSERT INTO `t_job_log` VALUES (3424, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:35');
INSERT INTO `t_job_log` VALUES (3425, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:36');
INSERT INTO `t_job_log` VALUES (3426, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:37');
INSERT INTO `t_job_log` VALUES (3427, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:52:38');
INSERT INTO `t_job_log` VALUES (3428, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:39');
INSERT INTO `t_job_log` VALUES (3429, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:40');
INSERT INTO `t_job_log` VALUES (3430, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:41');
INSERT INTO `t_job_log` VALUES (3431, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:42');
INSERT INTO `t_job_log` VALUES (3432, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:43');
INSERT INTO `t_job_log` VALUES (3433, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:44');
INSERT INTO `t_job_log` VALUES (3434, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:45');
INSERT INTO `t_job_log` VALUES (3435, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:46');
INSERT INTO `t_job_log` VALUES (3436, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:47');
INSERT INTO `t_job_log` VALUES (3437, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:48');
INSERT INTO `t_job_log` VALUES (3438, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:49');
INSERT INTO `t_job_log` VALUES (3439, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:50');
INSERT INTO `t_job_log` VALUES (3440, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:51');
INSERT INTO `t_job_log` VALUES (3441, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:52');
INSERT INTO `t_job_log` VALUES (3442, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:53');
INSERT INTO `t_job_log` VALUES (3443, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:54');
INSERT INTO `t_job_log` VALUES (3444, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:55');
INSERT INTO `t_job_log` VALUES (3445, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:56');
INSERT INTO `t_job_log` VALUES (3446, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:57');
INSERT INTO `t_job_log` VALUES (3447, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:52:58');
INSERT INTO `t_job_log` VALUES (3448, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:52:59');
INSERT INTO `t_job_log` VALUES (3449, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:00');
INSERT INTO `t_job_log` VALUES (3450, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:01');
INSERT INTO `t_job_log` VALUES (3451, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:02');
INSERT INTO `t_job_log` VALUES (3452, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:03');
INSERT INTO `t_job_log` VALUES (3453, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:04');
INSERT INTO `t_job_log` VALUES (3454, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:05');
INSERT INTO `t_job_log` VALUES (3455, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:06');
INSERT INTO `t_job_log` VALUES (3456, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:07');
INSERT INTO `t_job_log` VALUES (3457, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:08');
INSERT INTO `t_job_log` VALUES (3458, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:09');
INSERT INTO `t_job_log` VALUES (3459, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:10');
INSERT INTO `t_job_log` VALUES (3460, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:11');
INSERT INTO `t_job_log` VALUES (3461, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:12');
INSERT INTO `t_job_log` VALUES (3462, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:13');
INSERT INTO `t_job_log` VALUES (3463, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:14');
INSERT INTO `t_job_log` VALUES (3464, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:15');
INSERT INTO `t_job_log` VALUES (3465, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:16');
INSERT INTO `t_job_log` VALUES (3466, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:17');
INSERT INTO `t_job_log` VALUES (3467, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:18');
INSERT INTO `t_job_log` VALUES (3468, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:19');
INSERT INTO `t_job_log` VALUES (3469, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:20');
INSERT INTO `t_job_log` VALUES (3470, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:21');
INSERT INTO `t_job_log` VALUES (3471, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:22');
INSERT INTO `t_job_log` VALUES (3472, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:23');
INSERT INTO `t_job_log` VALUES (3473, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:24');
INSERT INTO `t_job_log` VALUES (3474, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:25');
INSERT INTO `t_job_log` VALUES (3475, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:53:26');
INSERT INTO `t_job_log` VALUES (3476, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:27');
INSERT INTO `t_job_log` VALUES (3477, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:28');
INSERT INTO `t_job_log` VALUES (3478, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:29');
INSERT INTO `t_job_log` VALUES (3479, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:30');
INSERT INTO `t_job_log` VALUES (3480, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:53:31');
INSERT INTO `t_job_log` VALUES (3481, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:32');
INSERT INTO `t_job_log` VALUES (3482, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:33');
INSERT INTO `t_job_log` VALUES (3483, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:34');
INSERT INTO `t_job_log` VALUES (3484, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:35');
INSERT INTO `t_job_log` VALUES (3485, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:36');
INSERT INTO `t_job_log` VALUES (3486, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:37');
INSERT INTO `t_job_log` VALUES (3487, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:38');
INSERT INTO `t_job_log` VALUES (3488, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:39');
INSERT INTO `t_job_log` VALUES (3489, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:40');
INSERT INTO `t_job_log` VALUES (3490, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:41');
INSERT INTO `t_job_log` VALUES (3491, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:42');
INSERT INTO `t_job_log` VALUES (3492, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:43');
INSERT INTO `t_job_log` VALUES (3493, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:44');
INSERT INTO `t_job_log` VALUES (3494, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:45');
INSERT INTO `t_job_log` VALUES (3495, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:46');
INSERT INTO `t_job_log` VALUES (3496, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:47');
INSERT INTO `t_job_log` VALUES (3497, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:53:48');
INSERT INTO `t_job_log` VALUES (3498, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:49');
INSERT INTO `t_job_log` VALUES (3499, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:50');
INSERT INTO `t_job_log` VALUES (3500, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:51');
INSERT INTO `t_job_log` VALUES (3501, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:52');
INSERT INTO `t_job_log` VALUES (3502, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:53:53');
INSERT INTO `t_job_log` VALUES (3503, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:54');
INSERT INTO `t_job_log` VALUES (3504, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:55');
INSERT INTO `t_job_log` VALUES (3505, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:56');
INSERT INTO `t_job_log` VALUES (3506, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:53:57');
INSERT INTO `t_job_log` VALUES (3507, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:58');
INSERT INTO `t_job_log` VALUES (3508, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:53:59');
INSERT INTO `t_job_log` VALUES (3509, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:00');
INSERT INTO `t_job_log` VALUES (3510, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:01');
INSERT INTO `t_job_log` VALUES (3511, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:02');
INSERT INTO `t_job_log` VALUES (3512, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:03');
INSERT INTO `t_job_log` VALUES (3513, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:04');
INSERT INTO `t_job_log` VALUES (3514, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:05');
INSERT INTO `t_job_log` VALUES (3515, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:06');
INSERT INTO `t_job_log` VALUES (3516, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:07');
INSERT INTO `t_job_log` VALUES (3517, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:08');
INSERT INTO `t_job_log` VALUES (3518, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:09');
INSERT INTO `t_job_log` VALUES (3519, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:10');
INSERT INTO `t_job_log` VALUES (3520, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:11');
INSERT INTO `t_job_log` VALUES (3521, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:12');
INSERT INTO `t_job_log` VALUES (3522, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:13');
INSERT INTO `t_job_log` VALUES (3523, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:14');
INSERT INTO `t_job_log` VALUES (3524, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:15');
INSERT INTO `t_job_log` VALUES (3525, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:16');
INSERT INTO `t_job_log` VALUES (3526, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:17');
INSERT INTO `t_job_log` VALUES (3527, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:18');
INSERT INTO `t_job_log` VALUES (3528, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:19');
INSERT INTO `t_job_log` VALUES (3529, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:20');
INSERT INTO `t_job_log` VALUES (3530, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:21');
INSERT INTO `t_job_log` VALUES (3531, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:22');
INSERT INTO `t_job_log` VALUES (3532, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:23');
INSERT INTO `t_job_log` VALUES (3533, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:24');
INSERT INTO `t_job_log` VALUES (3534, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:25');
INSERT INTO `t_job_log` VALUES (3535, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:26');
INSERT INTO `t_job_log` VALUES (3536, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:27');
INSERT INTO `t_job_log` VALUES (3537, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:28');
INSERT INTO `t_job_log` VALUES (3538, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:29');
INSERT INTO `t_job_log` VALUES (3539, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:30');
INSERT INTO `t_job_log` VALUES (3540, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:31');
INSERT INTO `t_job_log` VALUES (3541, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:32');
INSERT INTO `t_job_log` VALUES (3542, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:33');
INSERT INTO `t_job_log` VALUES (3543, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:34');
INSERT INTO `t_job_log` VALUES (3544, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:35');
INSERT INTO `t_job_log` VALUES (3545, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:36');
INSERT INTO `t_job_log` VALUES (3546, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:37');
INSERT INTO `t_job_log` VALUES (3547, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:38');
INSERT INTO `t_job_log` VALUES (3548, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:39');
INSERT INTO `t_job_log` VALUES (3549, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:41');
INSERT INTO `t_job_log` VALUES (3550, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:41');
INSERT INTO `t_job_log` VALUES (3551, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:42');
INSERT INTO `t_job_log` VALUES (3552, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:43');
INSERT INTO `t_job_log` VALUES (3553, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:44');
INSERT INTO `t_job_log` VALUES (3554, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:45');
INSERT INTO `t_job_log` VALUES (3555, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:46');
INSERT INTO `t_job_log` VALUES (3556, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:47');
INSERT INTO `t_job_log` VALUES (3557, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:48');
INSERT INTO `t_job_log` VALUES (3558, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:49');
INSERT INTO `t_job_log` VALUES (3559, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:50');
INSERT INTO `t_job_log` VALUES (3560, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:51');
INSERT INTO `t_job_log` VALUES (3561, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:52');
INSERT INTO `t_job_log` VALUES (3562, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:54:53');
INSERT INTO `t_job_log` VALUES (3563, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:54');
INSERT INTO `t_job_log` VALUES (3564, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:55');
INSERT INTO `t_job_log` VALUES (3565, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:56');
INSERT INTO `t_job_log` VALUES (3566, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:54:57');
INSERT INTO `t_job_log` VALUES (3567, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:58');
INSERT INTO `t_job_log` VALUES (3568, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:54:59');
INSERT INTO `t_job_log` VALUES (3569, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:55:00');
INSERT INTO `t_job_log` VALUES (3570, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:01');
INSERT INTO `t_job_log` VALUES (3571, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:02');
INSERT INTO `t_job_log` VALUES (3572, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:03');
INSERT INTO `t_job_log` VALUES (3573, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:04');
INSERT INTO `t_job_log` VALUES (3574, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:05');
INSERT INTO `t_job_log` VALUES (3575, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:06');
INSERT INTO `t_job_log` VALUES (3576, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:55:07');
INSERT INTO `t_job_log` VALUES (3577, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:55:08');
INSERT INTO `t_job_log` VALUES (3578, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:55:09');
INSERT INTO `t_job_log` VALUES (3579, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:10');
INSERT INTO `t_job_log` VALUES (3580, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:11');
INSERT INTO `t_job_log` VALUES (3581, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:12');
INSERT INTO `t_job_log` VALUES (3582, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:55:13');
INSERT INTO `t_job_log` VALUES (3583, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:55:14');
INSERT INTO `t_job_log` VALUES (3584, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:15');
INSERT INTO `t_job_log` VALUES (3585, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:55:16');
INSERT INTO `t_job_log` VALUES (3586, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:55:17');
INSERT INTO `t_job_log` VALUES (3587, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:55:18');
INSERT INTO `t_job_log` VALUES (3588, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:55:19');
INSERT INTO `t_job_log` VALUES (3589, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:20');
INSERT INTO `t_job_log` VALUES (3590, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:55:21');
INSERT INTO `t_job_log` VALUES (3591, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:22');
INSERT INTO `t_job_log` VALUES (3592, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:55:23');
INSERT INTO `t_job_log` VALUES (3593, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:24');
INSERT INTO `t_job_log` VALUES (3594, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:55:25');
INSERT INTO `t_job_log` VALUES (3595, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:26');
INSERT INTO `t_job_log` VALUES (3596, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:27');
INSERT INTO `t_job_log` VALUES (3597, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:28');
INSERT INTO `t_job_log` VALUES (3598, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:29');
INSERT INTO `t_job_log` VALUES (3599, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:55:30');
INSERT INTO `t_job_log` VALUES (3600, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:55:31');
INSERT INTO `t_job_log` VALUES (3601, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:56:27');
INSERT INTO `t_job_log` VALUES (3602, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:28');
INSERT INTO `t_job_log` VALUES (3603, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:56:29');
INSERT INTO `t_job_log` VALUES (3604, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:30');
INSERT INTO `t_job_log` VALUES (3605, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:56:31');
INSERT INTO `t_job_log` VALUES (3606, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:32');
INSERT INTO `t_job_log` VALUES (3607, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-13 08:56:33');
INSERT INTO `t_job_log` VALUES (3608, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:34');
INSERT INTO `t_job_log` VALUES (3609, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:35');
INSERT INTO `t_job_log` VALUES (3610, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:56:36');
INSERT INTO `t_job_log` VALUES (3611, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:37');
INSERT INTO `t_job_log` VALUES (3612, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:38');
INSERT INTO `t_job_log` VALUES (3613, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:56:39');
INSERT INTO `t_job_log` VALUES (3614, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:40');
INSERT INTO `t_job_log` VALUES (3615, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:41');
INSERT INTO `t_job_log` VALUES (3616, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:42');
INSERT INTO `t_job_log` VALUES (3617, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:43');
INSERT INTO `t_job_log` VALUES (3618, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:44');
INSERT INTO `t_job_log` VALUES (3619, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-13 08:56:45');
INSERT INTO `t_job_log` VALUES (3620, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:46');
INSERT INTO `t_job_log` VALUES (3621, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:47');
INSERT INTO `t_job_log` VALUES (3622, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:56:48');
INSERT INTO `t_job_log` VALUES (3623, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:49');
INSERT INTO `t_job_log` VALUES (3624, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:50');
INSERT INTO `t_job_log` VALUES (3625, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:51');
INSERT INTO `t_job_log` VALUES (3626, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-13 08:56:52');
INSERT INTO `t_job_log` VALUES (3627, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:53');
INSERT INTO `t_job_log` VALUES (3628, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-13 08:56:54');
INSERT INTO `t_job_log` VALUES (3629, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:55');
INSERT INTO `t_job_log` VALUES (3630, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:56:56');
INSERT INTO `t_job_log` VALUES (3631, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-13 08:56:57');
INSERT INTO `t_job_log` VALUES (3632, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:58');
INSERT INTO `t_job_log` VALUES (3633, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:56:59');
INSERT INTO `t_job_log` VALUES (3634, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:00');
INSERT INTO `t_job_log` VALUES (3635, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:01');
INSERT INTO `t_job_log` VALUES (3636, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:02');
INSERT INTO `t_job_log` VALUES (3637, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:03');
INSERT INTO `t_job_log` VALUES (3638, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:57:04');
INSERT INTO `t_job_log` VALUES (3639, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:05');
INSERT INTO `t_job_log` VALUES (3640, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:06');
INSERT INTO `t_job_log` VALUES (3641, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:07');
INSERT INTO `t_job_log` VALUES (3642, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:57:08');
INSERT INTO `t_job_log` VALUES (3643, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-13 08:57:09');
INSERT INTO `t_job_log` VALUES (3644, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-13 08:57:10');
INSERT INTO `t_job_log` VALUES (3645, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:11');
INSERT INTO `t_job_log` VALUES (3646, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:57:12');
INSERT INTO `t_job_log` VALUES (3647, 3, 'testTask', 'test', 'hello world', '0', NULL, 5, '2021-10-13 08:57:13');
INSERT INTO `t_job_log` VALUES (3648, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:14');
INSERT INTO `t_job_log` VALUES (3649, 3, 'testTask', 'test', 'hello world', '0', NULL, 5, '2021-10-13 08:57:15');
INSERT INTO `t_job_log` VALUES (3650, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:57:16');
INSERT INTO `t_job_log` VALUES (3651, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:17');
INSERT INTO `t_job_log` VALUES (3652, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:18');
INSERT INTO `t_job_log` VALUES (3653, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:19');
INSERT INTO `t_job_log` VALUES (3654, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:57:20');
INSERT INTO `t_job_log` VALUES (3655, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:21');
INSERT INTO `t_job_log` VALUES (3656, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:22');
INSERT INTO `t_job_log` VALUES (3657, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-13 08:57:23');
INSERT INTO `t_job_log` VALUES (3658, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:24');
INSERT INTO `t_job_log` VALUES (3659, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-13 08:57:25');
INSERT INTO `t_job_log` VALUES (3660, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-13 08:57:26');
INSERT INTO `t_job_log` VALUES (3661, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:27');
INSERT INTO `t_job_log` VALUES (3662, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:28');
INSERT INTO `t_job_log` VALUES (3663, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:29');
INSERT INTO `t_job_log` VALUES (3664, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-13 08:57:30');
INSERT INTO `t_job_log` VALUES (3665, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:31');
INSERT INTO `t_job_log` VALUES (3666, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:32');
INSERT INTO `t_job_log` VALUES (3667, 3, 'testTask', 'test', 'hello world', '0', NULL, 4, '2021-10-13 08:57:33');
INSERT INTO `t_job_log` VALUES (3668, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:34');
INSERT INTO `t_job_log` VALUES (3669, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 08:57:35');
INSERT INTO `t_job_log` VALUES (3670, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:57:36');
INSERT INTO `t_job_log` VALUES (3671, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:57:37');
INSERT INTO `t_job_log` VALUES (3672, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:57:38');
INSERT INTO `t_job_log` VALUES (3673, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:57:39');
INSERT INTO `t_job_log` VALUES (3674, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:57:40');
INSERT INTO `t_job_log` VALUES (3675, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:57:41');
INSERT INTO `t_job_log` VALUES (3676, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:57:42');
INSERT INTO `t_job_log` VALUES (3677, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:57:43');
INSERT INTO `t_job_log` VALUES (3678, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:57:44');
INSERT INTO `t_job_log` VALUES (3679, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:57:45');
INSERT INTO `t_job_log` VALUES (3680, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:57:46');
INSERT INTO `t_job_log` VALUES (3681, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:57:47');
INSERT INTO `t_job_log` VALUES (3682, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:57:48');
INSERT INTO `t_job_log` VALUES (3683, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:57:49');
INSERT INTO `t_job_log` VALUES (3684, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:57:50');
INSERT INTO `t_job_log` VALUES (3685, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:57:51');
INSERT INTO `t_job_log` VALUES (3686, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:57:52');
INSERT INTO `t_job_log` VALUES (3687, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:57:53');
INSERT INTO `t_job_log` VALUES (3688, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:57:54');
INSERT INTO `t_job_log` VALUES (3689, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:57:55');
INSERT INTO `t_job_log` VALUES (3690, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:57:56');
INSERT INTO `t_job_log` VALUES (3691, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:57:57');
INSERT INTO `t_job_log` VALUES (3692, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:57:58');
INSERT INTO `t_job_log` VALUES (3693, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:57:59');
INSERT INTO `t_job_log` VALUES (3694, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:00');
INSERT INTO `t_job_log` VALUES (3695, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:01');
INSERT INTO `t_job_log` VALUES (3696, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:58:02');
INSERT INTO `t_job_log` VALUES (3697, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:03');
INSERT INTO `t_job_log` VALUES (3698, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:04');
INSERT INTO `t_job_log` VALUES (3699, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:05');
INSERT INTO `t_job_log` VALUES (3700, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:06');
INSERT INTO `t_job_log` VALUES (3701, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:07');
INSERT INTO `t_job_log` VALUES (3702, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:08');
INSERT INTO `t_job_log` VALUES (3703, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:09');
INSERT INTO `t_job_log` VALUES (3704, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:10');
INSERT INTO `t_job_log` VALUES (3705, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:11');
INSERT INTO `t_job_log` VALUES (3706, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:12');
INSERT INTO `t_job_log` VALUES (3707, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:13');
INSERT INTO `t_job_log` VALUES (3708, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:14');
INSERT INTO `t_job_log` VALUES (3709, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:15');
INSERT INTO `t_job_log` VALUES (3710, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:16');
INSERT INTO `t_job_log` VALUES (3711, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:17');
INSERT INTO `t_job_log` VALUES (3712, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:18');
INSERT INTO `t_job_log` VALUES (3713, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:19');
INSERT INTO `t_job_log` VALUES (3714, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:20');
INSERT INTO `t_job_log` VALUES (3715, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:21');
INSERT INTO `t_job_log` VALUES (3716, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:22');
INSERT INTO `t_job_log` VALUES (3717, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:23');
INSERT INTO `t_job_log` VALUES (3718, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:24');
INSERT INTO `t_job_log` VALUES (3719, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:25');
INSERT INTO `t_job_log` VALUES (3720, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:26');
INSERT INTO `t_job_log` VALUES (3721, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:27');
INSERT INTO `t_job_log` VALUES (3722, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:28');
INSERT INTO `t_job_log` VALUES (3723, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:58:29');
INSERT INTO `t_job_log` VALUES (3724, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:30');
INSERT INTO `t_job_log` VALUES (3725, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:31');
INSERT INTO `t_job_log` VALUES (3726, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:32');
INSERT INTO `t_job_log` VALUES (3727, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:33');
INSERT INTO `t_job_log` VALUES (3728, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:34');
INSERT INTO `t_job_log` VALUES (3729, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:35');
INSERT INTO `t_job_log` VALUES (3730, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:36');
INSERT INTO `t_job_log` VALUES (3731, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:37');
INSERT INTO `t_job_log` VALUES (3732, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:38');
INSERT INTO `t_job_log` VALUES (3733, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:39');
INSERT INTO `t_job_log` VALUES (3734, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:40');
INSERT INTO `t_job_log` VALUES (3735, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 08:58:41');
INSERT INTO `t_job_log` VALUES (3736, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:42');
INSERT INTO `t_job_log` VALUES (3737, 3, 'testTask', 'test', 'hello world', '0', NULL, 16, '2021-10-13 08:58:43');
INSERT INTO `t_job_log` VALUES (3738, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:44');
INSERT INTO `t_job_log` VALUES (3739, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:45');
INSERT INTO `t_job_log` VALUES (3740, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:46');
INSERT INTO `t_job_log` VALUES (3741, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:47');
INSERT INTO `t_job_log` VALUES (3742, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:48');
INSERT INTO `t_job_log` VALUES (3743, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:49');
INSERT INTO `t_job_log` VALUES (3744, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:50');
INSERT INTO `t_job_log` VALUES (3745, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:51');
INSERT INTO `t_job_log` VALUES (3746, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:52');
INSERT INTO `t_job_log` VALUES (3747, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:53');
INSERT INTO `t_job_log` VALUES (3748, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:54');
INSERT INTO `t_job_log` VALUES (3749, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:55');
INSERT INTO `t_job_log` VALUES (3750, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:56');
INSERT INTO `t_job_log` VALUES (3751, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:57');
INSERT INTO `t_job_log` VALUES (3752, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 08:58:58');
INSERT INTO `t_job_log` VALUES (3753, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:58:59');
INSERT INTO `t_job_log` VALUES (3754, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:59:00');
INSERT INTO `t_job_log` VALUES (3755, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:59:01');
INSERT INTO `t_job_log` VALUES (3756, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 08:59:02');
INSERT INTO `t_job_log` VALUES (3757, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:03:42');
INSERT INTO `t_job_log` VALUES (3758, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:03:43');
INSERT INTO `t_job_log` VALUES (3759, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:03:43');
INSERT INTO `t_job_log` VALUES (3760, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:03:44');
INSERT INTO `t_job_log` VALUES (3761, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:03:45');
INSERT INTO `t_job_log` VALUES (3762, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:03:46');
INSERT INTO `t_job_log` VALUES (3763, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:03:47');
INSERT INTO `t_job_log` VALUES (3764, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:03:48');
INSERT INTO `t_job_log` VALUES (3765, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:03:49');
INSERT INTO `t_job_log` VALUES (3766, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:03:50');
INSERT INTO `t_job_log` VALUES (3767, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:03:51');
INSERT INTO `t_job_log` VALUES (3768, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:03:52');
INSERT INTO `t_job_log` VALUES (3769, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:03:53');
INSERT INTO `t_job_log` VALUES (3770, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:03:54');
INSERT INTO `t_job_log` VALUES (3771, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:03:55');
INSERT INTO `t_job_log` VALUES (3772, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:03:56');
INSERT INTO `t_job_log` VALUES (3773, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:03:57');
INSERT INTO `t_job_log` VALUES (3774, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:03:58');
INSERT INTO `t_job_log` VALUES (3775, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:03:59');
INSERT INTO `t_job_log` VALUES (3776, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:00');
INSERT INTO `t_job_log` VALUES (3777, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:01');
INSERT INTO `t_job_log` VALUES (3778, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:02');
INSERT INTO `t_job_log` VALUES (3779, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:03');
INSERT INTO `t_job_log` VALUES (3780, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:04');
INSERT INTO `t_job_log` VALUES (3781, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:05');
INSERT INTO `t_job_log` VALUES (3782, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:06');
INSERT INTO `t_job_log` VALUES (3783, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:07');
INSERT INTO `t_job_log` VALUES (3784, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:08');
INSERT INTO `t_job_log` VALUES (3785, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:09');
INSERT INTO `t_job_log` VALUES (3786, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:10');
INSERT INTO `t_job_log` VALUES (3787, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:11');
INSERT INTO `t_job_log` VALUES (3788, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:12');
INSERT INTO `t_job_log` VALUES (3789, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:13');
INSERT INTO `t_job_log` VALUES (3790, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:14');
INSERT INTO `t_job_log` VALUES (3791, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:15');
INSERT INTO `t_job_log` VALUES (3792, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:16');
INSERT INTO `t_job_log` VALUES (3793, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:17');
INSERT INTO `t_job_log` VALUES (3794, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:18');
INSERT INTO `t_job_log` VALUES (3795, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:19');
INSERT INTO `t_job_log` VALUES (3796, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:20');
INSERT INTO `t_job_log` VALUES (3797, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:21');
INSERT INTO `t_job_log` VALUES (3798, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:22');
INSERT INTO `t_job_log` VALUES (3799, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:23');
INSERT INTO `t_job_log` VALUES (3800, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:24');
INSERT INTO `t_job_log` VALUES (3801, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:25');
INSERT INTO `t_job_log` VALUES (3802, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:26');
INSERT INTO `t_job_log` VALUES (3803, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:27');
INSERT INTO `t_job_log` VALUES (3804, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:28');
INSERT INTO `t_job_log` VALUES (3805, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:29');
INSERT INTO `t_job_log` VALUES (3806, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:30');
INSERT INTO `t_job_log` VALUES (3807, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:31');
INSERT INTO `t_job_log` VALUES (3808, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:32');
INSERT INTO `t_job_log` VALUES (3809, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:33');
INSERT INTO `t_job_log` VALUES (3810, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:34');
INSERT INTO `t_job_log` VALUES (3811, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:35');
INSERT INTO `t_job_log` VALUES (3812, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:36');
INSERT INTO `t_job_log` VALUES (3813, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:37');
INSERT INTO `t_job_log` VALUES (3814, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:38');
INSERT INTO `t_job_log` VALUES (3815, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:39');
INSERT INTO `t_job_log` VALUES (3816, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:40');
INSERT INTO `t_job_log` VALUES (3817, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:41');
INSERT INTO `t_job_log` VALUES (3818, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:42');
INSERT INTO `t_job_log` VALUES (3819, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:43');
INSERT INTO `t_job_log` VALUES (3820, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:44');
INSERT INTO `t_job_log` VALUES (3821, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:45');
INSERT INTO `t_job_log` VALUES (3822, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:46');
INSERT INTO `t_job_log` VALUES (3823, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:47');
INSERT INTO `t_job_log` VALUES (3824, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:48');
INSERT INTO `t_job_log` VALUES (3825, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:49');
INSERT INTO `t_job_log` VALUES (3826, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:50');
INSERT INTO `t_job_log` VALUES (3827, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:51');
INSERT INTO `t_job_log` VALUES (3828, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:52');
INSERT INTO `t_job_log` VALUES (3829, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:53');
INSERT INTO `t_job_log` VALUES (3830, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:54');
INSERT INTO `t_job_log` VALUES (3831, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:55');
INSERT INTO `t_job_log` VALUES (3832, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:56');
INSERT INTO `t_job_log` VALUES (3833, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:57');
INSERT INTO `t_job_log` VALUES (3834, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:04:58');
INSERT INTO `t_job_log` VALUES (3835, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:04:59');
INSERT INTO `t_job_log` VALUES (3836, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:00');
INSERT INTO `t_job_log` VALUES (3837, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:01');
INSERT INTO `t_job_log` VALUES (3838, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:02');
INSERT INTO `t_job_log` VALUES (3839, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:03');
INSERT INTO `t_job_log` VALUES (3840, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:04');
INSERT INTO `t_job_log` VALUES (3841, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:05');
INSERT INTO `t_job_log` VALUES (3842, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:06');
INSERT INTO `t_job_log` VALUES (3843, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:07');
INSERT INTO `t_job_log` VALUES (3844, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:08');
INSERT INTO `t_job_log` VALUES (3845, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:09');
INSERT INTO `t_job_log` VALUES (3846, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:10');
INSERT INTO `t_job_log` VALUES (3847, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:11');
INSERT INTO `t_job_log` VALUES (3848, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:12');
INSERT INTO `t_job_log` VALUES (3849, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:13');
INSERT INTO `t_job_log` VALUES (3850, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:14');
INSERT INTO `t_job_log` VALUES (3851, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:15');
INSERT INTO `t_job_log` VALUES (3852, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:16');
INSERT INTO `t_job_log` VALUES (3853, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:17');
INSERT INTO `t_job_log` VALUES (3854, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:18');
INSERT INTO `t_job_log` VALUES (3855, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:19');
INSERT INTO `t_job_log` VALUES (3856, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:20');
INSERT INTO `t_job_log` VALUES (3857, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:21');
INSERT INTO `t_job_log` VALUES (3858, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:22');
INSERT INTO `t_job_log` VALUES (3859, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:23');
INSERT INTO `t_job_log` VALUES (3860, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:24');
INSERT INTO `t_job_log` VALUES (3861, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:25');
INSERT INTO `t_job_log` VALUES (3862, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:26');
INSERT INTO `t_job_log` VALUES (3863, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:27');
INSERT INTO `t_job_log` VALUES (3864, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:28');
INSERT INTO `t_job_log` VALUES (3865, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:29');
INSERT INTO `t_job_log` VALUES (3866, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:30');
INSERT INTO `t_job_log` VALUES (3867, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:31');
INSERT INTO `t_job_log` VALUES (3868, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:32');
INSERT INTO `t_job_log` VALUES (3869, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:33');
INSERT INTO `t_job_log` VALUES (3870, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:34');
INSERT INTO `t_job_log` VALUES (3871, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:35');
INSERT INTO `t_job_log` VALUES (3872, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:36');
INSERT INTO `t_job_log` VALUES (3873, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:37');
INSERT INTO `t_job_log` VALUES (3874, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:38');
INSERT INTO `t_job_log` VALUES (3875, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:39');
INSERT INTO `t_job_log` VALUES (3876, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:40');
INSERT INTO `t_job_log` VALUES (3877, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:41');
INSERT INTO `t_job_log` VALUES (3878, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:42');
INSERT INTO `t_job_log` VALUES (3879, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:43');
INSERT INTO `t_job_log` VALUES (3880, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:44');
INSERT INTO `t_job_log` VALUES (3881, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:45');
INSERT INTO `t_job_log` VALUES (3882, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:46');
INSERT INTO `t_job_log` VALUES (3883, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:47');
INSERT INTO `t_job_log` VALUES (3884, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:48');
INSERT INTO `t_job_log` VALUES (3885, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:49');
INSERT INTO `t_job_log` VALUES (3886, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:50');
INSERT INTO `t_job_log` VALUES (3887, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:51');
INSERT INTO `t_job_log` VALUES (3888, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:52');
INSERT INTO `t_job_log` VALUES (3889, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:05:53');
INSERT INTO `t_job_log` VALUES (3890, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:54');
INSERT INTO `t_job_log` VALUES (3891, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:55');
INSERT INTO `t_job_log` VALUES (3892, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:56');
INSERT INTO `t_job_log` VALUES (3893, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:57');
INSERT INTO `t_job_log` VALUES (3894, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:58');
INSERT INTO `t_job_log` VALUES (3895, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:05:59');
INSERT INTO `t_job_log` VALUES (3896, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:00');
INSERT INTO `t_job_log` VALUES (3897, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:01');
INSERT INTO `t_job_log` VALUES (3898, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:02');
INSERT INTO `t_job_log` VALUES (3899, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:03');
INSERT INTO `t_job_log` VALUES (3900, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:04');
INSERT INTO `t_job_log` VALUES (3901, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:05');
INSERT INTO `t_job_log` VALUES (3902, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:06');
INSERT INTO `t_job_log` VALUES (3903, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:07');
INSERT INTO `t_job_log` VALUES (3904, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:08');
INSERT INTO `t_job_log` VALUES (3905, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:09');
INSERT INTO `t_job_log` VALUES (3906, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:10');
INSERT INTO `t_job_log` VALUES (3907, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:11');
INSERT INTO `t_job_log` VALUES (3908, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:12');
INSERT INTO `t_job_log` VALUES (3909, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:13');
INSERT INTO `t_job_log` VALUES (3910, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:14');
INSERT INTO `t_job_log` VALUES (3911, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:15');
INSERT INTO `t_job_log` VALUES (3912, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:16');
INSERT INTO `t_job_log` VALUES (3913, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:17');
INSERT INTO `t_job_log` VALUES (3914, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:18');
INSERT INTO `t_job_log` VALUES (3915, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:19');
INSERT INTO `t_job_log` VALUES (3916, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:20');
INSERT INTO `t_job_log` VALUES (3917, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:21');
INSERT INTO `t_job_log` VALUES (3918, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:22');
INSERT INTO `t_job_log` VALUES (3919, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:23');
INSERT INTO `t_job_log` VALUES (3920, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:24');
INSERT INTO `t_job_log` VALUES (3921, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:25');
INSERT INTO `t_job_log` VALUES (3922, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:26');
INSERT INTO `t_job_log` VALUES (3923, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:27');
INSERT INTO `t_job_log` VALUES (3924, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:28');
INSERT INTO `t_job_log` VALUES (3925, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:29');
INSERT INTO `t_job_log` VALUES (3926, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:30');
INSERT INTO `t_job_log` VALUES (3927, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:31');
INSERT INTO `t_job_log` VALUES (3928, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:32');
INSERT INTO `t_job_log` VALUES (3929, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:33');
INSERT INTO `t_job_log` VALUES (3930, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:34');
INSERT INTO `t_job_log` VALUES (3931, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:35');
INSERT INTO `t_job_log` VALUES (3932, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:36');
INSERT INTO `t_job_log` VALUES (3933, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:37');
INSERT INTO `t_job_log` VALUES (3934, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:38');
INSERT INTO `t_job_log` VALUES (3935, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:39');
INSERT INTO `t_job_log` VALUES (3936, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:40');
INSERT INTO `t_job_log` VALUES (3937, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:41');
INSERT INTO `t_job_log` VALUES (3938, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:42');
INSERT INTO `t_job_log` VALUES (3939, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:43');
INSERT INTO `t_job_log` VALUES (3940, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:44');
INSERT INTO `t_job_log` VALUES (3941, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:45');
INSERT INTO `t_job_log` VALUES (3942, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:46');
INSERT INTO `t_job_log` VALUES (3943, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:06:47');
INSERT INTO `t_job_log` VALUES (3944, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:48');
INSERT INTO `t_job_log` VALUES (3945, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:49');
INSERT INTO `t_job_log` VALUES (3946, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:50');
INSERT INTO `t_job_log` VALUES (3947, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:51');
INSERT INTO `t_job_log` VALUES (3948, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:52');
INSERT INTO `t_job_log` VALUES (3949, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:53');
INSERT INTO `t_job_log` VALUES (3950, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:54');
INSERT INTO `t_job_log` VALUES (3951, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:55');
INSERT INTO `t_job_log` VALUES (3952, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:56');
INSERT INTO `t_job_log` VALUES (3953, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:57');
INSERT INTO `t_job_log` VALUES (3954, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:58');
INSERT INTO `t_job_log` VALUES (3955, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:06:59');
INSERT INTO `t_job_log` VALUES (3956, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:00');
INSERT INTO `t_job_log` VALUES (3957, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:01');
INSERT INTO `t_job_log` VALUES (3958, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:02');
INSERT INTO `t_job_log` VALUES (3959, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:03');
INSERT INTO `t_job_log` VALUES (3960, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:04');
INSERT INTO `t_job_log` VALUES (3961, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:05');
INSERT INTO `t_job_log` VALUES (3962, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:06');
INSERT INTO `t_job_log` VALUES (3963, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:07');
INSERT INTO `t_job_log` VALUES (3964, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:08');
INSERT INTO `t_job_log` VALUES (3965, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:09');
INSERT INTO `t_job_log` VALUES (3966, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:10');
INSERT INTO `t_job_log` VALUES (3967, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:11');
INSERT INTO `t_job_log` VALUES (3968, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:12');
INSERT INTO `t_job_log` VALUES (3969, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:13');
INSERT INTO `t_job_log` VALUES (3970, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:14');
INSERT INTO `t_job_log` VALUES (3971, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:15');
INSERT INTO `t_job_log` VALUES (3972, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:16');
INSERT INTO `t_job_log` VALUES (3973, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:17');
INSERT INTO `t_job_log` VALUES (3974, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:18');
INSERT INTO `t_job_log` VALUES (3975, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:19');
INSERT INTO `t_job_log` VALUES (3976, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:20');
INSERT INTO `t_job_log` VALUES (3977, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:21');
INSERT INTO `t_job_log` VALUES (3978, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:22');
INSERT INTO `t_job_log` VALUES (3979, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:23');
INSERT INTO `t_job_log` VALUES (3980, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:24');
INSERT INTO `t_job_log` VALUES (3981, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:25');
INSERT INTO `t_job_log` VALUES (3982, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:26');
INSERT INTO `t_job_log` VALUES (3983, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:27');
INSERT INTO `t_job_log` VALUES (3984, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:28');
INSERT INTO `t_job_log` VALUES (3985, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:29');
INSERT INTO `t_job_log` VALUES (3986, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:30');
INSERT INTO `t_job_log` VALUES (3987, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:31');
INSERT INTO `t_job_log` VALUES (3988, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:32');
INSERT INTO `t_job_log` VALUES (3989, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:33');
INSERT INTO `t_job_log` VALUES (3990, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:34');
INSERT INTO `t_job_log` VALUES (3991, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:35');
INSERT INTO `t_job_log` VALUES (3992, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:36');
INSERT INTO `t_job_log` VALUES (3993, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:37');
INSERT INTO `t_job_log` VALUES (3994, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:38');
INSERT INTO `t_job_log` VALUES (3995, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:39');
INSERT INTO `t_job_log` VALUES (3996, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:40');
INSERT INTO `t_job_log` VALUES (3997, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:41');
INSERT INTO `t_job_log` VALUES (3998, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:42');
INSERT INTO `t_job_log` VALUES (3999, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 09:07:43');
INSERT INTO `t_job_log` VALUES (4000, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 09:07:44');
INSERT INTO `t_job_log` VALUES (4001, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 09:07:45');
INSERT INTO `t_job_log` VALUES (4002, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 09:07:46');
INSERT INTO `t_job_log` VALUES (4003, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 09:07:47');
INSERT INTO `t_job_log` VALUES (4004, 2, 'testTask', 'test1', NULL, '0', NULL, 5, '2021-10-13 10:24:40');
INSERT INTO `t_job_log` VALUES (4005, 2, 'testTask', 'test1', NULL, '0', NULL, 2, '2021-10-13 10:24:51');
INSERT INTO `t_job_log` VALUES (4006, 2, 'testTask', 'test1', NULL, '0', NULL, 3, '2021-10-13 10:25:00');
INSERT INTO `t_job_log` VALUES (4007, 2, 'testTask', 'test1', NULL, '0', NULL, 2, '2021-10-13 10:25:10');
INSERT INTO `t_job_log` VALUES (4008, 2, 'testTask', 'test1', NULL, '0', NULL, 2, '2021-10-13 10:25:20');
INSERT INTO `t_job_log` VALUES (4009, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-10-13 10:25:30');
INSERT INTO `t_job_log` VALUES (4010, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:10');
INSERT INTO `t_job_log` VALUES (4011, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:10');
INSERT INTO `t_job_log` VALUES (4012, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:18:11');
INSERT INTO `t_job_log` VALUES (4013, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:12');
INSERT INTO `t_job_log` VALUES (4014, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:18:13');
INSERT INTO `t_job_log` VALUES (4015, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:18:14');
INSERT INTO `t_job_log` VALUES (4016, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:18:16');
INSERT INTO `t_job_log` VALUES (4017, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:16');
INSERT INTO `t_job_log` VALUES (4018, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:17');
INSERT INTO `t_job_log` VALUES (4019, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:18');
INSERT INTO `t_job_log` VALUES (4020, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:19');
INSERT INTO `t_job_log` VALUES (4021, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:20');
INSERT INTO `t_job_log` VALUES (4022, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:18:21');
INSERT INTO `t_job_log` VALUES (4023, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:18:22');
INSERT INTO `t_job_log` VALUES (4024, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:23');
INSERT INTO `t_job_log` VALUES (4025, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:24');
INSERT INTO `t_job_log` VALUES (4026, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:25');
INSERT INTO `t_job_log` VALUES (4027, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:18:26');
INSERT INTO `t_job_log` VALUES (4028, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:27');
INSERT INTO `t_job_log` VALUES (4029, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:28');
INSERT INTO `t_job_log` VALUES (4030, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:29');
INSERT INTO `t_job_log` VALUES (4031, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:30');
INSERT INTO `t_job_log` VALUES (4032, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:18:31');
INSERT INTO `t_job_log` VALUES (4033, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:18:32');
INSERT INTO `t_job_log` VALUES (4034, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:33');
INSERT INTO `t_job_log` VALUES (4035, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:34');
INSERT INTO `t_job_log` VALUES (4036, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:35');
INSERT INTO `t_job_log` VALUES (4037, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:36');
INSERT INTO `t_job_log` VALUES (4038, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:18:37');
INSERT INTO `t_job_log` VALUES (4039, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:38');
INSERT INTO `t_job_log` VALUES (4040, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:39');
INSERT INTO `t_job_log` VALUES (4041, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:18:40');
INSERT INTO `t_job_log` VALUES (4042, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:18:41');
INSERT INTO `t_job_log` VALUES (4043, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:42');
INSERT INTO `t_job_log` VALUES (4044, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:43');
INSERT INTO `t_job_log` VALUES (4045, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:44');
INSERT INTO `t_job_log` VALUES (4046, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:45');
INSERT INTO `t_job_log` VALUES (4047, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:46');
INSERT INTO `t_job_log` VALUES (4048, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:47');
INSERT INTO `t_job_log` VALUES (4049, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:48');
INSERT INTO `t_job_log` VALUES (4050, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:49');
INSERT INTO `t_job_log` VALUES (4051, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:50');
INSERT INTO `t_job_log` VALUES (4052, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:18:51');
INSERT INTO `t_job_log` VALUES (4053, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:52');
INSERT INTO `t_job_log` VALUES (4054, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:53');
INSERT INTO `t_job_log` VALUES (4055, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:54');
INSERT INTO `t_job_log` VALUES (4056, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:55');
INSERT INTO `t_job_log` VALUES (4057, 3, 'testTask', 'test', 'hello world', '0', NULL, 2, '2021-10-13 13:18:56');
INSERT INTO `t_job_log` VALUES (4058, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:57');
INSERT INTO `t_job_log` VALUES (4059, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:58');
INSERT INTO `t_job_log` VALUES (4060, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:18:59');
INSERT INTO `t_job_log` VALUES (4061, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:00');
INSERT INTO `t_job_log` VALUES (4062, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:01');
INSERT INTO `t_job_log` VALUES (4063, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:02');
INSERT INTO `t_job_log` VALUES (4064, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:03');
INSERT INTO `t_job_log` VALUES (4065, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:04');
INSERT INTO `t_job_log` VALUES (4066, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:05');
INSERT INTO `t_job_log` VALUES (4067, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:06');
INSERT INTO `t_job_log` VALUES (4068, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:07');
INSERT INTO `t_job_log` VALUES (4069, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:08');
INSERT INTO `t_job_log` VALUES (4070, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:09');
INSERT INTO `t_job_log` VALUES (4071, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:10');
INSERT INTO `t_job_log` VALUES (4072, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:11');
INSERT INTO `t_job_log` VALUES (4073, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:12');
INSERT INTO `t_job_log` VALUES (4074, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:13');
INSERT INTO `t_job_log` VALUES (4075, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:14');
INSERT INTO `t_job_log` VALUES (4076, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:15');
INSERT INTO `t_job_log` VALUES (4077, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:16');
INSERT INTO `t_job_log` VALUES (4078, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:17');
INSERT INTO `t_job_log` VALUES (4079, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:18');
INSERT INTO `t_job_log` VALUES (4080, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:19');
INSERT INTO `t_job_log` VALUES (4081, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:20');
INSERT INTO `t_job_log` VALUES (4082, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:21');
INSERT INTO `t_job_log` VALUES (4083, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:22');
INSERT INTO `t_job_log` VALUES (4084, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:23');
INSERT INTO `t_job_log` VALUES (4085, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:24');
INSERT INTO `t_job_log` VALUES (4086, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:25');
INSERT INTO `t_job_log` VALUES (4087, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:26');
INSERT INTO `t_job_log` VALUES (4088, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:27');
INSERT INTO `t_job_log` VALUES (4089, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:28');
INSERT INTO `t_job_log` VALUES (4090, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:29');
INSERT INTO `t_job_log` VALUES (4091, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:30');
INSERT INTO `t_job_log` VALUES (4092, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:31');
INSERT INTO `t_job_log` VALUES (4093, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:32');
INSERT INTO `t_job_log` VALUES (4094, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:33');
INSERT INTO `t_job_log` VALUES (4095, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:34');
INSERT INTO `t_job_log` VALUES (4096, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:35');
INSERT INTO `t_job_log` VALUES (4097, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:36');
INSERT INTO `t_job_log` VALUES (4098, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:37');
INSERT INTO `t_job_log` VALUES (4099, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:38');
INSERT INTO `t_job_log` VALUES (4100, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:39');
INSERT INTO `t_job_log` VALUES (4101, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:40');
INSERT INTO `t_job_log` VALUES (4102, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:41');
INSERT INTO `t_job_log` VALUES (4103, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:42');
INSERT INTO `t_job_log` VALUES (4104, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:43');
INSERT INTO `t_job_log` VALUES (4105, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:44');
INSERT INTO `t_job_log` VALUES (4106, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:45');
INSERT INTO `t_job_log` VALUES (4107, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:46');
INSERT INTO `t_job_log` VALUES (4108, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:47');
INSERT INTO `t_job_log` VALUES (4109, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:48');
INSERT INTO `t_job_log` VALUES (4110, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:49');
INSERT INTO `t_job_log` VALUES (4111, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:50');
INSERT INTO `t_job_log` VALUES (4112, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:51');
INSERT INTO `t_job_log` VALUES (4113, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:52');
INSERT INTO `t_job_log` VALUES (4114, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:53');
INSERT INTO `t_job_log` VALUES (4115, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:54');
INSERT INTO `t_job_log` VALUES (4116, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:19:55');
INSERT INTO `t_job_log` VALUES (4117, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:56');
INSERT INTO `t_job_log` VALUES (4118, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:57');
INSERT INTO `t_job_log` VALUES (4119, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:58');
INSERT INTO `t_job_log` VALUES (4120, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:19:59');
INSERT INTO `t_job_log` VALUES (4121, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:00');
INSERT INTO `t_job_log` VALUES (4122, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:01');
INSERT INTO `t_job_log` VALUES (4123, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:02');
INSERT INTO `t_job_log` VALUES (4124, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:03');
INSERT INTO `t_job_log` VALUES (4125, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:04');
INSERT INTO `t_job_log` VALUES (4126, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:05');
INSERT INTO `t_job_log` VALUES (4127, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:06');
INSERT INTO `t_job_log` VALUES (4128, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:07');
INSERT INTO `t_job_log` VALUES (4129, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:08');
INSERT INTO `t_job_log` VALUES (4130, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:09');
INSERT INTO `t_job_log` VALUES (4131, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:10');
INSERT INTO `t_job_log` VALUES (4132, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:11');
INSERT INTO `t_job_log` VALUES (4133, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:12');
INSERT INTO `t_job_log` VALUES (4134, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:13');
INSERT INTO `t_job_log` VALUES (4135, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:14');
INSERT INTO `t_job_log` VALUES (4136, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:15');
INSERT INTO `t_job_log` VALUES (4137, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:16');
INSERT INTO `t_job_log` VALUES (4138, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:17');
INSERT INTO `t_job_log` VALUES (4139, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:18');
INSERT INTO `t_job_log` VALUES (4140, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:19');
INSERT INTO `t_job_log` VALUES (4141, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:20');
INSERT INTO `t_job_log` VALUES (4142, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:21');
INSERT INTO `t_job_log` VALUES (4143, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:22');
INSERT INTO `t_job_log` VALUES (4144, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:23');
INSERT INTO `t_job_log` VALUES (4145, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:24');
INSERT INTO `t_job_log` VALUES (4146, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:25');
INSERT INTO `t_job_log` VALUES (4147, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:26');
INSERT INTO `t_job_log` VALUES (4148, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:27');
INSERT INTO `t_job_log` VALUES (4149, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:28');
INSERT INTO `t_job_log` VALUES (4150, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:29');
INSERT INTO `t_job_log` VALUES (4151, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:30');
INSERT INTO `t_job_log` VALUES (4152, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:31');
INSERT INTO `t_job_log` VALUES (4153, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:32');
INSERT INTO `t_job_log` VALUES (4154, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:33');
INSERT INTO `t_job_log` VALUES (4155, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:34');
INSERT INTO `t_job_log` VALUES (4156, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:35');
INSERT INTO `t_job_log` VALUES (4157, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:36');
INSERT INTO `t_job_log` VALUES (4158, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:37');
INSERT INTO `t_job_log` VALUES (4159, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:38');
INSERT INTO `t_job_log` VALUES (4160, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:39');
INSERT INTO `t_job_log` VALUES (4161, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:40');
INSERT INTO `t_job_log` VALUES (4162, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:41');
INSERT INTO `t_job_log` VALUES (4163, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:42');
INSERT INTO `t_job_log` VALUES (4164, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:43');
INSERT INTO `t_job_log` VALUES (4165, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:44');
INSERT INTO `t_job_log` VALUES (4166, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:45');
INSERT INTO `t_job_log` VALUES (4167, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:46');
INSERT INTO `t_job_log` VALUES (4168, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:47');
INSERT INTO `t_job_log` VALUES (4169, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:48');
INSERT INTO `t_job_log` VALUES (4170, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:49');
INSERT INTO `t_job_log` VALUES (4171, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:50');
INSERT INTO `t_job_log` VALUES (4172, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:51');
INSERT INTO `t_job_log` VALUES (4173, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:52');
INSERT INTO `t_job_log` VALUES (4174, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:53');
INSERT INTO `t_job_log` VALUES (4175, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:54');
INSERT INTO `t_job_log` VALUES (4176, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:55');
INSERT INTO `t_job_log` VALUES (4177, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:56');
INSERT INTO `t_job_log` VALUES (4178, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:57');
INSERT INTO `t_job_log` VALUES (4179, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:20:58');
INSERT INTO `t_job_log` VALUES (4180, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:20:59');
INSERT INTO `t_job_log` VALUES (4181, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:00');
INSERT INTO `t_job_log` VALUES (4182, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:01');
INSERT INTO `t_job_log` VALUES (4183, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:02');
INSERT INTO `t_job_log` VALUES (4184, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:03');
INSERT INTO `t_job_log` VALUES (4185, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:21:04');
INSERT INTO `t_job_log` VALUES (4186, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:05');
INSERT INTO `t_job_log` VALUES (4187, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:06');
INSERT INTO `t_job_log` VALUES (4188, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:21:07');
INSERT INTO `t_job_log` VALUES (4189, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:08');
INSERT INTO `t_job_log` VALUES (4190, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:21:09');
INSERT INTO `t_job_log` VALUES (4191, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:21:10');
INSERT INTO `t_job_log` VALUES (4192, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:11');
INSERT INTO `t_job_log` VALUES (4193, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:12');
INSERT INTO `t_job_log` VALUES (4194, 3, 'testTask', 'test', 'hello world', '0', NULL, 0, '2021-10-13 13:21:13');
INSERT INTO `t_job_log` VALUES (4195, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:14');
INSERT INTO `t_job_log` VALUES (4196, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:15');
INSERT INTO `t_job_log` VALUES (4197, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:16');
INSERT INTO `t_job_log` VALUES (4198, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:17');
INSERT INTO `t_job_log` VALUES (4199, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:18');
INSERT INTO `t_job_log` VALUES (4200, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:19');
INSERT INTO `t_job_log` VALUES (4201, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:20');
INSERT INTO `t_job_log` VALUES (4202, 3, 'testTask', 'test', 'hello world', '0', NULL, 1, '2021-10-13 13:21:21');
INSERT INTO `t_job_log` VALUES (4203, 3, 'testTask', 'test', 'hello world', '0', NULL, 3, '2021-10-13 13:21:22');
INSERT INTO `t_job_log` VALUES (4204, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 87944, '2021-10-13 17:18:45');
INSERT INTO `t_job_log` VALUES (4205, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 73668, '2021-10-13 17:25:48');
INSERT INTO `t_job_log` VALUES (4206, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 93031, '2021-10-14 09:13:19');
INSERT INTO `t_job_log` VALUES (4207, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 60405, '2021-10-14 09:15:23');
INSERT INTO `t_job_log` VALUES (4208, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 19536, '2021-10-14 09:26:15');
INSERT INTO `t_job_log` VALUES (4209, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 39095, '2021-10-14 09:26:56');
INSERT INTO `t_job_log` VALUES (4210, 12, 'dbBackupTask', 'dbBackup', '', '0', NULL, 12020, '2021-10-14 09:28:39');
INSERT INTO `t_job_log` VALUES (4211, 2, 'testTask', 'test1', NULL, '0', NULL, 526, '2021-11-01 14:16:03');
INSERT INTO `t_job_log` VALUES (4212, 2, 'testTask', 'test1', NULL, '0', NULL, 76, '2021-11-01 14:16:10');
INSERT INTO `t_job_log` VALUES (4213, 2, 'testTask', 'test1', NULL, '0', NULL, 20, '2021-11-01 14:16:20');
INSERT INTO `t_job_log` VALUES (4214, 2, 'testTask', 'test1', NULL, '0', NULL, 34, '2021-11-01 14:16:30');
INSERT INTO `t_job_log` VALUES (4215, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:16:40');
INSERT INTO `t_job_log` VALUES (4216, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:16:50');
INSERT INTO `t_job_log` VALUES (4217, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-11-01 14:17:00');
INSERT INTO `t_job_log` VALUES (4218, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:17:10');
INSERT INTO `t_job_log` VALUES (4219, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:17:20');
INSERT INTO `t_job_log` VALUES (4220, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:17:30');
INSERT INTO `t_job_log` VALUES (4221, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:17:40');
INSERT INTO `t_job_log` VALUES (4222, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:17:50');
INSERT INTO `t_job_log` VALUES (4223, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:18:00');
INSERT INTO `t_job_log` VALUES (4224, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-11-01 14:18:10');
INSERT INTO `t_job_log` VALUES (4225, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-11-01 14:18:20');
INSERT INTO `t_job_log` VALUES (4226, 2, 'testTask', 'test1', NULL, '0', NULL, 49, '2021-11-01 14:18:30');
INSERT INTO `t_job_log` VALUES (4227, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:18:40');
INSERT INTO `t_job_log` VALUES (4228, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:18:50');
INSERT INTO `t_job_log` VALUES (4229, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-11-01 14:19:00');
INSERT INTO `t_job_log` VALUES (4230, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:19:10');
INSERT INTO `t_job_log` VALUES (4231, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:19:20');
INSERT INTO `t_job_log` VALUES (4232, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:19:30');
INSERT INTO `t_job_log` VALUES (4233, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:19:40');
INSERT INTO `t_job_log` VALUES (4234, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:19:50');
INSERT INTO `t_job_log` VALUES (4235, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-11-01 14:20:00');
INSERT INTO `t_job_log` VALUES (4236, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:20:10');
INSERT INTO `t_job_log` VALUES (4237, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:20:20');
INSERT INTO `t_job_log` VALUES (4238, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-11-01 14:20:30');
INSERT INTO `t_job_log` VALUES (4239, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-11-01 14:20:40');
INSERT INTO `t_job_log` VALUES (4240, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-11-01 14:20:50');
INSERT INTO `t_job_log` VALUES (4241, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:21:00');
INSERT INTO `t_job_log` VALUES (4242, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-11-01 14:21:10');
INSERT INTO `t_job_log` VALUES (4243, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:21:20');
INSERT INTO `t_job_log` VALUES (4244, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:21:30');
INSERT INTO `t_job_log` VALUES (4245, 2, 'testTask', 'test1', NULL, '0', NULL, 0, '2021-11-01 14:21:40');
INSERT INTO `t_job_log` VALUES (4246, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:21:50');
INSERT INTO `t_job_log` VALUES (4247, 2, 'testTask', 'test1', NULL, '0', NULL, 1, '2021-11-01 14:22:00');

-- ----------------------------
-- Table structure for t_log
-- ----------------------------
DROP TABLE IF EXISTS `t_log`;
CREATE TABLE `t_log`  (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志ID',
  `USERNAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作用户',
  `OPERATION` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '操作内容',
  `TIME` decimal(11, 0) NULL DEFAULT NULL COMMENT '耗时',
  `METHOD` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '操作方法',
  `PARAMS` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '方法参数',
  `IP` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作者IP',
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `location` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作地点',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `t_log_create_time`(`CREATE_TIME`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1310 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_log
-- ----------------------------
INSERT INTO `t_log` VALUES (1011, 'MrBird', '新增菜单/按钮', 33, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=179, parentId=125, menuName=导入视频, url=null, perms=eximport:importVedio, icon=null, type=1, orderNum=null, createTime=Tue Aug 17 17:39:33 CST 2021, modifyTime=null)\"', '192.168.123.145', '2021-08-17 17:39:33', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1012, 'MrBird', '修改角色', 157, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Tue Aug 17 17:39:55 CST 2021, menuIds=1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178)\"', '192.168.123.145', '2021-08-17 17:39:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1013, 'MrBird', '新增菜单/按钮', 37, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=180, parentId=3, menuName=导入, url=null, perms=eximport:importVedio, icon=null, type=1, orderNum=null, createTime=Wed Aug 18 00:09:51 CST 2021, modifyTime=null)\"', '192.168.123.145', '2021-08-18 00:09:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1014, 'MrBird', '修改角色', 81, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Aug 18 00:09:59 CST 2021, menuIds=1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178)\"', '192.168.123.145', '2021-08-18 00:09:59', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1015, 'MrBird', '新增菜单/按钮', 81, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=181, parentId=0, menuName=课程管理, url=, perms=, icon=layui-icon-container, type=0, orderNum=1, createTime=Fri Aug 20 16:27:51 CST 2021, modifyTime=null)\"', '0:0:0:0:0:0:0:1', '2021-08-20 16:27:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1016, 'MrBird', '修改菜单/按钮', 88, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=1, parentId=0, menuName=系统管理, url=, perms=, icon=layui-icon-setting, type=0, orderNum=6, createTime=null, modifyTime=Fri Aug 20 16:28:12 CST 2021)\"', '0:0:0:0:0:0:0:1', '2021-08-20 16:28:13', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1017, 'MrBird', '新增菜单/按钮', 48, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=182, parentId=181, menuName=视频课程, url=/course, perms=, icon=, type=0, orderNum=1, createTime=Fri Aug 20 16:38:58 CST 2021, modifyTime=null)\"', '0:0:0:0:0:0:0:1', '2021-08-20 16:38:59', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1018, 'MrBird', '修改菜单/按钮', 16, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=182, parentId=181, menuName=视频课程, url=/course, perms=course:view, icon=, type=0, orderNum=1, createTime=null, modifyTime=Fri Aug 20 16:39:34 CST 2021)\"', '0:0:0:0:0:0:0:1', '2021-08-20 16:39:35', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1019, 'MrBird', '修改角色', 130, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Fri Aug 20 16:39:49 CST 2021, menuIds=181,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164)\"', '0:0:0:0:0:0:0:1', '2021-08-20 16:39:49', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1020, 'MrBird', '修改菜单/按钮', 15, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=181, parentId=0, menuName=课程管理, url=, perms=, icon=layui-icon-appstore, type=0, orderNum=1, createTime=null, modifyTime=Fri Aug 20 16:40:20 CST 2021)\"', '0:0:0:0:0:0:0:1', '2021-08-20 16:40:20', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1021, 'MrBird', '新增菜单/按钮', 46, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=183, parentId=182, menuName=新增课程, url=null, perms=course:add, icon=null, type=1, orderNum=null, createTime=Fri Aug 20 16:41:15 CST 2021, modifyTime=null)\"', '0:0:0:0:0:0:0:1', '2021-08-20 16:41:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1022, 'MrBird', '修改角色', 62, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Fri Aug 20 16:46:33 CST 2021, menuIds=181,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164)\"', '0:0:0:0:0:0:0:1', '2021-08-20 16:46:34', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1023, 'MrBird', '新增菜单/按钮', 65, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=184, parentId=181, menuName=视频管理, url=/vedio, perms=vedio:view, icon=, type=0, orderNum=2, createTime=Fri Aug 20 17:14:13 CST 2021, modifyTime=null)\"', '0:0:0:0:0:0:0:1', '2021-08-20 17:14:13', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1024, 'MrBird', '修改角色', 68, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Fri Aug 20 17:14:19 CST 2021, menuIds=181,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164)\"', '0:0:0:0:0:0:0:1', '2021-08-20 17:14:20', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1025, 'MrBird', '修改角色', 117, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Sat Aug 21 11:30:18 CST 2021, menuIds=181,182,183,184,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164)\"', '0:0:0:0:0:0:0:1', '2021-08-21 11:30:18', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1026, 'MrBird', '新增菜单/按钮', 5, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=185, parentId=182, menuName=更新课程, url=null, perms=course:update, icon=null, type=1, orderNum=null, createTime=Sat Aug 21 16:41:29 CST 2021, modifyTime=null)\"', '0:0:0:0:0:0:0:1', '2021-08-21 16:41:29', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1027, 'MrBird', '新增菜单/按钮', 19, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=186, parentId=182, menuName=删除课程, url=null, perms=course:delete, icon=null, type=1, orderNum=null, createTime=Sat Aug 21 16:41:57 CST 2021, modifyTime=null)\"', '0:0:0:0:0:0:0:1', '2021-08-21 16:41:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1028, 'MrBird', '修改角色', 55, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Sat Aug 21 16:42:10 CST 2021, menuIds=181,182,183,185,186,184,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164)\"', '0:0:0:0:0:0:0:1', '2021-08-21 16:42:10', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1029, 'MrBird', '新增Course', 82, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: Course(courseId=1, courseName=测试课程, courseTitle=测试课程标题, courseType=null, status=1, createTime=Sun Aug 22 11:19:53 CST 2021, modifyTime=null, description=测试课程描述, vediourl=null)', '0:0:0:0:0:0:0:1', '2021-08-22 11:19:53', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1030, 'MrBird', '新增Course', 26, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: Course(courseId=2, courseName=课程测试2, courseTitle=, courseType=null, status=1, createTime=Sun Aug 22 14:39:47 CST 2021, modifyTime=null, description=, vediourl=null)', '0:0:0:0:0:0:0:1', '2021-08-22 14:39:48', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1031, 'MrBird', '修改角色', 117, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Sun Aug 22 15:22:08 CST 2021, menuIds=181,182,183,185,186,184,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,180,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164)\"', '0:0:0:0:0:0:0:1', '2021-08-22 15:22:08', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1032, 'MrBird', '修改角色', 61, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Sun Aug 22 16:59:18 CST 2021, menuIds=181,182,183,185,186,184,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,180,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164)\"', '0:0:0:0:0:0:0:1', '2021-08-22 16:59:18', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1033, 'MrBird', '新增菜单/按钮', 35, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=187, parentId=182, menuName=上传视频, url=null, perms=eximport:importVideo, icon=null, type=1, orderNum=null, createTime=Sun Aug 22 17:06:27 CST 2021, modifyTime=null)\"', '0:0:0:0:0:0:0:1', '2021-08-22 17:06:28', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1034, 'MrBird', '修改角色', 69, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Sun Aug 22 17:06:36 CST 2021, menuIds=181,182,183,185,186,187,184,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,180,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164)\"', '0:0:0:0:0:0:0:1', '2021-08-22 17:06:37', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1035, 'MrBird', '新增菜单/按钮', 926, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=188, parentId=181, menuName=科目管理, url=/subject, perms=subject:view, icon=, type=0, orderNum=1, createTime=Wed Aug 25 15:09:46 CST 2021, modifyTime=null)\"', '192.168.48.200', '2021-08-25 15:09:47', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1036, 'MrBird', '新增菜单/按钮', 2691, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=189, parentId=188, menuName=新增科目, url=null, perms=subject:add, icon=null, type=1, orderNum=null, createTime=Wed Aug 25 15:12:09 CST 2021, modifyTime=null)\"', '192.168.48.200', '2021-08-25 15:12:11', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1037, 'MrBird', '新增菜单/按钮', 217, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=190, parentId=188, menuName=更新科目, url=null, perms=subject_update, icon=null, type=1, orderNum=null, createTime=Wed Aug 25 15:12:53 CST 2021, modifyTime=null)\"', '192.168.48.200', '2021-08-25 15:12:54', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1038, 'MrBird', '新增菜单/按钮', 237, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=191, parentId=188, menuName=删除科目, url=null, perms=subject:delete, icon=null, type=1, orderNum=null, createTime=Wed Aug 25 15:13:16 CST 2021, modifyTime=null)\"', '192.168.48.200', '2021-08-25 15:13:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1039, 'MrBird', '修改角色', 3512, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Aug 25 15:14:10 CST 2021, menuIds=181,182,183,185,186,187,188,184,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,180,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164)\"', '192.168.48.200', '2021-08-25 15:14:14', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1040, 'MrBird', '新增菜单/按钮', 4952, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=192, parentId=188, menuName=导出Excel, url=null, perms=subject:export, icon=null, type=1, orderNum=null, createTime=Wed Aug 25 15:24:26 CST 2021, modifyTime=null)\"', '192.168.48.200', '2021-08-25 15:24:29', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1041, 'MrBird', '修改角色', 6710, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Aug 25 15:24:49 CST 2021, menuIds=181,182,183,185,186,187,188,189,190,191,192,184,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,180,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164)\"', '192.168.48.200', '2021-08-25 15:24:57', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1042, 'MrBird', '新增Course', 25078, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=3, courseName=课程测试4, courseTitle=课程测试4标题, courseType=null, ctypeName=null, status=1, createTime=Thu Aug 26 11:07:47 CST 2021, modifyTime=null, description=描述, videourl=files\\\\20210826\\\\暖场PPT2.mp4, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-08-26 11:07:48', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1043, 'MrBird', '新增Course', 687, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=4, courseName=课程测试5, courseTitle=, courseType=null, ctypeName=null, status=1, createTime=Thu Aug 26 11:12:33 CST 2021, modifyTime=null, description=, videourl={$detail.videourl}, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-08-26 11:12:34', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1044, 'MrBird', '新增Course', 1344, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=5, courseName=课程测试6, courseTitle=, courseType=null, ctypeName=null, status=1, createTime=Thu Aug 26 11:13:46 CST 2021, modifyTime=null, description=, videourl={$detail.videourl}, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-08-26 11:13:47', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1045, 'MrBird', '新增Course', 18544, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=6, courseName=课程测试7, courseTitle=, courseType=null, ctypeName=null, status=1, createTime=Thu Aug 26 11:14:43 CST 2021, modifyTime=null, description=, videourl={$detail.videourl}, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-08-26 11:14:45', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1046, 'MrBird', '新增Course', 1224, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=7, courseName=课程测试8, courseTitle=, courseType=1, ctypeName=null, status=1, createTime=Thu Aug 26 11:15:10 CST 2021, modifyTime=null, description=, videourl={$detail.videourl}, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-08-26 11:15:11', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1047, 'MrBird', '修改菜单/按钮', 50028, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=3, parentId=0, menuName=用户管理, url=/system/user/admin, perms=user:view, icon=, type=0, orderNum=1, createTime=null, modifyTime=Fri Aug 27 16:16:22 CST 2021)\"', '0:0:0:0:0:0:0:1', '2021-08-27 16:16:42', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1048, 'MrBird', '修改菜单/按钮', 26686, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=3, parentId=1, menuName=用户管理, url=/system/user/admin, perms=user:view, icon=, type=0, orderNum=1, createTime=null, modifyTime=Fri Aug 27 16:18:03 CST 2021)\"', '0:0:0:0:0:0:0:1', '2021-08-27 16:18:23', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1049, 'MrBird', '修改菜单/按钮', 1045, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=3, parentId=1, menuName=用户管理, url=/system/admin, perms=user:view, icon=, type=0, orderNum=1, createTime=null, modifyTime=Fri Aug 27 16:23:19 CST 2021)\"', '0:0:0:0:0:0:0:1', '2021-08-27 16:23:21', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1050, 'MrBird', '新增Subject', 6540, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=3, pSubjectId=0, pSubjectTitle=null, title=岗位技工, content=岗位技工-主, imgUrl=/images/subject/2021-08-31/2b00d33b-ab25-4fab-83e8-d17dea150b7d.jpg, createTime=Tue Aug 31 17:47:17 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-08-31 17:47:20', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1051, 'MrBird', '删除Subject', 214, 'com.dzwx.gen.controller.SubjectController.deleteSubject()', ' subjectId: \"1\"', '192.168.48.200', '2021-08-31 17:51:57', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1052, 'MrBird', '新增Subject', 6398, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=4, pSubjectId=0, pSubjectTitle=null, title=岗位技工, content=岗位技工-主, imgUrl=/images/subject/2021-08-31/38a08647-4bf9-498e-8757-a2b0a1ee258b.jpg, createTime=Tue Aug 31 17:58:15 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-08-31 17:58:22', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1053, 'MrBird', '新增菜单/按钮', 15225, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=193, parentId=1, menuName=管理员, url=/system/user, perms=user:view, icon=, type=0, orderNum=2, createTime=Wed Sep 01 00:46:49 CST 2021, modifyTime=null)\"', '0:0:0:0:0:0:0:1', '2021-09-01 00:46:49', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1054, 'MrBird', '新增菜单/按钮', 10169, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=194, parentId=1, menuName=管理员, url=/system/user, perms=user:view, icon=, type=0, orderNum=2, createTime=Wed Sep 01 00:46:49 CST 2021, modifyTime=null)\"', '0:0:0:0:0:0:0:1', '2021-09-01 00:46:49', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1055, 'MrBird', '修改角色', 3159, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Sep 01 00:48:43 CST 2021, menuIds=181,182,183,185,186,187,188,189,190,191,192,184,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,180,4,14,15,16,162,193,194,5,17,18,19,163,6,20,21,22,164)\"', '0:0:0:0:0:0:0:1', '2021-09-01 00:48:47', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1056, 'MrBird', '删除菜单/按钮', 318, 'com.dzwx.system.controller.MenuController.deleteMenus()', ' menuIds: \"194\"', '0:0:0:0:0:0:0:1', '2021-09-01 00:49:08', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1057, 'MrBird', '修改菜单/按钮', 259, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=4, parentId=1, menuName=角色管理, url=/system/role, perms=role:view, icon=, type=0, orderNum=3, createTime=null, modifyTime=Wed Sep 01 00:49:48 CST 2021)\"', '0:0:0:0:0:0:0:1', '2021-09-01 00:49:49', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1058, 'MrBird', '修改菜单/按钮', 228, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=5, parentId=1, menuName=菜单管理, url=/system/menu, perms=menu:view, icon=, type=0, orderNum=4, createTime=null, modifyTime=Wed Sep 01 00:49:55 CST 2021)\"', '0:0:0:0:0:0:0:1', '2021-09-01 00:49:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1059, 'MrBird', '修改菜单/按钮', 215, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=6, parentId=1, menuName=部门管理, url=/system/dept, perms=dept:view, icon=, type=0, orderNum=5, createTime=null, modifyTime=Wed Sep 01 00:50:00 CST 2021)\"', '0:0:0:0:0:0:0:1', '2021-09-01 00:50:01', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1060, 'MrBird', '新增Subject', 244, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=5, pSubjectId=4, pSubjectTitle=null, title=砌筑工, content=砌筑工, imgUrl=/images/subject/2021-09-01/e700ab74-9e5d-4ca0-9403-6b622b75fcb8.jpg, createTime=Wed Sep 01 09:50:06 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-01 09:50:07', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1061, 'MrBird', '新增Subject', 238, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=6, pSubjectId=4, pSubjectTitle=null, title=混凝土工, content=混凝土, imgUrl=/images/subject/2021-09-01/39836dcb-bd64-4d58-b00e-8808362a4cf1.jpg, createTime=Wed Sep 01 09:50:33 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-01 09:50:34', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1062, 'MrBird', '新增Subject', 218, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=7, pSubjectId=0, pSubjectTitle=null, title=test, content=test, imgUrl=, createTime=Wed Sep 01 09:50:50 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-01 09:50:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1063, 'MrBird', '新增Subject', 274, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=8, pSubjectId=4, pSubjectTitle=null, title=test, content=test, imgUrl=, createTime=Wed Sep 01 09:50:59 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-01 09:51:00', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1064, 'MrBird', '删除Subject', 222, 'com.dzwx.gen.controller.SubjectController.deleteSubject()', ' subjectIds: \"7\"', '192.168.48.200', '2021-09-01 09:51:38', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1065, 'MrBird', '新增Subject', 234, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=9, pSubjectId=0, pSubjectTitle=null, title=ttt, content=ttt, imgUrl=, createTime=Wed Sep 01 09:51:43 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-01 09:51:44', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1066, 'MrBird', '删除Subject', 268, 'com.dzwx.gen.controller.SubjectController.deleteSubject()', ' subjectIds: \"9,8\"', '192.168.48.200', '2021-09-01 09:51:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1067, 'MrBird', '修改菜单/按钮', 371, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=190, parentId=188, menuName=更新科目, url=null, perms=subject:update, icon=null, type=1, orderNum=null, createTime=null, modifyTime=Wed Sep 01 09:56:49 CST 2021)\"', '192.168.48.200', '2021-09-01 09:56:50', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1068, 'MrBird', '新增Subject', 321, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=10, pSubjectId=4, pSubjectTitle=null, title=钢筋工, content=test, imgUrl=subject/2021-09-01/e3677b9a-032b-410d-9263-b7207fb2b99a.jpg, createTime=Wed Sep 01 15:30:35 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-01 15:30:35', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1069, 'MrBird', '新增用户', 565, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=9, username=testuser, password=ad322b6af7ae32a79f149a7509b387f1, deptId=null, email=, mobile=, status=1, createTime=Wed Sep 01 16:43:10 CST 2021, modifyTime=null, lastLoginTime=null, sex=0, avatar=default.jpg, theme=black, isTab=1, description=, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=2, roleName=null, deptIds=, stringPermissions=null, roles=null, isUser=null, idcard=null, nationality=null, nationalityName=null, idcardFont=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null)\"', '0:0:0:0:0:0:0:1', '2021-09-01 16:43:11', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1070, 'MrBird', '修改菜单/按钮', 1121, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=182, parentId=181, menuName=课程视频, url=/course, perms=course:view, icon=, type=0, orderNum=1, createTime=null, modifyTime=Wed Sep 01 17:31:33 CST 2021)\"', '0:0:0:0:0:0:0:1', '2021-09-01 17:31:35', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1071, 'MrBird', '新增Course', 217, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=8, courseName=视频测试, courseTitle=, courseType=1, ctypeName=null, status=1, createTime=Wed Sep 01 23:19:20 CST 2021, modifyTime=null, description=无, thumbnail=null, videourl=files\\\\20210901\\\\暖场PPT2.mp4, videosize=144742503, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-01 23:19:20', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1072, 'MrBird', '修改Subject', 6566, 'com.dzwx.gen.controller.SubjectController.updateSubject()', ' subject: \"Subject(subjectId=10, pSubjectId=null, pSubjectTitle=null, title=钢筋工, content=test1, imgUrl=/subject/2021-09-02/0583f720-8057-4667-a4a0-ac216a027bde.jpg, createTime=null, creator=null)\"', '192.168.48.200', '2021-09-02 15:49:23', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1073, 'MrBird', '导出Subject', 6347, 'com.dzwx.gen.controller.SubjectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" subject: \"Subject(subjectId=null, pSubjectId=null, pSubjectTitle=null, title=null, content=null, imgUrl=null, createTime=null, creator=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@65827cb0', '192.168.48.200', '2021-09-02 16:09:20', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1074, 'MrBird', '新增Subject', 214, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=11, pSubjectId=4, pSubjectTitle=null, title=水泥工, content=test, imgUrl=/subject/2021-09-02/54e752df-291e-4751-8f02-943281cc77c9.jpg, createTime=Thu Sep 02 16:29:58 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-02 16:29:59', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1075, 'MrBird', '新增Subject', 273, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=12, pSubjectId=0, pSubjectTitle=null, title=test, content=, imgUrl=, createTime=Thu Sep 02 16:30:35 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-02 16:30:35', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1076, 'MrBird', '新增Subject', 253, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=13, pSubjectId=0, pSubjectTitle=null, title=qwertyuiii, content=, imgUrl=, createTime=Thu Sep 02 16:32:30 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-02 16:32:30', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1077, 'MrBird', '修改Subject', 3830, 'com.dzwx.gen.controller.SubjectController.updateSubject()', ' subject: \"Subject(subjectId=13, pSubjectId=null, pSubjectTitle=null, title=qwertyuiii, content=, imgUrl=, createTime=null, creator=null)\"', '192.168.48.200', '2021-09-02 16:34:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1078, 'MrBird', '删除Subject', 243, 'com.dzwx.gen.controller.SubjectController.deleteSubject()', ' subjectIds: \"13\"', '192.168.48.200', '2021-09-02 16:34:21', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1079, 'MrBird', '删除Subject', 280, 'com.dzwx.gen.controller.SubjectController.deleteSubject()', ' subjectIds: \"12\"', '192.168.48.200', '2021-09-02 16:34:23', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1080, 'MrBird', '删除Course', 723, 'com.dzwx.gen.controller.CourseController.deleteCourse()', ' courseIds: \"4\"', '0:0:0:0:0:0:0:1', '2021-09-02 16:36:19', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1081, 'MrBird', '删除Subject', 9091, 'com.dzwx.gen.controller.SubjectController.deleteSubject()', ' subjectIds: \"11\"', '192.168.48.200', '2021-09-02 16:42:36', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1082, 'MrBird', '修改菜单/按钮', 482, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=3, parentId=1, menuName=用户管理, url=/system/user, perms=user:view, icon=, type=0, orderNum=1, createTime=null, modifyTime=Wed Sep 08 13:52:08 CST 2021)\"', '192.168.48.200', '2021-09-08 13:52:09', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1083, 'MrBird', '修改菜单/按钮', 306, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=193, parentId=1, menuName=管理员, url=/system/admin, perms=user:view, icon=, type=0, orderNum=2, createTime=null, modifyTime=Wed Sep 08 13:52:15 CST 2021)\"', '192.168.48.200', '2021-09-08 13:52:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1084, 'MrBird', '新增角色', 249, 'com.dzwx.system.controller.RoleController.addRole()', ' role: \"Role(roleId=81, roleName=APP用户, remark=无后台系统使用权, createTime=Wed Sep 08 14:01:39 CST 2021, modifyTime=null, menuIds=)\"', '192.168.48.200', '2021-09-08 14:01:39', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1085, 'MrBird', '新增用户', 428, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=10, username=张三, password=7d3d22d184449ce83f1a7ae5707bf4ed, deptId=null, email=null, mobile=, status=1, createTime=Wed Sep 08 14:09:04 CST 2021, modifyTime=null, lastLoginTime=null, sex=null, avatar=default.jpg, theme=black, isTab=1, description=, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=3, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=null, nickname=null, idcard=null, nationality=null, nationalityName=null, idcardFont=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null)\"', '192.168.48.200', '2021-09-08 14:09:05', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1086, 'MrBird', '新增用户', 291, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=11, username=李四, password=a67cec6cc9d9e8c6bfda0a94b9d79459, deptId=null, email=null, mobile=, status=1, createTime=Wed Sep 08 14:10:54 CST 2021, modifyTime=null, lastLoginTime=null, sex=null, avatar=default.jpg, theme=black, isTab=1, description=, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=3, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=null, nickname=null, idcard=null, nationality=null, nationalityName=null, idcardFont=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null)\"', '192.168.48.200', '2021-09-08 14:10:55', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1087, 'MrBird', '新增用户', 404, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=12, username=王五, password=371b2260f2e740fb2fa1991afd056c43, deptId=null, email=null, mobile=, status=1, createTime=Wed Sep 08 14:11:11 CST 2021, modifyTime=null, lastLoginTime=null, sex=null, avatar=default.jpg, theme=black, isTab=1, description=, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=3, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=1, nickname=null, idcard=null, nationality=null, nationalityName=null, idcardFont=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null)\"', '192.168.48.200', '2021-09-08 14:11:12', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1088, 'MrBird', '新增用户', 43295, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=13, username=GG, password=3ddaaef8cf35c3d0a4c7a5a34eaec213, deptId=null, email=null, mobile=, status=1, createTime=Wed Sep 08 14:12:43 CST 2021, modifyTime=null, lastLoginTime=null, sex=null, avatar=default.jpg, theme=black, isTab=1, description=, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=3, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=1, nickname=null, idcard=null, nationality=null, nationalityName=null, idcardFont=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null)\"', '192.168.48.200', '2021-09-08 14:13:27', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1089, 'MrBird', '删除用户', 423, 'com.dzwx.system.controller.UserController.deleteUsers()', ' userIds: \"12\"', '192.168.48.200', '2021-09-08 14:14:39', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1090, 'MrBird', '删除用户', 450, 'com.dzwx.system.controller.UserController.deleteUsers()', ' userIds: \"13\"', '192.168.48.200', '2021-09-08 14:14:43', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1091, 'MrBird', '新增用户', 18215, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=14, username=张三, password=7d3d22d184449ce83f1a7ae5707bf4ed, deptId=null, email=null, mobile=, status=1, createTime=Wed Sep 08 14:16:41 CST 2021, modifyTime=null, lastLoginTime=null, sex=2, avatar=default.jpg, theme=black, isTab=1, description=, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=3, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=1, nickname=null, idcard=null, nationality=null, nationalityName=null, idcardFont=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null)\"', '192.168.48.200', '2021-09-08 14:16:55', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1092, 'MrBird', '修改菜单/按钮', 382, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=193, parentId=1, menuName=管理员, url=/system/admin, perms=admin:view, icon=, type=0, orderNum=2, createTime=null, modifyTime=Wed Sep 08 14:27:01 CST 2021)\"', '192.168.48.200', '2021-09-08 14:27:02', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1093, 'MrBird', '新增菜单/按钮', 286, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=195, parentId=193, menuName=新增, url=null, perms=admin:add, icon=null, type=1, orderNum=null, createTime=Wed Sep 08 14:27:54 CST 2021, modifyTime=null)\"', '192.168.48.200', '2021-09-08 14:27:55', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1094, 'MrBird', '修改菜单/按钮', 1325, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=195, parentId=193, menuName=新增管理员, url=null, perms=admin:add, icon=null, type=1, orderNum=null, createTime=null, modifyTime=Wed Sep 08 14:28:08 CST 2021)\"', '192.168.48.200', '2021-09-08 14:28:09', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1095, 'MrBird', '新增菜单/按钮', 16298, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=196, parentId=193, menuName=修改管理员, url=null, perms=admin:update, icon=null, type=1, orderNum=null, createTime=Wed Sep 08 14:32:16 CST 2021, modifyTime=null)\"', '192.168.48.200', '2021-09-08 14:32:29', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1096, 'MrBird', '新增菜单/按钮', 12881, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=197, parentId=193, menuName=删除管理员, url=null, perms=admin:delete, icon=null, type=1, orderNum=null, createTime=Wed Sep 08 14:33:11 CST 2021, modifyTime=null)\"', '192.168.48.200', '2021-09-08 14:33:18', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1097, 'MrBird', '新增菜单/按钮', 10610, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=198, parentId=193, menuName=重置密码, url=null, perms=admin:password:reset, icon=null, type=1, orderNum=null, createTime=Wed Sep 08 14:34:26 CST 2021, modifyTime=null)\"', '192.168.48.200', '2021-09-08 14:34:34', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1098, 'MrBird', '新增菜单/按钮', 9755, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=199, parentId=193, menuName=导出Ｅｘｃｅｌ, url=null, perms=, icon=null, type=1, orderNum=null, createTime=Wed Sep 08 14:35:12 CST 2021, modifyTime=null)\"', '192.168.48.200', '2021-09-08 14:35:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1099, 'MrBird', '修改菜单/按钮', 18559, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=199, parentId=193, menuName=导出Ｅｘｃｅｌ, url=null, perms=ａｄｍｉｎ:export, icon=null, type=1, orderNum=null, createTime=null, modifyTime=Wed Sep 08 14:35:58 CST 2021)\"', '192.168.48.200', '2021-09-08 14:36:11', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1100, 'MrBird', '修改角色', 14122, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Sep 08 14:39:37 CST 2021, menuIds=181,182,183,185,186,187,188,189,190,191,192,184,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,180,193,195,196,197,198,199,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164)\"', '192.168.48.200', '2021-09-08 14:39:42', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1101, 'MrBird', '新增用户', 3552, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=15, username=李四, password=a67cec6cc9d9e8c6bfda0a94b9d79459, deptId=null, email=null, mobile=13047495791, status=1, createTime=Wed Sep 08 14:43:52 CST 2021, modifyTime=null, lastLoginTime=null, sex=2, avatar=default.jpg, theme=black, isTab=1, description=test, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=3, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=1, nickname=null, idcard=null, nationality=null, nationalityName=null, idcardFont=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null)\"', '192.168.48.200', '2021-09-08 14:43:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1102, 'MrBird', '修改菜单/按钮', 3791, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=187, parentId=182, menuName=上传视频, url=null, perms=course:upload, icon=null, type=1, orderNum=null, createTime=null, modifyTime=Wed Sep 08 14:55:45 CST 2021)\"', '0:0:0:0:0:0:0:1', '2021-09-08 14:55:47', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1103, 'MrBird', '删除用户', 723, 'com.dzwx.system.controller.UserController.deleteUsers()', ' userIds: \"9\"', '192.168.48.200', '2021-09-08 15:12:40', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1104, 'MrBird', '修改角色', 29805, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Sep 08 16:19:54 CST 2021, menuIds=181,182,183,185,186,187,188,189,190,191,192,184,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,180,193,195,196,197,198,199,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164)\"', '0:0:0:0:0:0:0:1', '2021-09-08 16:20:24', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1105, 'MrBird', '新增Course', 208, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=9, courseName=课程视频测试1, courseTitle=null, courseType=null, ctypeName=null, status=1, createTime=Wed Sep 08 21:38:38 CST 2021, modifyTime=null, description=无, thumbnail=, videourl=course\\\\videos\\\\2021-09-08\\\\704f94c6-cc3d-425a-9909-153a2c39995b.mp4, videosize=15691201, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-08 21:38:39', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1106, 'MrBird', '新增Course', 183, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=10, courseName=课程视频测试2, courseTitle=null, courseType=1, ctypeName=null, status=1, createTime=Wed Sep 08 21:42:18 CST 2021, modifyTime=null, description=无, thumbnail=course\\\\images\\\\2021-09-08\\\\a1d7d31a-927e-42b3-8d4c-07b609692f78.png, videourl=course\\\\videos\\\\2021-09-08\\\\6eca9bc0-3545-48b4-b0c7-34be898821a4.mp4, videosize=15691201, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-08 21:42:18', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1107, 'MrBird', '修改Course', 223, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=10, courseName=课程视频测试2, courseTitle=null, courseType=null, ctypeName=null, status=1, createTime=null, modifyTime=null, description=无, thumbnail=course\\\\images\\\\2021-09-08\\\\8b76c27d-ec38-45e4-8908-38d540c5da44.jpg, videourl=course\\\\videos\\\\2021-09-08\\\\6eca9bc0-3545-48b4-b0c7-34be898821a4.mp4, videosize=15691201, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-08 22:52:14', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1108, 'MrBird', '修改Course', 215, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=10, courseName=课程视频测试2, courseTitle=null, courseType=null, ctypeName=null, status=1, createTime=null, modifyTime=null, description=无, thumbnail=course\\\\images\\\\2021-09-08\\\\8b76c27d-ec38-45e4-8908-38d540c5da44.jpg, videourl=course\\\\videos\\\\2021-09-08\\\\31d8dc83-3ab8-4673-a977-681b73d78769.mp4, videosize=143967200, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-08 23:02:19', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1109, 'MrBird', '修改Course', 1122, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=1, courseName=测试课程, courseTitle=null, subject=null, subjectName=null, status=1, createTime=null, modifyTime=null, description=测试课程描述, thumbnail=upload\\\\20210901\\\\193654.png, videourl=, videosize=0, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-09 11:27:36', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1110, 'MrBird', '修改Course', 688, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=7, courseName=课程测试8, courseTitle=null, subject=null, subjectName=null, status=1, createTime=null, modifyTime=null, description=, thumbnail=, videourl={$detail.videourl}, videosize=0, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-09 11:27:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1111, 'MrBird', '修改Course', 6347, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=10, courseName=课程视频测试2, courseTitle=null, subject=null, subjectName=null, status=1, createTime=null, modifyTime=null, description=无, thumbnail=course\\\\images\\\\2021-09-08\\\\8b76c27d-ec38-45e4-8908-38d540c5da44.jpg, videourl=course\\\\videos\\\\2021-09-08\\\\31d8dc83-3ab8-4673-a977-681b73d78769.mp4, videosize=143967200, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-09 14:27:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1112, 'MrBird', '修改Course', 5576, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=2, courseName=课程测试2, courseTitle=null, subject=6, subjectName=null, status=1, createTime=null, modifyTime=null, description=, thumbnail=, videourl=20210901/PPT2_Trim.mp4, videosize=0, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-09 14:35:48', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1113, 'MrBird', '修改Course', 4372, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=2, courseName=课程测试2, courseTitle=null, subject=6, subjectName=null, status=1, createTime=null, modifyTime=null, description=, thumbnail=, videourl=20210901/PPT2_Trim.mp4, videosize=0, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-09 14:35:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1114, 'MrBird', '修改Course', 3248, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=3, courseName=课程测试4, courseTitle=null, subject=4, subjectName=null, status=1, createTime=null, modifyTime=null, description=描述, thumbnail=, videourl=20210826\\\\暖场PPT2.mp4, videosize=0, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-09 14:36:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1115, 'MrBird', '新增Course', 3751, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=11, courseName=安全教育, courseTitle=null, subject=10, subjectName=null, status=1, createTime=Thu Sep 09 14:37:45 CST 2021, modifyTime=null, description=无, thumbnail=course\\\\images\\\\2021-09-09\\\\287db78e-9156-4ffd-9821-79e6108190b0.jpg, videourl=course\\\\videos\\\\2021-09-09\\\\904d15f0-46c4-4962-8071-6b7457b019f4.mp4, videosize=144742503, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-09 14:37:47', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1116, 'MrBird', '新增菜单/按钮', 9087, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=200, parentId=182, menuName=导出课程视频, url=null, perms=course:export, icon=null, type=1, orderNum=null, createTime=Thu Sep 09 15:35:38 CST 2021, modifyTime=null)\"', '0:0:0:0:0:0:0:1', '2021-09-09 15:35:45', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1117, 'MrBird', '修改菜单/按钮', 4334, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=200, parentId=182, menuName=导出EXCEL, url=null, perms=course:export, icon=null, type=1, orderNum=null, createTime=null, modifyTime=Thu Sep 09 15:36:25 CST 2021)\"', '0:0:0:0:0:0:0:1', '2021-09-09 15:36:29', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1118, 'MrBird', '修改菜单/按钮', 6533, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=200, parentId=182, menuName=导出Excel, url=null, perms=course:export, icon=null, type=1, orderNum=null, createTime=null, modifyTime=Thu Sep 09 15:36:51 CST 2021)\"', '0:0:0:0:0:0:0:1', '2021-09-09 15:36:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1119, 'MrBird', '修改角色', 22272, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Thu Sep 09 15:37:03 CST 2021, menuIds=181,182,183,185,186,187,200,188,189,190,191,192,184,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,180,193,195,196,197,198,199,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164)\"', '0:0:0:0:0:0:0:1', '2021-09-09 15:37:25', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1120, 'MrBird', '修改Course', 5979, 'com.dzwx.gen.controller.CourseController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" course: \"Course(courseId=null, courseName=, courseTitle=null, subject=null, subjectName=null, status=, createTime=null, modifyTime=null, description=null, thumbnail=null, videourl=null, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@7121780f', '0:0:0:0:0:0:0:1', '2021-09-09 16:07:38', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1121, 'MrBird', '修改Course', 2863, 'com.dzwx.gen.controller.CourseController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" course: \"Course(courseId=null, courseName=, courseTitle=null, subject=null, subjectName=null, status=, createTime=null, modifyTime=null, description=null, thumbnail=null, videourl=null, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@68580869', '0:0:0:0:0:0:0:1', '2021-09-09 16:14:57', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1122, 'MrBird', '修改Course', 2038, 'com.dzwx.gen.controller.CourseController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" course: \"Course(courseId=null, courseName=, courseTitle=null, subject=null, subjectName=null, status=, createTime=null, modifyTime=null, description=null, thumbnail=null, videourl=null, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@2cda1b50', '0:0:0:0:0:0:0:1', '2021-09-09 16:18:33', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1123, 'MrBird', '修改Course', 1339, 'com.dzwx.gen.controller.CourseController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" course: \"Course(courseId=null, courseName=, courseTitle=null, subject=null, subjectName=null, status=, createTime=null, modifyTime=null, description=null, thumbnail=null, videourl=null, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@2c51f2cc', '0:0:0:0:0:0:0:1', '2021-09-09 16:28:14', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1124, 'MrBird', '删除Subject', 286, 'com.dzwx.gen.controller.SubjectController.deleteSubject()', ' subjectIds: \"10,6,5,4\"', '192.168.48.200', '2021-09-09 17:30:04', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1125, 'MrBird', '新增Subject', 266, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=14, pSubjectId=0, pSubjectTitle=null, title=岗位技工, content=岗位技工, imgUrl=, createTime=Thu Sep 09 17:34:07 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:34:07', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1126, 'MrBird', '新增Subject', 5000, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=15, pSubjectId=14, pSubjectTitle=null, title=砌筑工, content=砌筑工, imgUrl=, createTime=Thu Sep 09 17:39:15 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:39:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1127, 'MrBird', '新增Subject', 3574, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=16, pSubjectId=14, pSubjectTitle=null, title=砌筑工, content=砌筑工, imgUrl=, createTime=Thu Sep 09 17:39:15 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:39:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1128, 'MrBird', '新增Subject', 3041, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=17, pSubjectId=14, pSubjectTitle=null, title=砌筑工, content=砌筑工, imgUrl=, createTime=Thu Sep 09 17:39:15 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:39:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1129, 'MrBird', '删除Subject', 660, 'com.dzwx.gen.controller.SubjectController.deleteSubject()', ' subjectIds: \"16\"', '192.168.48.200', '2021-09-09 17:39:29', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1130, 'MrBird', '删除Subject', 816, 'com.dzwx.gen.controller.SubjectController.deleteSubject()', ' subjectIds: \"17\"', '192.168.48.200', '2021-09-09 17:39:38', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1131, 'MrBird', '新增Subject', 8860, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=18, pSubjectId=14, pSubjectTitle=null, title=混凝土工, content=, imgUrl=, createTime=Thu Sep 09 17:40:06 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:40:10', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1132, 'MrBird', '新增Subject', 246, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=19, pSubjectId=14, pSubjectTitle=null, title=钢筋工, content=, imgUrl=, createTime=Thu Sep 09 17:40:15 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:40:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1133, 'MrBird', '新增Subject', 910, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=20, pSubjectId=14, pSubjectTitle=null, title=架子工, content=, imgUrl=, createTime=Thu Sep 09 17:40:26 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:40:27', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1134, 'MrBird', '新增Subject', 308, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=21, pSubjectId=14, pSubjectTitle=null, title=手工木工, content=, imgUrl=, createTime=Thu Sep 09 17:40:35 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:40:35', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1135, 'MrBird', '新增Subject', 508, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=22, pSubjectId=14, pSubjectTitle=null, title=焊工, content=, imgUrl=, createTime=Thu Sep 09 17:40:44 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:40:45', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1136, 'MrBird', '新增Subject', 535, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=23, pSubjectId=14, pSubjectTitle=null, title=防水工, content=, imgUrl=, createTime=Thu Sep 09 17:40:55 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:40:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1137, 'MrBird', '新增Subject', 364, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=24, pSubjectId=14, pSubjectTitle=null, title=模板工, content=, imgUrl=, createTime=Thu Sep 09 17:41:05 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:41:06', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1138, 'MrBird', '新增Subject', 376, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=25, pSubjectId=14, pSubjectTitle=null, title=电工, content=, imgUrl=, createTime=Thu Sep 09 17:41:16 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:41:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1139, 'MrBird', '新增Subject', 296, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=26, pSubjectId=14, pSubjectTitle=null, title=管道工, content=, imgUrl=, createTime=Thu Sep 09 17:41:24 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:41:24', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1140, 'MrBird', '新增Subject', 296, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=27, pSubjectId=14, pSubjectTitle=null, title=起重机械操作工, content=, imgUrl=, createTime=Thu Sep 09 17:41:34 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:41:35', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1141, 'MrBird', '新增Subject', 670, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=28, pSubjectId=14, pSubjectTitle=null, title=抹灰工, content=, imgUrl=, createTime=Thu Sep 09 17:41:44 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:41:44', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1142, 'MrBird', '新增Subject', 258, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=29, pSubjectId=14, pSubjectTitle=null, title=镶贴工, content=, imgUrl=, createTime=Thu Sep 09 17:41:52 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:41:53', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1143, 'MrBird', '新增Subject', 295, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=30, pSubjectId=14, pSubjectTitle=null, title=油漆工, content=, imgUrl=, createTime=Thu Sep 09 17:42:03 CST 2021, creator=MrBird)\"', '192.168.48.200', '2021-09-09 17:42:04', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1144, 'MrBird', '新增用户', 1998, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=16, username=测试, password=f2e6c03c625d9f0d80c24137d4fcddb7, deptId=null, email=null, mobile=13047495791, status=1, createTime=Thu Sep 09 17:42:46 CST 2021, modifyTime=null, lastLoginTime=null, sex=2, avatar=default.jpg, theme=black, isTab=1, description=, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=3, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=1, nickname=null, idcard=null, nationality=null, nationalityName=null, idcardFront=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null, subjectId=15, subjectName=null)\"', '192.168.48.200', '2021-09-09 17:42:46', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1145, 'MrBird', '新增用户', 112, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=17, username=zxh, password=c79e926a22ca8a6971fd399fc2992025, deptId=null, email=null, mobile=13047495791, status=1, createTime=Thu Sep 09 20:30:01 CST 2021, modifyTime=null, lastLoginTime=null, sex=0, avatar=default.jpg, theme=black, isTab=1, description=, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=3, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=1, nickname=null, idcard=370602199409064617, nationality=null, nationalityName=null, idcardFront=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null, subjectId=15, subjectName=null)\"', '192.168.0.101', '2021-09-09 20:30:01', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1146, 'MrBird', '删除用户', 302, 'com.dzwx.system.controller.UserController.deleteUsers()', ' userIds: \"14\"', '192.168.0.101', '2021-09-09 20:31:16', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1147, 'MrBird', '删除用户', 151, 'com.dzwx.system.controller.UserController.deleteUsers()', ' userIds: \"15\"', '192.168.0.101', '2021-09-09 20:49:48', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1148, 'MrBird', '暂停定时任务', 2822, 'com.dzwx.job.controller.JobController.pauseJob()', ' jobIds: \"11,3,2,1\"', '0:0:0:0:0:0:0:1', '2021-09-10 10:19:27', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1149, 'MrBird', '修改Subject', 284, 'com.dzwx.gen.controller.SubjectController.updateSubject()', ' subject: \"Subject(subjectId=30, pSubjectId=null, pSubjectTitle=null, title=油漆工, content=, imgUrl=/subject/2021-09-10/6ebf23b9-4703-434c-a281-bf7e15842f3d.jpg, createTime=null, creator=null)\"', '192.168.48.200', '2021-09-10 15:03:33', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1150, 'MrBird', '修改Subject', 287, 'com.dzwx.gen.controller.SubjectController.updateSubject()', ' subject: \"Subject(subjectId=30, pSubjectId=null, pSubjectTitle=null, title=油漆工, content=, imgUrl=/upload/subject/2021-09-10/95835928-2849-4c86-8f4d-66a46f178519.jpg, createTime=null, creator=null)\"', '192.168.48.200', '2021-09-10 15:05:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1151, 'MrBird', '删除用户', 472, 'com.dzwx.system.controller.UserController.deleteUsers()', ' userIds: \"8\"', '192.168.48.200', '2021-09-10 15:14:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1152, 'MrBird', '删除用户', 445, 'com.dzwx.system.controller.UserController.deleteUsers()', ' userIds: \"17\"', '192.168.48.200', '2021-09-10 15:19:27', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1153, 'MrBird', '删除用户', 409, 'com.dzwx.system.controller.UserController.deleteUsers()', ' userIds: \"16\"', '192.168.48.200', '2021-09-10 15:19:29', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1154, 'MrBird', '新增用户', 275, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=18, username=张三, password=7d3d22d184449ce83f1a7ae5707bf4ed, deptId=null, email=null, mobile=13047495791, status=1, createTime=Fri Sep 10 15:20:57 CST 2021, modifyTime=null, lastLoginTime=null, sex=1, avatar=default.jpg, theme=black, isTab=1, description=test, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=3, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=1, nickname=null, idcard=370602199510030227, nationality=null, nationalityName=null, idcardFront=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null, subjectId=19, subjectName=null)\"', '192.168.48.200', '2021-09-10 15:20:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1155, 'MrBird', '新增用户', 86, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=19, username=里斯, password=1d83e7c33d92691c623fbd49108032f8, deptId=null, email=null, mobile=13047495798, status=1, createTime=Sat Sep 11 10:06:14 CST 2021, modifyTime=null, lastLoginTime=null, sex=1, avatar=default.jpg, theme=black, isTab=1, description=test, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=3, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=1, nickname=null, idcard=370705198703030268, nationality=null, nationalityName=null, idcardFront=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null, subjectId=18, subjectName=null, pSubjectId=null)\"', '192.168.0.102', '2021-09-11 10:06:15', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1156, 'MrBird', '修改用户', 99, 'com.dzwx.system.controller.UserController.updateUser()', ' user: \"User(userId=19, username=null, password=null, deptId=null, email=null, mobile=13047495798, status=1, createTime=null, modifyTime=Sat Sep 11 10:33:05 CST 2021, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=test, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=null, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=null, nickname=lisi, idcard=370705198703030268, nationality=null, nationalityName=null, idcardFront=/upload/certificate/19/78c6bc99-3957-40e5-be16-f524c381627f.jpg, idcardNegative=/upload/certificate/19/3d5e9818-14cf-404e-9909-8527a584e47e.jpg, oneInchPhoto=/upload/certificate/19/3605b942-43cc-4a3f-8a00-19e4c911c994.jpg, certificateNumber=null, faceCollection=null, subjectId=null, subjectName=null, pSubjectId=null)\"', '192.168.0.102', '2021-09-11 10:33:06', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1157, 'MrBird', '修改用户', 82, 'com.dzwx.system.controller.UserController.updateUser()', ' user: \"User(userId=19, username=null, password=null, deptId=null, email=null, mobile=13047495798, status=1, createTime=null, modifyTime=Sat Sep 11 10:39:49 CST 2021, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=test, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=null, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=null, nickname=lisi, idcard=370705198703030268, nationality=50, nationalityName=null, idcardFront=, idcardNegative=, oneInchPhoto=, certificateNumber=23456786543, faceCollection=null, subjectId=null, subjectName=null, pSubjectId=null)\"', '192.168.0.102', '2021-09-11 10:39:50', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1158, 'MrBird', '修改用户', 71, 'com.dzwx.system.controller.UserController.updateUser()', ' user: \"User(userId=19, username=null, password=null, deptId=null, email=null, mobile=13047495798, status=1, createTime=null, modifyTime=Sat Sep 11 10:43:23 CST 2021, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=test, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=null, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=null, nickname=lisi, idcard=370705198703030268, nationality=50, nationalityName=null, idcardFront=/upload/certificate/19/a265737f-f254-43f3-ad76-5c555025c13d.jpg, idcardNegative=/upload/certificate/19/256bb0fc-2403-4b1a-941d-e9c2dbf53329.jpg, oneInchPhoto=/upload/certificate/19/824955c7-3430-46e6-b31a-bb1364ba4762.jpg, certificateNumber=23456786543, faceCollection=null, subjectId=null, subjectName=null, pSubjectId=null)\"', '192.168.0.102', '2021-09-11 10:43:24', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1159, 'MrBird', '修改用户', 68, 'com.dzwx.system.controller.UserController.updateUser()', ' user: \"User(userId=19, username=null, password=null, deptId=null, email=null, mobile=13047495798, status=0, createTime=null, modifyTime=Sat Sep 11 10:44:15 CST 2021, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=test, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=null, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=null, nickname=lisi, idcard=370705198703030268, nationality=50, nationalityName=null, idcardFront=/upload/certificate/19/a265737f-f254-43f3-ad76-5c555025c13d.jpg, idcardNegative=/upload/certificate/19/256bb0fc-2403-4b1a-941d-e9c2dbf53329.jpg, oneInchPhoto=/upload/certificate/19/824955c7-3430-46e6-b31a-bb1364ba4762.jpg, certificateNumber=23456786543, faceCollection=null, subjectId=null, subjectName=null, pSubjectId=null)\"', '192.168.0.102', '2021-09-11 10:44:15', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1160, 'MrBird', '修改用户', 81, 'com.dzwx.system.controller.UserController.updateUser()', ' user: \"User(userId=19, username=null, password=null, deptId=null, email=null, mobile=13047495798, status=0, createTime=null, modifyTime=Sat Sep 11 10:44:35 CST 2021, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=test, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=null, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=null, nickname=, idcard=370705198703030268, nationality=50, nationalityName=null, idcardFront=/upload/certificate/19/a265737f-f254-43f3-ad76-5c555025c13d.jpg, idcardNegative=/upload/certificate/19/256bb0fc-2403-4b1a-941d-e9c2dbf53329.jpg, oneInchPhoto=/upload/certificate/19/824955c7-3430-46e6-b31a-bb1364ba4762.jpg, certificateNumber=23456786543, faceCollection=null, subjectId=null, subjectName=null, pSubjectId=null)\"', '192.168.0.102', '2021-09-11 10:44:35', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1161, 'MrBird', '修改用户', 83, 'com.dzwx.system.controller.UserController.updateUser()', ' user: \"User(userId=18, username=null, password=null, deptId=null, email=null, mobile=13047495791, status=1, createTime=null, modifyTime=Sat Sep 11 10:46:25 CST 2021, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=test, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=null, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=null, nickname=, idcard=370602199510030227, nationality=, nationalityName=null, idcardFront=, idcardNegative=, oneInchPhoto=, certificateNumber=, faceCollection=null, subjectId=null, subjectName=null, pSubjectId=null)\"', '192.168.0.102', '2021-09-11 10:46:26', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1162, 'MrBird', '新增用户', 66, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=20, username=测试用户1, password=6e52b595934be593aad445691687f666, deptId=null, email=null, mobile=13947587321, status=1, createTime=Sat Sep 11 10:48:19 CST 2021, modifyTime=null, lastLoginTime=null, sex=0, avatar=default.jpg, theme=black, isTab=1, description=, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=3, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=1, nickname=null, idcard=370402198702049876, nationality=null, nationalityName=null, idcardFront=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null, subjectId=18, subjectName=null, pSubjectId=null)\"', '192.168.0.102', '2021-09-11 10:48:19', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1163, 'MrBird', '修改用户', 84, 'com.dzwx.system.controller.UserController.updateUser()', ' user: \"User(userId=20, username=null, password=null, deptId=null, email=null, mobile=13947587321, status=1, createTime=null, modifyTime=Sat Sep 11 10:48:29 CST 2021, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=null, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=null, nickname=, idcard=370402198702049876, nationality=, nationalityName=null, idcardFront=, idcardNegative=, oneInchPhoto=, certificateNumber=, faceCollection=null, subjectId=null, subjectName=null, pSubjectId=null)\"', '192.168.0.102', '2021-09-11 10:48:30', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1164, 'MrBird', '修改用户', 67, 'com.dzwx.system.controller.UserController.updateUser()', ' user: \"User(userId=20, username=null, password=null, deptId=null, email=null, mobile=13947587321, status=1, createTime=null, modifyTime=Sat Sep 11 10:50:35 CST 2021, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=null, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=null, nickname=test, idcard=370402198702049876, nationality=0, nationalityName=null, idcardFront=, idcardNegative=, oneInchPhoto=/upload/certificate/20/cb7137cb-02bc-40dd-808e-45557ef91a82.jpg, certificateNumber=98643524565687765, faceCollection=null, subjectId=null, subjectName=null, pSubjectId=null)\"', '192.168.0.102', '2021-09-11 10:50:35', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1165, 'MrBird', '删除用户', 81, 'com.dzwx.system.controller.UserController.deleteUsers()', ' userIds: \"18\"', '192.168.0.102', '2021-09-11 10:50:46', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1166, 'MrBird', '修改用户', 623, 'com.dzwx.system.controller.UserController.updateUser()', ' user: \"User(userId=20, username=null, password=null, deptId=null, email=null, mobile=13947587321, status=1, createTime=null, modifyTime=Tue Sep 14 15:28:25 CST 2021, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=null, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=null, nickname=test, idcard=370402198702049876, nationality=0, nationalityName=null, idcardFront=/upload/certificate/20/f55c8c06-4d5a-4a5e-a0cd-7b9924dcc749.jpg, idcardNegative=/upload/certificate/20/28b21263-e8b6-4b74-9e17-e59c5f35262f.jpg, oneInchPhoto=/upload/certificate/20/cb7137cb-02bc-40dd-808e-45557ef91a82.jpg, certificateNumber=98643524565687765, faceCollection=null, subjectId=null, subjectName=null, pSubjectId=null)\"', '192.168.48.200', '2021-09-14 15:28:26', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1167, 'MrBird', '新增Course', 253, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=12, courseName=测试课程一, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Wed Sep 15 15:02:51 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null)\"', '192.168.48.200', '2021-09-15 15:02:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1168, 'MrBird', '新增Course', 351, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=13, courseName=test, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Wed Sep 15 16:28:27 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null)\"', '192.168.48.200', '2021-09-15 16:28:27', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1169, 'MrBird', '新增定时任务', 1659, 'com.dzwx.job.controller.JobController.addJob()', ' job: \"Job(jobId=12, beanName=DbBackupTask, methodName=数据库备份, params=, cronExpression=0 0 0 * * ? , status=1, remark=数据库备份每天0点执行，保留七天, createTime=Thu Sep 16 16:46:32 CST 2021, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-16 16:46:34', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1170, 'MrBird', '修改定时任务', 2586, 'com.dzwx.job.controller.JobController.updateJob()', ' job: \"Job(jobId=12, beanName=DbBackupTask, methodName=dbBackup, params=, cronExpression=0 0 0 * * ? , status=1, remark=数据库备份每天0点执行，保留七天, createTime=null, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-16 16:47:21', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1171, 'MrBird', '修改定时任务', 5390, 'com.dzwx.job.controller.JobController.updateJob()', ' job: \"Job(jobId=12, beanName=DbBackupTask, methodName=dbBackup, params=, cronExpression=0/1 * * * * ?, status=1, remark=数据库备份每天0点执行，保留七天, createTime=null, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-16 16:57:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1172, 'MrBird', '恢复定时任务', 4663, 'com.dzwx.job.controller.JobController.resumeJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-09-16 16:58:19', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1173, 'MrBird', '恢复定时任务', 2276, 'com.dzwx.job.controller.JobController.resumeJob()', ' jobIds: \"3\"', '0:0:0:0:0:0:0:1', '2021-09-16 17:21:48', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1174, 'MrBird', '暂停定时任务', 1158, 'com.dzwx.job.controller.JobController.pauseJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-09-16 17:22:06', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1175, 'MrBird', '执行定时任务', 3597, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"3\"', '0:0:0:0:0:0:0:1', '2021-09-16 17:22:28', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1176, 'MrBird', '删除定时任务', 3662, 'com.dzwx.job.controller.JobController.deleteJob()', ' jobIds: \"11\"', '0:0:0:0:0:0:0:1', '2021-09-16 17:33:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1177, 'MrBird', '删除定时任务', 1592, 'com.dzwx.job.controller.JobController.deleteJob()', ' jobIds: \"1\"', '0:0:0:0:0:0:0:1', '2021-09-16 17:34:27', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1178, 'MrBird', '修改定时任务', 2973, 'com.dzwx.job.controller.JobController.updateJob()', ' job: \"Job(jobId=12, beanName=dbBackupTask, methodName=dbBackup, params=, cronExpression=0/1 * * * * ?, status=1, remark=数据库备份每天0点执行，保留七天, createTime=null, createTimeFrom=null, createTimeTo=null)\"', '127.0.0.1', '2021-09-16 17:39:55', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1179, 'MrBird', '新增用户', 2047, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=21, username=张三丰, password=5c73790d59291d83e49d92f18caac213, deptId=null, email=null, mobile=13047495791, status=1, createTime=Fri Sep 17 13:42:07 CST 2021, modifyTime=null, lastLoginTime=null, sex=1, avatar=default.jpg, theme=black, isTab=1, description=test, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=3, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=1, nickname=null, idcard=380907199510090389, nationality=null, nationalityName=null, idcardFront=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null, subjectId=21, subjectName=null, pSubjectId=null)\"', '192.168.48.200', '2021-09-17 13:42:09', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1180, 'MrBird', '新增用户', 345, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=null, username=张三丰, password=null, deptId=null, email=null, mobile=13047495791, status=1, createTime=null, modifyTime=null, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=test, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=3, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=1, nickname=null, idcard=380907199510090389, nationality=null, nationalityName=null, idcardFront=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null, subjectId=21, subjectName=null, pSubjectId=null)\"', '192.168.48.200', '2021-09-17 13:42:09', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1181, 'MrBird', '修改用户', 482, 'com.dzwx.system.controller.UserController.updateUser()', ' user: \"User(userId=21, username=null, password=null, deptId=null, email=null, mobile=13047495791, status=1, createTime=null, modifyTime=Fri Sep 17 13:53:52 CST 2021, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=test, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=null, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=null, nickname=, idcard=380907199510090389, nationality=, nationalityName=null, idcardFront=, idcardNegative=, oneInchPhoto=/upload/certificate/21/e8c89818-7fb9-4fea-9896-19fe5dcf25f7.jpg, certificateNumber=, faceCollection=null, subjectId=null, subjectName=null, pSubjectId=null)\"', '192.168.48.200', '2021-09-17 13:53:53', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1182, 'MrBird', '恢复定时任务', 583, 'com.dzwx.job.controller.JobController.resumeJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-09-22 17:24:50', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1183, 'MrBird', '暂停定时任务', 1121, 'com.dzwx.job.controller.JobController.pauseJob()', ' jobIds: \"3\"', '0:0:0:0:0:0:0:1', '2021-09-22 17:25:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1184, 'MrBird', '执行定时任务', 817, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-09-22 17:28:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1185, 'MrBird', '执行定时任务', 850, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-09-22 17:32:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1186, 'MrBird', '执行定时任务', 14281, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-09-22 17:33:34', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1187, 'MrBird', '暂停定时任务', 807, 'com.dzwx.job.controller.JobController.pauseJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-09-22 17:40:31', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1188, 'MrBird', '执行定时任务', 716, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-09-22 17:41:22', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1189, 'MrBird', '执行定时任务', 939, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-09-22 17:46:29', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1190, 'MrBird', '执行定时任务', 926, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-09-22 17:49:09', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1191, 'MrBird', '执行定时任务', 719, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-09-22 17:49:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1192, 'MrBird', '修改定时任务', 2636, 'com.dzwx.job.controller.JobController.updateJob()', ' job: \"Job(jobId=12, beanName=dbBackupTask, methodName=dbBackup, params=, cronExpression=0 0 0 * * ? , status=1, remark=数据库备份每天0点执行，保留七天, createTime=null, createTimeFrom=null, createTimeTo=null)\"', '0:0:0:0:0:0:0:1', '2021-09-22 17:57:12', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1193, 'MrBird', '新增菜单/按钮', 628, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=201, parentId=1, menuName=备份文件, url=, perms=, icon=, type=0, orderNum=6, createTime=Fri Sep 24 14:28:07 CST 2021, modifyTime=null)\"', '0:0:0:0:0:0:0:1', '2021-09-24 14:28:08', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1194, 'MrBird', '修改菜单/按钮', 2333, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=201, parentId=1, menuName=备份文件, url=/system/backups, perms=backups:view, icon=, type=0, orderNum=6, createTime=null, modifyTime=Fri Sep 24 14:46:15 CST 2021)\"', '0:0:0:0:0:0:0:1', '2021-09-24 14:46:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1195, 'MrBird', '新增菜单/按钮', 398, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=202, parentId=201, menuName=备份下载, url=null, perms=, icon=null, type=1, orderNum=null, createTime=Fri Sep 24 14:50:21 CST 2021, modifyTime=null)\"', '0:0:0:0:0:0:0:1', '2021-09-24 14:50:21', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1196, 'MrBird', '修改菜单/按钮', 507, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=202, parentId=201, menuName=备份下载, url=null, perms=backups:download, icon=null, type=1, orderNum=null, createTime=null, modifyTime=Fri Sep 24 14:50:51 CST 2021)\"', '0:0:0:0:0:0:0:1', '2021-09-24 14:50:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1197, 'MrBird', '修改角色', 7460, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Fri Sep 24 14:51:06 CST 2021, menuIds=181,182,183,185,186,187,200,188,189,190,191,192,184,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,180,193,195,196,197,198,199,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164,201,202)\"', '0:0:0:0:0:0:0:1', '2021-09-24 14:51:14', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1198, 'MrBird', '执行定时任务', 4644, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-09-24 17:54:36', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1199, 'MrBird', '新增Course', 72, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=14, courseName=第一节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Sat Sep 25 09:54:34 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null)\"', '192.168.0.102', '2021-09-25 09:54:35', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1200, 'MrBird', '删除Course', 54, 'com.dzwx.gen.controller.CourseController.deleteCourse()', ' courseIds: \"12,13\"', '192.168.0.102', '2021-09-25 09:54:47', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1201, 'MrBird', '新增Course', 55, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=15, courseName=第二节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Sat Sep 25 09:55:00 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null)\"', '192.168.0.102', '2021-09-25 09:55:01', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1202, 'MrBird', '新增Course', 51, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=16, courseName=第三节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Sat Sep 25 09:55:11 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null)\"', '192.168.0.102', '2021-09-25 09:55:11', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1203, 'MrBird', '新增Course', 51, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=17, courseName=第四节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Sat Sep 25 09:55:24 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null)\"', '192.168.0.102', '2021-09-25 09:55:25', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1204, 'MrBird', '新增Course', 52, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=18, courseName=第五节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Sat Sep 25 09:55:35 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null)\"', '192.168.0.102', '2021-09-25 09:55:35', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1205, 'MrBird', '新增Course', 51, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=19, courseName=第六节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Sat Sep 25 09:55:44 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null)\"', '192.168.0.102', '2021-09-25 09:55:44', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1206, 'MrBird', '新增Course', 56, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=20, courseName=第七节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Sat Sep 25 09:55:51 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null)\"', '192.168.0.102', '2021-09-25 09:55:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1207, 'MrBird', '修改Course', 53, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=14, courseName=第一节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=null, modifyTime=null, description=, thumbnail=course\\\\images\\\\2021-09-25\\\\189fdb60-f833-4f23-baf3-091b91f74927.jpg, videourl=course\\\\videos\\\\2021-09-25\\\\e94a4b2f-9e6e-4098-95d4-786d60215eb5.mp4, videosize=1047854, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null)\"', '192.168.0.102', '2021-09-25 13:50:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1208, 'MrBird', '修改Course', 60, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=14, courseName=第一节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=null, modifyTime=null, description=砌筑工入门, thumbnail=course\\\\images\\\\2021-09-25\\\\189fdb60-f833-4f23-baf3-091b91f74927.jpg, videourl=course\\\\videos\\\\2021-09-25\\\\e94a4b2f-9e6e-4098-95d4-786d60215eb5.mp4, videosize=1047854, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null)\"', '192.168.0.102', '2021-09-25 14:21:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1209, 'MrBird', '修改Course', 60, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=14, courseName=第一节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=null, modifyTime=null, description=砌筑工入门, thumbnail=/upload\\\\course\\\\images\\\\2021-09-25\\\\f7cdaf53-232b-4d4c-8088-444853508d00.jpg, videourl=/upload\\\\course\\\\videos\\\\2021-09-25\\\\605b3e9f-edf9-4c77-bd74-53f23eef202c.mp4, videosize=1047854, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null)\"', '192.168.0.102', '2021-09-25 15:19:20', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1210, 'MrBird', '修改Course', 65, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=14, courseName=第一节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=null, modifyTime=null, description=砌筑工入门, thumbnail=\\\\upload\\\\course\\\\images\\\\2021-09-25\\\\69d5440f-d05c-42c0-98ce-5c4992fec0a8.jpg, videourl=\\\\upload\\\\course\\\\videos\\\\2021-09-25\\\\c54653bf-07a7-4253-ac4c-30910b5df8d3.mp4, videosize=1047854, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null)\"', '192.168.0.102', '2021-09-25 15:20:54', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1211, 'MrBird', '修改Course', 46, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=15, courseName=第二节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=null, modifyTime=null, description=, thumbnail=, videourl=\\\\upload\\\\course\\\\videos\\\\2021-09-25\\\\fe9332a8-2beb-44b1-bd01-7c80bc016eaf.mp4, videosize=498575, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null)\"', '192.168.0.102', '2021-09-25 15:30:35', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1212, 'MrBird', '修改Course', 296, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=14, courseName=第一节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=null, modifyTime=null, description=砌筑工入门, thumbnail=\\\\upload\\\\course\\\\images\\\\2021-09-25\\\\69d5440f-d05c-42c0-98ce-5c4992fec0a8.jpg, videourl=\\\\upload\\\\course\\\\videos\\\\2021-09-26\\\\9127f832-b3ae-4036-a3e6-da1f00ee87d8.mp4, videosize=173715807, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.48.200', '2021-09-26 14:42:23', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1213, 'MrBird', '新增用户', 363, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=22, username=牙牙, password=1c21b483ed663393723b48783d4dbe4f, deptId=null, email=null, mobile=13048958987, status=1, createTime=Mon Oct 11 10:17:27 CST 2021, modifyTime=null, lastLoginTime=null, sex=0, avatar=default.jpg, theme=black, isTab=1, description=test, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=3, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=1, nickname=null, idcard=370807199810048978, nationality=null, nationalityName=null, idcardFront=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null, subjectId=20, subjectName=null, pSubjectId=null)\"', '172.6.118.27', '2021-10-11 10:17:27', '美国|0|0|0|0');
INSERT INTO `t_log` VALUES (1214, 'MrBird', '新增菜单/按钮', 268, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=203, parentId=1, menuName=人脸检测记录, url=/face_detect, perms=face_detect:view, icon=, type=0, orderNum=1, createTime=Tue Oct 12 13:46:38 CST 2021, modifyTime=null)\"', '192.168.0.185', '2021-10-12 13:46:39', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1215, 'MrBird', '新增菜单/按钮', 192, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=204, parentId=203, menuName=导出, url=null, perms=face_detect:export, icon=null, type=1, orderNum=null, createTime=Tue Oct 12 13:48:01 CST 2021, modifyTime=null)\"', '192.168.0.185', '2021-10-12 13:48:02', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1216, 'MrBird', '修改角色', 3193, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Tue Oct 12 13:48:13 CST 2021, menuIds=181,182,183,185,186,187,200,188,189,190,191,192,184,2,8,23,10,24,170,136,171,172,127,128,129,131,175,101,102,103,104,105,106,107,108,173,109,110,174,137,138,165,139,166,115,132,133,135,134,126,159,116,117,119,120,121,122,123,118,125,167,168,169,179,178,1,3,11,12,13,160,161,180,203,193,195,196,197,198,199,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164,201,202)\"', '192.168.0.185', '2021-10-12 13:48:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1217, 'MrBird', '删除角色', 367, 'com.dzwx.system.controller.RoleController.deleteRoles()', ' roleIds: \"77\"', '192.168.0.185', '2021-10-13 09:22:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1218, 'MrBird', '删除角色', 460, 'com.dzwx.system.controller.RoleController.deleteRoles()', ' roleIds: \"2\"', '192.168.0.185', '2021-10-13 09:22:26', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1219, 'MrBird', '删除角色', 392, 'com.dzwx.system.controller.RoleController.deleteRoles()', ' roleIds: \"78\"', '192.168.0.185', '2021-10-13 09:22:28', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1220, 'MrBird', '删除角色', 302, 'com.dzwx.system.controller.RoleController.deleteRoles()', ' roleIds: \"79\"', '192.168.0.185', '2021-10-13 09:22:30', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1221, 'MrBird', '删除角色', 301, 'com.dzwx.system.controller.RoleController.deleteRoles()', ' roleIds: \"80\"', '192.168.0.185', '2021-10-13 09:22:33', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1222, 'MrBird', '删除菜单/按钮', 562, 'com.dzwx.system.controller.MenuController.deleteMenus()', ' menuIds: \"137\"', '192.168.0.185', '2021-10-13 09:22:45', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1223, 'MrBird', '删除菜单/按钮', 756, 'com.dzwx.system.controller.MenuController.deleteMenus()', ' menuIds: \"115\"', '192.168.0.185', '2021-10-13 09:23:20', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1224, 'MrBird', '删除菜单/按钮', 268, 'com.dzwx.system.controller.MenuController.deleteMenus()', ' menuIds: \"175\"', '192.168.0.185', '2021-10-13 09:23:32', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1225, 'MrBird', '删除用户', 286, 'com.dzwx.system.controller.UserController.deleteUsers()', ' userIds: \"3,4,5,6,7\"', '192.168.0.185', '2021-10-13 09:24:44', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1226, 'MrBird', '删除用户', 329, 'com.dzwx.system.controller.UserController.deleteUsers()', ' userIds: \"2\"', '192.168.0.185', '2021-10-13 09:24:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1227, 'MrBird', '删除部门', 539, 'com.dzwx.system.controller.DeptController.deleteDept()', ' deptIds: \"1,4,5,6,7,8,9\"', '192.168.0.185', '2021-10-13 09:25:19', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1228, 'MrBird', '修改部门', 217, 'com.dzwx.system.controller.DeptController.updateDept()', ' dept: \"Dept(deptId=10, parentId=null, deptName=系统部, orderNum=1, createTime=null, modifyTime=Wed Oct 13 09:25:25 CST 2021)\"', '192.168.0.185', '2021-10-13 09:25:25', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1229, 'MrBird', '修改用户', 565, 'com.dzwx.system.controller.UserController.updateUser()', ' user: \"User(userId=1, username=null, password=null, deptId=10, email=mrbird@qq.com, mobile=17788888888, status=1, createTime=null, modifyTime=Wed Oct 13 09:29:51 CST 2021, lastLoginTime=null, sex=0, avatar=null, theme=null, isTab=null, description=, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1, roleName=null, deptIds=10, stringPermissions=null, roles=null, isUser=null, nickname=null, idcard=null, nationality=null, nationalityName=null, idcardFront=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null, subjectId=null, subjectName=null, pSubjectId=null)\"', '192.168.0.185', '2021-10-13 09:29:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1230, 'MrBird', '新增用户', 291, 'com.dzwx.system.controller.UserController.addUser()', ' user: \"User(userId=23, username=test, password=50531c15181030c9a07bb7fb8551ac26, deptId=10, email=1307554362@qq.com, mobile=13047495791, status=1, createTime=Wed Oct 13 09:35:05 CST 2021, modifyTime=null, lastLoginTime=null, sex=2, avatar=default.jpg, theme=black, isTab=1, description=tetst, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=0, nickname=null, idcard=null, nationality=null, nationalityName=null, idcardFront=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null, subjectId=null, subjectName=null, pSubjectId=null)\"', '192.168.0.185', '2021-10-13 09:35:06', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1231, 'MrBird', '修改用户', 330, 'com.dzwx.system.controller.UserController.updateUser()', ' user: \"User(userId=23, username=null, password=null, deptId=10, email=1307554362@qq.com, mobile=13047495791, status=1, createTime=null, modifyTime=Wed Oct 13 09:35:44 CST 2021, lastLoginTime=null, sex=2, avatar=null, theme=null, isTab=null, description=tetst111, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=1, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=null, nickname=null, idcard=null, nationality=null, nationalityName=null, idcardFront=null, idcardNegative=null, oneInchPhoto=null, certificateNumber=null, faceCollection=null, subjectId=null, subjectName=null, pSubjectId=null)\"', '192.168.0.185', '2021-10-13 09:35:45', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1232, 'MrBird', '新增Subject', 202, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=31, pSubjectId=0, pSubjectTitle=null, title=test, content=ttt, imgUrl=/upload/subject/2021-10-13/c9f85bf1-61cd-4e03-92f7-97281f0766f0.png, createTime=Wed Oct 13 09:42:04 CST 2021, creator=MrBird)\"', '192.168.0.185', '2021-10-13 09:42:05', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1233, 'MrBird', '修改Subject', 232, 'com.dzwx.gen.controller.SubjectController.updateSubject()', ' subject: \"Subject(subjectId=31, pSubjectId=null, pSubjectTitle=null, title=test, content=ttt111, imgUrl=/upload/subject/2021-10-13/c9f85bf1-61cd-4e03-92f7-97281f0766f0.png, createTime=null, creator=null)\"', '192.168.0.185', '2021-10-13 09:42:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1234, 'MrBird', '删除Subject', 221, 'com.dzwx.gen.controller.SubjectController.deleteSubject()', ' subjectIds: \"31\"', '192.168.0.185', '2021-10-13 09:42:22', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1235, 'MrBird', '导出Subject', 124, 'com.dzwx.gen.controller.SubjectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" subject: \"Subject(subjectId=null, pSubjectId=null, pSubjectTitle=, title=, content=null, imgUrl=null, createTime=null, creator=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@6bc36dd8', '192.168.0.185', '2021-10-13 09:42:25', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1236, 'MrBird', '删除用户', 397, 'com.dzwx.system.controller.UserController.deleteUsers()', ' userIds: \"23\"', '192.168.0.185', '2021-10-13 09:49:56', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1237, 'MrBird', '删除Course', 198, 'com.dzwx.gen.controller.CourseController.deleteCourse()', ' courseIds: \"1,2,3,5,6,7,8,9,10,11\"', '192.168.0.185', '2021-10-13 09:53:41', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1238, 'MrBird', '删除菜单/按钮', 400, 'com.dzwx.system.controller.MenuController.deleteMenus()', ' menuIds: \"184\"', '192.168.0.185', '2021-10-13 09:58:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1239, 'MrBird', '修改角色', 2441, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Oct 13 10:21:39 CST 2021, menuIds=181,182,183,185,186,187,200,188,189,190,191,192,2,8,23,10,24,170,136,171,172,127,128,129,131,101,102,103,104,105,106,107,108,173,109,110,174,1,3,11,12,13,160,161,180,203,204,193,195,196,197,198,199,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164,201,202)\"', '113.124.245.34', '2021-10-13 10:21:42', '中国|华东|山东省|烟台市|电信');
INSERT INTO `t_log` VALUES (1240, 'MrBird', '修改角色', 2571, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Oct 13 10:21:41 CST 2021, menuIds=181,182,183,185,186,187,200,188,189,190,191,192,2,8,23,10,24,170,136,171,172,127,128,129,131,101,102,103,104,105,106,107,108,173,109,110,174,1,3,11,12,13,160,161,180,203,204,193,195,196,197,198,199,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164,201,202)\"', '113.124.245.34', '2021-10-13 10:21:44', '中国|华东|山东省|烟台市|电信');
INSERT INTO `t_log` VALUES (1241, 'MrBird', '导出', 105, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@46dffb2', '192.168.0.185', '2021-10-13 10:25:46', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1242, 'MrBird', '导出', 112, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@13184e7e', '192.168.0.185', '2021-10-13 10:26:45', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1243, 'MrBird', '导出', 141, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@79e1199d', '192.168.0.185', '2021-10-13 10:29:37', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1244, 'MrBird', '导出', 121, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@6a7acc49', '192.168.0.185', '2021-10-13 10:42:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1245, 'MrBird', '修改角色', 3092, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Oct 13 10:55:42 CST 2021, menuIds=181,182,183,185,186,187,200,188,189,190,191,192,2,8,23,10,24,170,136,171,172,127,128,129,131,101,102,103,104,105,106,107,108,173,109,110,174,115,132,133,135,134,126,1,3,11,12,13,160,161,180,203,204,193,195,196,197,198,199,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164,201,202)\"', '192.168.0.185', '2021-10-13 10:55:45', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1246, 'MrBird', '修改角色', 2778, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Oct 13 10:55:45 CST 2021, menuIds=181,182,183,185,186,187,200,188,189,190,191,192,2,8,23,10,24,170,136,171,172,127,128,129,131,101,102,103,104,105,106,107,108,173,109,110,174,115,132,133,135,134,126,1,3,11,12,13,160,161,180,203,204,193,195,196,197,198,199,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164,201,202)\"', '192.168.0.185', '2021-10-13 10:55:48', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1247, 'MrBird', '导出', 16156, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@44ced41', '192.168.0.185', '2021-10-13 11:05:36', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1248, 'MrBird', '导出', 127, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@120987e3', '192.168.0.185', '2021-10-13 11:12:44', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1249, 'MrBird', '导出', 26391, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@653321eb', '192.168.0.185', '2021-10-13 11:14:22', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1250, 'MrBird', '导出', 14101, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@32aa1dba', '192.168.0.185', '2021-10-13 11:17:33', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1251, 'MrBird', '导出', 15372, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@5e85bb4b', '192.168.0.185', '2021-10-13 11:17:52', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1252, 'MrBird', '导出', 15244, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@1851208c', '192.168.0.185', '2021-10-13 11:20:28', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1253, 'MrBird', '修改角色', 7852, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Oct 13 11:33:19 CST 2021, menuIds=181,182,183,185,186,187,200,188,189,190,191,192,2,8,23,10,24,170,136,171,172,127,128,129,131,101,102,103,104,105,106,107,108,173,109,110,174,115,132,133,135,134,126,1,3,11,12,13,160,161,180,203,204,193,195,196,197,198,199,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164,201)\"', '0:0:0:0:0:0:0:1', '2021-10-13 11:33:28', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1254, 'MrBird', '修改角色', 7964, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Wed Oct 13 11:34:17 CST 2021, menuIds=181,182,183,185,186,187,200,188,189,190,191,192,2,8,23,10,24,170,136,171,172,127,128,129,131,101,102,103,104,105,106,107,108,173,109,110,174,115,132,133,135,134,126,1,3,11,12,13,160,161,180,203,204,193,195,196,197,198,199,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164,201,202)\"', '0:0:0:0:0:0:0:1', '2021-10-13 11:34:25', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1255, 'MrBird', '导出', 15340, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@3def043b', '192.168.0.185', '2021-10-13 13:25:58', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1256, 'MrBird', '导出', 5611, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@afa2895', '192.168.0.185', '2021-10-13 13:26:24', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1257, 'MrBird', '导出', 12547, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@4a9cefc8', '192.168.0.185', '2021-10-13 13:29:33', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1258, 'MrBird', '导出', 11876, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@8db5d91', '192.168.0.185', '2021-10-13 13:32:17', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1259, 'MrBird', '导出', 11066, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@413c57f3', '192.168.0.185', '2021-10-13 13:33:46', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1260, 'MrBird', '导出', 19375, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@1d23c3ef', '192.168.0.185', '2021-10-13 13:54:46', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1261, 'MrBird', '导出', 14739, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@dd100fd', '192.168.0.185', '2021-10-13 14:20:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1262, 'MrBird', '导出', 5662, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@42e768b4', '192.168.0.185', '2021-10-13 14:30:26', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1263, 'MrBird', '导出', 21443, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@5febf8e6', '192.168.0.185', '2021-10-13 14:38:50', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1264, 'MrBird', '导出', 27622, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@49def649', '192.168.0.185', '2021-10-13 15:11:20', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1265, 'MrBird', '导出', 3381, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@5f9cf2b2', '113.124.245.34', '2021-10-13 15:20:42', '中国|华东|山东省|烟台市|电信');
INSERT INTO `t_log` VALUES (1266, 'MrBird', '执行定时任务', 4168, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-10-13 17:18:41', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1267, 'MrBird', '执行定时任务', 3302, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-10-13 17:25:43', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1268, 'MrBird', '执行定时任务', 513, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-10-14 09:13:18', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1269, 'MrBird', '执行定时任务', 442, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-10-14 09:15:22', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1270, 'MrBird', '执行定时任务', 25489, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-10-14 09:26:14', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1271, 'MrBird', '执行定时任务', 466, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-10-14 09:26:55', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1272, 'MrBird', '执行定时任务', 605, 'com.dzwx.job.controller.JobController.runJob()', ' jobIds: \"12\"', '0:0:0:0:0:0:0:1', '2021-10-14 09:28:38', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1273, 'MrBird', '导出', 153, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@585a311b', '113.124.245.34', '2021-10-14 16:40:31', '中国|华东|山东省|烟台市|电信');
INSERT INTO `t_log` VALUES (1274, 'MrBird', '导出', 2758, 'com.dzwx.gen.controller.FaceDetectController.export()', ' queryRequest: \"QueryRequest(pageSize=10, pageNum=1, field=createTime, order=null)\" faceDetect: \"FaceDetect(id=null, userId=null, courseId=null, username=, subjectName=null, courseName=, faceDetectUrl=null, time=null, createTime=null, oneInchPhoto=null)\" response: org.apache.shiro.web.servlet.ShiroHttpServletResponse@70b0bffd', '113.124.245.34', '2021-10-14 17:16:42', '中国|华东|山东省|烟台市|电信');
INSERT INTO `t_log` VALUES (1275, 'MrBird', '删除Course', 258, 'com.dzwx.gen.controller.CourseController.deleteCourse()', ' courseIds: \"16,17,18,19,20\"', '192.168.0.185', '2021-10-29 09:25:30', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1276, 'MrBird', '修改Course', 210, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=14, courseName=第一节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=null, modifyTime=null, description=砌筑工入门, thumbnail=\\\\upload\\\\course\\\\images\\\\2021-09-25\\\\69d5440f-d05c-42c0-98ce-5c4992fec0a8.jpg, videourl=\\\\upload\\\\course\\\\videos\\\\2021-10-29\\\\a879de40-b784-4f46-9380-fcfcffcb6b82.mp4, videosize=173715807, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-10-29 09:43:47', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1277, 'MrBird', '修改Course', 219, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=15, courseName=第二节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=null, modifyTime=null, description=, thumbnail=, videourl=\\\\upload\\\\course\\\\videos\\\\2021-09-25\\\\fe9332a8-2beb-44b1-bd01-7c80bc016eaf.mp4, videosize=498575, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-10-29 09:44:07', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1278, 'MrBird', '删除Course', 253, 'com.dzwx.gen.controller.CourseController.deleteCourse()', ' courseIds: \"15\"', '192.168.0.185', '2021-10-29 09:44:11', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1279, 'MrBird', '新增Course', 207, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=21, courseName=test, courseTitle=null, subject=null, subjectName=null, status=1, createTime=Fri Oct 29 09:45:02 CST 2021, modifyTime=null, description=, thumbnail=, videourl=\\\\upload\\\\course\\\\videos\\\\2021-10-29\\\\a381e87f-e522-4ffc-aa95-729784f0e5ef.mp4, videosize=173715807, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-10-29 09:45:03', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1280, 'MrBird', '删除Course', 115105, 'com.dzwx.gen.controller.CourseController.deleteCourse()', ' courseIds: \"21\"', '192.168.0.185', '2021-10-29 09:47:05', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1281, 'MrBird', '新增Course', 191, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=22, courseName=test, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Fri Oct 29 10:12:26 CST 2021, modifyTime=null, description=, thumbnail=, videourl=\\\\upload\\\\course\\\\videos\\\\2021-10-29\\\\dee53df6-86f1-41f5-91fa-3b4fbcbd6863.mp4, videosize=1130018, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-10-29 10:12:27', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1282, 'MrBird', '新增菜单/按钮', 314, 'com.dzwx.system.controller.MenuController.addMenu()', ' menu: \"Menu(menuId=205, parentId=3, menuName=生成证书, url=null, perms=user:generate_certificate, icon=null, type=1, orderNum=null, createTime=Fri Oct 29 15:44:39 CST 2021, modifyTime=null)\"', '192.168.0.185', '2021-10-29 15:44:40', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1283, 'MrBird', '修改菜单/按钮', 253, 'com.dzwx.system.controller.MenuController.updateMenu()', ' menu: \"Menu(menuId=205, parentId=3, menuName=生成证书, url=null, perms=user:generateCertificate, icon=null, type=1, orderNum=null, createTime=null, modifyTime=Fri Oct 29 15:44:50 CST 2021)\"', '192.168.0.185', '2021-10-29 15:44:51', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1284, 'MrBird', '修改角色', 2819, 'com.dzwx.system.controller.RoleController.updateRole()', ' role: \"Role(roleId=1, roleName=系统管理员, remark=系统管理员，拥有所有操作权限 ^_^, createTime=null, modifyTime=Fri Oct 29 15:45:18 CST 2021, menuIds=181,182,183,185,186,187,200,188,189,190,191,192,2,8,23,10,24,170,136,171,172,127,128,129,131,101,102,103,104,105,106,107,108,173,109,110,174,115,132,133,135,134,126,1,3,11,12,13,160,161,180,205,203,204,193,195,196,197,198,199,4,14,15,16,162,5,17,18,19,163,6,20,21,22,164,201,202)\"', '192.168.0.185', '2021-10-29 15:45:21', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1285, 'MrBird', '修改用户', 482, 'com.dzwx.system.controller.UserController.updateUser()', ' user: \"User(userId=21, username=null, password=null, deptId=null, email=null, mobile=13047495791, status=1, createTime=null, modifyTime=Mon Nov 01 13:50:41 CST 2021, lastLoginTime=null, sex=1, avatar=null, theme=null, isTab=null, description=test, deptName=null, createTimeFrom=null, createTimeTo=null, roleId=null, roleName=null, deptIds=null, stringPermissions=null, roles=null, isUser=null, nickname=test, idcard=380907199510090389, nationality=, nationalityName=null, idcardFront=/upload/certificate/21/6bae25dc-7928-400b-af45-e72c7ccc9b12.jpg, idcardNegative=/upload/certificate/21/8206d195-a9c7-48e1-ad1e-375898c4283b.jpg, oneInchPhoto=/upload/certificate/21/e8c89818-7fb9-4fea-9896-19fe5dcf25f7.jpg, certificateNumber=12333333, faceCollection=null, certificateUrl=null, subjectId=15, subjectName=null, pSubjectId=null, playFinishedCourses=null)\"', '192.168.0.185', '2021-11-01 13:50:42', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1286, 'MrBird', '修改Subject', 280, 'com.dzwx.gen.controller.SubjectController.updateSubject()', ' subject: \"Subject(subjectId=30, pSubjectId=null, pSubjectTitle=null, title=油漆工, content=, imgUrl=/upload/subject/2021-09-10/95835928-2849-4c86-8f4d-66a46f178519.jpg, createTime=null, creator=null)\"', '192.168.0.185', '2021-11-01 14:00:07', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1287, 'MrBird', '新增Course', 254, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=23, courseName=第三节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Wed Nov 17 12:43:00 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-11-17 12:43:01', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1288, 'MrBird', '新增Course', 248, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=24, courseName=第四节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Wed Nov 17 12:43:23 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-11-17 12:43:23', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1289, 'MrBird', '新增Course', 260, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=25, courseName=第五节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Wed Nov 17 12:44:13 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-11-17 12:44:14', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1290, 'MrBird', '新增Course', 275, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=26, courseName=第六, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Wed Nov 17 12:44:31 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-11-17 12:44:32', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1291, 'MrBird', '新增Course', 256, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=27, courseName=di7, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Wed Nov 17 12:51:52 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-11-17 12:51:53', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1292, 'MrBird', '新增Course', 305, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=28, courseName=di8, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Wed Nov 17 12:52:03 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-11-17 12:52:03', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1293, 'MrBird', '新增Course', 246, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=29, courseName=di9, courseTitle=null, subject=15, subjectName=null, status=1, createTime=Wed Nov 17 12:52:12 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-11-17 12:52:13', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1294, 'MrBird', '新增Subject', 222, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=31, pSubjectId=0, pSubjectTitle=null, title=测试-公共课, content=, imgUrl=, createTime=Tue Dec 07 13:50:34 CST 2021, creator=MrBird, isPublic=1)\"', '192.168.0.185', '2021-12-07 13:50:34', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1295, 'MrBird', '修改Subject', 346, 'com.dzwx.gen.controller.SubjectController.updateSubject()', ' subject: \"Subject(subjectId=31, pSubjectId=null, pSubjectTitle=null, title=测试-公共课, content=, imgUrl=, createTime=null, creator=null, isPublic=0)\"', '192.168.0.185', '2021-12-07 14:44:43', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1296, 'MrBird', '修改Subject', 239, 'com.dzwx.gen.controller.SubjectController.updateSubject()', ' subject: \"Subject(subjectId=31, pSubjectId=null, pSubjectTitle=null, title=测试-公共课, content=, imgUrl=, createTime=null, creator=null, isPublic=1)\"', '192.168.0.185', '2021-12-07 14:45:03', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1297, 'MrBird', '新增Course', 322, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=30, courseName=1-----, courseTitle=null, subject=31, subjectName=null, status=1, createTime=Wed Dec 08 14:05:13 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-12-08 14:05:14', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1298, 'MrBird', '新增Course', 189, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=31, courseName=2-------, courseTitle=null, subject=31, subjectName=null, status=1, createTime=Wed Dec 08 14:05:25 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-12-08 14:05:26', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1299, 'MrBird', '新增Course', 230, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=32, courseName=3-------, courseTitle=null, subject=31, subjectName=null, status=1, createTime=Wed Dec 08 14:05:33 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-12-08 14:05:33', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1300, 'MrBird', '新增Course', 239, 'com.dzwx.gen.controller.CourseController.addCourse()', ' course: \"Course(courseId=33, courseName=4-------, courseTitle=null, subject=31, subjectName=null, status=1, createTime=Wed Dec 08 14:05:39 CST 2021, modifyTime=null, description=, thumbnail=, videourl=, videosize=null, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-12-08 14:05:40', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1301, 'MrBird', '删除Subject', 386, 'com.dzwx.gen.controller.SubjectController.deleteSubject()', ' subjectIds: \"31\"', '192.168.0.185', '2021-12-09 14:18:05', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1302, 'MrBird', '新增Subject', 277, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=32, pSubjectId=0, pSubjectTitle=null, title=警示教育, content=, imgUrl=, createTime=Thu Dec 09 14:18:13 CST 2021, creator=MrBird, isPublic=1)\"', '192.168.0.185', '2021-12-09 14:18:14', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1303, 'MrBird', '新增Subject', 304, 'com.dzwx.gen.controller.SubjectController.addSubject()', ' subject: \"Subject(subjectId=33, pSubjectId=0, pSubjectTitle=null, title=三类人员继续教育, content=, imgUrl=, createTime=Thu Dec 09 14:18:23 CST 2021, creator=MrBird, isPublic=1)\"', '192.168.0.185', '2021-12-09 14:18:24', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1304, 'MrBird', '修改Course', 443, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=30, courseName=1-----, courseTitle=null, subject=32, subjectName=null, status=1, createTime=null, modifyTime=null, description=, thumbnail=, videourl=, videosize=0, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-12-09 14:19:46', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1305, 'MrBird', '修改Course', 680, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=31, courseName=2-------, courseTitle=null, subject=32, subjectName=null, status=1, createTime=null, modifyTime=null, description=, thumbnail=, videourl=, videosize=0, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-12-09 14:19:53', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1306, 'MrBird', '修改Course', 383, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=31, courseName=2-------, courseTitle=null, subject=32, subjectName=null, status=1, createTime=null, modifyTime=null, description=, thumbnail=, videourl=, videosize=0, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-12-09 14:19:54', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1307, 'MrBird', '修改Course', 640, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=32, courseName=3-------, courseTitle=null, subject=32, subjectName=null, status=1, createTime=null, modifyTime=null, description=, thumbnail=, videourl=, videosize=0, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-12-09 14:20:01', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1308, 'MrBird', '修改Course', 428, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=33, courseName=4-------, courseTitle=null, subject=32, subjectName=null, status=1, createTime=null, modifyTime=null, description=, thumbnail=, videourl=, videosize=0, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-12-09 14:20:09', '内网IP|0|0|内网IP|内网IP');
INSERT INTO `t_log` VALUES (1309, 'MrBird', '修改Course', 280, 'com.dzwx.gen.controller.CourseController.updateCourse()', ' course: \"Course(courseId=22, courseName=第二节, courseTitle=null, subject=15, subjectName=null, status=1, createTime=null, modifyTime=null, description=, thumbnail=, videourl=\\\\upload\\\\course\\\\videos\\\\2021-10-29\\\\dee53df6-86f1-41f5-91fa-3b4fbcbd6863.mp4, videosize=1130018, videoSizeHuman=null, createTimeFrom=null, createTimeTo=null, collectionUser=null, playTime=null)\"', '192.168.0.185', '2021-12-09 14:27:42', '内网IP|0|0|内网IP|内网IP');

-- ----------------------------
-- Table structure for t_login_log
-- ----------------------------
DROP TABLE IF EXISTS `t_login_log`;
CREATE TABLE `t_login_log`  (
  `ID` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `USERNAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `LOGIN_TIME` datetime(0) NOT NULL COMMENT '登录时间',
  `LOCATION` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录地点',
  `IP` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `SYSTEM` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作系统',
  `BROWSER` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '浏览器',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `t_login_log_login_time`(`LOGIN_TIME`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 541 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '登录日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_login_log
-- ----------------------------
INSERT INTO `t_login_log` VALUES (70, 'mrbird', '2021-08-17 13:09:56', '内网IP|0|0|内网IP|内网IP', '192.168.123.145', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (71, 'mrbird', '2021-08-19 10:55:17', '内网IP|0|0|内网IP|内网IP', '192.168.33.179', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (72, 'mrbird', '2021-08-19 11:01:29', '内网IP|0|0|内网IP|内网IP', '192.168.33.179', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (73, 'mrbird', '2021-08-19 14:58:26', '内网IP|0|0|内网IP|内网IP', '10.10.2.215', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (74, 'mrbird', '2021-08-19 16:03:10', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (75, 'mrbird', '2021-08-20 10:59:54', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (76, 'mrbird', '2021-08-20 17:14:32', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (77, 'mrbird', '2021-08-22 16:59:40', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (78, 'mrbird', '2021-08-22 17:06:45', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (79, 'mrbird', '2021-08-24 11:27:48', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (80, 'MrBird', '2021-08-24 16:51:47', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (81, 'MrBird', '2021-08-24 16:59:37', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (82, 'MrBird', '2021-08-25 14:34:55', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (83, 'MrBird', '2021-08-25 14:50:35', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (84, 'MrBird', '2021-08-25 15:06:43', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (85, 'MrBird', '2021-08-25 15:19:35', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (86, 'MrBird', '2021-08-25 16:13:00', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (87, 'MrBird', '2021-08-25 16:15:41', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (88, 'MrBird', '2021-08-25 16:50:56', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (89, 'MrBird', '2021-08-26 13:45:27', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (90, 'MrBird', '2021-08-26 14:13:18', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (91, 'MrBird', '2021-08-26 14:59:26', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (92, 'MrBird', '2021-08-26 15:04:43', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (93, 'MrBird', '2021-08-26 15:59:14', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (94, 'MrBird', '2021-08-27 09:07:57', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (95, 'MrBird', '2021-08-27 09:39:19', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (96, 'mrbird', '2021-08-27 16:23:52', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (97, 'MrBird', '2021-08-31 15:17:58', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (98, 'MrBird', '2021-08-31 15:22:30', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (99, 'MrBird', '2021-08-31 15:26:45', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (100, 'MrBird', '2021-08-31 17:22:00', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (101, 'MrBird', '2021-08-31 17:41:03', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (102, 'MrBird', '2021-08-31 17:46:44', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (103, 'MrBird', '2021-08-31 17:51:42', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (104, 'MrBird', '2021-08-31 17:57:28', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (105, 'mrbird', '2021-09-01 00:49:29', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (106, 'MrBird', '2021-09-01 09:49:31', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (107, 'MrBird', '2021-09-01 10:10:37', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (108, 'MrBird', '2021-09-01 14:33:37', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (109, 'MrBird', '2021-09-01 14:57:03', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (110, 'MrBird', '2021-09-01 15:29:31', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (111, 'MrBird', '2021-09-01 15:52:55', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (112, 'MrBird', '2021-09-01 16:00:21', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (113, 'MrBird', '2021-09-01 16:22:11', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (114, 'MrBird', '2021-09-01 16:25:34', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (115, 'MrBird', '2021-09-01 16:34:04', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (116, 'MrBird', '2021-09-01 16:38:29', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (117, 'MrBird', '2021-09-01 16:48:21', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (118, 'MrBird', '2021-09-01 16:54:34', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (119, 'MrBird', '2021-09-01 17:21:14', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (120, 'MrBird', '2021-09-01 17:37:11', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (121, 'MrBird', '2021-09-01 17:39:32', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (122, 'MrBird', '2021-09-02 15:49:04', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (123, 'MrBird', '2021-09-02 15:53:59', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (124, 'MrBird', '2021-09-02 15:58:32', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (125, 'MrBird', '2021-09-02 16:06:16', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (126, 'MrBird', '2021-09-02 16:29:30', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (127, 'MrBird', '2021-09-06 16:23:24', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (128, 'MrBird', '2021-09-07 16:13:15', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (129, 'MrBird', '2021-09-07 16:21:22', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (130, 'MrBird', '2021-09-07 16:55:50', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (131, 'mrbird', '2021-09-08 11:24:24', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Windows 10', 'Chrome 92');
INSERT INTO `t_login_log` VALUES (132, 'MrBird', '2021-09-08 13:47:11', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (133, 'MrBird', '2021-09-08 13:50:21', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (134, 'Scott', '2021-09-08 13:53:59', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (135, 'MrBird', '2021-09-08 13:54:15', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (136, 'MrBird', '2021-09-08 14:08:51', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (137, 'MrBird', '2021-09-08 14:26:17', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (138, 'MrBird', '2021-09-08 14:38:51', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (139, 'MrBird', '2021-09-08 14:43:15', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (140, 'MrBird', '2021-09-08 14:51:00', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (141, 'MrBird', '2021-09-08 14:55:08', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (142, 'MrBird', '2021-09-08 15:01:32', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (143, 'MrBird', '2021-09-08 15:05:59', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (144, 'MrBird', '2021-09-08 17:52:28', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (145, 'MrBird', '2021-09-08 20:54:24', '内网IP|0|0|内网IP|内网IP', '192.168.0.101', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (146, 'MrBird', '2021-09-09 12:46:16', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (147, 'MrBird', '2021-09-09 13:24:09', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (148, 'MrBird', '2021-09-09 13:31:22', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (149, 'MrBird', '2021-09-09 13:35:37', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (150, 'MrBird', '2021-09-09 13:48:24', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (151, 'MrBird', '2021-09-09 14:20:10', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (152, 'MrBird', '2021-09-09 14:39:22', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (153, 'MrBird', '2021-09-09 17:29:23', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (154, 'MrBird', '2021-09-09 17:37:52', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (155, 'MrBird', '2021-09-09 20:26:12', '内网IP|0|0|内网IP|内网IP', '192.168.0.101', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (156, 'MrBird', '2021-09-09 20:29:40', '内网IP|0|0|内网IP|内网IP', '192.168.0.101', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (157, 'MrBird', '2021-09-09 20:35:15', '内网IP|0|0|内网IP|内网IP', '192.168.0.101', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (158, 'MrBird', '2021-09-10 14:58:02', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (159, 'MrBird', '2021-09-10 15:05:47', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (160, 'MrBird', '2021-09-11 09:05:45', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (161, 'MrBird', '2021-09-11 09:42:06', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (162, 'MrBird', '2021-09-14 15:24:33', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (163, '测试用户1', '2021-09-14 17:27:47', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (164, '测试用户1', '2021-09-14 17:30:30', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (165, '测试用户1', '2021-09-14 17:31:09', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (166, '测试用户1', '2021-09-14 17:34:15', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (167, '测试用户1', '2021-09-14 17:38:02', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (168, '测试用户1', '2021-09-15 13:56:27', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (169, 'MrBird', '2021-09-15 13:57:26', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (170, 'MrBird', '2021-09-15 14:12:34', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (171, 'MrBird', '2021-09-15 14:15:07', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (172, 'MrBird', '2021-09-16 10:26:46', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (173, 'mrbird', '2021-09-16 12:30:09', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (174, '测试用户1', '2021-09-16 13:51:13', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (175, '测试用户1', '2021-09-16 15:45:06', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (176, '测试用户1', '2021-09-16 15:55:19', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (177, '测试用户1', '2021-09-16 17:07:12', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (178, '测试用户1', '2021-09-16 17:07:16', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (179, '测试用户1', '2021-09-16 17:07:16', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (180, '测试用户1', '2021-09-16 17:07:34', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (181, '测试用户1', '2021-09-17 10:03:30', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (182, '测试用户1', '2021-09-17 10:14:35', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (183, '测试用户1', '2021-09-17 10:14:55', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (184, '测试用户1', '2021-09-17 10:44:06', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (185, '测试用户1', '2021-09-17 10:44:07', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (186, '测试用户1', '2021-09-17 10:50:13', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (187, '里斯', '2021-09-17 13:40:52', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (188, 'MrBird', '2021-09-17 13:41:31', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (189, '张三丰', '2021-09-17 13:42:30', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (190, 'MrBird', '2021-09-17 13:53:43', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (191, '张三丰', '2021-09-17 14:15:44', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (192, '张三丰', '2021-09-17 14:15:45', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (193, '张三丰', '2021-09-17 14:33:40', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (194, '张三丰', '2021-09-17 14:34:05', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (195, '张三丰', '2021-09-17 14:35:15', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (196, '张三丰', '2021-09-17 14:36:59', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (197, '张三丰', '2021-09-17 14:40:03', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (198, '张三丰', '2021-09-17 14:40:06', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (199, '张三丰', '2021-09-17 14:40:43', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (200, '张三丰', '2021-09-17 14:40:51', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (201, '张三丰', '2021-09-17 14:41:34', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (202, '张三丰', '2021-09-17 14:42:28', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (203, '张三丰', '2021-09-17 15:01:15', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (204, '张三丰', '2021-09-17 15:09:49', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (205, '张三丰', '2021-09-17 15:09:50', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (206, '张三丰', '2021-09-17 15:10:48', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (207, '张三丰', '2021-09-17 15:14:14', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (208, '张三丰', '2021-09-17 15:19:10', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (209, '张三丰', '2021-09-17 15:22:15', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (210, '张三丰', '2021-09-17 15:27:46', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (211, '张三丰', '2021-09-17 15:28:06', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (212, '张三丰', '2021-09-17 15:30:12', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (213, '张三丰', '2021-09-17 15:31:16', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (214, '张三丰', '2021-09-17 15:33:51', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (215, '张三丰', '2021-09-17 15:34:51', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (216, '张三丰', '2021-09-17 15:35:21', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (217, '张三丰', '2021-09-17 15:37:54', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (218, '张三丰', '2021-09-17 15:48:55', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (219, '张三丰', '2021-09-18 17:26:46', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (220, '张三丰', '2021-09-18 17:32:13', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (221, '张三丰', '2021-09-18 17:32:55', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (222, '张三丰', '2021-09-18 17:36:24', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (223, '张三丰', '2021-09-18 17:36:26', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (224, '张三丰', '2021-09-18 17:36:54', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (225, '张三丰', '2021-09-18 17:41:54', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (226, '张三丰', '2021-09-18 17:47:15', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (227, '张三丰', '2021-09-19 09:29:39', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (228, '张三丰', '2021-09-19 09:47:00', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (229, '张三丰', '2021-09-19 09:50:45', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (230, '张三丰', '2021-09-19 09:51:13', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (231, '测试用户1', '2021-09-19 10:02:03', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (232, '张三丰', '2021-09-19 10:02:24', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (233, '张三丰', '2021-09-19 10:03:32', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (234, '测试用户1', '2021-09-19 10:04:58', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (235, '里斯', '2021-09-19 10:06:02', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (236, '里斯', '2021-09-19 10:09:05', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (237, '张三丰', '2021-09-19 10:20:15', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (238, '张三丰', '2021-09-19 10:20:22', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (239, '张三丰', '2021-09-19 10:27:34', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (240, '张三丰', '2021-09-19 10:27:36', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (241, '张三丰', '2021-09-19 10:27:39', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (242, '张三丰', '2021-09-19 10:58:36', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (243, '张三丰', '2021-09-19 10:58:37', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (244, '张三丰', '2021-09-19 11:02:47', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (245, '张三丰', '2021-09-19 11:18:27', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (246, '张三丰', '2021-09-19 11:19:43', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (247, '张三丰', '2021-09-19 11:20:32', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (248, '张三丰', '2021-09-19 11:23:20', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (249, '张三丰', '2021-09-19 11:28:22', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (250, '张三丰', '2021-09-19 11:28:22', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (251, '张三丰', '2021-09-19 11:28:25', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (252, '张三丰', '2021-09-19 11:36:19', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (253, '张三丰', '2021-09-19 11:36:19', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (254, '张三丰', '2021-09-19 11:36:20', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (255, '张三丰', '2021-09-19 11:42:14', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (256, '张三丰', '2021-09-19 11:42:14', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (257, '张三丰', '2021-09-19 11:42:15', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (258, '张三丰', '2021-09-19 11:42:18', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (259, '张三丰', '2021-09-19 11:45:02', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (260, '张三丰', '2021-09-19 13:26:07', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (261, '张三丰', '2021-09-19 13:29:19', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (262, '张三丰', '2021-09-19 13:32:27', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (263, '张三丰', '2021-09-19 13:33:28', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (264, '张三丰', '2021-09-19 13:36:14', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (265, '张三丰', '2021-09-19 13:39:29', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (266, '张三丰', '2021-09-19 13:40:09', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (267, '张三丰', '2021-09-19 13:40:19', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (268, '张三丰', '2021-09-19 13:40:35', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (269, '张三丰', '2021-09-19 13:42:06', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (270, '张三丰', '2021-09-19 13:43:15', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (271, '张三丰', '2021-09-19 13:44:05', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (272, '张三丰', '2021-09-19 13:45:59', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (273, '里斯', '2021-09-19 13:49:33', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (274, '里斯', '2021-09-19 13:49:46', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (275, '里斯', '2021-09-19 13:50:38', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (276, '里斯', '2021-09-19 13:51:18', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (277, '张三丰', '2021-09-19 13:54:04', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (278, '里斯', '2021-09-19 14:05:33', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (279, '里斯', '2021-09-19 14:05:36', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (280, '里斯', '2021-09-19 14:05:37', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (281, '里斯', '2021-09-19 14:05:57', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (282, '张三丰', '2021-09-19 14:06:31', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (283, '张三丰', '2021-09-19 14:25:40', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (284, '张三丰', '2021-09-19 14:25:43', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (285, '张三丰', '2021-09-19 14:26:10', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (286, '张三丰', '2021-09-19 14:26:40', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (287, '张三丰', '2021-09-19 14:29:07', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (288, '张三丰', '2021-09-19 14:31:42', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (289, '张三丰', '2021-09-19 14:32:47', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (290, '张三丰', '2021-09-19 14:36:27', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (291, '张三丰', '2021-09-19 14:39:18', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (292, '张三丰', '2021-09-19 15:00:28', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (293, '张三丰', '2021-09-19 15:00:28', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (294, '张三丰', '2021-09-19 15:01:21', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (295, '张三丰', '2021-09-19 15:02:06', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (296, '张三丰', '2021-09-19 15:03:11', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (297, '张三丰', '2021-09-19 15:06:18', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', '', '');
INSERT INTO `t_login_log` VALUES (298, '张三丰', '2021-09-22 10:42:27', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (299, '张三丰', '2021-09-22 10:43:47', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (300, '张三丰', '2021-09-22 10:45:25', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (301, '张三丰', '2021-09-22 15:12:41', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (302, '里斯', '2021-09-22 15:13:01', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (303, '里斯', '2021-09-22 15:15:25', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (304, '里斯', '2021-09-22 15:16:31', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (305, '张三丰', '2021-09-22 16:54:56', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (306, '张三丰', '2021-09-22 16:57:49', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (307, '张三丰', '2021-09-22 17:00:46', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (308, '张三丰', '2021-09-22 17:01:53', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (309, '张三丰', '2021-09-22 17:02:05', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (310, '张三丰', '2021-09-22 17:05:03', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (311, '张三丰', '2021-09-22 17:28:50', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (312, '张三丰', '2021-09-22 17:29:27', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (313, '张三丰', '2021-09-22 17:30:04', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (314, '张三丰', '2021-09-23 21:03:19', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (315, '张三丰', '2021-09-23 21:11:41', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (316, '张三丰', '2021-09-23 21:11:41', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (317, '张三丰', '2021-09-23 21:11:44', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (318, '张三丰', '2021-09-23 21:11:44', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (319, '张三丰', '2021-09-23 21:12:03', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (320, '张三丰', '2021-09-23 21:19:58', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (321, '张三丰', '2021-09-23 21:24:10', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (322, '张三丰', '2021-09-23 21:25:05', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (323, '张三丰', '2021-09-23 21:30:22', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (324, '张三丰', '2021-09-23 21:31:09', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (325, '张三丰', '2021-09-23 21:32:27', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (326, '里斯', '2021-09-23 21:33:50', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (327, '里斯', '2021-09-23 21:34:16', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (328, '张三丰', '2021-09-23 21:35:47', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (329, '张三丰', '2021-09-23 21:36:32', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (330, '张三丰', '2021-09-23 21:38:08', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (331, '张三丰', '2021-09-23 21:40:10', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (332, '张三丰', '2021-09-23 21:40:49', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (333, '张三丰', '2021-09-23 21:41:39', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (334, '张三丰', '2021-09-23 21:45:37', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (335, '张三丰', '2021-09-23 21:46:26', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (336, '张三丰', '2021-09-23 21:46:45', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (337, '张三丰', '2021-09-23 21:47:54', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (338, '张三丰', '2021-09-23 21:50:06', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (339, '张三丰', '2021-09-23 21:50:31', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (340, '张三丰', '2021-09-23 21:52:22', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (341, '张三丰', '2021-09-23 21:54:39', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (342, '张三丰', '2021-09-23 21:55:06', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (343, '张三丰', '2021-09-23 21:56:17', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (344, '张三丰', '2021-09-23 21:56:52', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (345, '张三丰', '2021-09-23 22:02:13', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (346, '张三丰', '2021-09-23 22:03:12', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (347, '张三丰', '2021-09-23 22:22:00', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (348, '张三丰', '2021-09-23 22:22:04', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (349, '张三丰', '2021-09-23 22:22:05', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (350, '张三丰', '2021-09-23 22:26:00', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (351, '张三丰', '2021-09-23 22:26:28', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (352, '张三丰', '2021-09-23 22:27:40', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (353, '张三丰', '2021-09-23 22:29:08', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (354, '张三丰', '2021-09-23 22:30:23', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (355, '张三丰', '2021-09-23 22:35:43', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (356, '张三丰', '2021-09-23 22:35:44', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (357, '张三丰', '2021-09-23 22:39:30', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (358, '张三丰', '2021-09-23 22:40:35', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (359, '张三丰', '2021-09-23 22:41:51', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (360, '张三丰', '2021-09-23 22:42:05', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (361, '张三丰', '2021-09-23 22:46:53', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (362, '张三丰', '2021-09-23 22:46:53', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (363, '张三丰', '2021-09-23 22:59:27', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (364, '张三丰', '2021-09-23 22:59:29', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (365, '张三丰', '2021-09-23 23:01:28', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (366, '张三丰', '2021-09-23 23:03:04', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (367, '张三丰', '2021-09-23 23:05:32', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (368, '张三丰', '2021-09-23 23:05:47', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (369, '张三丰', '2021-09-23 23:06:14', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (370, '张三丰', '2021-09-23 23:06:54', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (371, 'mrbird', '2021-09-24 14:27:10', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (372, 'mrbird', '2021-09-24 15:07:53', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (373, '张三丰', '2021-09-24 17:03:08', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (374, '张三丰', '2021-09-24 17:26:58', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (375, '张三丰', '2021-09-24 17:26:59', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (376, '张三丰', '2021-09-24 17:27:00', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (377, '张三丰', '2021-09-24 17:29:11', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (378, '张三丰', '2021-09-24 17:29:40', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (379, '张三丰', '2021-09-25 08:49:57', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (380, '张三丰', '2021-09-25 08:50:00', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (381, '张三丰', '2021-09-25 09:08:54', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (382, '张三丰', '2021-09-25 09:10:10', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (383, '张三丰', '2021-09-25 09:11:00', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (384, '张三丰', '2021-09-25 09:12:56', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (385, '张三丰', '2021-09-25 09:14:21', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (386, '张三丰', '2021-09-25 09:15:01', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (387, '张三丰', '2021-09-25 09:18:19', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (388, '张三丰', '2021-09-25 09:38:55', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (389, '张三丰', '2021-09-25 09:39:05', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (390, '张三丰', '2021-09-25 09:40:20', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (391, '张三丰', '2021-09-25 09:40:25', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (392, '张三丰', '2021-09-25 09:40:54', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (393, '张三丰', '2021-09-25 09:52:41', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (394, 'MrBird', '2021-09-25 09:54:06', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (395, '张三丰', '2021-09-25 09:56:43', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (396, '张三丰', '2021-09-25 09:57:16', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (397, '张三丰', '2021-09-25 09:58:39', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (398, '张三丰', '2021-09-25 10:00:49', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (399, '张三丰', '2021-09-25 10:01:41', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (400, '张三丰', '2021-09-25 10:02:15', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (401, '张三丰', '2021-09-25 10:03:48', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (402, '张三丰', '2021-09-25 10:04:51', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (403, '张三丰', '2021-09-25 10:05:59', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (404, '张三丰', '2021-09-25 10:19:00', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (405, '张三丰', '2021-09-25 10:19:01', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (406, '张三丰', '2021-09-25 11:07:15', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (407, '张三丰', '2021-09-25 11:08:22', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (408, '张三丰', '2021-09-25 11:10:58', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (409, '张三丰', '2021-09-25 11:24:07', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (410, '张三丰', '2021-09-25 14:59:15', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (411, '张三丰', '2021-09-25 14:59:57', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (412, '张三丰', '2021-09-25 15:00:55', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (413, '张三丰', '2021-09-25 15:03:58', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (414, '张三丰', '2021-09-25 15:07:49', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (415, '张三丰', '2021-09-25 15:08:43', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (416, '张三丰', '2021-09-25 15:09:44', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (417, 'MrBird', '2021-09-25 15:18:39', '内网IP|0|0|内网IP|内网IP', '192.168.0.102', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (418, '张三丰', '2021-09-25 15:25:47', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (419, '张三丰', '2021-09-25 15:28:22', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (420, '张三丰', '2021-09-25 15:29:17', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (421, '张三丰', '2021-09-25 15:32:50', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (422, '张三丰', '2021-09-25 15:46:58', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (423, '张三丰', '2021-09-25 15:47:26', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (424, '张三丰', '2021-09-25 15:48:29', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (425, '张三丰', '2021-09-25 15:48:42', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (426, '张三丰', '2021-09-25 15:49:44', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (427, '张三丰', '2021-09-25 15:53:08', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (428, '张三丰', '2021-09-25 15:56:30', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (429, '张三丰', '2021-09-25 15:57:25', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (430, '张三丰', '2021-09-25 16:19:49', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (431, '张三丰', '2021-09-25 16:20:41', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (432, '张三丰', '2021-09-25 16:20:54', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (433, '张三丰', '2021-09-25 16:22:30', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (434, '张三丰', '2021-09-25 16:24:12', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (435, '张三丰', '2021-09-25 16:25:01', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (436, '张三丰', '2021-09-25 16:26:38', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (437, '张三丰', '2021-09-25 16:30:02', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (438, '张三丰', '2021-09-25 16:31:16', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (439, '张三丰', '2021-09-25 16:32:39', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (440, '张三丰', '2021-09-25 16:33:12', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (441, '张三丰', '2021-09-25 16:33:51', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (442, '张三丰', '2021-09-25 16:34:28', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (443, '张三丰', '2021-09-25 16:35:14', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (444, '张三丰', '2021-09-25 16:35:37', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (445, '张三丰', '2021-09-25 16:36:05', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (446, '张三丰', '2021-09-25 16:36:41', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (447, '张三丰', '2021-09-25 16:37:16', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (448, '张三丰', '2021-09-25 16:38:34', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (449, '张三丰', '2021-09-25 16:42:32', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (450, '张三丰', '2021-09-26 10:07:46', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (451, '张三丰', '2021-09-26 10:33:59', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (452, '张三丰', '2021-09-26 14:36:11', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (453, 'MrBird', '2021-09-26 14:41:35', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', 'Windows 10', 'Chrome 93');
INSERT INTO `t_login_log` VALUES (454, '张三丰', '2021-09-26 15:20:55', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (455, '张三丰', '2021-09-26 15:32:31', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (456, '张三丰', '2021-09-26 15:33:31', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (457, '张三丰', '2021-09-26 15:36:44', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (458, '张三丰', '2021-09-26 15:47:35', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (459, '张三丰', '2021-09-26 15:49:35', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (460, '张三丰', '2021-09-26 15:49:39', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (461, '张三丰', '2021-09-26 15:50:59', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (462, '张三丰', '2021-09-26 16:00:37', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (463, '张三丰', '2021-09-26 16:22:03', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (464, '张三丰', '2021-09-26 16:29:43', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (465, '张三丰', '2021-09-26 17:06:53', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (466, '张三丰', '2021-09-26 17:09:37', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (467, '张三丰', '2021-09-26 17:10:08', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (468, '张三丰', '2021-09-26 17:10:53', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (469, '张三丰', '2021-09-26 17:13:06', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (470, '张三丰', '2021-09-26 17:23:50', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (471, '张三丰', '2021-09-26 17:24:47', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (472, '张三丰', '2021-09-26 17:25:12', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (473, '张三丰', '2021-09-26 17:42:15', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (474, '张三丰', '2021-09-26 17:42:42', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (475, '张三丰', '2021-09-26 17:43:12', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (476, '张三丰', '2021-09-26 17:44:44', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (477, '张三丰', '2021-09-26 17:47:08', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (478, '张三丰', '2021-09-26 17:47:23', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (479, '张三丰', '2021-09-26 17:47:42', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (480, '张三丰', '2021-09-26 17:49:20', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (481, '张三丰', '2021-09-26 17:50:24', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (482, '张三丰', '2021-09-26 17:52:02', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (483, '张三丰', '2021-09-27 13:53:09', '内网IP|0|0|内网IP|内网IP', '192.168.48.200', '', '');
INSERT INTO `t_login_log` VALUES (484, 'MrBird', '2021-10-08 09:04:01', '美国|0|0|0|0', '172.6.118.27', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (485, '张三丰', '2021-10-08 15:14:29', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (486, 'MrBird', '2021-10-11 10:14:02', '美国|0|0|0|0', '172.6.118.27', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (487, 'MrBird', '2021-10-11 10:16:50', '美国|0|0|0|0', '172.6.118.27', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (488, '牙牙', '2021-10-11 10:23:54', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (489, '牙牙', '2021-10-11 10:25:33', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (490, '张三丰', '2021-10-11 10:26:06', '美国|0|0|0|0', '172.6.118.27', '', '');
INSERT INTO `t_login_log` VALUES (491, '张三丰', '2021-10-11 16:43:32', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (492, '张三丰', '2021-10-11 16:46:07', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (493, 'MrBird', '2021-10-11 17:00:33', '中国|华东|山东省|烟台市|电信', '113.124.245.34', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (494, 'MrBird', '2021-10-11 17:31:11', '美国|0|0|0|0', '172.6.118.27', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (495, 'MrBird', '2021-10-12 09:16:18', '中国|华东|山东省|烟台市|电信', '113.124.245.34', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (496, '牙牙', '2021-10-12 09:56:42', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (497, 'MrBird', '2021-10-12 10:30:37', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (498, 'MrBird', '2021-10-12 13:44:35', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (499, 'MrBird', '2021-10-12 16:46:11', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (500, 'MrBird', '2021-10-13 08:44:36', '美国|0|0|0|0', '172.6.118.27', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (501, 'MrBird', '2021-10-13 09:17:08', '中国|华东|山东省|烟台市|电信', '113.124.245.34', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (502, 'MrBird', '2021-10-13 09:20:55', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (503, '张三丰', '2021-10-13 09:59:50', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (504, 'MrBird', '2021-10-13 10:01:46', '中国|华东|山东省|烟台市|电信', '113.124.245.34', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (505, 'mrbird', '2021-10-13 10:46:18', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (506, 'mrbird', '2021-10-13 10:56:51', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (507, 'mrbird', '2021-10-13 11:33:45', '内网IP|0|0|内网IP|内网IP', '127.0.0.1', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (508, 'MrBird', '2021-10-13 13:12:08', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (509, 'MrBird', '2021-10-13 13:33:23', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (510, '牙牙', '2021-10-13 15:24:26', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (511, '牙牙', '2021-10-13 15:26:30', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (512, '牙牙', '2021-10-13 15:26:47', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (513, '牙牙', '2021-10-13 15:26:54', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (514, '朱晓辉', '2021-10-13 16:57:27', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (515, '张三丰', '2021-10-14 09:20:56', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (516, '张三丰', '2021-10-14 09:26:55', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (517, '张三丰', '2021-10-14 09:54:31', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (518, '张三丰', '2021-10-14 09:54:47', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (519, '张三丰', '2021-10-14 10:13:30', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (520, '张三丰', '2021-10-14 13:59:19', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (521, '张三丰', '2021-10-14 16:09:39', '中国|华东|山东省|烟台市|电信', '113.124.245.34', '', '');
INSERT INTO `t_login_log` VALUES (522, '张三丰', '2021-10-14 16:37:16', '中国|华东|山东省|济南市|联通', '112.224.65.201', '', '');
INSERT INTO `t_login_log` VALUES (523, '张三丰', '2021-10-14 16:37:40', '中国|华东|山东省|济南市|联通', '112.224.65.201', '', '');
INSERT INTO `t_login_log` VALUES (524, 'MrBird', '2021-10-21 09:00:26', '中国|华东|山东省|青岛市|电信', '140.75.133.151', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (525, 'MrBird', '2021-10-21 14:41:57', '中国|华东|山东省|青岛市|电信', '140.75.133.151', 'Windows 10', 'Chrome 94');
INSERT INTO `t_login_log` VALUES (526, 'MrBird', '2021-10-29 09:19:08', '中国|华东|山东省|青岛市|电信', '140.75.133.151', 'Windows 10', 'Chrome 95');
INSERT INTO `t_login_log` VALUES (527, 'MrBird', '2021-10-29 09:21:00', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 95');
INSERT INTO `t_login_log` VALUES (528, '张三丰', '2021-10-29 10:08:25', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', '', '');
INSERT INTO `t_login_log` VALUES (529, '张三丰', '2021-10-29 10:08:29', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', '', '');
INSERT INTO `t_login_log` VALUES (530, 'MrBird', '2021-11-01 13:48:13', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 95');
INSERT INTO `t_login_log` VALUES (531, 'MrBird', '2021-11-01 13:56:10', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 95');
INSERT INTO `t_login_log` VALUES (532, 'MrBird', '2021-11-01 14:10:08', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 95');
INSERT INTO `t_login_log` VALUES (533, 'MrBird', '2021-11-01 14:14:41', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 95');
INSERT INTO `t_login_log` VALUES (534, 'MrBird', '2021-11-04 13:03:50', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 95');
INSERT INTO `t_login_log` VALUES (535, 'MrBird', '2021-11-17 12:39:49', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 95');
INSERT INTO `t_login_log` VALUES (536, '张三丰', '2021-11-17 12:42:07', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', '', '');
INSERT INTO `t_login_log` VALUES (537, 'MrBird', '2021-12-07 08:55:13', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 96');
INSERT INTO `t_login_log` VALUES (538, 'MrBird', '2021-12-08 13:14:03', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 96');
INSERT INTO `t_login_log` VALUES (539, '张三丰', '2021-12-08 13:32:51', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', '', '');
INSERT INTO `t_login_log` VALUES (540, 'MrBird', '2021-12-09 09:19:52', '内网IP|0|0|内网IP|内网IP', '192.168.0.185', 'Windows 10', 'Chrome 96');

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu`  (
  `MENU_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单/按钮ID',
  `PARENT_ID` bigint(20) NOT NULL COMMENT '上级菜单ID',
  `MENU_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单/按钮名称',
  `URL` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单URL',
  `PERMS` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '权限标识',
  `ICON` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `TYPE` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型 0菜单 1按钮',
  `ORDER_NUM` bigint(20) NULL DEFAULT NULL COMMENT '排序',
  `CREATE_TIME` datetime(0) NOT NULL COMMENT '创建时间',
  `MODIFY_TIME` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`MENU_ID`) USING BTREE,
  INDEX `t_menu_parent_id`(`PARENT_ID`) USING BTREE,
  INDEX `t_menu_menu_id`(`MENU_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 206 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES (1, 0, '系统管理', '', '', 'layui-icon-setting', '0', 6, '2017-12-27 16:39:07', '2021-08-20 16:28:13');
INSERT INTO `t_menu` VALUES (2, 0, '系统监控', '', '', 'layui-icon-alert', '0', 2, '2017-12-27 16:45:51', '2019-06-13 11:20:40');
INSERT INTO `t_menu` VALUES (3, 1, '用户管理', '/system/user', 'user:view', '', '0', 1, '2017-12-27 16:47:13', '2021-09-08 13:52:08');
INSERT INTO `t_menu` VALUES (4, 1, '角色管理', '/system/role', 'role:view', '', '0', 3, '2017-12-27 16:48:09', '2021-09-01 00:49:49');
INSERT INTO `t_menu` VALUES (5, 1, '菜单管理', '/system/menu', 'menu:view', '', '0', 4, '2017-12-27 16:48:57', '2021-09-01 00:49:56');
INSERT INTO `t_menu` VALUES (6, 1, '部门管理', '/system/dept', 'dept:view', '', '0', 5, '2017-12-27 16:57:33', '2021-09-01 00:50:00');
INSERT INTO `t_menu` VALUES (8, 2, '在线用户', '/monitor/online', 'online:view', '', '0', 1, '2017-12-27 16:59:33', '2019-06-13 14:30:31');
INSERT INTO `t_menu` VALUES (10, 2, '系统日志', '/monitor/log', 'log:view', '', '0', 2, '2017-12-27 17:00:50', '2019-06-13 14:30:37');
INSERT INTO `t_menu` VALUES (11, 3, '新增用户', NULL, 'user:add', NULL, '1', NULL, '2017-12-27 17:02:58', NULL);
INSERT INTO `t_menu` VALUES (12, 3, '修改用户', NULL, 'user:update', NULL, '1', NULL, '2017-12-27 17:04:07', NULL);
INSERT INTO `t_menu` VALUES (13, 3, '删除用户', NULL, 'user:delete', NULL, '1', NULL, '2017-12-27 17:04:58', NULL);
INSERT INTO `t_menu` VALUES (14, 4, '新增角色', NULL, 'role:add', NULL, '1', NULL, '2017-12-27 17:06:38', NULL);
INSERT INTO `t_menu` VALUES (15, 4, '修改角色', NULL, 'role:update', NULL, '1', NULL, '2017-12-27 17:06:38', NULL);
INSERT INTO `t_menu` VALUES (16, 4, '删除角色', NULL, 'role:delete', NULL, '1', NULL, '2017-12-27 17:06:38', NULL);
INSERT INTO `t_menu` VALUES (17, 5, '新增菜单', NULL, 'menu:add', NULL, '1', NULL, '2017-12-27 17:08:02', NULL);
INSERT INTO `t_menu` VALUES (18, 5, '修改菜单', NULL, 'menu:update', NULL, '1', NULL, '2017-12-27 17:08:02', NULL);
INSERT INTO `t_menu` VALUES (19, 5, '删除菜单', NULL, 'menu:delete', NULL, '1', NULL, '2017-12-27 17:08:02', NULL);
INSERT INTO `t_menu` VALUES (20, 6, '新增部门', NULL, 'dept:add', NULL, '1', NULL, '2017-12-27 17:09:24', NULL);
INSERT INTO `t_menu` VALUES (21, 6, '修改部门', NULL, 'dept:update', NULL, '1', NULL, '2017-12-27 17:09:24', NULL);
INSERT INTO `t_menu` VALUES (22, 6, '删除部门', NULL, 'dept:delete', NULL, '1', NULL, '2017-12-27 17:09:24', NULL);
INSERT INTO `t_menu` VALUES (23, 8, '踢出用户', NULL, 'user:kickout', NULL, '1', NULL, '2017-12-27 17:11:13', NULL);
INSERT INTO `t_menu` VALUES (24, 10, '删除日志', NULL, 'log:delete', NULL, '1', NULL, '2017-12-27 17:11:45', '2019-06-06 05:56:40');
INSERT INTO `t_menu` VALUES (101, 0, '任务调度', NULL, NULL, 'layui-icon-time-circle', '0', 3, '2018-02-24 15:52:57', NULL);
INSERT INTO `t_menu` VALUES (102, 101, '定时任务', '/job/job', 'job:view', '', '0', 1, '2018-02-24 15:53:53', '2018-04-25 09:05:12');
INSERT INTO `t_menu` VALUES (103, 102, '新增任务', NULL, 'job:add', NULL, '1', NULL, '2018-02-24 15:55:10', NULL);
INSERT INTO `t_menu` VALUES (104, 102, '修改任务', NULL, 'job:update', NULL, '1', NULL, '2018-02-24 15:55:53', NULL);
INSERT INTO `t_menu` VALUES (105, 102, '删除任务', NULL, 'job:delete', NULL, '1', NULL, '2018-02-24 15:56:18', NULL);
INSERT INTO `t_menu` VALUES (106, 102, '暂停任务', NULL, 'job:pause', NULL, '1', NULL, '2018-02-24 15:57:08', NULL);
INSERT INTO `t_menu` VALUES (107, 102, '恢复任务', NULL, 'job:resume', NULL, '1', NULL, '2018-02-24 15:58:21', NULL);
INSERT INTO `t_menu` VALUES (108, 102, '立即执行任务', NULL, 'job:run', NULL, '1', NULL, '2018-02-24 15:59:45', NULL);
INSERT INTO `t_menu` VALUES (109, 101, '调度日志', '/job/log', 'job:log:view', '', '0', 2, '2018-02-24 16:00:45', '2019-06-09 02:48:19');
INSERT INTO `t_menu` VALUES (110, 109, '删除日志', NULL, 'job:log:delete', NULL, '1', NULL, '2018-02-24 16:01:21', NULL);
INSERT INTO `t_menu` VALUES (115, 0, '其他模块', NULL, NULL, 'layui-icon-gift', '0', 5, '2019-05-27 10:18:07', NULL);
INSERT INTO `t_menu` VALUES (116, 115, 'Apex图表', '', '', NULL, '0', 2, '2019-05-27 10:21:35', NULL);
INSERT INTO `t_menu` VALUES (117, 116, '线性图表', '/others/apex/line', 'apex:line:view', NULL, '0', 1, '2019-05-27 10:24:49', NULL);
INSERT INTO `t_menu` VALUES (118, 115, '高德地图', '/others/map', 'map:view', '', '0', 3, '2019-05-27 17:13:12', '2019-06-12 15:33:00');
INSERT INTO `t_menu` VALUES (119, 116, '面积图表', '/others/apex/area', 'apex:area:view', NULL, '0', 2, '2019-05-27 18:49:22', NULL);
INSERT INTO `t_menu` VALUES (120, 116, '柱形图表', '/others/apex/column', 'apex:column:view', NULL, '0', 3, '2019-05-27 18:51:33', NULL);
INSERT INTO `t_menu` VALUES (121, 116, '雷达图表', '/others/apex/radar', 'apex:radar:view', NULL, '0', 4, '2019-05-27 18:56:05', NULL);
INSERT INTO `t_menu` VALUES (122, 116, '条形图表', '/others/apex/bar', 'apex:bar:view', NULL, '0', 5, '2019-05-27 18:57:02', NULL);
INSERT INTO `t_menu` VALUES (123, 116, '混合图表', '/others/apex/mix', 'apex:mix:view', '', '0', 6, '2019-05-27 18:58:04', '2019-06-06 02:55:23');
INSERT INTO `t_menu` VALUES (125, 115, '导入导出', '/others/eximport', 'others:eximport:view', '', '0', 4, '2019-05-27 19:01:57', '2019-06-13 01:20:14');
INSERT INTO `t_menu` VALUES (126, 132, '系统图标', '/others/dzwx/icon', 'dzwx:icons:view', '', '0', 4, '2019-05-27 19:03:18', '2019-06-06 03:05:26');
INSERT INTO `t_menu` VALUES (127, 2, '请求追踪', '/monitor/httptrace', 'httptrace:view', '', '0', 6, '2019-05-27 19:06:38', '2019-06-13 14:36:43');
INSERT INTO `t_menu` VALUES (128, 2, '系统信息', NULL, NULL, NULL, '0', 7, '2019-05-27 19:08:23', NULL);
INSERT INTO `t_menu` VALUES (129, 128, 'JVM信息', '/monitor/jvm', 'jvm:view', '', '0', 1, '2019-05-27 19:08:50', '2019-06-13 14:36:51');
INSERT INTO `t_menu` VALUES (131, 128, '服务器信息', '/monitor/server', 'server:view', '', '0', 3, '2019-05-27 19:10:07', '2019-06-13 14:37:04');
INSERT INTO `t_menu` VALUES (132, 115, 'DZWX组件', '', '', NULL, '0', 1, '2019-05-27 19:13:54', NULL);
INSERT INTO `t_menu` VALUES (133, 132, '表单组件', '/others/dzwx/form', 'dzwx:form:view', NULL, '0', 1, '2019-05-27 19:14:45', NULL);
INSERT INTO `t_menu` VALUES (134, 132, 'DZWX工具', '/others/dzwx/tools', 'dzwx:tools:view', '', '0', 3, '2019-05-29 10:11:22', '2019-06-12 13:21:27');
INSERT INTO `t_menu` VALUES (135, 132, '表单组合', '/others/dzwx/form/group', 'dzwx:formgroup:view', NULL, '0', 2, '2019-05-29 10:16:19', NULL);
INSERT INTO `t_menu` VALUES (136, 2, '登录日志', '/monitor/loginlog', 'loginlog:view', '', '0', 3, '2019-05-29 15:56:15', '2019-06-13 14:35:56');
INSERT INTO `t_menu` VALUES (160, 3, '密码重置', NULL, 'user:password:reset', NULL, '1', NULL, '2019-06-13 08:40:13', NULL);
INSERT INTO `t_menu` VALUES (161, 3, '导出Excel', NULL, 'user:export', NULL, '1', NULL, '2019-06-13 08:40:34', NULL);
INSERT INTO `t_menu` VALUES (162, 4, '导出Excel', NULL, 'role:export', NULL, '1', NULL, '2019-06-13 14:29:00', '2019-06-13 14:29:11');
INSERT INTO `t_menu` VALUES (163, 5, '导出Excel', NULL, 'menu:export', NULL, '1', NULL, '2019-06-13 14:29:32', NULL);
INSERT INTO `t_menu` VALUES (164, 6, '导出Excel', NULL, 'dept:export', NULL, '1', NULL, '2019-06-13 14:29:59', NULL);
INSERT INTO `t_menu` VALUES (170, 10, '导出Excel', NULL, 'log:export', NULL, '1', NULL, '2019-06-13 14:34:55', NULL);
INSERT INTO `t_menu` VALUES (171, 136, '删除日志', NULL, 'loginlog:delete', NULL, '1', NULL, '2019-06-13 14:35:27', '2019-06-13 14:36:08');
INSERT INTO `t_menu` VALUES (172, 136, '导出Excel', NULL, 'loginlog:export', NULL, '1', NULL, '2019-06-13 14:36:26', NULL);
INSERT INTO `t_menu` VALUES (173, 102, '导出Excel', NULL, 'job:export', NULL, '1', NULL, '2019-06-13 14:37:25', NULL);
INSERT INTO `t_menu` VALUES (174, 109, '导出Excel', NULL, 'job:log:export', NULL, '1', NULL, '2019-06-13 14:37:46', '2019-06-13 14:38:02');
INSERT INTO `t_menu` VALUES (180, 3, '导入', NULL, 'eximport:importVedio', NULL, '1', NULL, '2021-08-18 00:09:51', NULL);
INSERT INTO `t_menu` VALUES (181, 0, '课程管理', '', '', 'layui-icon-appstore', '0', 1, '2021-08-20 16:27:51', '2021-08-20 16:40:20');
INSERT INTO `t_menu` VALUES (182, 181, '课程视频', '/course', 'course:view', '', '0', 1, '2021-08-20 16:38:59', '2021-09-01 17:31:34');
INSERT INTO `t_menu` VALUES (183, 182, '新增课程', NULL, 'course:add', NULL, '1', NULL, '2021-08-20 16:41:16', NULL);
INSERT INTO `t_menu` VALUES (185, 182, '更新课程', NULL, 'course:update', NULL, '1', NULL, '2021-08-21 16:41:29', NULL);
INSERT INTO `t_menu` VALUES (186, 182, '删除课程', NULL, 'course:delete', NULL, '1', NULL, '2021-08-21 16:41:58', NULL);
INSERT INTO `t_menu` VALUES (187, 182, '上传视频', NULL, 'course:upload', NULL, '1', NULL, '2021-08-22 17:06:28', '2021-09-08 14:55:45');
INSERT INTO `t_menu` VALUES (188, 181, '科目管理', '/subject', 'subject:view', '', '0', 1, '2021-08-25 15:09:47', NULL);
INSERT INTO `t_menu` VALUES (189, 188, '新增科目', NULL, 'subject:add', NULL, '1', NULL, '2021-08-25 15:12:09', NULL);
INSERT INTO `t_menu` VALUES (190, 188, '更新科目', NULL, 'subject:update', NULL, '1', NULL, '2021-08-25 15:12:53', '2021-09-01 09:56:49');
INSERT INTO `t_menu` VALUES (191, 188, '删除科目', NULL, 'subject:delete', NULL, '1', NULL, '2021-08-25 15:13:17', NULL);
INSERT INTO `t_menu` VALUES (192, 188, '导出Excel', NULL, 'subject:export', NULL, '1', NULL, '2021-08-25 15:24:27', NULL);
INSERT INTO `t_menu` VALUES (193, 1, '管理员', '/system/admin', 'admin:view', '', '0', 2, '2021-09-01 00:46:49', '2021-09-08 14:27:02');
INSERT INTO `t_menu` VALUES (195, 193, '新增管理员', NULL, 'admin:add', NULL, '1', NULL, '2021-09-08 14:27:55', '2021-09-08 14:28:09');
INSERT INTO `t_menu` VALUES (196, 193, '修改管理员', NULL, 'admin:update', NULL, '1', NULL, '2021-09-08 14:32:16', NULL);
INSERT INTO `t_menu` VALUES (197, 193, '删除管理员', NULL, 'admin:delete', NULL, '1', NULL, '2021-09-08 14:33:11', NULL);
INSERT INTO `t_menu` VALUES (198, 193, '重置密码', NULL, 'admin:password:reset', NULL, '1', NULL, '2021-09-08 14:34:26', NULL);
INSERT INTO `t_menu` VALUES (199, 193, '导出Excel', NULL, 'admin:export', NULL, '1', NULL, '2021-09-08 14:35:13', '2021-09-08 14:35:58');
INSERT INTO `t_menu` VALUES (200, 182, '导出Excel', NULL, 'course:export', NULL, '1', NULL, '2021-09-09 15:35:39', '2021-09-09 15:36:51');
INSERT INTO `t_menu` VALUES (201, 1, '备份文件', '/system/backups', 'backups:view', '', '0', 6, '2021-09-24 14:28:08', '2021-09-24 14:46:15');
INSERT INTO `t_menu` VALUES (202, 201, '备份下载', NULL, 'backups:download', NULL, '1', NULL, '2021-09-24 14:50:21', '2021-09-24 14:50:52');
INSERT INTO `t_menu` VALUES (203, 1, '人脸检测记录', '/face_detect', 'face_detect:view', '', '0', 1, '2021-10-12 13:46:39', NULL);
INSERT INTO `t_menu` VALUES (204, 203, '导出', NULL, 'face_detect:export', NULL, '1', NULL, '2021-10-12 13:48:02', NULL);
INSERT INTO `t_menu` VALUES (205, 3, '生成证书', NULL, 'user:generateCertificate', NULL, '1', NULL, '2021-10-29 15:44:39', '2021-10-29 15:44:51');

-- ----------------------------
-- Table structure for t_nationality
-- ----------------------------
DROP TABLE IF EXISTS `t_nationality`;
CREATE TABLE `t_nationality`  (
  `NATIONALITY_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '民族ID',
  `NATIONALITY_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '民族名称',
  `ORDER_NUM` bigint(20) NULL DEFAULT NULL COMMENT '排序',
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `MODIFY_TIME` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`NATIONALITY_ID`) USING BTREE,
  INDEX `t_dept_dept_name`(`NATIONALITY_NAME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 58 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_nationality
-- ----------------------------
INSERT INTO `t_nationality` VALUES (0, '汉族', 1, NULL, NULL);
INSERT INTO `t_nationality` VALUES (2, '满族', 2, NULL, NULL);
INSERT INTO `t_nationality` VALUES (3, '蒙古族', 3, NULL, NULL);
INSERT INTO `t_nationality` VALUES (4, '回族', 4, NULL, NULL);
INSERT INTO `t_nationality` VALUES (5, '藏族', 5, NULL, NULL);
INSERT INTO `t_nationality` VALUES (6, '维吾尔族', 6, NULL, NULL);
INSERT INTO `t_nationality` VALUES (7, '苗族', 7, NULL, NULL);
INSERT INTO `t_nationality` VALUES (8, '彝族', 8, NULL, NULL);
INSERT INTO `t_nationality` VALUES (9, '壮族', 9, NULL, NULL);
INSERT INTO `t_nationality` VALUES (10, '布依族', 10, NULL, NULL);
INSERT INTO `t_nationality` VALUES (11, '侗族', 11, NULL, NULL);
INSERT INTO `t_nationality` VALUES (12, '瑶族', 12, NULL, NULL);
INSERT INTO `t_nationality` VALUES (13, '白族', 13, NULL, NULL);
INSERT INTO `t_nationality` VALUES (14, '土家族', 14, NULL, NULL);
INSERT INTO `t_nationality` VALUES (15, '哈尼族', 15, NULL, NULL);
INSERT INTO `t_nationality` VALUES (16, '哈萨克族', 16, NULL, NULL);
INSERT INTO `t_nationality` VALUES (17, '傣族', 17, NULL, NULL);
INSERT INTO `t_nationality` VALUES (18, '黎族', 18, NULL, NULL);
INSERT INTO `t_nationality` VALUES (19, '傈僳族', 19, NULL, NULL);
INSERT INTO `t_nationality` VALUES (20, '佤族', 20, NULL, NULL);
INSERT INTO `t_nationality` VALUES (21, '畲族', 21, NULL, NULL);
INSERT INTO `t_nationality` VALUES (22, '高山族', 22, NULL, NULL);
INSERT INTO `t_nationality` VALUES (23, '拉祜族', 23, NULL, NULL);
INSERT INTO `t_nationality` VALUES (24, '水族', 24, NULL, NULL);
INSERT INTO `t_nationality` VALUES (25, '东乡族', 25, NULL, NULL);
INSERT INTO `t_nationality` VALUES (26, '纳西族', 26, NULL, NULL);
INSERT INTO `t_nationality` VALUES (27, '景颇族', 27, NULL, NULL);
INSERT INTO `t_nationality` VALUES (28, '柯尔克孜族', 28, NULL, NULL);
INSERT INTO `t_nationality` VALUES (29, '土族', 29, NULL, NULL);
INSERT INTO `t_nationality` VALUES (30, '达斡尔族', 30, NULL, NULL);
INSERT INTO `t_nationality` VALUES (31, '仫佬族', 31, NULL, NULL);
INSERT INTO `t_nationality` VALUES (32, '羌族', 32, NULL, NULL);
INSERT INTO `t_nationality` VALUES (33, '布朗族', 33, NULL, NULL);
INSERT INTO `t_nationality` VALUES (34, '撒拉族', 34, NULL, NULL);
INSERT INTO `t_nationality` VALUES (35, '毛南族', 35, NULL, NULL);
INSERT INTO `t_nationality` VALUES (36, '仡佬族', 36, NULL, NULL);
INSERT INTO `t_nationality` VALUES (37, '锡伯族', 37, NULL, NULL);
INSERT INTO `t_nationality` VALUES (38, '阿昌族', 38, NULL, NULL);
INSERT INTO `t_nationality` VALUES (39, '普米族', 39, NULL, NULL);
INSERT INTO `t_nationality` VALUES (40, '朝鲜族', 40, NULL, NULL);
INSERT INTO `t_nationality` VALUES (41, '塔吉克族', 41, NULL, NULL);
INSERT INTO `t_nationality` VALUES (42, '怒族', 42, NULL, NULL);
INSERT INTO `t_nationality` VALUES (43, '乌孜别克族', 43, NULL, NULL);
INSERT INTO `t_nationality` VALUES (44, '俄罗斯族', 44, NULL, NULL);
INSERT INTO `t_nationality` VALUES (45, '鄂温克族', 45, NULL, NULL);
INSERT INTO `t_nationality` VALUES (46, '德昂族', 46, NULL, NULL);
INSERT INTO `t_nationality` VALUES (47, '保安族', 47, NULL, NULL);
INSERT INTO `t_nationality` VALUES (48, '裕固族', 48, NULL, NULL);
INSERT INTO `t_nationality` VALUES (49, '京族', 49, NULL, NULL);
INSERT INTO `t_nationality` VALUES (50, '塔塔尔族', 50, NULL, NULL);
INSERT INTO `t_nationality` VALUES (51, '独龙族', 51, NULL, NULL);
INSERT INTO `t_nationality` VALUES (52, '鄂伦春族', 52, NULL, NULL);
INSERT INTO `t_nationality` VALUES (53, '赫哲族', 53, NULL, NULL);
INSERT INTO `t_nationality` VALUES (54, '门巴族', 54, NULL, NULL);
INSERT INTO `t_nationality` VALUES (55, '珞巴族', 55, NULL, NULL);
INSERT INTO `t_nationality` VALUES (56, '基诺族', 56, NULL, NULL);
INSERT INTO `t_nationality` VALUES (57, '其他民族', 57, NULL, NULL);

-- ----------------------------
-- Table structure for t_play_finish_record
-- ----------------------------
DROP TABLE IF EXISTS `t_play_finish_record`;
CREATE TABLE `t_play_finish_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 345 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_play_finish_record
-- ----------------------------
INSERT INTO `t_play_finish_record` VALUES (340, 21, 14, '2021-10-22 10:42:40');
INSERT INTO `t_play_finish_record` VALUES (341, 21, 14, '2021-10-29 10:43:00');
INSERT INTO `t_play_finish_record` VALUES (344, 21, 22, '2021-11-02 09:05:18');

-- ----------------------------
-- Table structure for t_play_record
-- ----------------------------
DROP TABLE IF EXISTS `t_play_record`;
CREATE TABLE `t_play_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `time` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '播放时长',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 346 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_play_record
-- ----------------------------
INSERT INTO `t_play_record` VALUES (1, 20, 14, 30000, '2021-09-26 14:08:12');
INSERT INTO `t_play_record` VALUES (4, 20, 15, 600000, '2021-09-26 15:57:05');
INSERT INTO `t_play_record` VALUES (5, 20, 15, 2000, '2021-09-26 15:57:53');
INSERT INTO `t_play_record` VALUES (205, 21, 16, 2809, '2021-10-11 14:10:30');
INSERT INTO `t_play_record` VALUES (316, 21, 15, 0, '2021-10-15 11:18:34');
INSERT INTO `t_play_record` VALUES (339, 21, 14, 59039, '2021-10-29 10:10:03');
INSERT INTO `t_play_record` VALUES (341, 21, 22, 4495, '2021-11-02 09:05:36');
INSERT INTO `t_play_record` VALUES (344, 21, 30, 0, '2021-12-08 15:35:32');
INSERT INTO `t_play_record` VALUES (345, 21, 31, 0, '2021-12-08 15:35:32');

-- ----------------------------
-- Table structure for t_privilege
-- ----------------------------
DROP TABLE IF EXISTS `t_privilege`;
CREATE TABLE `t_privilege`  (
  `code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `enabled` bit(1) NOT NULL,
  `sequence` int(11) NOT NULL,
  `is_global` tinyint(1) NOT NULL,
  PRIMARY KEY (`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_privilege
-- ----------------------------
INSERT INTO `t_privilege` VALUES ('ALTER', 'ALTER', b'1', 5, 0);
INSERT INTO `t_privilege` VALUES ('ALTER ROUTINE', 'ALTER ROUTINE', b'1', 11, 0);
INSERT INTO `t_privilege` VALUES ('CREATE', 'CREATE', b'1', 7, 0);
INSERT INTO `t_privilege` VALUES ('CREATE ROUTINE', 'CREATE ROUTINE', b'1', 12, 0);
INSERT INTO `t_privilege` VALUES ('CREATE TEMPORARY TABLES', 'CREATE TEMPORARY TABLES', b'1', 99, 0);
INSERT INTO `t_privilege` VALUES ('CREATE USER', 'CREATE USER', b'0', 99, 0);
INSERT INTO `t_privilege` VALUES ('CREATE VIEW', 'CREATE VIEW', b'1', 99, 0);
INSERT INTO `t_privilege` VALUES ('DELETE', 'DELETE', b'1', 4, 0);
INSERT INTO `t_privilege` VALUES ('DROP', 'DROP', b'1', 8, 0);
INSERT INTO `t_privilege` VALUES ('EVENT', 'EVENT', b'1', 99, 0);
INSERT INTO `t_privilege` VALUES ('EXECUTE', 'EXECUTE', b'1', 6, 0);
INSERT INTO `t_privilege` VALUES ('FILE', 'FILE', b'1', 99, 1);
INSERT INTO `t_privilege` VALUES ('GRANT OPTION', 'GRANT OPTION', b'0', 99, 0);
INSERT INTO `t_privilege` VALUES ('INDEX', 'INDEX', b'1', 9, 0);
INSERT INTO `t_privilege` VALUES ('INSERT', 'INSERT', b'1', 3, 0);
INSERT INTO `t_privilege` VALUES ('LOCK TABLES', 'LOCK TABLES', b'1', 99, 0);
INSERT INTO `t_privilege` VALUES ('PROCESS', 'PROCESS', b'1', 99, 1);
INSERT INTO `t_privilege` VALUES ('REFERENCES', 'REFERENCES', b'1', 99, 0);
INSERT INTO `t_privilege` VALUES ('REPLICATION CLIENT', 'REPLICATION CLIENT', b'1', 99, 1);
INSERT INTO `t_privilege` VALUES ('REPLICATION SLAVE', 'REPLICATION SLAVE', b'1', 99, 1);
INSERT INTO `t_privilege` VALUES ('SELECT', 'SELECT', b'1', 1, 0);
INSERT INTO `t_privilege` VALUES ('SHOW DATABASES', 'SHOW DATABASES', b'0', 99, 0);
INSERT INTO `t_privilege` VALUES ('SHOW VIEW', 'SHOW VIEW', b'1', 10, 0);
INSERT INTO `t_privilege` VALUES ('SHUTDOWN', 'SHUTDOWN', b'0', 99, 0);
INSERT INTO `t_privilege` VALUES ('SUPER', 'SUPER', b'0', 99, 1);
INSERT INTO `t_privilege` VALUES ('TRIGGER', 'TRIGGER', b'1', 99, 0);
INSERT INTO `t_privilege` VALUES ('UPDATE', 'UPDATE', b'1', 2, 0);

-- ----------------------------
-- Table structure for t_question
-- ----------------------------
DROP TABLE IF EXISTS `t_question`;
CREATE TABLE `t_question`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '选择题ID',
  `course_id` bigint(20) NOT NULL COMMENT '所属课程ID',
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题型',
  `title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '题目',
  `option_a` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '选项A',
  `option_b` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '选项B',
  `option_c` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '选项C',
  `option_d` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '选项D',
  `answer` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '答案',
  `score` int(2) NULL DEFAULT NULL COMMENT '分数',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `creator` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_question
-- ----------------------------

-- ----------------------------
-- Table structure for t_question_type
-- ----------------------------
DROP TABLE IF EXISTS `t_question_type`;
CREATE TABLE `t_question_type`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '题型ID',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '题型名称',
  `code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_question_type
-- ----------------------------
INSERT INTO `t_question_type` VALUES (1, '单选题', 'single_choice');
INSERT INTO `t_question_type` VALUES (2, '多选题', 'multiple_choice');
INSERT INTO `t_question_type` VALUES (3, '判断题', 'true_or_false');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role`  (
  `ROLE_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `ROLE_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `REMARK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `CREATE_TIME` datetime(0) NOT NULL COMMENT '创建时间',
  `MODIFY_TIME` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`ROLE_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES (1, '系统管理员', '系统管理员，拥有所有操作权限 ^_^', '2019-06-14 16:23:11', '2021-10-29 15:45:18');
INSERT INTO `t_role` VALUES (3, 'APP用户', '无后台系统使用权', '2021-09-08 14:01:39', NULL);

-- ----------------------------
-- Table structure for t_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_role_menu`;
CREATE TABLE `t_role_menu`  (
  `ROLE_ID` bigint(20) NOT NULL COMMENT '角色ID',
  `MENU_ID` bigint(20) NOT NULL COMMENT '菜单/按钮ID',
  INDEX `t_role_menu_menu_id`(`MENU_ID`) USING BTREE,
  INDEX `t_role_menu_role_id`(`ROLE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role_menu
-- ----------------------------
INSERT INTO `t_role_menu` VALUES (1, 181);
INSERT INTO `t_role_menu` VALUES (1, 182);
INSERT INTO `t_role_menu` VALUES (1, 183);
INSERT INTO `t_role_menu` VALUES (1, 185);
INSERT INTO `t_role_menu` VALUES (1, 186);
INSERT INTO `t_role_menu` VALUES (1, 187);
INSERT INTO `t_role_menu` VALUES (1, 200);
INSERT INTO `t_role_menu` VALUES (1, 188);
INSERT INTO `t_role_menu` VALUES (1, 189);
INSERT INTO `t_role_menu` VALUES (1, 190);
INSERT INTO `t_role_menu` VALUES (1, 191);
INSERT INTO `t_role_menu` VALUES (1, 192);
INSERT INTO `t_role_menu` VALUES (1, 2);
INSERT INTO `t_role_menu` VALUES (1, 8);
INSERT INTO `t_role_menu` VALUES (1, 23);
INSERT INTO `t_role_menu` VALUES (1, 10);
INSERT INTO `t_role_menu` VALUES (1, 24);
INSERT INTO `t_role_menu` VALUES (1, 170);
INSERT INTO `t_role_menu` VALUES (1, 136);
INSERT INTO `t_role_menu` VALUES (1, 171);
INSERT INTO `t_role_menu` VALUES (1, 172);
INSERT INTO `t_role_menu` VALUES (1, 127);
INSERT INTO `t_role_menu` VALUES (1, 128);
INSERT INTO `t_role_menu` VALUES (1, 129);
INSERT INTO `t_role_menu` VALUES (1, 131);
INSERT INTO `t_role_menu` VALUES (1, 101);
INSERT INTO `t_role_menu` VALUES (1, 102);
INSERT INTO `t_role_menu` VALUES (1, 103);
INSERT INTO `t_role_menu` VALUES (1, 104);
INSERT INTO `t_role_menu` VALUES (1, 105);
INSERT INTO `t_role_menu` VALUES (1, 106);
INSERT INTO `t_role_menu` VALUES (1, 107);
INSERT INTO `t_role_menu` VALUES (1, 108);
INSERT INTO `t_role_menu` VALUES (1, 173);
INSERT INTO `t_role_menu` VALUES (1, 109);
INSERT INTO `t_role_menu` VALUES (1, 110);
INSERT INTO `t_role_menu` VALUES (1, 174);
INSERT INTO `t_role_menu` VALUES (1, 115);
INSERT INTO `t_role_menu` VALUES (1, 132);
INSERT INTO `t_role_menu` VALUES (1, 133);
INSERT INTO `t_role_menu` VALUES (1, 135);
INSERT INTO `t_role_menu` VALUES (1, 134);
INSERT INTO `t_role_menu` VALUES (1, 126);
INSERT INTO `t_role_menu` VALUES (1, 1);
INSERT INTO `t_role_menu` VALUES (1, 3);
INSERT INTO `t_role_menu` VALUES (1, 11);
INSERT INTO `t_role_menu` VALUES (1, 12);
INSERT INTO `t_role_menu` VALUES (1, 13);
INSERT INTO `t_role_menu` VALUES (1, 160);
INSERT INTO `t_role_menu` VALUES (1, 161);
INSERT INTO `t_role_menu` VALUES (1, 180);
INSERT INTO `t_role_menu` VALUES (1, 205);
INSERT INTO `t_role_menu` VALUES (1, 203);
INSERT INTO `t_role_menu` VALUES (1, 204);
INSERT INTO `t_role_menu` VALUES (1, 193);
INSERT INTO `t_role_menu` VALUES (1, 195);
INSERT INTO `t_role_menu` VALUES (1, 196);
INSERT INTO `t_role_menu` VALUES (1, 197);
INSERT INTO `t_role_menu` VALUES (1, 198);
INSERT INTO `t_role_menu` VALUES (1, 199);
INSERT INTO `t_role_menu` VALUES (1, 4);
INSERT INTO `t_role_menu` VALUES (1, 14);
INSERT INTO `t_role_menu` VALUES (1, 15);
INSERT INTO `t_role_menu` VALUES (1, 16);
INSERT INTO `t_role_menu` VALUES (1, 162);
INSERT INTO `t_role_menu` VALUES (1, 5);
INSERT INTO `t_role_menu` VALUES (1, 17);
INSERT INTO `t_role_menu` VALUES (1, 18);
INSERT INTO `t_role_menu` VALUES (1, 19);
INSERT INTO `t_role_menu` VALUES (1, 163);
INSERT INTO `t_role_menu` VALUES (1, 6);
INSERT INTO `t_role_menu` VALUES (1, 20);
INSERT INTO `t_role_menu` VALUES (1, 21);
INSERT INTO `t_role_menu` VALUES (1, 22);
INSERT INTO `t_role_menu` VALUES (1, 164);
INSERT INTO `t_role_menu` VALUES (1, 201);
INSERT INTO `t_role_menu` VALUES (1, 202);

-- ----------------------------
-- Table structure for t_subject
-- ----------------------------
DROP TABLE IF EXISTS `t_subject`;
CREATE TABLE `t_subject`  (
  `SUBJECT_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '科目ID',
  `P_SUBJECT_ID` bigint(20) NOT NULL COMMENT '父科目ID',
  `TITLE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '科目标题',
  `CONTENT` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '介绍文本',
  `IMG_URL` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '缩略图URL',
  `PRICE` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额',
  `PUBLIC` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '是否公共课（0不是 1 是）',
  `CREATE_TIME` datetime(0) NOT NULL COMMENT '创建时间',
  `CREATOR` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  PRIMARY KEY (`SUBJECT_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_subject
-- ----------------------------
INSERT INTO `t_subject` VALUES (14, 0, '岗位技工', '岗位技工', '', NULL, '0', '2021-09-09 17:34:07', 'MrBird');
INSERT INTO `t_subject` VALUES (15, 14, '砌筑工', '砌筑工', '', NULL, '0', '2021-09-09 17:39:15', 'MrBird');
INSERT INTO `t_subject` VALUES (18, 14, '混凝土工', '', '', NULL, '0', '2021-09-09 17:40:07', 'MrBird');
INSERT INTO `t_subject` VALUES (19, 14, '钢筋工', '', '', NULL, '0', '2021-09-09 17:40:16', 'MrBird');
INSERT INTO `t_subject` VALUES (20, 14, '架子工', '', '', NULL, '0', '2021-09-09 17:40:26', 'MrBird');
INSERT INTO `t_subject` VALUES (21, 14, '手工木工', '', '', NULL, '0', '2021-09-09 17:40:35', 'MrBird');
INSERT INTO `t_subject` VALUES (22, 14, '焊工', '', '', NULL, '0', '2021-09-09 17:40:45', 'MrBird');
INSERT INTO `t_subject` VALUES (23, 14, '防水工', '', '', NULL, '0', '2021-09-09 17:40:55', 'MrBird');
INSERT INTO `t_subject` VALUES (24, 14, '模板工', '', '', NULL, '0', '2021-09-09 17:41:06', 'MrBird');
INSERT INTO `t_subject` VALUES (25, 14, '电工', '', '', NULL, '0', '2021-09-09 17:41:16', 'MrBird');
INSERT INTO `t_subject` VALUES (26, 14, '管道工', '', '', NULL, '0', '2021-09-09 17:41:24', 'MrBird');
INSERT INTO `t_subject` VALUES (27, 14, '起重机械操作工', '', '', NULL, '0', '2021-09-09 17:41:35', 'MrBird');
INSERT INTO `t_subject` VALUES (28, 14, '抹灰工', '', '', NULL, '0', '2021-09-09 17:41:44', 'MrBird');
INSERT INTO `t_subject` VALUES (29, 14, '镶贴工', '', '', NULL, '0', '2021-09-09 17:41:53', 'MrBird');
INSERT INTO `t_subject` VALUES (30, 14, '油漆工', '', '/upload/subject/2021-09-10/95835928-2849-4c86-8f4d-66a46f178519.jpg', NULL, '0', '2021-09-09 17:42:04', 'MrBird');
INSERT INTO `t_subject` VALUES (32, 0, '警示教育', '', '', NULL, '1', '2021-12-09 14:18:14', 'MrBird');
INSERT INTO `t_subject` VALUES (33, 0, '三类人员继续教育', '', '', NULL, '1', '2021-12-09 14:18:24', 'MrBird');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `USER_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `USERNAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `PASSWORD` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `DEPT_ID` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `EMAIL` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `MOBILE` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `STATUS` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态 0锁定 1有效',
  `CREATE_TIME` datetime(0) NOT NULL COMMENT '创建时间',
  `MODIFY_TIME` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `LAST_LOGIN_TIME` datetime(0) NULL DEFAULT NULL COMMENT '最近访问时间',
  `SSEX` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别 0男 1女 2保密',
  `IS_TAB` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否开启tab，0关闭 1开启',
  `THEME` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主题',
  `AVATAR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `DESCRIPTION` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `NICKNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `HEAD_URL` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `NATIONALITY` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '民族',
  `IS_USER` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为用户 0管理员 1用户',
  `IDCARD` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `IDCARD_FRONT` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证正面',
  `IDCARD_NEGATIVE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证反面',
  `ONE_INCH_PHOTO` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '一寸照片',
  `CERTIFICATE_NUMBER` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '证书编号',
  `FACE_COLLECTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人脸采集图片',
  `SUBJECT_ID` bigint(20) NULL DEFAULT NULL COMMENT '科目ID',
  `CERTIFICATE_URL` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '合格证路径',
  PRIMARY KEY (`USER_ID`) USING BTREE,
  INDEX `t_user_username`(`USERNAME`) USING BTREE,
  INDEX `t_user_mobile`(`MOBILE`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (1, 'MrBird', 'cb62ad1497597283961545d608f80241', 10, 'mrbird@qq.com', '17788888888', '1', '2019-06-14 20:39:22', '2021-10-13 09:29:51', '2021-12-09 09:19:53', '0', '1', 'black', 'cnrhVkzwxjPwAaCfPbdc.png', '', 'MrBird', NULL, '1', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_user` VALUES (19, '里斯', '1d83e7c33d92691c623fbd49108032f8', NULL, NULL, '3', '1', '2021-09-11 10:06:15', '2021-10-29 16:04:56', '2021-09-23 21:34:16', '1', '1', 'black', 'default.jpg', 'test', '', NULL, '50', '1', '370705198703030268', '/upload/certificate/19/197cb15e-e569-4e3c-9427-407dba14f5f7.jpg', '/upload/certificate/19/75700f63-6166-44ae-81d2-951907c3051f.jpg', '/upload/certificate/19/3334e655-f3c1-4968-94f4-ebce7641bad3.jpg', '23456786543', NULL, 15, NULL);
INSERT INTO `t_user` VALUES (20, '测试用户1', '1e939e074df5375deb915125a4c77b16', NULL, NULL, '2', '1', '2021-09-11 10:48:19', '2021-10-29 16:05:20', '2021-09-19 10:04:58', '0', '1', 'black', 'avatar', '', 'nickname', NULL, '2', '1', '32323', 'idcardFront', 'idcardNegative', 'oneInchPhoto', 'certificateNumber', '32323', 15, NULL);
INSERT INTO `t_user` VALUES (21, '张三丰', '5c73790d59291d83e49d92f18caac213', NULL, NULL, '13047495791', '1', '2021-09-17 13:42:07', '2021-11-04 13:11:44', '2021-12-08 13:32:51', '1', '1', 'black', '/upload/avatar/21/4f326e0d-30d4-4a04-a7e2-2a6ed4a28331.jpg', 'test', 'test', NULL, '', '1', '380907199510090389', '/upload/certificate/21/6bae25dc-7928-400b-af45-e72c7ccc9b12.jpg', '/upload/certificate/21/8206d195-a9c7-48e1-ad1e-375898c4283b.jpg', '/upload/certificate/21/30014766-067d-4aa7-b22e-a85e84efd2bc.jpg', '12333333', NULL, 15, '\\upload\\certificate\\21\\certificate.png');
INSERT INTO `t_user` VALUES (22, '朱晓辉', '5fcb1227caa8ffc262e7d32497a83383', NULL, NULL, '11', '1', '2021-10-11 10:17:27', '2021-10-29 16:06:02', '2021-10-13 16:57:27', '0', '1', 'black', 'default.jpg', 'test', NULL, NULL, NULL, '1', '370602199510030227', '/upload/certificate/22/829c6d4e-f279-4a7a-83db-e39bd04792ac.jpg', '/upload/certificate/22/82810cf8-2aa8-4f83-a445-63c1400392d8.jpg', '/upload/certificate/22/e3b7d14f-1a1c-4782-af68-9d8f08536849.jpg', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_user_data_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_user_data_permission`;
CREATE TABLE `t_user_data_permission`  (
  `USER_ID` bigint(20) NOT NULL,
  `DEPT_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`USER_ID`, `DEPT_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户数据权限关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user_data_permission
-- ----------------------------
INSERT INTO `t_user_data_permission` VALUES (1, 10);

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role`  (
  `USER_ID` bigint(20) NOT NULL COMMENT '用户ID',
  `ROLE_ID` bigint(20) NOT NULL COMMENT '角色ID',
  INDEX `t_user_role_user_id`(`USER_ID`) USING BTREE,
  INDEX `t_user_role_role_id`(`ROLE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES (10, 3);
INSERT INTO `t_user_role` VALUES (11, 3);
INSERT INTO `t_user_role` VALUES (22, 3);
INSERT INTO `t_user_role` VALUES (1, 1);

SET FOREIGN_KEY_CHECKS = 1;
