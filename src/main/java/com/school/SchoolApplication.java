package com.school;

import com.school.common.annotation.SchoolShiro;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * @author MrBird
 */
@SchoolShiro
public class SchoolApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(SchoolApplication.class)
                .web(WebApplicationType.SERVLET)
                .run(args);
    }

}
