package com.school.common.converter;

import java.net.InetAddress;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.wuwenze.poi.convert.WriteConverter;
import com.wuwenze.poi.exception.ExcelKitWriteConverterException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ImageUrlConverter implements WriteConverter {
    private String port;
    private String contextPath;
    private String hostIp;

    @Override
    public String convert(Object value) {
        if (value == null) {
            return StringUtils.EMPTY;
        } else {
            try {
                YamlPropertiesFactoryBean yamlMapFactoryBean = new YamlPropertiesFactoryBean();
                // 可以加载多个yml文件
                yamlMapFactoryBean.setResources(new ClassPathResource("application.yml"));
                Properties properties = yamlMapFactoryBean.getObject();

                // 获取yml里的参数
                port = properties.getProperty("server.port");
                contextPath = properties.getProperty("server.servlet.context-path");
                hostIp = properties.getProperty("prop.host-ip");
                InetAddress address = InetAddress.getLocalHost();
                String url = String.format("http://%s:%s", hostIp, port);
                if (StringUtils.isNotBlank(contextPath)) {
                    url += contextPath;
                }
                url += value;
                return url;
            } catch (Exception e) {
                String message = "获取地址异常";
                log.error(message, e);
                throw new ExcelKitWriteConverterException(message);
            }
        }
    }
}
