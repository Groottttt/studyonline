package com.school.common.converter;

import org.apache.commons.lang3.StringUtils;

import com.wuwenze.poi.convert.WriteConverter;
import com.wuwenze.poi.exception.ExcelKitWriteConverterException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PlayTimeConverter implements WriteConverter {
    @Override
    public String convert(Object value) {
        if (value == null) {
            return StringUtils.EMPTY;
        } else {
            try {
                Long date = Long.valueOf((String) value);
                long days = date / 1000 / 60 / 60 / 24;
                double daysRound = Math.floor(days);
                double hours = date / 1000 / 60 / 60 - (24 * daysRound);
                double hoursRound = Math.floor(hours);
                double minutes = date / 1000 / 60 - (24 * 60 * daysRound) - (60 * hoursRound);
                double minutesRound = Math.floor(minutes);
                double seconds = date / 1000 - (24 * 60 * 60 * daysRound) - (60 * 60 * hoursRound)
                        - (60 * minutesRound);
                double secondsRound = Math.floor(seconds);
                String res = String.format("%02d", (int) hoursRound) + ":" + String.format("%02d", (int) minutesRound)
                        + ":" + String.format("%02d", (int) secondsRound);
                return res;
            } catch (Exception e) {
                String message = "时间转换异常";
                log.error(message, e);
                throw new ExcelKitWriteConverterException(message);
            }
        }
    }
}
