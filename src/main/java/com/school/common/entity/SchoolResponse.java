package com.school.common.entity;

import org.springframework.http.HttpStatus;

import java.util.HashMap;

/**
 * @author MrBird
 */
public class SchoolResponse extends HashMap<String, Object> {

    private static final long serialVersionUID = -8713837118340960775L;

    public SchoolResponse code(HttpStatus status) {
        put("code", status.value());
        return this;
    }

    public SchoolResponse message(String message) {
        put("message", message);
        return this;
    }

    public SchoolResponse data(Object data) {
        put("data", data);
        return this;
    }

    public SchoolResponse success() {
        code(HttpStatus.OK);
        return this;
    }

    public SchoolResponse fail() {
        code(HttpStatus.INTERNAL_SERVER_ERROR);
        return this;
    }

    @Override
    public SchoolResponse put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
