package com.school.common.exception;

/**
 * SCHOOL系统内部异常
 *
 * @author MrBird
 */
public class SchoolException extends RuntimeException {

    private static final long serialVersionUID = -994962710559017255L;

    public SchoolException(String message) {
        super(message);
    }
}
