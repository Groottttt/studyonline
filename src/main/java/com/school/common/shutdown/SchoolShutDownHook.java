package com.school.common.shutdown;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

/**
 * @author MrBird
 */
@Slf4j
@Component
public class SchoolShutDownHook {

    @EventListener(classes = {ContextClosedEvent.class})
    public void onSchoolApplicationClosed(@NonNull ApplicationEvent event) {
        log.info("SCHOOL系统已关闭，Bye");
    }
}
