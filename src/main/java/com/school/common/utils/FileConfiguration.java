package com.school.common.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FileConfiguration {
	
	@Value("${prop.upload-folder}")
	private String resourceDir;

	@Value("${spring.datasource.dynamic.datasource.base.url}")
	private String url;

	@Value("${spring.datasource.dynamic.datasource.base.username}")
	private String username;

	@Value("${spring.datasource.dynamic.datasource.base.password}")
	private String password;
	
	public String getResourceDir() {
		return resourceDir;
	}
	
	public void setResourceDir(String resourceDir) {
		this.resourceDir = resourceDir;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
