package com.school.excel.entity;

import com.school.common.converter.TimeConverter;
import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author MrBird
 */
@Data
@Excel("用户信息表")
public class ExamExcel implements Serializable, Cloneable {


    /**
     * 用户 ID
     */
    @ExcelField(value = "用户ID")
    private Long userId;

    /**
     * 用户名
     */
    @ExcelField(value = "姓名")
    private String username;

    /**
     * 身份证号
     */
    @ExcelField(value = "身份证号")
    private String idcard;

    /**
     * 成绩
     */
    @ExcelField(value = "成绩")
    private String score;

    /**
     * 是否获得证书
     */
    @ExcelField(value = "是否获得证书")
    private String hasCertificate;

    /**
     * 联系电话
     */
    @ExcelField(value = "联系电话")
    // @Desensitization(type = DesensitizationType.PHONE)
    private String mobile;




    /**
     * 科目名称
     */
    @ExcelField(value = "专业名称")
    private String title;

    /**
     * 班级名称
     */
    @ExcelField(value = "班级名称")
    private String classesName;

    /**
     * 单位名称
     */
    @ExcelField(value = "单位名称")
    private String workPlaceName;


    /**
     * 创建时间
     */
    @ExcelField(value = "创建时间", writeConverter = TimeConverter.class)
    private Date createTime;





}

