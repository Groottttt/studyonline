package com.school.excel.entity;

import com.school.common.converter.TimeConverter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * @author MrBird
 */
@Data
@Excel("用户信息表")
public class UserExcel implements Serializable, Cloneable {

    /**
     * 用户状态：有效
     */
    public static final String STATUS_VALID = "1";
    /**
     * 用户状态：锁定
     */
    public static final String STATUS_LOCK = "0";
    /**
     * 默认头像
     */
    public static final String DEFAULT_AVATAR = "default.jpg";
    /**
     * 默认密码
     */
    public static final String DEFAULT_PASSWORD = "1234qwer";
    /**
     * 性别男
     */
    public static final String SEX_MALE = "0";
    /**
     * 性别女
     */
    public static final String SEX_FEMALE = "1";
    /**
     * 性别保密
     */
    public static final String SEX_UNKNOW = "2";
    /**
     * 黑色主题
     */
    public static final String THEME_BLACK = "black";
    /**
     * 白色主题
     */
    public static final String THEME_WHITE = "white";
    /**
     * TAB开启
     */
    public static final String TAB_OPEN = "1";
    /**
     * 管理员
     */
    public static final String IS_ADMIN = "0";
    /**
     * 用户
     */
    public static final String IS_USER = "1";
    /**
     * TAB关闭
     */
    public static final String TAB_CLOSE = "0";

    private static final long serialVersionUID = -4352868070794165001L;
    /**
     * 用户 ID
     */
    @ExcelField(value = "用户ID")
    private Long userId;

    /**
     * 用户名
     */
    @ExcelField(value = "姓名")
    private String username;

    /**
     * 身份证号
     */
    @ExcelField(value = "身份证号")
    private String idcard;

    /**
     * 联系电话
     */
    @ExcelField(value = "联系电话")
    // @Desensitization(type = DesensitizationType.PHONE)
    private String mobile;

    /**
     * 性别 0男 1女 2 保密
     */
    @ExcelField(value = "性别", writeConverterExp = "0=男,1=女,2=保密")
    private String sex;

    /**
     * 昵称
     */
    @ExcelField("昵称")
    private String nickname;

    /**
     * 科目名称
     */
    @ExcelField(value = "专业名称")
    private String subjectName;

    /**
     * 班级名称
     */
    @ExcelField(value = "班级名称")
    private String classesName;

    /**
     * 单位名称
     */
    @ExcelField(value = "单位名称")
    private String workPlaceName;

    /**
     * 完成课时数
     */
    @ExcelField(value = "完成课时数")
    private String finishCourseNum;

    /**
     * 民族
     */
    @ExcelField(value = "民族")
    private String nationalityName;

    /**
     * 证书编号
     */
    @ExcelField(value = "证书编号")
    private String certificateNumber;
    /**
     * 描述
     */
    @ExcelField(value = "个人描述")
    private String description;

    /**
     * 状态 0锁定 1有效
     */
    @ExcelField(value = "状态", writeConverterExp = "0=锁定,1=有效")
    private String status;

    /**
     * 创建时间
     */
    @ExcelField(value = "创建时间", writeConverter = TimeConverter.class)
    private Date createTime;

    /**
     * 修改时间
     */
    @ExcelField(value = "修改时间", writeConverter = TimeConverter.class)
    private Date modifyTime;

    /**
     * 最近访问时间
     */
    @ExcelField(value = "最近访问时间", writeConverter = TimeConverter.class)
    @JsonFormat(pattern = "yyyy年MM月dd日 HH时mm分ss秒", timezone = "GMT+8")
    private Date lastLoginTime;

    public String getId() {
        return StringUtils.lowerCase(username);
    }

    @Override
    public UserExcel clone() throws CloneNotSupportedException {
        return (UserExcel) super.clone();
    }

}
