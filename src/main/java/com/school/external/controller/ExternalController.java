package com.school.external.controller;

import java.io.*;
import java.util.*;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;

import com.school.gen.mapper.*;
import com.school.common.utils.SchoolUtil;
import com.school.gen.entity.*;
import com.school.gen.service.ICourseService;
import com.school.gen.service.impl.ExamFrequencyServiceImpl;
import com.school.system.mapper.CertificateMapper;
import com.school.system.mapper.UserMapper;
import com.google.zxing.WriterException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.school.common.annotation.ControllerEndpoint;
import com.school.common.annotation.Limit;
import com.school.common.controller.BaseController;
import com.school.common.entity.SchoolResponse;
import com.school.common.exception.SchoolException;
import com.school.common.utils.Md5Util;
import com.school.external.service.ExternalService;
import com.school.gen.service.ISubjectService;
import com.school.gen.service.IExamService;
import com.school.gen.service.IQuestionService;
import com.school.monitor.service.ILoginLogService;
import com.school.system.entity.User;
import com.school.system.service.IUserService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Validated
@Controller
@RequestMapping("external")
@RequiredArgsConstructor
public class ExternalController extends BaseController {

    private final ILoginLogService loginLogService;
    private final IUserService userService;
    private final ExternalService externalService;
    private final ISubjectService subjectService;
    private final IExamService examService;
    private final IQuestionService questionService;
    private final CourseMapper courseMapper;
    private final ICourseService courseService;
    private final PlayFinishRecordMapper playFinishRecordMapper;
    private final SubjectMapper subjectMapper;
    private final UserMapper userMapper;
    private final ExamFrequencyServiceImpl examFrequencyService;
    private final ExamFrequencyMapper examFrequencyMapper;
    private final CertificateMapper certificateMapper;
    private final ExamMapper examMapper;

    @PostMapping("login")
    @ResponseBody
    @Limit(key = "login", period = 60, count = 10, name = "登录接口", prefix = "limit")
    public SchoolResponse login(@NotBlank(message = "{required}") String mobile,
                                @NotBlank(message = "{required}") String password, boolean rememberMe, HttpServletRequest request)
            throws SchoolException {
        User user = userService.findByMobile(mobile);
        if (user != null) {
            UsernamePasswordToken token = new UsernamePasswordToken(user.getMobile(),
                    Md5Util.encrypt(user.getUsername().toLowerCase(), password), rememberMe);
            super.login(token);
            // 保存登录日志
            loginLogService.saveLoginLog(user.getUsername());
            userService.updateLoginTime(user.getUsername());
            return new SchoolResponse().success().data(user);
        } else {
            return new SchoolResponse().fail().data("用户不存在");
        }
    }

    @PostMapping("examLogin")
    @ResponseBody
    @Limit(key = "examLogin", period = 60, count = 10, name = "考试登录接口", prefix = "limit")
    public SchoolResponse examLogin(@NotBlank(message = "{required}") String idcard,
                                    @NotBlank(message = "{required}") String password, boolean rememberMe, HttpServletRequest request)
            throws SchoolException {
        User user = userService.findByIdcard(idcard);
        if (user != null) {
            UsernamePasswordToken token = new UsernamePasswordToken(user.getMobile(),
                    Md5Util.encrypt(user.getUsername().toLowerCase(), password), rememberMe);
            super.login(token);
            // 保存登录日志
            loginLogService.saveLoginLog(user.getUsername());
            userService.updateLoginTime(user.getUsername());
            return new SchoolResponse().success().data(user);
        } else {
            return new SchoolResponse().fail().data("用户不存在");
        }
    }

    @GetMapping("subjects")
    @ResponseBody
    public SchoolResponse listSubject() {
        return new SchoolResponse().success().data(subjectService.findSubjectTree());
    }

    @GetMapping("subject")
    @ResponseBody
    public SchoolResponse getSubject(@RequestParam(value = "user_id", required = true) long userId) {
        return externalService.getSubject(userId);
    }

    @GetMapping("courses")
    @ResponseBody
    public SchoolResponse listCourse(@RequestParam(value = "subject_id", required = true) long subjectId,
                                     @RequestParam(value = "user_id", required = true) long userId) {
        return externalService.listCourse(subjectId, userId);
    }

    @GetMapping("course")
    @ResponseBody
    public SchoolResponse getCourse(@RequestParam(value = "course_id", required = true) long courseId,
                                    @RequestParam(value = "user_id", required = true) long userId) {
        return externalService.getCourse(courseId, userId);
    }

    @GetMapping("collections")
    @ResponseBody
    public SchoolResponse listCollection(@RequestParam(value = "user_id", required = true) long userId) {
        return externalService.listCollection(userId);
    }

    @PostMapping("collection")
    @ResponseBody
    public SchoolResponse collection(@RequestParam(value = "user_id", required = true) long userId,
                                     @RequestParam(value = "obj_id", required = true) Integer objId) {
        return externalService.collection(userId, objId);
    }

    @GetMapping("user")
    @ResponseBody
    public SchoolResponse getUser(@RequestParam(value = "user_id", required = true) long userId) {
        System.out.println(userService.getById(userId).getPassword());
        return new SchoolResponse().success().data(userService.getById(userId));
    }

    @PutMapping("user")
    @ResponseBody
    public SchoolResponse updateUser(@RequestParam(value = "user_id", required = true) long userId,
                                     @RequestBody User user) {
        user.setUserId(userId);
        user.setPassword("111111");
        return externalService.updateUser(user);
    }

    // 图片上传测试
    @ResponseBody
    @PostMapping("upload/{photoType}")
    @ControllerEndpoint(exceptionMessage = "上传文件失败")
    public Map upload(MultipartFile file, @NotBlank(message = "{required}") @PathVariable String photoType,
                      @RequestParam(value = "user_id", required = true) long userId, HttpServletRequest request) {
        String prefix = "";
        // 保存上传
        OutputStream out = null;

        InputStream fileInput = null;
        try {
            if (file != null) {
                String originalName = file.getOriginalFilename();
                prefix = originalName.substring(originalName.lastIndexOf(".") + 1);
                String uuid = UUID.randomUUID() + "";
                String filepath = File.separator + "data" + File.separator + "SCHOOL" + File.separator + "upload"
                        + File.separator + photoType + File.separator + userId + File.separator + uuid + "." + prefix;

                File files = new File(filepath);
                // 打印查看上传路径
                System.out.println(filepath);
                if (!files.getParentFile().exists()) {
                    files.getParentFile().mkdirs();
                }
                file.transferTo(files);
                Map<String, Object> map2 = new HashMap<>();
                Map<String, Object> map = new HashMap<>();
                map.put("code", 0);
                map.put("msg", "");
                map.put("data", map2);
                map2.put("src", "/upload/" + photoType + "/" + userId + "/" + uuid + "." + prefix);
                return map;
            }

        } catch (Exception e) {
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (fileInput != null) {
                    fileInput.close();
                }
            } catch (IOException e) {
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("code", 1);
        map.put("msg", "");
        return map;

    }


    @PostMapping("play_record")
    @ResponseBody
    public SchoolResponse playRecord(@RequestBody PlayRecord playRecord) throws IOException, WriterException {
        Course course = courseMapper.findById(playRecord.getCourseId());
        if (course != null && course.getDuration() != null) {
            if (course.getDuration() < playRecord.getTime()) {
                playRecord.setTime(course.getDuration());
                PlayFinishRecord playFinishRecord = playFinishRecordMapper.findByUserIdAndCourseId(playRecord.getUserId(), playRecord.getCourseId());
                if (playFinishRecord == null) {
                    PlayFinishRecord playFinishRecord1 = new PlayFinishRecord();
                    playFinishRecord1.setUserId(playRecord.getUserId());
                    playFinishRecord1.setCourseId(playRecord.getCourseId());
                    this.externalService.savePlayFinishRecord(playFinishRecord1);
                }
            }
        }

        return externalService.savePlayRecord(playRecord);
    }

    @GetMapping("play_record")
    @ResponseBody
    public SchoolResponse listPlayRecord(@RequestParam(value = "user_id", required = true) long userId) throws IOException {
        return externalService.listPlayRecord(userId);
    }

    @PostMapping("face_detect")
    @ResponseBody
    public SchoolResponse faceDetect(@RequestBody FaceDetect faceDetect) {
        return externalService.saveFaceDetect(faceDetect);
    }

    @GetMapping("version_check")
    @ResponseBody
    public SchoolResponse getNewVersion() {
        return externalService.getNewVersion();
    }

    @GetMapping("download_apk")
    @ControllerEndpoint(exceptionMessage = "下载失败")
    public void downloadApk(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SchoolResponse schoolResponse = externalService.getNewVersion();
        Version version = (Version) schoolResponse.get("data");
        String filePath = File.separator + "SCHOOL" + version.getUrl();
        File file = new File(filePath);
        if (file.exists()) {
            // 设置响应头和客户端保存文件名
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/vnd.android.package-archive");
            response.setHeader("Content-Disposition", "attachment;fileName=" + "SCHOOL.apk");

            InputStream inputStream = new FileInputStream(file);
            // 激活下载操作
            OutputStream os = response.getOutputStream();
            // 循环写入输出流
            byte[] b = new byte[1024];
            int length;
            while ((length = inputStream.read(b)) > 0) {
                os.write(b, 0, length);
            }
            response.addHeader("Content-Length", "" + length);
            // 关闭。
            os.close();
            inputStream.close();
        }
    }

    @PostMapping("play_finish_record")
    @ResponseBody
    public SchoolResponse playFinishRecord(@RequestBody PlayFinishRecord playFinishRecord) throws IOException, WriterException {
        Course course = courseMapper.findById(playFinishRecord.getCourseId());

        return externalService.savePlayFinishRecord(playFinishRecord);
    }

    @GetMapping("exam_frequency")
    @ResponseBody
    public SchoolResponse getExamFrequency(@RequestParam(value = "user_id", required = true) long userId, @RequestParam(value = "course_id", required = true) long courseId) {
        return externalService.getExamFrequency(userId, courseId);
    }


    @GetMapping("qrCode")
    public String qrCode() throws IOException {
        return SchoolUtil.view("qrCode");
    }

    @PutMapping("update_exam_frequency_course")
    @ResponseBody
    public SchoolResponse updateExamFrequencyByCourse(@RequestParam(value = "user_id", required = true) long userId, @RequestParam(value = "course_id", required = true) long courseId) {
        return externalService.updateExamFrequencyByCourseId(userId, courseId);
    }

    @PutMapping("update_exam_frequency_subject")
    @ResponseBody
    public SchoolResponse updateExamFrequencyBySubject(@RequestParam(value = "user_id", required = true) long userId, @RequestParam(value = "subject_id", required = true) long subjectId) {
        return externalService.updateExamFrequencyBySubjectId(userId, subjectId);
    }

    @GetMapping("find_course")
    @ResponseBody
    public SchoolResponse finCourse(@RequestParam(value = "course_id", required = true) long courseId) {
        return new SchoolResponse().data(courseMapper.findById(courseId));
    }

    @GetMapping("find_exam")
    @ResponseBody
    public SchoolResponse findExam(@RequestParam(value = "user_id", required = true) long userId) {
        return externalService.getExam(userId);
    }

    @GetMapping("question")
    @ResponseBody
    public SchoolResponse listQuestion(@RequestParam(value = "course_id", required = true) long courseId) {
        return questionService.listQuestion(courseId);
    }

    @PostMapping("exam")
    @ResponseBody
    public SchoolResponse exam(@RequestBody Exam exam) {
        return externalService.saveExam(exam);
    }




    @GetMapping("question_play")
    @ResponseBody
    public SchoolResponse question(@RequestParam(value = "course_id", required = true) Long courseId, @RequestParam(value = "user_id", required = true) Long userId) {
        Course course = new Course();
        Long subjectId = null;
        if (courseId == -1) {
            String zhuyao = "主要负责人";
            subjectId = subjectMapper.findIdByTitle(zhuyao);
        } else {
            subjectId = courseMapper.findSubjectById(courseId);
        }
        Subject san = subjectMapper.findById(subjectId);
        Subject subject1 = new Subject();
        User user = userMapper.getById(userId);
        subject1 = subjectMapper.findById(subjectId);
        Subject ps = subjectMapper.findById(Long.valueOf(subject1.getPSubjectId()));
        if (subject1.getPSubjectTitle() != null) {
            subject1.setPSubjectTitle(ps.getTitle());
            String name = "三类人员继续教育";
            long id = subjectMapper.findIdByTitle(name);
            if (Long.valueOf(san.getPSubjectId()) == id) {
                course.setSubject(Long.valueOf(subjectId));
                List<Course> courses = courseService.findCourses(course);
                PlayFinishRecord playFinishRecord4 = new PlayFinishRecord();
                playFinishRecord4.setUserId(userId);
                List<PlayFinishRecord> playFinishRecords = playFinishRecordMapper.listPlayFinishRecords(playFinishRecord4);
                int num = 0;
                for (Course course1 : courses) {
                    for (PlayFinishRecord playFinishRecord1 : playFinishRecords) {
                        if (course1.getCourseId().longValue() == playFinishRecord1.getCourseId().longValue()) {
                            num += 1;
                        }
                    }
                }
                if (num == courses.size()) {
                    ExamFrequency examFrequency = new ExamFrequency();
                    examFrequency.setFrequency(2);
                    examFrequency.setCreator(user.getUsername());
                    examFrequency.setSubjectId(id);
                    examFrequency.setCreateTime(new Date());
                    examFrequency.setUserId(userId);
                    ExamFrequency eFExist = examFrequencyMapper.getByUserAndSubject(examFrequency);
                    if (eFExist == null) {
                        examFrequencyService.save(examFrequency);
                    }

                }
            } else {
                PlayFinishRecord playFinishRecord4 = new PlayFinishRecord();
                playFinishRecord4.setUserId(userId);

                playFinishRecord4.setCourseId(courseId);
                List<PlayFinishRecord> playFinishRecordList = playFinishRecordMapper.listPlayFinishRecords(playFinishRecord4);
                if (playFinishRecordList.size() != 0) {
                    ExamFrequency examFrequency = new ExamFrequency();
                    examFrequency.setFrequency(2);
                    examFrequency.setCreator(user.getUsername());
                    examFrequency.setCourseId(courseId);
                    examFrequency.setCreateTime(new Date());
                    examFrequency.setUserId(userId);
                    ExamFrequency eFExist = examFrequencyMapper.getByUserAndCourse(examFrequency);
                    if (eFExist == null) {
                        examFrequencyService.save(examFrequency);
                    }

                }
            }
        } else {
            PlayFinishRecord playFinishRecord4 = new PlayFinishRecord();
            playFinishRecord4.setUserId(userId);
            playFinishRecord4.setCourseId(courseId);
            List<PlayFinishRecord> playFinishRecordList = playFinishRecordMapper.listPlayFinishRecords(playFinishRecord4);
            if (playFinishRecordList.size() != 0) {
                ExamFrequency examFrequency = new ExamFrequency();
                examFrequency.setFrequency(2);
                examFrequency.setCreator(user.getUsername());
                examFrequency.setCourseId(courseId);
                examFrequency.setCreateTime(new Date());
                examFrequency.setUserId(userId);
                ExamFrequency eFExist = examFrequencyMapper.getByUserAndCourse(examFrequency);
                if (eFExist == null) {
                    examFrequencyService.save(examFrequency);
                }

            }
        }

        return new SchoolResponse().success().data(subject1);
    }

    @GetMapping("list/{courseId}")
    @ResponseBody
    public SchoolResponse questionList(@NotBlank(message = "{required}") @PathVariable String courseId) {
        Question question = new Question();
        question.setCourseId(Long.valueOf(courseId));
        List<Question> questions = questionService.findQuestions(question);
        return new SchoolResponse().success().data(questions);
    }

    @GetMapping("subList/{subjectId}")
    @ResponseBody
    public SchoolResponse questionSubList(@NotBlank(message = "{required}") @PathVariable String subjectId) {
        Subject subject = subjectMapper.findById(Long.valueOf(subjectId));
        return questionService.listSubQuestions(Long.valueOf(subject.getPSubjectId()));
    }




}

