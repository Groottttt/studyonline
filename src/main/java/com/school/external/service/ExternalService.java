package com.school.external.service;

import com.school.common.entity.SchoolResponse;
import com.school.gen.entity.Exam;
import com.school.gen.entity.FaceDetect;
import com.school.gen.entity.PlayFinishRecord;
import com.school.gen.entity.PlayRecord;
import com.school.system.entity.User;
import com.google.zxing.WriterException;

import java.io.IOException;

public interface ExternalService {

    SchoolResponse getSubject(long userId);

    SchoolResponse listCourse(long subjectId, long userId);

    SchoolResponse getCourse(long courseId, long userId);

    SchoolResponse listCollection(long userId);

    SchoolResponse collection(long userId, long objId);

    SchoolResponse updateUser(User user);

    SchoolResponse savePlayRecord(PlayRecord playRecord) throws IOException;

    SchoolResponse listPlayRecord(long userId) throws IOException;

    SchoolResponse saveFaceDetect(FaceDetect faceDetect);

    SchoolResponse getNewVersion();

    SchoolResponse savePlayFinishRecord(PlayFinishRecord playFinishRecord) throws IOException, WriterException;


    SchoolResponse getExam(long userId);

    SchoolResponse saveExam(Exam exam);

    SchoolResponse getExamFrequency(long userId, long courseId);


    SchoolResponse updateExamFrequencyByCourseId(long userId, long courseId);

    SchoolResponse updateExamFrequencyBySubjectId(long userId, long subjectId);
}
