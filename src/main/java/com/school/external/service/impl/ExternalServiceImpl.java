package com.school.external.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import com.coremedia.iso.IsoFile;

import com.school.gen.entity.*;
import com.school.gen.mapper.*;
import com.school.gen.service.impl.CourseServiceImpl;
import com.google.zxing.WriterException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.school.common.entity.SchoolResponse;
import com.school.common.utils.Md5Util;
import com.school.external.service.ExternalService;
import com.school.system.entity.User;
import com.school.system.mapper.UserMapper;
import com.school.system.service.impl.UserServiceImpl;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ExternalServiceImpl implements ExternalService {

    private final SubjectMapper subjectMapper;
    private final UserMapper userMapper;
    private final CourseMapper courseMapper;
    private final PlayRecordMapper playRecordMapper;
    private final CollectionMapper collectionMapper;
    private final FaceDetectMapper faceDetectMapper;
    private final VersionMapper versionMapper;
    private final PlayFinishRecordMapper playFinishRecordMapper;
    private final UserServiceImpl userService;
    private final ExamMapper examMapper;
    private final CourseServiceImpl courseService;
    private final SubExamMapper subExamMapper;
    private final ExamFrequencyMapper examFrequencyMapper;
    private final DictMapper dictMapper;


    @Override
    public SchoolResponse getSubject(long userId) {
        User user = userMapper.getById(userId);
        if (user == null) {
            return new SchoolResponse().fail().data("用户不存在");
        }
        if (user.getSubjectId() == null) {
            return new SchoolResponse().fail().data("用户暂未配置科目");
        }
        Subject subject = subjectMapper.findById(Long.parseLong(user.getSubjectId()));
        List<Subject> subjects2 = subjectMapper.listByPublic(true);
        for (Subject subject2 : subjects2) {
            if (subject.getTitle().equals(subject2.getTitle())) {
                return new SchoolResponse().success().data(subjects2);
            }
        }
        List<Subject> subjects = new ArrayList<>();
        subjects.add(subject);
        subjects.addAll(subjects2);
        return new SchoolResponse().success().data(subjects);
    }

    @Override
    public SchoolResponse listCourse(long subjectId, long userId) {
        Subject subject = subjectMapper.findById(subjectId);

        if (subject == null) {
            return new SchoolResponse().fail().data("科目不存在");
        }
        Course course = new Course();
        course.setSubject(subjectId);
        course.setCollectionUser(userId);
        List<Course> courses = courseMapper.listCourseDetail(course);
        return new SchoolResponse().success().data(courses);
    }

    @Override
    public SchoolResponse getCourse(long courseId, long userId) {
        Course courseParam = new Course();
        courseParam.setCourseId(courseId);
        courseParam.setCollectionUser(userId);
        Course course = courseMapper.findCourseByIdAndUserId(courseParam);
        PlayRecord playRecordParam = new PlayRecord();
        playRecordParam.setUserId(userId);
        playRecordParam.setCourseId(courseId);
        PlayRecord playRecord = playRecordMapper.getLastPlayRecord(playRecordParam);
        if (playRecord != null) {
            course.setPlayTime(playRecord.getTime());
        }
        return new SchoolResponse().success().data(course);
    }

    @Override
    public SchoolResponse listCollection(long userId) {
        List<Course> courses = courseMapper.listCollectionCourse(userId);
        return new SchoolResponse().success().data(courses);
    }

    @Override
    public SchoolResponse collection(long userId, long objId) {
        User user = userMapper.getById(userId);
        if (user == null) {
            return new SchoolResponse().fail().data("用户不存在");
        }
        Course course = courseMapper.findById(objId);
        if (course == null) {
            return new SchoolResponse().fail().data("课程不存在");
        }
        CollectionEntity collection = collectionMapper.getById(userId, objId);
        if (collection == null) {
            collection = new CollectionEntity();
            collection.setObjId(objId);
            collection.setUserId(userId);
            collectionMapper.insert(collection);
        } else {
            collectionMapper.delete(userId, objId);
        }

        return new SchoolResponse().success();
    }

    @Override
    public SchoolResponse updateUser(User user) {
        User user1 = userMapper.getById(user.getUserId());
        if (StringUtils.isNotBlank(user.getIdcard())) {
            user1.setIdcard(user.getIdcard());
        }
        if (StringUtils.isNotBlank(user.getSubjectId())) {
            user1.setSubjectId(user.getSubjectId());
        }
        if (StringUtils.isNotBlank(user.getOneInchPhoto())) {
            user1.setOneInchPhoto(user.getOneInchPhoto());
        }
        if (StringUtils.isNotBlank(user.getIdcardFront())) {
            user1.setIdcardFront(user.getIdcardFront());
        }
        if (StringUtils.isNotBlank(user.getIdcardNegative())) {
            user1.setIdcardNegative(user.getIdcardNegative());
        }
        if (StringUtils.isNotBlank(user.getCertificateNumber())) {
            user1.setCertificateNumber(user.getCertificateNumber());
        }
        if (StringUtils.isNotBlank(user.getNickname())) {
            user1.setNickname(user.getNickname());
        }
        if (StringUtils.isNotBlank(user.getAvatar())) {
            user1.setAvatar(user.getAvatar());
        }
        if (StringUtils.isNotBlank(user.getPassword())) {
            user1.setPassword(Md5Util.encrypt(user1.getUsername().toLowerCase(), user.getPassword()));
        }
        if (StringUtils.isNotBlank(user.getNationality())) {
            user1.setNationality(user.getNationality());
        }
        if (StringUtils.isNotBlank(user.getFaceCollection())) {
            user1.setFaceCollection(user.getFaceCollection());
        }
        user1.setModifyTime(new Date());
        userMapper.updateById(user1);
        return new SchoolResponse().success();
    }

    @Override
    public SchoolResponse savePlayRecord(PlayRecord playRecord) throws IOException {
        Course course1 = courseMapper.findById(playRecord.getCourseId());
        if (course1.getDuration() == null) {
            if (course1.getVideourl().contains("mp4")) {
                IsoFile isoFile = new IsoFile(File.separator + "data" + File.separator + "SCHOOL" + course1.getVideourl());
                long date = isoFile.getMovieBox().getMovieHeaderBox().getDuration();
                course1.setDuration(date);
                courseService.updateCourse(course1);
            }
        }
        PlayFinishRecord playFinishRecord = playFinishRecordMapper.findByUserIdAndCourseId(playRecord.getUserId(), playRecord.getCourseId());
        if (playFinishRecord == null) {
            playRecord.setCreateTime(new Date());
            PlayRecord p1 = playRecordMapper.findByUserIdAndCourseId(playRecord.getUserId(),playRecord.getCourseId());
            if (p1!=null){
                if (p1.getTime()<=playRecord.getTime()){
                    playRecordMapper.deleteByUseridAndCourseId(playRecord);
                    playRecordMapper.insert(playRecord);
                }
            }
            else{
                playRecordMapper.insert(playRecord);
            }
        } else {
            playRecord.setCreateTime(new Date());
            playRecord.setTime(course1.getDuration() + 59);
            playRecordMapper.deleteByUseridAndCourseId(playRecord);
            playRecordMapper.insert(playRecord);
        }
        return new SchoolResponse().success();
    }

    @Override
    public SchoolResponse listPlayRecord(long userId) throws IOException {
        PlayRecord playRecord = new PlayRecord();
        playRecord.setUserId(userId);
        List<PlayRecord> playRecords = playRecordMapper.listPlayRecords(playRecord);
        List<PlayRecord> playRecords1 = new ArrayList<>();
        for (PlayRecord playRecord1 : playRecords) {
            Course course = courseMapper.findById(playRecord1.getCourseId());
            if (course.getVideourl().contains("mp4")) {
                IsoFile isoFile = new IsoFile(File.separator + "data" + File.separator + "SCHOOL" + course.getVideourl());
                long lengthInSeconds =
                        isoFile.getMovieBox().getMovieHeaderBox().getDuration();
                playRecord1.setDuration(lengthInSeconds + 59);
                playRecords1.add(playRecord1);
            }
        }
        return new SchoolResponse().success().data(playRecords1);
    }

    @Override
    public SchoolResponse saveFaceDetect(FaceDetect faceDetect) {
        faceDetect.setCreateTime(new Date());
        faceDetectMapper.insert(faceDetect);
        return new SchoolResponse().success();
    }

    @Override
    public SchoolResponse getNewVersion() {
        Version version = versionMapper.getLastVersion();
        return new SchoolResponse().success().data(version);
    }

    @Override
    public SchoolResponse savePlayFinishRecord(PlayFinishRecord playFinishRecord)  {
        playFinishRecord.setCreateTime(new Date());
        playFinishRecordMapper.insert(playFinishRecord);
        return new SchoolResponse().success();
    }

    @Override
    public SchoolResponse getExam(long userId) {
        User user = userMapper.getById(userId);
        if (user == null) {
            return new SchoolResponse().fail().data("用户不存在");
        }
        List<Exam> exam = examMapper.findCourseId(userId);
        if (exam.size() == 0) {
            return new SchoolResponse().fail().data("该用户没有考试");
        }
        List<Exam> exams = examMapper.findExamById(userId);
        for (Exam e : exams
        ) {
            System.out.println(e);
            if (e.getSubject().getPSubjectId()!=0){
                Subject ps = subjectMapper.findById(Long.valueOf(e.getSubject().getPSubjectId()));
                e.getSubject().setPSubjectTitle(ps.getTitle());
            }
        }
        return new SchoolResponse().success().data(exams);
    }

    @Override
    public SchoolResponse saveExam(Exam exam) {

        exam.setCreateTime(new Date());
        examMapper.insert(exam);
        long subjectId;
        if (exam.getCourseId()==-1){

            String zhuyao = "主要负责人";
            subjectId = subjectMapper.findIdByTitle(zhuyao);
        }else {
            subjectId = subjectMapper.findByCourseId(exam.getCourseId());
        }


        Subject subject = subjectMapper.findById(subjectId);
        String name = "三类人员继续教育";
        Long id = subjectMapper.findIdByTitle(name);
        if (Long.valueOf(subject.getPSubjectId()).equals(id)) {
            updateExamFrequencyBySubjectId(exam.getUserId(), id);
        } else {
            updateExamFrequencyByCourseId(exam.getUserId(), exam.getCourseId());
        }
//        for (SubExam subExam : exam.getSubExams()) {
//            subExam.setExamId(exam.getId());
//            subExamMapper.insert(subExam);
//        }

        return new SchoolResponse().success();
    }


    @Override
    public SchoolResponse getExamFrequency(long userId, long courseId) {
        int examF = 0;
        String name = "三类人员继续教育";
        Long id = subjectMapper.findIdByTitle(name);
        Subject subject = new Subject();
        if (courseId==-1){
            subject = subjectMapper.findById(id);
        }else {
            long subjectId = subjectMapper.findByCourseId(courseId);
            subject = subjectMapper.findById(subjectId);
        }
        if (Long.valueOf(subject.getSubjectId()).equals(id)) {
            ExamFrequency examFrequencyParam = new ExamFrequency();
            examFrequencyParam.setUserId(userId);
            examFrequencyParam.setSubjectId(Long.valueOf(subject.getSubjectId()));
            ExamFrequency examFrequency = examFrequencyMapper.getByUserAndSubject(examFrequencyParam);
            if (examFrequency != null) {
                examF = examFrequency.getFrequency();
            } else {
                examF = 0;
            }
        } else {
            ExamFrequency examFrequencyParam = new ExamFrequency();
            examFrequencyParam.setUserId(userId);
            examFrequencyParam.setCourseId(courseId);
            ExamFrequency examFrequency = examFrequencyMapper.getByUserAndCourse(examFrequencyParam);
            if (examFrequency != null) {
                examF = examFrequency.getFrequency();
            } else {
                examF = 0;
            }
        }


        return new SchoolResponse().success().data(examF);

    }

    @Override
    public SchoolResponse updateExamFrequencyByCourseId(long userId, long courseId) {
        this.examFrequencyMapper.updateExamFrequencyByCourseId(userId, courseId);
        return new SchoolResponse().success();
    }

    @Override
    public SchoolResponse updateExamFrequencyBySubjectId(long userId, long subjectId) {
        this.examFrequencyMapper.updateExamFrequencyBySubjectId(userId, subjectId);
        return new SchoolResponse().success();
    }
}

