package com.school.gen.constant;

public class DictConsts {
    //考试机会
    public static final String EXAM_OPPORTUNITY="exam_opportunity";

    //考试时长（mins）
    public static final String EXAM_TIME="exam_time";

    //题量
    public static final String QUESTION_QUANTITY="question_quantity";
}
