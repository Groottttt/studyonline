package com.school.gen.controller;

import com.coremedia.iso.IsoFile;
import com.school.common.annotation.ControllerEndpoint;
import com.school.common.utils.DateUtil;
import com.school.common.utils.FileConfiguration;
import com.school.common.controller.BaseController;
import com.school.common.entity.SchoolResponse;
import com.school.common.entity.QueryRequest;
import com.school.common.entity.Strings;
import com.school.common.exception.SchoolException;
import com.school.gen.entity.Course;
import com.school.gen.service.ICourseService;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 课程表 Controller
 *
 * @author MrBird
 * @date 2021-08-22 00:23:17
 */
@Slf4j
@Validated
@Controller
@RequestMapping("course")
@RequiredArgsConstructor
public class CourseController extends BaseController {

    @Autowired
    private FileConfiguration fileConfiguration;
    private final ICourseService courseService;

    @GetMapping
    @ResponseBody
    @RequiresPermissions("course:view")
    public SchoolResponse getAllCourses(Course course) {
        return new SchoolResponse().success().data(courseService.findCourses(course));
    }

    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("course:view")
    public SchoolResponse courseList(QueryRequest request, Course course) {
        Map<String, Object> dataTable = getDataTable(this.courseService.findCourses(request, course));
        return new SchoolResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增Course", exceptionMessage = "新增Course失败")
    @ResponseBody
    @PostMapping
    @RequiresPermissions("course:add")
    public SchoolResponse addCourse(@Valid Course course) {
        this.courseService.createCourse(course);
        return new SchoolResponse().success();
    }

    @ControllerEndpoint(operation = "删除Course", exceptionMessage = "删除Course失败")
    @GetMapping("delete/{courseIds}")
    @ResponseBody
    @RequiresPermissions("course:delete")
    public SchoolResponse deleteCourse(@NotBlank(message = "{required}") @PathVariable String courseIds) {
        this.courseService.deleteCourses(StringUtils.split(courseIds, Strings.COMMA));
        return new SchoolResponse().success();
    }

    @ControllerEndpoint(operation = "修改Course", exceptionMessage = "修改Course失败")
    @PostMapping("update")
    @ResponseBody
    @RequiresPermissions("course:update")
    public SchoolResponse updateCourse(Course course) {
        this.courseService.updateCourse(course);
        return new SchoolResponse().success();
    }

    @ControllerEndpoint(operation = "修改Course", exceptionMessage = "导出Excel失败")
    @GetMapping("excel")
    @ResponseBody
    @RequiresPermissions("course:export")
    public void export(QueryRequest queryRequest, Course course, HttpServletResponse response) {
        List<Course> courses = this.courseService.findCourses(queryRequest, course).getRecords();
        ExcelKit.$Export(Course.class, response).downXlsx(courses, false);
    }

    @ResponseBody
    @PostMapping("upload")
    @RequiresPermissions("course:upload")
    @ControllerEndpoint(exceptionMessage = "上传文件失败")
    public SchoolResponse upload(MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new SchoolException("上传文件为空");
        }
        String filename = file.getOriginalFilename();
        String uuid = UUID.randomUUID() + "";
        String prefix = filename.substring(filename.lastIndexOf(".") + 1);
        // 开始导入操作
        String rootPath = fileConfiguration.getResourceDir();
        String currentDate = DateUtil.formatFullTime(LocalDateTime.now(), DateUtil.DAY_TIME_SPLIT_PATTERN);
        String fileType = file.getContentType() == null ? "others" : file.getContentType().split("/")[0] + "s";
        File directory = new File(rootPath + File.separator + "upload" + File.separator + "course" + File.separator
                + fileType + File.separator + currentDate);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        String absolutePath = directory.getAbsolutePath(); // 获取绝对路径
        File uploadpath = new File(absolutePath + File.separator + uuid + "." + prefix);
        System.out.println(uploadpath);
        file.transferTo(uploadpath);
        Stopwatch stopwatch = Stopwatch.createStarted();
        IsoFile isoFile = new IsoFile(File.separator + "data" + File.separator + "SCHOOL" +File.separator + "upload" + File.separator + "course" + File.separator + fileType + File.separator
                + currentDate + File.separator + uuid + "." + prefix);
        long lengthInSeconds =
                isoFile.getMovieBox().getMovieHeaderBox().getDuration();
        ImmutableMap<String, Object> result = ImmutableMap.of("filepath",
                File.separator + "upload" + File.separator + "course" + File.separator + fileType + File.separator
                        + currentDate + File.separator + uuid + "." + prefix,
                "name", filename, "filesize", file.getSize(), "duration",lengthInSeconds,"time", stopwatch.stop().toString());
        return new SchoolResponse().success().data(result);
    }

    @ResponseBody
    @GetMapping("select/{subjectId}")
    @ControllerEndpoint(exceptionMessage = "获取课程失败")
    public SchoolResponse getCourse(@NotBlank(message = "{required}") @PathVariable String subjectId)
            throws SchoolException {
        Course course = new Course();
        course.setSubject(Long.valueOf(subjectId));
        return new SchoolResponse().success().data(courseService.findCourses(course));
    }
}
