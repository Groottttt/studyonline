package com.school.gen.controller;

import com.school.common.annotation.ControllerEndpoint;
import com.school.common.controller.BaseController;
import com.school.common.entity.SchoolResponse;
import com.school.common.entity.QueryRequest;
import com.school.common.entity.Strings;
import com.school.common.exception.SchoolException;
import com.school.gen.entity.Course;
import com.school.gen.entity.CourseType;
import com.school.gen.service.ICourseTypeService;
import com.school.system.entity.ClassPojo;
import com.school.system.entity.Dept;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * 部门表 Controller
 *
 * @author MrBird
 * @date 2021-08-25 10:18:37
 */
@Slf4j
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("courseType")
public class CourseTypeController extends BaseController {

    private final ICourseTypeService courseTypeService;

    @GetMapping("list")
    public SchoolResponse courseTypeList(CourseType courseType, QueryRequest request) {
        return new SchoolResponse().success().data(getDataTable(courseTypeService.courseTypeList(courseType, request)));
    }

    @ResponseBody
    @PostMapping("add")
    public SchoolResponse addCourseType(@Valid CourseType courseType) {
        return courseTypeService.addCourseType(courseType);

    }
    @GetMapping("delete/{courseTypeIds}")
    @ControllerEndpoint(operation = "删除用户", exceptionMessage = "删除用户失败")
    public SchoolResponse deleteCourseType(@NotBlank(message = "{required}") @PathVariable String courseTypeIds) {
        return courseTypeService.deleteCourseType(StringUtils.split(courseTypeIds, Strings.COMMA));
    }
}
