package com.school.gen.controller;


import com.school.external.service.impl.ExternalServiceImpl;
import com.school.gen.mapper.ExamFrequencyMapper;
import com.google.zxing.WriterException;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.school.common.annotation.ControllerEndpoint;
import com.school.common.controller.BaseController;
import com.school.common.entity.SchoolResponse;
import com.school.gen.entity.ExamFrequency;
import com.school.gen.service.IExamFrequencyService;
import com.school.system.entity.User;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
@Validated
@Controller
@RequestMapping("exam_frequency")
@RequiredArgsConstructor
public class ExamFrequencyController extends BaseController {


    private final IExamFrequencyService examFrequencyService;


    @ControllerEndpoint(operation = "新增考试机会", exceptionMessage = "新增考试机会失败")
    @ResponseBody
    @PostMapping
    @RequiresPermissions("user:addExamFrequency")
    public SchoolResponse addExamFrequency(ExamFrequency examFrequency) throws IOException, WriterException {

        User user = getCurrentUser();
        examFrequency.setCreator(user.getUsername());

        if (!this.examFrequencyService.createExamFrequency(examFrequency)){
            return new SchoolResponse().fail().message("新增考试机会失败,未看完所有视频");
        }

        int examF = 0;
        if (examFrequency.getCourseId() != null) {
            examFrequency = this.examFrequencyService.findByUserAndCourse(examFrequency);
        } else {
            examFrequency = this.examFrequencyService.findByUserAndSubject(examFrequency);
        }
        examF = examFrequency.getFrequency();
        return new SchoolResponse().success().data(examF);
    }

}
