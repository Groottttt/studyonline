package com.school.gen.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.school.common.annotation.ControllerEndpoint;
import com.school.common.controller.BaseController;
import com.school.common.entity.SchoolResponse;
import com.school.common.entity.QueryRequest;
import com.school.gen.entity.FaceDetect;
import com.school.gen.service.IFaceDetectService;
import com.wuwenze.poi.ExcelKit;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Validated
@Controller
@RequestMapping("face_detect")
@RequiredArgsConstructor
public class FaceDetectController extends BaseController {

    private final IFaceDetectService faceDetectService;

    @GetMapping
    @ResponseBody
    @RequiresPermissions("face_detect:view")
    public SchoolResponse getAllSubjects(FaceDetect faceDetect) {
        return new SchoolResponse().success().data(faceDetectService.findFaceDetects(faceDetect));
    }

    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("face_detect:view")
    public SchoolResponse subjectList(QueryRequest request, FaceDetect faceDetect) {
        Map<String, Object> dataTable = getDataTable(this.faceDetectService.findFaceDetects(request, faceDetect));
        return new SchoolResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "导出", exceptionMessage = "导出Excel失败")
    @GetMapping("excel")
    @ResponseBody
    @RequiresPermissions("face_detect:export")
    public void export(QueryRequest queryRequest, FaceDetect faceDetect, HttpServletResponse response) {
        List<FaceDetect> faceDetects = this.faceDetectService.findFaceDetects(queryRequest, faceDetect).getRecords();
        ExcelKit.$Export(FaceDetect.class, response).downXlsx(faceDetects, false);
    }
}
