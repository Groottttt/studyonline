package com.school.gen.controller;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.school.common.exception.SchoolException;
import com.school.common.utils.DateUtil;
import com.school.common.utils.FileConfiguration;
import com.school.gen.entity.*;
import com.school.gen.mapper.SubjectMapper;
import com.school.gen.service.ISubjectService;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.school.common.annotation.ControllerEndpoint;
import com.school.common.controller.BaseController;
import com.school.common.entity.SchoolResponse;
import com.school.common.entity.QueryRequest;
import com.school.common.entity.Strings;
import com.school.gen.service.ICourseService;
import com.school.gen.service.IQuestionService;
import com.school.system.entity.User;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Validated
@Controller
@RequestMapping("question")
@RequiredArgsConstructor
public class QuestionController extends BaseController {
    private final IQuestionService questionService;
    private final ICourseService courseService;
    @Autowired
    private FileConfiguration fileConfiguration;
    private final ISubjectService subjectService;
    private SubjectMapper subjectMapper;

    @GetMapping
    @ResponseBody
    @RequiresPermissions("question:view")
    public SchoolResponse getAllCourses(Course course) {
        return new SchoolResponse().success().data(courseService.findCourses(course));
    }

    @GetMapping("append")
    @ResponseBody
    @RequiresPermissions("question:upload")
    public SchoolResponse questionAppend(Long subject, Long courseId, String path) throws IOException {
        User user = getCurrentUser();
        this.questionService.uploadQuestion(subject,courseId,path, user.getUsername());
        return new SchoolResponse().success();
    }


    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("question:view")
    public SchoolResponse courseList(QueryRequest request, Course course, Subject subject) {
        Map<String, Object> result1 = this.courseService.findCoursesHasQuestion(request, course, subject);
        Map<String, Object> dataTable1 = getDataTable((IPage) result1.get("ipage"));
        MyHashMap<String, Object> dataTable = new MyHashMap<String, Object>();
        dataTable1.put("total", result1.get("pages"));
        return new SchoolResponse().success().data(dataTable1);
    }

    @ControllerEndpoint(operation = "新增Question", exceptionMessage = "新增Question失败")
    @PostMapping
    @ResponseBody
    @RequiresPermissions("question:add")
    public SchoolResponse addQuestion(@Valid Question question) {
        User user = getCurrentUser();
        question.setCreator(user.getUsername());
        this.questionService.createQuestion(question);
        return new SchoolResponse().success();
    }

    @ControllerEndpoint(operation = "编辑Question", exceptionMessage = "编辑Question失败")
    @PostMapping("update")
    @ResponseBody
    @RequiresPermissions("question:update")
    public SchoolResponse updateQuestion(Question question) {
        this.questionService.updateQuestion(question);
        return new SchoolResponse().success();
    }

    @ControllerEndpoint(operation = "删除Question", exceptionMessage = "删除Question失败")
    @GetMapping("delete/{questionIds}")
    @ResponseBody
    @RequiresPermissions("question:delete")
    public SchoolResponse deleteSubject(@NotBlank(message = "{required}") @PathVariable String questionIds) {

        return this.questionService.deleteQuestion(StringUtils.split(questionIds, Strings.COMMA));
    }

    @ControllerEndpoint(operation = "删除Question", exceptionMessage = "删除Question失败")
    @GetMapping("delete")
    @ResponseBody
    @RequiresPermissions("question:delete")
    public SchoolResponse deleteSubjectByCourseId(@RequestParam(value = "course_id", required = true) Long courseId) {

        return this.questionService.deleteByCourse(courseId);
    }

    @GetMapping("list/{courseId}")
    @ResponseBody
    public SchoolResponse questionList(@NotBlank(message = "{required}") @PathVariable String courseId) {
        Question question = new Question();
        question.setCourseId(Long.valueOf(courseId));
        List<Question> questions = questionService.findQuestions(question);
        return new SchoolResponse().success().data(questions);
    }

    @GetMapping("subList/{subjectId}")
    @ResponseBody
    public SchoolResponse questionSubList(@NotBlank(message = "{required}") @PathVariable String subjectId) {
        Question question = new Question();
        question.setSubject(Long.valueOf(subjectId));
        List<Question> questions = questionService.findQuestions(question);
        System.out.println(questions);
        return new SchoolResponse().success().data(questions);
    }


    @PostMapping("upload")
    @ResponseBody
    public SchoolResponse uploadQuestion(@RequestPart(value = "file") MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new SchoolException("上传文件为空");
        }
        String filename = file.getOriginalFilename();
        String uuid = UUID.randomUUID() + "";
        String prefix = filename.substring(filename.lastIndexOf(".") + 1);

        // 开始导入操作
        String rootPath = fileConfiguration.getResourceDir();
        String currentDate = DateUtil.formatFullTime(LocalDateTime.now(), DateUtil.DAY_TIME_SPLIT_PATTERN);
        String fileType = file.getContentType() == null ? "others" : file.getContentType().split("/")[0] + "s";
        File directory = new File(rootPath + File.separator + "upload" + File.separator + "question" + File.separator
                + fileType + File.separator + currentDate);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        String absolutePath = directory.getAbsolutePath(); // 获取绝对路径
        File uploadpath = new File(absolutePath + File.separator + uuid + "." + prefix);
        String path = absolutePath + File.separator + uuid + "." + prefix;
        file.transferTo(uploadpath);
        ImmutableMap<String, Object> result = ImmutableMap.of("filepath", path);
        return new SchoolResponse().success().data(result);
    }

}
