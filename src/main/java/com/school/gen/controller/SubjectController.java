package com.school.gen.controller;

import com.school.common.annotation.ControllerEndpoint;
import com.school.common.controller.BaseController;
import com.school.common.entity.SchoolResponse;
import com.school.common.entity.QueryRequest;
import com.school.common.entity.Strings;
import com.school.common.exception.SchoolException;
import com.school.gen.entity.Subject;
import com.school.gen.service.ISubjectService;
import com.school.system.entity.User;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Controller
 *
 * @author MrBird
 * @date 2021-08-25 14:51:20
 */
@Slf4j
@Validated
@Controller
@RequestMapping("subject")
@RequiredArgsConstructor
public class SubjectController extends BaseController {

    private final ISubjectService subjectService;


    @GetMapping
    @ResponseBody
    @RequiresPermissions("subject:view")
    public SchoolResponse getAllSubjects(Subject subject) {
        return new SchoolResponse().success().data(subjectService.findSubjects(subject));
    }

    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("subject:view")
    public SchoolResponse subjectList(QueryRequest request, Subject subject) {
        Map<String, Object> dataTable = getDataTable(this.subjectService.findSubjects(request, subject));
        return new SchoolResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增Subject", exceptionMessage = "新增Subject失败")
    @PostMapping
    @ResponseBody
    @RequiresPermissions("subject:add")
    public SchoolResponse addSubject(@Valid Subject subject) {
        User user = getCurrentUser();
        subject.setCreator(user.getUsername());
        this.subjectService.createSubject(subject);
        return new SchoolResponse().success();
    }

    @ControllerEndpoint(operation = "删除Subject", exceptionMessage = "删除Subject失败")
    @GetMapping("delete/{subjectIds}")
    @ResponseBody
    @RequiresPermissions("subject:delete")
    public SchoolResponse deleteSubject(@NotBlank(message = "{required}") @PathVariable String subjectIds) {

        return this.subjectService.deleteSubject(StringUtils.split(subjectIds, Strings.COMMA));
    }

    @ControllerEndpoint(operation = "修改Subject", exceptionMessage = "修改Subject失败")
    @PostMapping("update")
    @ResponseBody
    @RequiresPermissions("subject:update")
    public SchoolResponse updateSubject(Subject subject) {
        this.subjectService.updateSubject(subject);
        return new SchoolResponse().success();
    }

    @ControllerEndpoint(operation = "导出Subject", exceptionMessage = "导出Excel失败")
    @GetMapping("excel")
    @ResponseBody
    @RequiresPermissions("subject:export")
    public void export(QueryRequest queryRequest, Subject subject, HttpServletResponse response) {
        List<Subject> subjects = this.subjectService.findSubjects(queryRequest, subject).getRecords();
        ExcelKit.$Export(Subject.class, response).downXlsx(subjects, false);
    }

    @GetMapping("list/pid")
    @ResponseBody
    @RequiresPermissions("subject:view")
    public SchoolResponse subjectListByPid(QueryRequest request,
                                           @RequestParam(value = "subject_id", required = true) Long subjectId) {
        List<Subject> subjects = this.subjectService.findSubjectsByPid(subjectId);
        return new SchoolResponse().success().data(subjects);
    }

    // 图片上传测试
    @ResponseBody
    @PostMapping("upload")
    @ControllerEndpoint(exceptionMessage = "上传文件失败")
    public Map upload(MultipartFile file, HttpServletRequest request) {
        String prefix = "";
        String dateStr = "";
        // 保存上传
        OutputStream out = null;
        InputStream fileInput = null;
        try {
            if (file != null) {
                String originalName = file.getOriginalFilename();
                prefix = originalName.substring(originalName.lastIndexOf(".") + 1);
                Date date = new Date();
                String uuid = UUID.randomUUID() + "";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                dateStr = simpleDateFormat.format(date);
                String filepath = File.separator + "data" + File.separator + "SCHOOL" + File.separator + "upload"
                        + File.separator + "subject" + File.separator + dateStr + File.separator + uuid + "." + prefix;

                File files = new File(filepath);
                // 打印查看上传路径
                System.out.println(filepath);
                if (!files.getParentFile().exists()) {
                    files.getParentFile().mkdirs();
                }
                file.transferTo(files);
                Map<String, Object> map2 = new HashMap<>();
                Map<String, Object> map = new HashMap<>();
                map.put("code", 0);
                map.put("msg", "");
                map.put("data", map2);
                map2.put("src", "/upload/subject/" + dateStr + "/" + uuid + "." + prefix);
                return map;
            }

        } catch (Exception e) {
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (fileInput != null) {
                    fileInput.close();
                }
            } catch (IOException e) {
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("code", 1);
        map.put("msg", "");
        return map;

    }

    @ResponseBody
    @GetMapping("select/tree")
    @ControllerEndpoint(exceptionMessage = "获取科目树失败")
    public SchoolResponse getCourseTypeTree() throws SchoolException {
        return new SchoolResponse().success().data(subjectService.findSubjectTree());
    }


}
