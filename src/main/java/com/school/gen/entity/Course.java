package com.school.gen.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotBlank;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.school.common.converter.TimeConverter;
import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;

/**
 * 课程表 Entity
 *
 * @author MrBird
 * @date 2021-08-22 00:23:17
 */
@Data
@TableName("t_course")
@Excel("课程视频信息表")
public class Course implements Serializable {

    /**
     * 状态：有效
     */
    public static final String STATUS_VALID = "1";
    /**
     * 状态：锁定
     */
    public static final String STATUS_LOCK = "0";

    private static final long serialVersionUID = 4352868070896565001L;

    /**
     * 课程ID
     */
    @TableId(value = "COURSE_ID", type = IdType.AUTO)
    @ExcelField(value = "ID")
    private Long courseId;

    /**
     * 课程名称
     */
    @TableField("COURSE_NAME")
    @ExcelField(value = "名称")
    private String courseName;

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * 课程标题
     */
    @TableField("COURSE_TITLE")
    private String courseTitle;

    /**
     * 课程分类
     */
    @TableField("SUBJECT")
    private Long subject;

    /**
     * 分类名称
     */
    @ExcelField(value = "科目")
    @TableField(exist = false)
    private String subjectName;

    /**
     * 状态 0锁定 1有效
     */
    @TableField("STATUS")
    @NotBlank(message = "{required}")
    @ExcelField(value = "是否有效", writeConverterExp = "0=锁定,1=有效")
    private String status;

    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    @ExcelField(value = "创建时间", writeConverter = TimeConverter.class)
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField("MODIFY_TIME")
    @ExcelField(value = "修改时间", writeConverter = TimeConverter.class)
    private Date modifyTime;

    /**
     * 描述
     */
    @TableField("DESCRIPTION")
    @ExcelField(value = "描述")
    private String description;

    /**
     * 缩略图
     */
    @TableField("THUMBNAIL")
    @ExcelField(value = "缩略图路径")
    private String thumbnail;

    /**
     * 视频URL
     */
    @TableField("VIDEOURL")
    @ExcelField(value = "视频路径")
    private String videourl;

    /**
     * 视频大小
     */
    @TableField("VIDEOSIZE")
    private Long videosize;

    @TableField("DURATION")
    private Long duration;
    /**
     * 是否是文章
     */
    @TableField("isarticle")
    private String isarticle;
    /**
     * 文章内容
     */
    @TableField("aryiclecontent")
    private String aryiclecontent;


    @TableField(exist = false)
    @ExcelField(value = "视频大小")
    private String videoSizeHuman;

    @TableField(exist = false)
    private String createTimeFrom;

    @TableField(exist = false)
    private String createTimeTo;

    @TableField(exist = false)
    private Long collectionUser;

    @TableField(exist = false)
    private Long playTime;

    @TableField("PRICE")
    @ExcelField(value = "价格")
    private BigDecimal price;
}
