package com.school.gen.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 部门表 Entity
 *
 * @author MrBird
 * @date 2021-08-25 10:18:37
 */
@Data
@TableName("t_course_type")
public class CourseType implements Serializable {

	public static final Long TOP_NODE = 0L;
	private static final long serialVersionUID = 5702271568363798327L;

	/**
	 * 课程类型ID
	 */
	@TableId(value = "CTYPE_ID", type = IdType.AUTO)
	private Long ctypeId;


	/**
	 * 课程类型名称
	 */
	@TableField("CTYPE_NAME")
	private String ctypeName;



	/**
	 * 创建时间
	 */
	@TableField("CREATE_TIME")
	private Date createTime;

	/**
	 * 详情
	 */
	private String introduce	;

	@TableField(exist = false)
	private String startTime	;

	@TableField(exist = false)
	private String endTime	;











}
