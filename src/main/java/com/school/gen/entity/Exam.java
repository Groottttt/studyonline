package com.school.gen.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wuwenze.poi.annotation.ExcelField;
import lombok.Data;

@Data

@TableName("t_exam")
public class Exam implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("user_id")
    private Long userId;

    @TableField("course_id")
    private Long courseId;
    
    @TableField("score")
    private Integer score;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    
    @TableField("creator")
    private String creator;


    private Course course;

    private Subject subject;
    private String courseName;
    private String title;
    /**
     * 班级名称
     */
    @ExcelField(value = "班级名称")
    private String classesName;

    /**
     * 单位名称
     */
    @ExcelField(value = "单位名称")
    private String workPlaceName;

    /**
     * 用户名
     */
    @ExcelField(value = "姓名")
    private String username;

    /**
     * 身份证号
     */
    @ExcelField(value = "身份证号")
    private String idcard;


    /**
     * 是否获得证书
     */
    @ExcelField(value = "是否获得证书")
    private String hasCertificate;

    /**
     * 联系电话
     */
    @ExcelField(value = "联系电话")
    // @Desensitization(type = DesensitizationType.PHONE)
    private String mobile;



    @TableField(exist = false)
    @JsonFormat
    private List<SubExam> subExams;
}
