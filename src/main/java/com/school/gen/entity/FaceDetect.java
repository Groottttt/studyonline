package com.school.gen.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.school.common.converter.ImageUrlConverter;
import com.school.common.converter.PlayTimeConverter;
import com.school.common.converter.TimeConverter;
import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;

import lombok.Data;

@Data
@TableName("t_face_detect")
@Excel("人脸检测记录")
public class FaceDetect implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    @ExcelField(value = "ID")
    private Long id;

    @TableField("user_id")
    private Long userId;

    @TableField("course_id")
    private Long courseId;

    @TableField(exist = false)
    @ExcelField(value = "用户名")
    private String username;

    @TableField(exist = false)
    @ExcelField(value = "科目名")
    private String subjectName;

    @TableField(exist = false)
    @ExcelField(value = "课程名称")
    private String courseName;

    @TableField("face_detect_url")
    @ExcelField(value = "人脸检测图片URL", writeConverter = ImageUrlConverter.class)
    private String faceDetectUrl;

    @TableField("time")
    @ExcelField(value = "播放时间", writeConverter = PlayTimeConverter.class)
    private Long time;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @ExcelField(value = "创建时间", writeConverter = TimeConverter.class)
    private Date createTime;

    @TableField(exist = false)
    private String oneInchPhoto;

}
