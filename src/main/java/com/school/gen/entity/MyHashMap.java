package com.school.gen.entity;


import java.util.LinkedHashMap;

public class MyHashMap<String,Object> extends LinkedHashMap<String,Object> {

        /**
         * 使用HashMap中containsKey判断key是否已经存在
         * @param key
         * @param value
         * @return
         */
        @Override
        public Object put(String key, Object value) {
            Object newV = value;
            if (containsKey(key)) {
                Object oldV = get(key);
                if (key == "rows"){
                    newV = (Object) ((oldV.toString().split("]"))[0]+ "," + newV.toString().split("\\[")[1]);
                }
                else {
                    newV = (Object)((Integer)(((Integer.valueOf(oldV.toString()))+(Integer.valueOf(newV.toString())))));
                }
            }
            return super.put(key, newV);
        }
    }


