package com.school.gen.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

@Data
@TableName("t_play_record")
public class PlayRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("user_id")
    private Long userId;

    @TableField("course_id")
    private Long courseId;

    @TableField(exist = false)
    private String courseName;

    @TableField("time")
    private Long time;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    
    @TableField(exist = false)
    private Long subjectId;

    @TableField(exist = false)
    private Long duration;

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    @TableField(exist = false)
    private String subjectName;

    @TableField(exist = false)
    private String courseDuration;

    @TableField(exist = false)
    private String playTime;

    @TableField(exist = false)
    private String percent;


}
