package com.school.gen.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.school.common.converter.TimeConverter;
import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;

import lombok.Data;

@Data
@TableName("t_question")
@Excel("课程视频信息表")
public class Question implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    @ExcelField(value = "ID")
    private Long id;

    @TableField(value = "course_id")
    private Long courseId;

    @TableField(value ="subject")
    private Long subject;

    @TableField(value = "course_name", exist = false)
    @ExcelField(value = "课程名称")
    private String courseName;

    @TableField(value = "type")
    private String type;

    @TableField(value = "type_name", exist = false)
    @ExcelField(value = "类型")
    private String typeName;

    @TableField(value = "title")
    @ExcelField(value = "题目")
    private String title;

    @TableField(value = "option_a")
    @ExcelField(value = "选项A")
    private String optionA;

    @TableField(value = "option_b")
    @ExcelField(value = "选项B")
    private String optionB;

    @TableField(value = "option_c")
    @ExcelField(value = "选项C")
    private String optionC;

    @TableField(value = "option_d")
    @ExcelField(value = "选项D")
    private String optionD;

    @TableField(value = "option_e")
    @ExcelField(value = "选项E")
    private String optionE;

    @TableField(value = "answer")
    @ExcelField(value = "答案")
    private String answer;

    @TableField(value = "score")
    @ExcelField(value = "分数")
    private Integer score;

    @TableField("create_time")
    @ExcelField(value = "创建时间", writeConverter = TimeConverter.class)
    private Date createTime;

    @TableField("creator")
    @ExcelField(value = "创建者")
    private String creator;

}
