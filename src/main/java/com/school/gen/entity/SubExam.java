package com.school.gen.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

@Data
@TableName("t_sub_exam")
public class SubExam implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("exam_id")
    private Long examId;

    @TableField("question_id")
    private Long questionId;

    @TableField("answer")
    private String answer;
    
    @TableField("choice")
    private String choice;
    
    @TableField("score")
    private Integer score;
}
