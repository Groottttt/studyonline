package com.school.gen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.school.common.converter.TimeConverter;
import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;

/**
 * Entity
 *
 * @author MrBird
 * @date 2021-08-25 14:51:20
 */
@Data
@TableName("t_subject")
@Excel("科目信息表")
public class Subject implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * 公共课状态：是
     */
    public static final String PUBLIC_YES = "1";
    /**
     * 公共课状态：否
     */
    public static final String PUBLIC_NO = "0";

    /**
     * 科目ID
     */
    @TableId(value = "SUBJECT_ID", type = IdType.AUTO)
    private Long subjectId;

    /**
     * 父科目ID
     */
    @TableField("P_SUBJECT_ID")
    private Integer pSubjectId;

    /**
     * 科目标题
     */
    @TableField(exist = false)
    private String pSubjectTitle;


    @TableField(exist = false)
    private String createTimeFrom;

    @TableField(exist = false)
    private String subjectName;

    @TableField(exist = false)
    private String createTimeTo;
    /**
     * 科目标题
     */
    @TableField("TITLE")
    @ExcelField(value = "科目标题")
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 介绍文本
     */
    @TableField("CONTENT")
    @ExcelField(value = "介绍文本")
    private String content;

    /**
     * 缩略图URL
     */
    @TableField("IMG_URL")
    @ExcelField(value = "缩略图路径")
    private String imgUrl;

    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    @ExcelField(value = "创建时间", writeConverter = TimeConverter.class)
    private Date createTime;

    /**
     * 创建者
     */
    @TableField("CREATOR")
    @ExcelField(value = "创建者")
    private String creator;
    
    /**
     * 公共课 0否 1是
     */
    @TableField("PUBLIC")
    @NotBlank(message = "{required}")
    @ExcelField(value = "公共课", writeConverterExp = "0=否,1=是")
    private String isPublic;


    public long getVideosize() {
        return 0;
    }

    public void setVideoSizeHuman(String valueOf) {
    }
}
