package com.school.gen.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

@Data
@TableName("t_dict")
public class Version implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @TableField("version")
    private String version;
    
    @TableField("url")
    private String url;
    
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

}
