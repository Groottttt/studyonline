package com.school.gen.mapper;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.school.gen.entity.CollectionEntity;

public interface CollectionMapper extends BaseMapper<CollectionEntity> {

    CollectionEntity getById(@Param("userId") Long userId, @Param("objId") Long objId);

    int delete(@Param("userId") Long userId, @Param("objId") Long objId);
}
