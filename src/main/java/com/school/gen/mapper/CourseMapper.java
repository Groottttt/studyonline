package com.school.gen.mapper;

import com.school.gen.entity.Course;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * 课程表 Mapper
 *
 * @author MrBird
 * @date 2021-08-22 00:23:17
 */
public interface CourseMapper extends BaseMapper<Course> {

    /**
     * 通过id查找课程
     *
     * @param courseId ID
     * @return 课程
     */
    Course findById(Long courseId);

    IPage<Course> findCourseDetailPage(Page<Course> page, @Param("course") Course course);

    List<Course> listCourseDetail(@Param("course") Course course);

    List<Course> listCollectionCourse(Long userId);

    Course findCourseByIdAndUserId(@Param("course") Course course);

    List<Course> findList(@Param("course") Course course);

    IPage<Course> findCourseDetailHasQuestionPage(Page<Course> page, @Param("course") Course course);

    Long findSubjectById(@Param("courseId") Long courseId);

}
