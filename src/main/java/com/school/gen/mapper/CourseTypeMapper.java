package com.school.gen.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.school.gen.entity.CourseType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 部门表 Mapper
 *
 * @author MrBird
 * @date 2021-08-25 10:18:37
 */
public interface CourseTypeMapper extends BaseMapper<CourseType> {

    IPage<CourseType> getAllTypeList(Page<CourseType> page, @Param("courseType") CourseType courseType);

}
