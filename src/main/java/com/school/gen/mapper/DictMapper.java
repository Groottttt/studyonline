package com.school.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.school.gen.entity.Dict;

import io.lettuce.core.dynamic.annotation.Param;

public interface DictMapper extends BaseMapper<Dict> {

    Dict getDictByNameAndType(@Param("dict") Dict dict);
}
