package com.school.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.school.gen.entity.ExamFrequency;

public interface ExamFrequencyMapper extends BaseMapper<ExamFrequency>{

    ExamFrequency getByUserAndCourse(ExamFrequency examFrequency);


    ExamFrequency getByUserAndSubject(ExamFrequency examFrequency);

    void updateExamFrequencyByCourseId(long userId,long courseId);

    void updateExamFrequencyBySubjectId(long userId,long subjectId);

    void save(ExamFrequency examFrequency);
}
