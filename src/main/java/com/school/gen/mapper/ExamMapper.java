package com.school.gen.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.school.gen.entity.Exam;
import org.apache.ibatis.annotations.Param;

public interface ExamMapper extends BaseMapper<Exam> {


    List<Exam> list();

    List<Exam> findExamById(long userId);

    List<Exam> findCourseId(long userId);

    List<Exam> list(@Param("exam") Exam exam);

    long countExamDetail(long userId);

    IPage<Exam> findExamDetailPage(Page<Exam> page, long userId);

    long countExam(Long userId);

    void deleteAll(Long userId);

    void updateScore(@Param("exam") Exam exam);

    Exam findByUserIdandCourseId(Long courseId,Long userId);

    List<Exam> exportExamExcel(String time);
}
