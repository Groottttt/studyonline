package com.school.gen.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.school.gen.entity.FaceDetect;

public interface FaceDetectMapper extends BaseMapper<FaceDetect> {

    Long countFaceDetect(@Param("faceDetect") FaceDetect faceDetect);

    /**
     * 查找科目
     *
     * @param page       分页
     * @param FaceDetect 科目
     * @return IPage<User>
     */
    <T> IPage<FaceDetect> findFaceDetectPage(Page<T> page, @Param("faceDetect") FaceDetect faceDetect);

    FaceDetect get(@Param("faceDetect") FaceDetect faceDetect);

    List<FaceDetect> list(@Param("faceDetect") FaceDetect faceDetect);

}
