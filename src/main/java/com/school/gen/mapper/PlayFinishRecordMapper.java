package com.school.gen.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.school.gen.entity.PlayFinishRecord;

public interface PlayFinishRecordMapper extends BaseMapper<PlayFinishRecord> {
    List<PlayFinishRecord> listPlayFinishRecords(@Param("playFinishRecord") PlayFinishRecord playFinishRecord);

    int deleteByUseridAndCourseId(@Param("playFinishRecord") PlayFinishRecord playFinishRecord);

    List<PlayFinishRecord> countFinishRecords(Long userId, Long subjectId);

    PlayFinishRecord findByUserIdAndCourseId(Long userId, Long courseId);

    void deleteAll(Long userId);
}
