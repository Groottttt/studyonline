package com.school.gen.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.school.gen.entity.PlayRecord;

public interface PlayRecordMapper extends BaseMapper<PlayRecord> {

    PlayRecord getLastPlayRecord(@Param("playRecord") PlayRecord playRecord);

    List<PlayRecord> listPlayRecords(@Param("playRecord") PlayRecord playRecord);

    int deleteByUseridAndCourseId(@Param("playRecord") PlayRecord playRecord);

    long countPlayRecordDetail(long userId);

    IPage<PlayRecord> findPlayRecordDetailPage(Page<PlayRecord> page, long userId);


    List<PlayRecord> findPlayRecords(long userId);

    PlayRecord findByUserIdAndCourseId(Long userId, Long courseId);

    void deleteAll(Long userId);


}
