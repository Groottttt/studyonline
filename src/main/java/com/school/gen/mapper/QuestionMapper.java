package com.school.gen.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.school.gen.entity.Question;

public interface QuestionMapper extends BaseMapper<Question> {
    Long countQuestion(@Param("question") Question question);

    <T> IPage<Question> findPage(Page<T> page, @Param("question") Question question);

    Question findById(Long id);

    int removeByCourseId(Long courseId);

    List<Question> findList(@Param("question") Question question);

    List<Question> findSubList(@Param("subjectId")Long subjectId);

    List<Question> findSubSingleList(@Param("subjectId")Long subjectId);

    List<Question> findSubJudgeList(@Param("subjectId")Long subjectId);

    List<Question> findSubMultipleList(@Param("subjectId")Long subjectId);



    Question findSById(Long questionId);
}
