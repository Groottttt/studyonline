package com.school.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.school.gen.entity.SubExam;

public interface SubExamMapper extends BaseMapper<SubExam>{

}
