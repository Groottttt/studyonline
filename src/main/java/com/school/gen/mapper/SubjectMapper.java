package com.school.gen.mapper;

import com.school.gen.entity.Subject;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * Mapper
 *
 * @author ZhuXH
 * @date 2021-08-25 14:51:20
 */
public interface SubjectMapper extends BaseMapper<Subject> {

    Long countSubject(@Param("subject") Subject subject);

    /**
     * 查找科目
     *
     * @param page    分页
     * @param subject 科目
     * @return IPage<User>
     */
    <T> IPage<Subject> findSubjectPage(Page<T> page, @Param("subject") Subject subject);

    List<Subject> findSubjectByPid(Long subjectId);

    Subject findById(Long subjectId);
    
    List<Subject> listByPublic(Boolean isPublic);

    IPage<Subject> findSubjectDetailHasQuestionPage(Page<Subject> page,@Param("id") long id);

    long findIdByTitle(@Param("name") String name);

    long findByCourseId(long courseId);

    List<Subject> findChildSubject(long subjectId);

    List<Subject> findAll();
}
