package com.school.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.school.gen.entity.Version;

public interface VersionMapper extends BaseMapper<Version> {

    Version getLastVersion();
}
