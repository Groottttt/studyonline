package com.school.gen.service;

import com.school.gen.entity.Course;
import com.school.gen.entity.Subject;
import com.school.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 课程表 Service接口
 *
 * @author MrBird
 * @date 2021-08-22 00:23:17
 */
public interface ICourseService extends IService<Course> {

    /**
     * 通过id查找课程
     *
     * @param courseId ID
     * @return 课程
     */
    Course findById(Long courseId);

    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param course  course
     * @return IPage<Course>
     */
    IPage<Course> findCourses(QueryRequest request, Course course);

    /**
     * 查询（所有）
     *
     * @param course course
     * @return List<Course>
     */
    List<Course> findCourses(Course course);

    /**
     * 新增
     *
     * @param course course
     */
    void createCourse(Course course);

    /**
     * 修改
     *
     * @param course course
     */
    void updateCourse(Course course);

    /**
     * 删除
     *
     * @param course course
     */
    void deleteCourses(String[] userIds);

    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param course  course
     * @return IPage<Course>
     */
    Map<String,Object> findCoursesHasQuestion(QueryRequest request, Course course, Subject subject);
}
