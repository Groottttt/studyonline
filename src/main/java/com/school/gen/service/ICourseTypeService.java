package com.school.gen.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.school.common.entity.SchoolResponse;
import com.school.gen.entity.CourseType;
import com.school.common.entity.QueryRequest;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 部门表 Service接口
 *
 * @author MrBird
 * @date 2021-08-25 10:18:37
 */
public interface ICourseTypeService extends IService<CourseType> {



    IPage<CourseType> courseTypeList(CourseType courseType, QueryRequest request);

    SchoolResponse addCourseType(CourseType courseType);

    SchoolResponse deleteCourseType(String[] split);
}
