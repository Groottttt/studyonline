package com.school.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.school.gen.entity.ExamFrequency;
import com.google.zxing.WriterException;

import java.io.IOException;

public interface IExamFrequencyService extends IService<ExamFrequency> {

    Boolean createExamFrequency(ExamFrequency examFrequency) throws IOException, WriterException;

    void saveExamFrequency(ExamFrequency examFrequency);

    ExamFrequency findByUserAndSubject(ExamFrequency examFrequency);

    ExamFrequency findByUserAndCourse(ExamFrequency examFrequency);

}
