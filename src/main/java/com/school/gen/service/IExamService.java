package com.school.gen.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.school.gen.entity.Exam;

public interface IExamService extends IService<Exam> {
    
    List<Exam> list(Exam exam);

    List<Exam> findExamById(long userId);


}
