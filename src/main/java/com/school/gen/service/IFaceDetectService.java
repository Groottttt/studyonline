package com.school.gen.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.school.common.entity.QueryRequest;
import com.school.gen.entity.FaceDetect;

public interface IFaceDetectService extends IService<FaceDetect> {

    FaceDetect findById(Long faceDetectId);

    /**
     * 查询（分页）
     *
     * @param request    QueryRequest
     * @param FaceDetect FaceDetect
     * @return IPage<FaceDetect>
     */
    IPage<FaceDetect> findFaceDetects(QueryRequest request, FaceDetect faceDetect);

    /**
     * 查询（所有）
     *
     * @param FaceDetect FaceDetect
     * @return List<FaceDetect>
     */
    List<FaceDetect> findFaceDetects(FaceDetect faceDetect);

    void deleteByUserIds(String[] userIds);

}
