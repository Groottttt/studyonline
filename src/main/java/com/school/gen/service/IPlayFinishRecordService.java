package com.school.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.school.gen.entity.PlayFinishRecord;

public interface IPlayFinishRecordService extends IService<PlayFinishRecord> {

    /**
     * @param playRecord
     */
    void create(PlayFinishRecord playFinishRecord);

    void deleteByUserIds(String[] userIds);
}
