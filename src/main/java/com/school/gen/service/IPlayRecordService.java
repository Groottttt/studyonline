package com.school.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.school.gen.entity.PlayRecord;

public interface IPlayRecordService extends IService<PlayRecord> {

    /**
     * @param playRecord
     */
    void create(PlayRecord playRecord);

    void deleteByUserIds(String[] userIds);
}
