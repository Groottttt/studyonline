package com.school.gen.service;

import java.io.IOException;
import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.school.common.entity.SchoolResponse;
import com.school.common.entity.QueryRequest;
import com.school.gen.entity.Question;

public interface IQuestionService {

    IPage<Question> findQuestions(QueryRequest request, Question question);

    List<Question> findQuestions(Question question);

    void createQuestion(Question question);

    void updateQuestion(Question question);

    SchoolResponse deleteQuestion(String[] id);

    SchoolResponse deleteByCourse(Long courseId);
    
    Question findById(Long questionId);
    
    SchoolResponse listQuestion(Long courseId);

    void uploadQuestion(Long subject,Long courseId, String file, String creator) throws IOException;

    Question findSById(Long questionId);

    SchoolResponse listSubQuestions(Long subjectId);
}
