package com.school.gen.service;

import com.school.gen.entity.Subject;
import com.school.common.entity.DeptTree;
import com.school.common.entity.SchoolResponse;
import com.school.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.school.system.entity.QuestionTable;

import java.util.List;

/**
 * @author ZhuXH
 *
 */
public interface ISubjectService extends IService<Subject> {

    Subject findById(Long subjectId);

    List<Subject> findSubjectsByPid(Long subjectId);

    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param subject subject
     * @return IPage<Subject>
     */
    IPage<Subject> findSubjects(QueryRequest request, Subject subject);

    /**
     * 查询（所有）
     *
     * @param subject subject
     * @return List<Subject>
     */
    List<Subject> findSubjects(Subject subject);

    /**
     * 新增
     *
     * @param subject subject
     */
    void createSubject(Subject subject);

    /**
     * 修改
     *
     * @param subject subject
     */
    void updateSubject(Subject subject);

    /**
     * 删除
     *
     * @param subjectIds subjectIds
     */
    SchoolResponse deleteSubject(String[] subjectIds);
    
    /**
     * 获取科目树（下拉选使用）
     *
     * @return 科目树集合
     */
    List<DeptTree<Subject>> findSubjectTree();

    List<QuestionTable> findSubjectsHasQuestion(QueryRequest request, Subject subject);

    long findIdByTitle(String name);
}
