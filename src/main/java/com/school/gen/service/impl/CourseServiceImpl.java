package com.school.gen.service.impl;

import com.school.common.entity.SchoolConstant;
import com.school.common.entity.QueryRequest;
import com.school.common.utils.FileConfiguration;
import com.school.common.utils.HumanSizeUtil;
import com.school.common.utils.SortUtil;
import com.school.gen.entity.Course;
import com.school.gen.entity.Subject;
import com.school.gen.mapper.CourseMapper;
import com.school.gen.service.ICourseService;

import com.school.gen.service.ISubjectService;
import com.school.system.entity.QuestionTable;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.io.File;
import java.util.*;

/**
 * 课程表 Service实现
 *
 * @author MrBird
 * @date 2021-08-22 00:23:17
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {
    private final CourseMapper courseMapper;
    @Autowired
    private FileConfiguration fileConfiguration;
    private final ISubjectService subjectService;

    @Override
    public Course findById(Long courseId) {
        return baseMapper.findById(courseId);
    }

    @Override
    public IPage<Course> findCourses(QueryRequest request, Course course) {
        if (StringUtils.isNotBlank(course.getCreateTimeFrom())
                && StringUtils.equals(course.getCreateTimeFrom(), course.getCreateTimeTo())) {
            course.setCreateTimeFrom(course.getCreateTimeFrom() + SchoolConstant.DAY_START_PATTERN_SUFFIX);
            course.setCreateTimeTo(course.getCreateTimeTo() + SchoolConstant.DAY_END_PATTERN_SUFFIX);
        }
        LambdaQueryWrapper<Course> queryWrapper = new LambdaQueryWrapper<>();
        // 设置查询条件
        Page<Course> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "course_id", SchoolConstant.ORDER_DESC, false);
        IPage<Course> coursesIPage = baseMapper.findCourseDetailPage(page, course);
        coursesIPage = reBuildCourses(coursesIPage);

        return coursesIPage;
    }

    @Override
    public List<Course> findCourses(Course course) {
        return baseMapper.findList(course);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createCourse(Course course) {
        course.setCreateTime(new Date());
        this.save(course);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateCourse(Course course) {
        this.saveOrUpdate(course);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteCourses(String[] userIds) {
        List<String> list = Arrays.asList(userIds);
        List<Course> courseList = this.baseMapper.selectList(null);
        String rootPath = fileConfiguration.getResourceDir();
        for (String id : list) {
            for (Course course : courseList) {
                if (Long.valueOf(id).longValue() == course.getCourseId().longValue()) {
                    File file = new File(rootPath + course.getVideourl());
                    if (file.exists() && file.isFile() && file.delete()) {

                    }
                }
            }
        }
        // 删除视频课程
        removeByIds(list);
    }

    private static IPage<Course> reBuildCourses(IPage<Course> courses) {
        List<Course> courseList = courses.getRecords();
        for (Course course : courseList) {
            String videosize = HumanSizeUtil.humanReadableByteCountBin(course.getVideosize());
            course.setVideoSizeHuman(String.valueOf(videosize));
        }

        courses.setRecords(courseList);
        return courses;
    }

    @Override
    public Map<String, Object> findCoursesHasQuestion(QueryRequest request, Course course, Subject subject) {
        if (StringUtils.isNotBlank(course.getCreateTimeFrom())
                && StringUtils.equals(course.getCreateTimeFrom(), course.getCreateTimeTo())) {
            course.setCreateTimeFrom(course.getCreateTimeFrom() + SchoolConstant.DAY_START_PATTERN_SUFFIX);
            course.setCreateTimeTo(course.getCreateTimeTo() + SchoolConstant.DAY_END_PATTERN_SUFFIX);
        }
        LambdaQueryWrapper<Course> queryWrapper = new LambdaQueryWrapper<>();
        // 设置查询条件
        Page<Course> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "course_id", SchoolConstant.ORDER_ASC, false);
        IPage<Course> coursesIPage = baseMapper.findCourseDetailHasQuestionPage(page, course);
        coursesIPage = reBuildCourses(coursesIPage);
        List<QuestionTable> list = new ArrayList<>();
        IPage<QuestionTable> qTableIPage = new Page<>();
        for (Course course1 : coursesIPage.getRecords()) {
            QuestionTable qTable = new QuestionTable();
            qTable.setDescription(course1.getDescription());
            qTable.setCreateTime(course1.getCreateTime());
            qTable.setCourseId(course1.getCourseId());
            qTable.setSubjectName(course1.getSubjectName());
            qTable.setCourseName(course1.getCourseName());
            qTable.setPrice(course1.getPrice());
            list.add(qTable);
        }
        List<QuestionTable> list1 = subjectService.findSubjectsHasQuestion(request, subject);
        list.addAll(list1);
        qTableIPage.setRecords(list);
        Map<String, Object> map1 = new HashMap<>();
        map1.put("ipage", qTableIPage);
        map1.put("pages", coursesIPage.getPages());

        return map1;
    }
}