package com.school.gen.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.school.common.entity.QueryRequest;
import com.school.common.entity.SchoolResponse;
import com.school.gen.entity.CourseType;
import com.school.gen.mapper.CourseTypeMapper;
import com.school.gen.service.ICourseTypeService;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Date;

/**
 * 部门表 Service实现
 *
 * @author MrBird
 * @date 2021-08-25 10:18:37
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CourseTypeServiceImpl extends ServiceImpl<CourseTypeMapper, CourseType> implements ICourseTypeService {

    @Override
    public IPage<CourseType> courseTypeList(CourseType courseType, QueryRequest request) {

        Page<CourseType> page = new Page<>(request.getPageNum(), request.getPageSize());
        return baseMapper.getAllTypeList(page, courseType);
    }

    @Override
    public SchoolResponse addCourseType(CourseType courseType) {
        courseType.setCreateTime(new Date());
        if (baseMapper.insert(courseType)==1){
            return new SchoolResponse().success();
        }
        return new SchoolResponse().message("添加失败请稍后重试");
    }

    @Override
    public SchoolResponse deleteCourseType(String[] ids) {
        for (String id : ids) {
            baseMapper.deleteById(id);
        }
        return new SchoolResponse().success();
    }


}
