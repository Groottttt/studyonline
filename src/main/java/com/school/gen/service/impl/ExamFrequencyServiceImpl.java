package com.school.gen.service.impl;

import java.util.Date;

import com.school.gen.entity.*;
import com.school.gen.mapper.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.school.gen.service.IExamFrequencyService;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ExamFrequencyServiceImpl extends ServiceImpl<ExamFrequencyMapper, ExamFrequency>
        implements IExamFrequencyService {

    private final CourseMapper courseMapper;
    private final CourseServiceImpl courseService;
    private final PlayFinishRecordMapper playFinishRecordMapper;

    @Override
    public Boolean createExamFrequency(ExamFrequency examFrequency) {
        ExamFrequency eFExist;
        if (examFrequency.getCourseId() != null) {
             eFExist = baseMapper.getByUserAndCourse(examFrequency);
        } else {
             eFExist = baseMapper.getByUserAndSubject(examFrequency);
        }
        if (eFExist != null) {
            eFExist.setFrequency(eFExist.getFrequency() + 1);
            updateById(eFExist);
            return true;
        }
            /*
              数据库如果没有数据
             */
            int playNum = playFinishRecordMapper.countFinishRecords(examFrequency.getUserId(), examFrequency.getSubjectId()).size();
            Course c = new Course();
            c.setSubject(examFrequency.getSubjectId());
            int sumNum=courseService.findCourses(c).size();
            if (playNum>=sumNum){
                Course course = courseMapper.findById(examFrequency.getCourseId());
                if (course != null) {
                    int frequency = 1;
                    examFrequency.setFrequency(frequency);
                    examFrequency.setSubjectId(null);
                    examFrequency.setCreateTime(new Date());
                    this.save(examFrequency);
                    PlayRecord playRecord = new PlayRecord();
                    playRecord.setUserId(examFrequency.getUserId());
                    playRecord.setCourseId(examFrequency.getCourseId());
                    return true;
                }
        }
            return false;
    }

    @Override
    public ExamFrequency findByUserAndSubject(ExamFrequency examFrequency) {
        return baseMapper.getByUserAndSubject(examFrequency);
    }

    @Override
    public ExamFrequency findByUserAndCourse(ExamFrequency examFrequency) {
        return baseMapper.getByUserAndCourse(examFrequency);
    }


    @Override
    public void saveExamFrequency(ExamFrequency examFrequency) {
        this.save(examFrequency);
    }
}

