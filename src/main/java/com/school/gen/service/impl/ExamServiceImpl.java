package com.school.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.school.gen.entity.Exam;
import com.school.gen.entity.Subject;
import com.school.gen.mapper.ExamMapper;
import com.school.gen.mapper.SubjectMapper;
import com.school.gen.service.IExamService;
import com.school.system.entity.User;
import com.school.system.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ExamServiceImpl extends ServiceImpl<ExamMapper, Exam> implements IExamService {


    private final UserMapper userMapper;
    private final ExamMapper examMapper;
    private final SubjectMapper subjectMapper;

    @Override
    public List<Exam> list(Exam exam) {
        List<Exam> exams = baseMapper.list(exam);
        return exams;
    }

    @Override
    public List<Exam> findExamById(long userId) {
        return examMapper.findExamById(userId);
    }


}
