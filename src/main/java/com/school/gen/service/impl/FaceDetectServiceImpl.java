package com.school.gen.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.school.common.entity.SchoolConstant;
import com.school.common.entity.QueryRequest;
import com.school.common.utils.SortUtil;
import com.school.gen.entity.FaceDetect;
import com.school.gen.mapper.FaceDetectMapper;
import com.school.gen.service.IFaceDetectService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class FaceDetectServiceImpl extends ServiceImpl<FaceDetectMapper, FaceDetect> implements IFaceDetectService {
    @Override
    public IPage<FaceDetect> findFaceDetects(QueryRequest request, FaceDetect faceDetect) {
        Page<FaceDetect> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        page.setTotal(baseMapper.countFaceDetect(faceDetect));
        SortUtil.handlePageSort(request, page, "createTime", SchoolConstant.ORDER_DESC, false);
        return baseMapper.findFaceDetectPage(page, faceDetect);
    }

    @Override
    public List<FaceDetect> findFaceDetects(FaceDetect faceDetect) {
        LambdaQueryWrapper<FaceDetect> queryWrapper = new LambdaQueryWrapper<>();
        //  设置查询条件
        return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    public FaceDetect findById(Long faceDetectId) {
        //  Auto-generated method stub
        FaceDetect faceDetect = new FaceDetect();
        faceDetect.setId(faceDetectId);
        return baseMapper.get(faceDetect);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteByUserIds(String[] userIds) {
        List<String> list = Arrays.asList(userIds);
        baseMapper.delete(new LambdaQueryWrapper<FaceDetect>().in(FaceDetect::getUserId, list));
    }
}
