package com.school.gen.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.school.gen.entity.PlayFinishRecord;
import com.school.gen.mapper.PlayFinishRecordMapper;
import com.school.gen.service.IPlayFinishRecordService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PlayFinishRecordServiceImpl extends ServiceImpl<PlayFinishRecordMapper, PlayFinishRecord>
        implements IPlayFinishRecordService {
    private final PlayFinishRecordMapper playRecordMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(PlayFinishRecord playFinishRecord) {
        playFinishRecord.setCreateTime(new Date());
        this.save(playFinishRecord);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteByUserIds(String[] userIds) {
        List<String> list = Arrays.asList(userIds);
        baseMapper.delete(new LambdaQueryWrapper<PlayFinishRecord>().in(PlayFinishRecord::getUserId, list));
    }
}
