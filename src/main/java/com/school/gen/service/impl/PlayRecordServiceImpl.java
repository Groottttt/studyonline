package com.school.gen.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.school.gen.entity.PlayRecord;
import com.school.gen.mapper.PlayRecordMapper;
import com.school.gen.service.IPlayRecordService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PlayRecordServiceImpl extends ServiceImpl<PlayRecordMapper, PlayRecord> implements IPlayRecordService {
    private final PlayRecordMapper playRecordMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(PlayRecord playRecord) {
        playRecord.setCreateTime(new Date());
        this.save(playRecord);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteByUserIds(String[] userIds) {
        List<String> list = Arrays.asList(userIds);
        baseMapper.delete(new LambdaQueryWrapper<PlayRecord>().in(PlayRecord::getUserId, list));
    }
}
