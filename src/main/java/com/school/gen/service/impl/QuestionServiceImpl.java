package com.school.gen.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.school.gen.entity.Subject;
import com.school.gen.mapper.SubjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.school.common.entity.SchoolConstant;
import com.school.common.entity.SchoolResponse;
import com.school.common.entity.QueryRequest;
import com.school.common.utils.SortUtil;
import com.school.gen.constant.DictConsts;
import com.school.gen.entity.Course;
import com.school.gen.entity.Dict;
import com.school.gen.entity.Question;
import com.school.gen.mapper.CourseMapper;
import com.school.gen.mapper.DictMapper;
import com.school.gen.mapper.QuestionMapper;
import com.school.gen.service.IQuestionService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class QuestionServiceImpl extends ServiceImpl<QuestionMapper, Question> implements IQuestionService {

    private final CourseMapper courseMapper;
    private final DictMapper dictMapper;
    private final SubjectMapper subjectMapper;

    @Override
    public IPage<Question> findQuestions(QueryRequest request, Question question) {
        Page<Question> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        page.setTotal(baseMapper.countQuestion(question));
        SortUtil.handlePageSort(request, page, "createTime", SchoolConstant.ORDER_DESC, false);
        return baseMapper.findPage(page, question);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createQuestion(Question question) {
        question.setCreateTime(new Date());
        this.save(question);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateQuestion(Question question) {
        System.out.println(question);
        Question question2 = new Question();
        if (question.getCourseId() == null) {
            question2 = baseMapper.findSById(question.getId());
        } else {
            question2 = baseMapper.findById(question.getId());
        }
        if (StringUtils.isNotBlank(question.getTitle())) {
            question2.setTitle(question.getTitle());
        }
        if (StringUtils.isNotBlank(question.getOptionA())) {
            question2.setOptionA(question.getOptionA());
        }
        if (StringUtils.isNotBlank(question.getOptionB())) {
            question2.setOptionB(question.getOptionB());
        }
        if (StringUtils.isNotBlank(question.getOptionC())) {
            question2.setOptionC(question.getOptionC());
        }
        if (StringUtils.isNotBlank(question.getOptionD())) {
            question2.setOptionD(question.getOptionD());
        }
        if (StringUtils.isNotBlank(question.getOptionE())) {
            question2.setOptionE(question.getOptionE());
        }
        if (StringUtils.isNotBlank(String.valueOf(question.getSubject()))) {
            question2.setSubject(question.getSubject());
        }
        if (StringUtils.isNotBlank(question.getAnswer())) {
            question2.setAnswer(question.getAnswer());
        }
        if (question.getScore() != null) {
            question2.setScore(question.getScore());
        }
        updateById(question2);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SchoolResponse deleteQuestion(String[] id) {
        List<String> list = Arrays.asList(id);
        removeByIds(list);
        return new SchoolResponse().success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SchoolResponse deleteByCourse(Long courseId) {
        baseMapper.removeByCourseId(courseId);
        return new SchoolResponse().success();
    }

    @Override
    public List<Question> findQuestions(Question question) {
        if (question.getCourseId() != null) {
            return this.baseMapper.findList(question);
        } else {
            return this.baseMapper.findSubList(question.getSubject());
        }
    }

    @Override
    public Question findById(Long questionId) {
        return this.baseMapper.findById(questionId);
    }

    @Override
    public SchoolResponse listQuestion(Long courseId) {
        List<Question> questions = new ArrayList<>();
        Course course = courseMapper.findById(courseId);
        if (course.getSubjectName() != "") {
            Dict dictParam = new Dict();
            dictParam.setName(course.getSubjectName());
            dictParam.setType(DictConsts.QUESTION_QUANTITY);
            Dict dict = dictMapper.getDictByNameAndType(dictParam);
            if (dict != null) {
                int quantity = Integer.parseInt(dict.getContent());
                Question questionParam = new Question();
                questionParam.setCourseId(courseId);
                List<Question> questionList = baseMapper.findList(questionParam);
                if (questionList.size() == quantity) {
                    questions = questionList;
                } else if (questionList.size() < quantity) {
                    return new SchoolResponse().fail().message("获取试题出现错误，请联系管理员");
                } else {
                    Random random = new Random();
                    boolean[] bool = new boolean[questionList.size()];
                    int randInt = 0;
                    for (int i = 0; i < quantity; i++) {
                        do {
                            randInt = random.nextInt(questionList.size());
                        } while (bool[randInt]);
                        bool[randInt] = true;
                        questions.add(questionList.get(randInt));
                    }

                }
            } else {
                return new SchoolResponse().fail().message("获取试题出现错误，请联系管理员");
            }
        } else {
            return new SchoolResponse().fail().message("获取试题出现错误，请联系管理员");
        }
        return new SchoolResponse().success().data(questions);
    }

    @Override
    public void uploadQuestion(Long subject, Long courseId,String path, String creator) throws IOException {
        File file = new File(path);
        InputStream inputStream = new FileInputStream(file);
        Workbook wb = new XSSFWorkbook(inputStream);
        Sheet sheet = wb.getSheetAt(0);
        Row header = sheet.getRow(0);
        int title_num = 0;
        int type_num = 0;
        int option_a_num = 0;
        int option_b_num = 0;
        int option_c_num = 0;
        int option_d_num = 0;
        int option_e_num = 0;
        int answer_num = 0;
        for (int t = header.getFirstCellNum(); t <= header.getLastCellNum(); t++) {
            if (String.valueOf(header.getCell(t)).equals("题目")) {
                title_num = t;
            }
            if (String.valueOf(header.getCell(t)).equals("题型")) {
                type_num = t;
            }
            if (String.valueOf(header.getCell(t)).equals("选项A")) {
                option_a_num = t;
            }
            if (String.valueOf(header.getCell(t)).equals("选项B")) {
                option_b_num = t;
            }
            if (String.valueOf(header.getCell(t)).equals("选项C")) {
                option_c_num = t;
            }
            if (String.valueOf(header.getCell(t)).equals("选项D")) {
                option_d_num = t;
            }
            if (String.valueOf(header.getCell(t)).equals("选项E")) {
                option_e_num = t;
            }
            if (String.valueOf(header.getCell(t)).equals("答案")) {
                answer_num = t;
            }
        }
        for (int i = sheet.getFirstRowNum() + 1; i <= sheet.getLastRowNum(); i++) {
            Question question = new Question();
            Row row = sheet.getRow(i);
            for (int j = row.getFirstCellNum(); j <= row.getLastCellNum(); j++) {
                Cell cell = row.getCell(j);
                if (j == title_num) {
                    question.setTitle(String.valueOf(cell));
                }
                if (j == type_num) {
                    if (String.valueOf(cell).equals("单选题")) {
                        question.setType("single_choice");
                    }
                    if (String.valueOf(cell).equals("多选题")) {
                        question.setType("multiple_choice");
                    }
                    if (String.valueOf(cell).equals("判断题")) {
                        question.setType("true_or_false");
                    }
                }
                if (j == option_a_num) {
                    question.setOptionA(String.valueOf(cell));
                }
                if (j == option_b_num) {
                    question.setOptionB(String.valueOf(cell));
                }
                if (j == option_c_num) {
                    question.setOptionC(String.valueOf(cell));
                }
                if (j == option_d_num) {
                    question.setOptionD(String.valueOf(cell));
                }
                if (j == option_e_num) {
                    question.setOptionE(String.valueOf(cell));
                }
                if (j == answer_num) {
                    if (String.valueOf(cell).equals("对")) {
                        question.setAnswer(String.valueOf(1));
                    } else if (String.valueOf(cell).equals("错")) {
                        question.setAnswer(String.valueOf(0));
                    } else {
                        question.setAnswer(String.valueOf(cell));
                    }

                }
            }
            question.setCreateTime(new Date());
            question.setCreator(creator);
            System.out.println(courseId);
            if (courseId==null){
                question.setSubject(subject);
                question.setScore(1);
            }else {
                question.setSubject(subject);
                question.setCourseId(courseId);
                question.setScore(2);
            }


            this.save(question);
        }
    }

    @Override
    public Question findSById(Long questionId) {
        return this.baseMapper.findSById(questionId);
    }

    @Override
    public SchoolResponse listSubQuestions(Long subjectId) {
        List<Question> questions = new ArrayList<>();
        List<Question> singleQuestions = new ArrayList<>();
        List<Question> judgeQuestions = new ArrayList<>();
        List<Question> multipleQuestions = new ArrayList<>();
        Subject subject = subjectMapper.findById(subjectId);
        List<Question> singleQuestionList = baseMapper.findSubSingleList(subjectId);
        List<Question> judgeQuestionList = baseMapper.findSubJudgeList(subjectId);
        List<Question> multipleQuestionList = baseMapper.findSubMultipleList(subjectId);
        if (singleQuestionList.size() == 40) {
            singleQuestions = singleQuestionList;
        } else if (singleQuestionList.size() < 40) {
            return new SchoolResponse().fail().message("获取试题出现错误，请联系管理员");
        } else {
            Random random = new Random();
            boolean[] bool = new boolean[singleQuestionList.size()];
            int randInt = 0;
            for (int i = 0; i < 40; i++) {
                do {
                    randInt = random.nextInt(singleQuestionList.size());
                } while (bool[randInt]);
                bool[randInt] = true;
                singleQuestions.add(singleQuestionList.get(randInt));
            }
        }
        if (judgeQuestionList.size() == 30) {
            judgeQuestions = judgeQuestionList;
        } else if (judgeQuestionList.size() < 30) {
            return new SchoolResponse().fail().message("获取试题出现错误，请联系管理员");
        } else {
            Random random = new Random();
            boolean[] bool = new boolean[judgeQuestionList.size()];
            int randInt = 0;
            for (int i = 0; i < 30; i++) {
                do {
                    randInt = random.nextInt(judgeQuestionList.size());
                } while (bool[randInt]);
                bool[randInt] = true;
                judgeQuestions.add(judgeQuestionList.get(randInt));
            }
        }
        if (multipleQuestionList.size() == 30) {
            multipleQuestions = multipleQuestionList;
        } else if (multipleQuestionList.size() < 30) {
            return new SchoolResponse().fail().message("获取试题出现错误，请联系管理员");
        } else {
            Random random = new Random();
            boolean[] bool = new boolean[multipleQuestionList.size()];
            int randInt = 0;
            for (int i = 0; i < 30; i++) {
                do {
                    randInt = random.nextInt(multipleQuestionList.size());
                } while (bool[randInt]);
                bool[randInt] = true;
                multipleQuestions.add(multipleQuestionList.get(randInt));
            }
        }
        questions.addAll(singleQuestions);
        questions.addAll(multipleQuestions);
        questions.addAll(judgeQuestions);
        System.out.println(questions);
        return new SchoolResponse().success().data(questions);
    }


}
