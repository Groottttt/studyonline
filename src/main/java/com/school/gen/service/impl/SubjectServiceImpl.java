package com.school.gen.service.impl;

import com.school.common.entity.DeptTree;
import com.school.common.entity.SchoolConstant;
import com.school.common.entity.SchoolResponse;
import com.school.common.entity.QueryRequest;
import com.school.common.utils.SortUtil;
import com.school.common.utils.TreeUtil;
import com.school.gen.entity.Subject;
import com.school.gen.mapper.SubjectMapper;
import com.school.gen.service.ISubjectService;
import com.school.system.entity.QuestionTable;
import com.school.system.mapper.UserMapper;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.*;

/**
 * Service实现
 *
 * @author MrBird
 * @date 2021-08-25 14:51:20
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SubjectServiceImpl extends ServiceImpl<SubjectMapper, Subject> implements ISubjectService {

    private final SubjectMapper subjectMapper;

    private final UserMapper userMapper;

    @Override
    public Subject findById(Long subjectId) {
        return baseMapper.findById(subjectId);
    }

    @Override
    public List<Subject> findSubjectsByPid(Long subjectId) {
        return baseMapper.findSubjectByPid(subjectId);
    }

    @Override
    public IPage<Subject> findSubjects(QueryRequest request, Subject subject) {
        Page<Subject> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        page.setTotal(baseMapper.countSubject(subject));
        SortUtil.handlePageSort(request, page, "createTime", SchoolConstant.ORDER_DESC, false);
        return baseMapper.findSubjectPage(page, subject);
    }

    @Override
    public List<Subject> findSubjects(Subject subject) {
        LambdaQueryWrapper<Subject> queryWrapper = new LambdaQueryWrapper<>();
        // 设置查询条件
        return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createSubject(Subject subject) {
        subject.setCreateTime(new Date());
        this.save(subject);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateSubject(Subject subject) {
        Subject subject2 = baseMapper.findById(subject.getSubjectId());
        if (StringUtils.isNotBlank(subject.getContent())) {
            subject2.setContent(subject.getContent());
        }
        if (StringUtils.isNotBlank(subject.getImgUrl())) {
            subject2.setImgUrl(subject.getImgUrl());
        }
        updateById(subject);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SchoolResponse deleteSubject(String[] subjectIds) {
        List<String> list = Arrays.asList(subjectIds);
        List<Subject> subjects = subjectMapper.selectList(null);
        for (String id : list) {
            Long countLong = userMapper.countUserBySubjectId(Long.valueOf(id));
            if (countLong > 0) {
                return new SchoolResponse().fail().message("该科目存在关联用户，无法进行删除");
            }
            for (Subject subject : subjects) {
                if (Long.valueOf(id).longValue() == subject.getSubjectId().longValue()) {
                    if (subject.getPSubjectId() == 0) {
                        for (Subject subject1 : subjects) {
                            if (Long.valueOf(id).longValue() == subject1.getPSubjectId().longValue()) {

                                return new SchoolResponse().fail().message("该科目存在子科目，无法进行删除");
//                                countLong = userMapper.countUserBySubjectId(Long.valueOf(subject1.getSubjectId()));
//                                if (countLong > 0) {
//                                    return new SchoolResponse().fail().message("该科目下级目录存在关联用户，无法进行删除");
//                                }
                            }
                        }
                    }
                }
            }
        }

        removeByIds(list);

        return new SchoolResponse().success();
    }

    @Override
    public List<DeptTree<Subject>> findSubjectTree() {
        List<Subject> subjectList = baseMapper.selectList(new QueryWrapper<>());
        List<DeptTree<Subject>> trees = convertSubject(subjectList);
        return TreeUtil.buildDeptTree(trees);
    }

    private List<DeptTree<Subject>> convertSubject(List<Subject> Subject) {
        List<DeptTree<Subject>> trees = new ArrayList<>();
        Subject.forEach(subject -> {
            DeptTree<Subject> tree = new DeptTree<>();
            tree.setId(String.valueOf(subject.getSubjectId()));
            tree.setParentId(String.valueOf(subject.getPSubjectId()));
            tree.setName(subject.getTitle());
            tree.setData(subject);
            trees.add(tree);
        });
        return trees;
    }


    @Override
    public List<QuestionTable> findSubjectsHasQuestion(QueryRequest request, Subject subject) {
        if (StringUtils.isNotBlank(subject.getCreateTimeFrom())
                && StringUtils.equals(subject.getCreateTimeFrom(), subject.getCreateTimeTo())) {
            subject.setCreateTimeFrom(subject.getCreateTimeFrom() + SchoolConstant.DAY_START_PATTERN_SUFFIX);
            subject.setCreateTimeTo(subject.getCreateTimeTo() + SchoolConstant.DAY_END_PATTERN_SUFFIX);
        }
        LambdaQueryWrapper<Subject> queryWrapper = new LambdaQueryWrapper<>();
        // 设置查询条件
        Page<Subject> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "subject_id", SchoolConstant.ORDER_ASC, false);
        String name = "三类人员继续教育";
        long id = findIdByTitle(name);
        IPage<Subject> subjectsIPage = subjectMapper.findSubjectDetailHasQuestionPage(page, id);

        List<QuestionTable> list1 = new ArrayList<QuestionTable>();
        Integer a = 0;
        IPage<QuestionTable> qTableIPage = new Page<>();
        for (Subject subject1 : subjectsIPage.getRecords()) {
            QuestionTable qTable = new QuestionTable();
            qTable.setSubjectName(name);
            qTable.setCreateTime(subject1.getCreateTime());
            qTable.setSubjectId(id);
            list1.add(qTable);
        }
        qTableIPage.setRecords(list1);
        Map<String, Object> map1 = new HashMap<>();
        map1.put("ipage", qTableIPage);
        map1.put("pages", subjectsIPage.getPages());
        return list1;
    }

    @Override
    public long findIdByTitle(String name) {
        long id = subjectMapper.findIdByTitle(name);
        return id;
    }
}
