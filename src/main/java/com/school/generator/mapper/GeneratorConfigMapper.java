package com.school.generator.mapper;

import com.school.generator.entity.GeneratorConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author MrBird
 */
public interface GeneratorConfigMapper extends BaseMapper<GeneratorConfig> {

}
