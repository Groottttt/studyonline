package com.school.job.mapper;


import com.school.job.entity.JobLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author MrBird
 */
public interface JobLogMapper extends BaseMapper<JobLog> {
}