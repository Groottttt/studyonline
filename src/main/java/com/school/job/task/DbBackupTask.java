package com.school.job.task;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.school.common.utils.FileConfiguration;

/**
 * @author MrBird
 */
@Slf4j
@Component
public class DbBackupTask {
	@Autowired
	private FileConfiguration fileConfiguration;

	public void dbBackup() {
		log.info("准备备份数据库...");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		File newDir = new File(fileConfiguration.getResourceDir() + "backups");
		if (!newDir.exists()) {
			newDir.mkdir();
		}

		String url = fileConfiguration.getUrl();
		String server = url.substring(url.indexOf("//") + 2, url.lastIndexOf(":"));
		String port = url.substring(url.lastIndexOf(":") + 1, url.lastIndexOf("/"));
		String dbName = url.substring(url.lastIndexOf("/") + 1, url.lastIndexOf("?"));

		String OS = "cmd /c mysqldump -u";
		if (StringUtils.contains(System.getenv("OS"), "Windows")) {
			OS = "cmd /c mysqldump -u";
		} else if (StringUtils.contains(System.getenv("OS"), "Linux")) {
			OS = "/usr/bin/mysqldump -u";
		} else {
			log.error("数据库备份失败，未知系统类型");
			return;
		}
		String cmd = OS + fileConfiguration.getUsername() + " -p" + fileConfiguration.getPassword() + " -h" + server
				+ " -P" + port + " --databases " + dbName + " -r " + fileConfiguration.getResourceDir() + "backups/"
				+ sdf.format(new Date()) + "-" + dbName + ".sql";

		try {
			Process process = Runtime.getRuntime().exec(cmd);
			if (process.waitFor() == 0) {
				log.info("数据库备份完成！");
			}

			// 清理
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DATE, -7);
			String expiredFilePath = fileConfiguration.getResourceDir() + "backups/" + sdf.format(calendar.getTime()) + "-"
					+ dbName + ".sql";
			File expiredFile = new File(expiredFilePath);
			if (expiredFile.exists()) {
				FileUtils.deleteQuietly(expiredFile);
				log.info("备份文件清理成功：" + expiredFile);
			}
		} catch (Exception e) {
			log.error("数据库备份失败！");
			e.printStackTrace();
		}
	}
}
