package com.school.monitor.controller;

import com.school.common.annotation.ControllerEndpoint;
import com.school.common.controller.BaseController;
import com.school.common.entity.SchoolResponse;
import com.school.common.utils.DateUtil;
import com.school.monitor.endpoint.SchoolHttpTraceEndpoint;
import com.school.monitor.entity.SchoolHttpTrace;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.boot.actuate.trace.http.HttpTrace;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author MrBird
 */
@Slf4j
@RestController
@RequestMapping("school/actuator")
@RequiredArgsConstructor
public class SchoolActuatorController extends BaseController {

    private final SchoolHttpTraceEndpoint httpTraceEndpoint;

    @GetMapping("httptrace")
    @RequiresPermissions("httptrace:view")
    @ControllerEndpoint(exceptionMessage = "请求追踪失败")
    public SchoolResponse httpTraces(String method, String url) {
        List<HttpTrace> httpTraceList = httpTraceEndpoint.traces().getTraces();
        List<SchoolHttpTrace> schoolHttpTraces = new ArrayList<>();
        httpTraceList.forEach(t -> {
            SchoolHttpTrace schoolHttpTrace = new SchoolHttpTrace();
            schoolHttpTrace.setRequestTime(DateUtil.formatInstant(t.getTimestamp(), DateUtil.FULL_TIME_SPLIT_PATTERN));
            schoolHttpTrace.setMethod(t.getRequest().getMethod());
            schoolHttpTrace.setUrl(t.getRequest().getUri());
            schoolHttpTrace.setStatus(t.getResponse().getStatus());
            schoolHttpTrace.setTimeTaken(t.getTimeTaken());
            if (StringUtils.isNotBlank(method) && StringUtils.isNotBlank(url)) {
                if (StringUtils.equalsIgnoreCase(method, schoolHttpTrace.getMethod())
                        && StringUtils.containsIgnoreCase(schoolHttpTrace.getUrl().toString(), url)) {
                    schoolHttpTraces.add(schoolHttpTrace);
                }
            } else if (StringUtils.isNotBlank(method)) {
                if (StringUtils.equalsIgnoreCase(method, schoolHttpTrace.getMethod())) {
                    schoolHttpTraces.add(schoolHttpTrace);
                }
            } else if (StringUtils.isNotBlank(url)) {
                if (StringUtils.containsIgnoreCase(schoolHttpTrace.getUrl().toString(), url)) {
                    schoolHttpTraces.add(schoolHttpTrace);
                }
            } else {
                schoolHttpTraces.add(schoolHttpTrace);
            }
        });
        return new SchoolResponse().success()
                .data(getDataTable(schoolHttpTraces, schoolHttpTraces.size()));
    }
}
