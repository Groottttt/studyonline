package com.school.monitor.controller;

import com.school.common.controller.BaseController;
import com.school.common.entity.SchoolResponse;
import com.school.monitor.entity.ActiveUser;
import com.school.monitor.service.ISessionService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author MrBird
 */
@RestController
@RequestMapping("session")
@RequiredArgsConstructor
public class SessionController extends BaseController {

    private final ISessionService sessionService;

    @GetMapping("list")
    @RequiresPermissions("online:view")
    public SchoolResponse list(String username) {
        List<ActiveUser> list = sessionService.list(username);
        return new SchoolResponse().success()
                .data(getDataTable(list, CollectionUtils.size(list)));
    }

    @GetMapping("delete/{id}")
    @RequiresPermissions("user:kickout")
    public SchoolResponse forceLogout(@PathVariable String id) {
        sessionService.forceLogout(id);
        return new SchoolResponse().success();
    }
}
