package com.school.monitor.controller;

import com.school.common.entity.SchoolConstant;
import com.school.common.utils.SchoolUtil;
import com.school.monitor.endpoint.SchoolMetricsEndpoint;
import com.school.monitor.entity.JvmInfo;
import com.school.monitor.entity.ServerInfo;
import com.school.monitor.entity.TomcatInfo;
import com.school.monitor.helper.SchoolActuatorHelper;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static com.school.monitor.endpoint.SchoolMetricsEndpoint.SchoolMetricResponse;

/**
 * @author MrBird
 */
@Controller("monitorView")
@RequestMapping(SchoolConstant.VIEW_PREFIX + "monitor")
@RequiredArgsConstructor
public class ViewController {

    private final SchoolActuatorHelper actuatorHelper;

    @GetMapping("online")
    @RequiresPermissions("online:view")
    public String online() {
        return SchoolUtil.view("monitor/online");
    }

    @GetMapping("log")
    @RequiresPermissions("log:view")
    public String log() {
        return SchoolUtil.view("monitor/log");
    }

    @GetMapping("loginlog")
    @RequiresPermissions("loginlog:view")
    public String loginLog() {
        return SchoolUtil.view("monitor/loginLog");
    }

    @GetMapping("httptrace")
    @RequiresPermissions("httptrace:view")
    public String httptrace() {
        return SchoolUtil.view("monitor/httpTrace");
    }

    @GetMapping("jvm")
    @RequiresPermissions("jvm:view")
    public String jvmInfo(Model model) {
        List<SchoolMetricsEndpoint.SchoolMetricResponse> jvm = actuatorHelper.getMetricResponseByType("jvm");
        JvmInfo jvmInfo = actuatorHelper.getJvmInfoFromMetricData(jvm);
        model.addAttribute("jvm", jvmInfo);
        return SchoolUtil.view("monitor/jvmInfo");
    }

    @GetMapping("tomcat")
    @RequiresPermissions("tomcat:view")
    public String tomcatInfo(Model model) {
        List<SchoolMetricsEndpoint.SchoolMetricResponse> tomcat = actuatorHelper.getMetricResponseByType("tomcat");
        TomcatInfo tomcatInfo = actuatorHelper.getTomcatInfoFromMetricData(tomcat);
        model.addAttribute("tomcat", tomcatInfo);
        return SchoolUtil.view("monitor/tomcatInfo");
    }

    @GetMapping("server")
    @RequiresPermissions("server:view")
    public String serverInfo(Model model) {
        List<SchoolMetricsEndpoint.SchoolMetricResponse> jdbcInfo = actuatorHelper.getMetricResponseByType("jdbc");
        List<SchoolMetricsEndpoint.SchoolMetricResponse> systemInfo = actuatorHelper.getMetricResponseByType("system");
        List<SchoolMetricsEndpoint.SchoolMetricResponse> processInfo = actuatorHelper.getMetricResponseByType("process");

        ServerInfo serverInfo = actuatorHelper.getServerInfoFromMetricData(jdbcInfo, systemInfo, processInfo);
        model.addAttribute("server", serverInfo);
        return SchoolUtil.view("monitor/serverInfo");
    }

    @GetMapping("swagger")
    public String swagger() {
        return SchoolUtil.view("monitor/swagger");
    }
}
