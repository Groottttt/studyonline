package com.school.monitor.endpoint;

import com.school.common.annotation.SchoolEndPoint;
import org.springframework.boot.actuate.trace.http.HttpTrace;
import org.springframework.boot.actuate.trace.http.HttpTraceRepository;

import java.util.List;

/**
 * @author MrBird
 */
@SchoolEndPoint
public class SchoolHttpTraceEndpoint {

    private final HttpTraceRepository repository;

    public SchoolHttpTraceEndpoint(HttpTraceRepository repository) {
        this.repository = repository;
    }

    public SchoolHttpTraceDescriptor traces() {
        return new SchoolHttpTraceDescriptor(repository.findAll());
    }

    public static final class SchoolHttpTraceDescriptor {

        private final List<HttpTrace> traces;

        private SchoolHttpTraceDescriptor(List<HttpTrace> traces) {
            this.traces = traces;
        }

        public List<HttpTrace> getTraces() {
            return traces;
        }
    }
}
