package com.school.monitor.mapper;

import com.school.monitor.entity.SystemLog;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @author ZhuXH
 */
@InterceptorIgnore(tenantLine = "true")
public interface LogMapper extends BaseMapper<SystemLog> {

    Long countSystemLogDetail(@Param("log") SystemLog log);

    /**
     * 查找日志详细信息
     * 
     * @param page 分页对象
     * @param user 用户对象，用于传递查询条件
     * @return Ipage
     */
    <T> IPage<SystemLog> findSystemLogDetail(Page<T> page, @Param("log") SystemLog log);
}
