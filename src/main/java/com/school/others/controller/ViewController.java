package com.school.others.controller;

import com.school.common.entity.SchoolConstant;
import com.school.common.utils.SchoolUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author MrBird
 */
@Controller("othersView")
@RequestMapping(SchoolConstant.VIEW_PREFIX + "others")
public class ViewController {

    @GetMapping("school/form")
    @RequiresPermissions("school:form:view")
    public String schoolForm() {
        return SchoolUtil.view("others/school/form");
    }

    @GetMapping("school/form/group")
    @RequiresPermissions("school:formgroup:view")
    public String schoolFormGroup() {
        return SchoolUtil.view("others/school/formGroup");
    }

    @GetMapping("school/tools")
    @RequiresPermissions("school:tools:view")
    public String schoolTools() {
        return SchoolUtil.view("others/school/tools");
    }

    @GetMapping("school/icon")
    @RequiresPermissions("school:icons:view")
    public String schoolIcon() {
        return SchoolUtil.view("others/school/icon");
    }

    @GetMapping("school/others")
    @RequiresPermissions("others:school:others")
    public String schoolOthers() {
        return SchoolUtil.view("others/school/others");
    }

    @GetMapping("apex/line")
    @RequiresPermissions("apex:line:view")
    public String apexLine() {
        return SchoolUtil.view("others/apex/line");
    }

    @GetMapping("apex/area")
    @RequiresPermissions("apex:area:view")
    public String apexArea() {
        return SchoolUtil.view("others/apex/area");
    }

    @GetMapping("apex/column")
    @RequiresPermissions("apex:column:view")
    public String apexColumn() {
        return SchoolUtil.view("others/apex/column");
    }

    @GetMapping("apex/radar")
    @RequiresPermissions("apex:radar:view")
    public String apexRadar() {
        return SchoolUtil.view("others/apex/radar");
    }

    @GetMapping("apex/bar")
    @RequiresPermissions("apex:bar:view")
    public String apexBar() {
        return SchoolUtil.view("others/apex/bar");
    }

    @GetMapping("apex/mix")
    @RequiresPermissions("apex:mix:view")
    public String apexMix() {
        return SchoolUtil.view("others/apex/mix");
    }

    @GetMapping("map")
    @RequiresPermissions("map:view")
    public String map() {
        return SchoolUtil.view("others/map/gaodeMap");
    }

    @GetMapping("eximport")
    @RequiresPermissions("others:eximport:view")
    public String eximport() {
        return SchoolUtil.view("others/eximport/eximport");
    }

    @GetMapping("eximport/result")
    @RequiresPermissions("others:eximport:view")
    public String eximportResult() {
        return SchoolUtil.view("others/eximport/eximportResult");
    }

    @GetMapping("datapermission")
    @RequiresPermissions("others:datapermission")
    public String dataPermissionTest() {
        return SchoolUtil.view("others/datapermission/test");
    }
}
