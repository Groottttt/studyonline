package com.school.others.mapper;

import com.school.others.entity.Eximport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author MrBird
 */
public interface EximportMapper extends BaseMapper<Eximport> {

}
