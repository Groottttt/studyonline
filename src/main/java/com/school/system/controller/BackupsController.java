package com.school.system.controller;

import com.school.common.annotation.ControllerEndpoint;
import com.school.common.controller.BaseController;
import com.school.common.entity.SchoolResponse;
import com.school.common.utils.FileConfiguration;
import com.school.system.entity.BackupFile;

import lombok.RequiredArgsConstructor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 备份文件 Controller
 *
 * @author MrBird
 * @date 2021-08-22 00:23:17
 */
@Controller
@RequestMapping("backups")
@RequiredArgsConstructor
public class BackupsController extends BaseController {
	@Autowired
	private FileConfiguration fileConfiguration;

	@GetMapping("list")
	@ResponseBody
	@RequiresPermissions("backups:view")
	public SchoolResponse BackupFileList() {
		String resourceDir = fileConfiguration.getResourceDir();
		String themePath = resourceDir + File.separator + "backups" + File.separator + File.separator;
		themePath = themePath.replaceAll("\\*", "/");

		File themeDir = new File(themePath);
		File[] listFiles = themeDir.listFiles();
		Arrays.sort(listFiles, Comparator.comparingLong(File::lastModified));
		
		List<BackupFile> list = new LinkedList<BackupFile>();
		for (File file : listFiles) {
			BackupFile backupFile = new BackupFile();
			String fileName = file.getName();
			backupFile.setFilePath(file.getParent());
			backupFile.setFileName(fileName);
			String extName = fileName.substring(fileName.lastIndexOf(".") == -1 ? 0 : fileName.lastIndexOf("."));
			backupFile.setExtName(extName);
			backupFile.setCreateTime(file.lastModified());
			if (file.isDirectory()) {
				backupFile.setType("directory");
				Long size = FileUtils.sizeOfDirectory(file) / 1024 / 1024;
				backupFile.setFileSize(size == 0 ? String.valueOf(FileUtils.sizeOfDirectory(file) / 1024) + "K"
						: String.valueOf(size) + "M");
			} else if (file.isFile()) {
				backupFile.setType("file");
				Long size = FileUtils.sizeOf(file) / 1024 / 1024;
				backupFile.setFileSize(
						size == 0 ? String.valueOf(FileUtils.sizeOf(file) / 1024) + "K" : String.valueOf(size) + "M");
			}
			list.add(backupFile);
		}

		Map<String, Object> backupFiles = new HashMap<String, Object>();
		backupFiles.put("rows", list);
		return new SchoolResponse().success().data(backupFiles);
	}

	@GetMapping("download")
	@RequiresPermissions("backups:download")
	@ControllerEndpoint(exceptionMessage = "下载失败")
	public void download(String path, String fileName, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String filePath = path + File.separator + fileName;
		File backupFile = new File(filePath);

		// 设置响应头和客户端保存文件名
		response.setCharacterEncoding("utf-8");
		response.setContentType("multipart/form-data");
		response.setHeader("Content-Disposition", "attachment;fileName=" + backupFile.getName());

		InputStream inputStream = new FileInputStream(backupFile);
		// 激活下载操作
		OutputStream os = response.getOutputStream();
		// 循环写入输出流
		byte[] b = new byte[1024];
		int length;
		while ((length = inputStream.read(b)) > 0) {
			os.write(b, 0, length);
		}
		// 关闭。
		os.close();
		inputStream.close();
	}
}
