package com.school.system.controller;

import com.google.zxing.WriterException;
import com.school.common.annotation.ControllerEndpoint;
import com.school.common.controller.BaseController;
import com.school.common.entity.QueryRequest;
import com.school.common.entity.SchoolResponse;
import com.school.gen.entity.Exam;
import com.school.gen.entity.PlayRecord;
import com.school.system.entity.ClassPojo;
import com.school.system.entity.User;
import com.school.system.service.IClassService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

/**
 * @author Mr.Z
 * @title: ClassController
 * @projectName studyonline
 * @description: TODO
 * @date 2022/9/216:11
 */
@SuppressWarnings({"all"})
@Slf4j
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("class")
public class ClassController extends BaseController {
    private final IClassService classService;
    @GetMapping("list")
    public SchoolResponse classList(ClassPojo classPojo, QueryRequest request) {
        return new SchoolResponse().success().data(getDataTable(classService.findClassDetailList(classPojo, request)));
    }
    @PostMapping("add")
    @ControllerEndpoint(operation = "新增班级", exceptionMessage = "新增班级失败")
    public SchoolResponse classAdd(@Valid ClassPojo classPojo) {
        return classService.classAdd(classPojo);
    }
    @PostMapping("update")
    @ControllerEndpoint(operation = "修改班级", exceptionMessage = "修改班级失败")
    public SchoolResponse classUpdata(@Valid ClassPojo classPojo) {
        return classService.classUpdata(classPojo);
    }
    @GetMapping("delete/{classIds}")
    public SchoolResponse classDelete(@PathVariable Integer[] classIds) {
        return  classService.classDelete(classIds);
    }
    @GetMapping("findStudents/{classesName}")
    public SchoolResponse findStudents(@PathVariable String classesName, User user, QueryRequest request) throws IOException, WriterException {
        return new SchoolResponse().success().data(getDataTable(classService.findStudents(classesName, user, request)));
    }

    @GetMapping("findStudy/{userId}")
    public SchoolResponse findStudy(@PathVariable long userId, PlayRecord playRecord, QueryRequest request) throws IOException, WriterException {
        return new SchoolResponse().success().data(getDataTable(classService.findStudy(userId, playRecord, request)));

    }

    @GetMapping("findExam/{userId}")
    public SchoolResponse findExam(@PathVariable long userId, Exam exam, QueryRequest request) {
        return new SchoolResponse().success().data(getDataTable(classService.findExam(userId, exam, request)));
    }



}
