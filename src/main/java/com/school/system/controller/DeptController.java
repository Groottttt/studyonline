package com.school.system.controller;


import com.school.common.annotation.ControllerEndpoint;
import com.school.common.entity.SchoolResponse;
import com.school.common.entity.QueryRequest;
import com.school.common.entity.Strings;
import com.school.common.exception.SchoolException;
import com.school.system.entity.Dept;
import com.school.system.service.IDeptService;
import com.wuwenze.poi.ExcelKit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * @author MrBird
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("dept")
public class DeptController {

    private final IDeptService deptService;

    @GetMapping("select/tree")
    @ControllerEndpoint(exceptionMessage = "获取部门树失败")
    public SchoolResponse getDeptTree() throws SchoolException {
        return new SchoolResponse().success().data(deptService.findDept());
    }

    @GetMapping("tree")
    @ControllerEndpoint(exceptionMessage = "获取部门树失败")
    public SchoolResponse getDeptTree(Dept dept) throws SchoolException {
        return new SchoolResponse().success().data(deptService.findDept(dept));
    }

    @PostMapping
    @RequiresPermissions("dept:add")
    @ControllerEndpoint(operation = "新增部门", exceptionMessage = "新增部门失败")
    public SchoolResponse addDept(@Valid Dept dept) {
        deptService.createDept(dept);
        return new SchoolResponse().success();
    }

    @GetMapping("delete/{deptIds}")
    @RequiresPermissions("dept:delete")
    @ControllerEndpoint(operation = "删除部门", exceptionMessage = "删除部门失败")
    public SchoolResponse deleteDept(@NotBlank(message = "{required}") @PathVariable String deptIds) throws SchoolException {
        deptService.deleteDept(StringUtils.split(deptIds, Strings.COMMA));
        return new SchoolResponse().success();
    }

    @PostMapping("update")
    @RequiresPermissions("dept:update")
    @ControllerEndpoint(operation = "修改部门", exceptionMessage = "修改部门失败")
    public SchoolResponse updateDept(@Valid Dept dept) throws SchoolException {
        deptService.updateDept(dept);
        return new SchoolResponse().success();
    }

    @GetMapping("excel")
    @RequiresPermissions("dept:export")
    @ControllerEndpoint(exceptionMessage = "导出Excel失败")
    public void export(Dept dept, QueryRequest request, HttpServletResponse response) throws SchoolException {
        ExcelKit.$Export(Dept.class, response)
                .downXlsx(deptService.findDept(dept, request), false);
    }
}
