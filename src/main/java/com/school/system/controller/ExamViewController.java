package com.school.system.controller;

import com.school.common.controller.BaseController;
import com.school.common.utils.SchoolUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller("examView")
@RequiredArgsConstructor
public class ExamViewController extends BaseController {

    @GetMapping("/exam/login")
    public Object login(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(SchoolUtil.view("exam/examLogin"));
        return mav;
    }
    @GetMapping("/exam/exam")
    public Object exam(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(SchoolUtil.view("exam/exam"));
        return mav;
    }

}
