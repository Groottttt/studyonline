package com.school.system.controller;

import com.school.common.annotation.Limit;
import com.school.common.controller.BaseController;
import com.school.common.entity.SchoolResponse;
import com.school.common.exception.SchoolException;
import com.school.common.properties.SchoolProperties;
import com.school.common.service.ValidateCodeService;
import com.school.common.utils.Md5Util;
import com.school.monitor.service.ILoginLogService;
import com.school.system.entity.User;
import com.school.system.mapper.UserMapper;
import com.school.system.service.IUserService;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.util.Map;

/**
 * @author MrBird
 */
@Validated
@RestController
@RequiredArgsConstructor
public class LoginController extends BaseController {

    private final IUserService userService;
    private final ValidateCodeService validateCodeService;
    private final ILoginLogService loginLogService;
    private final SchoolProperties properties;
    private final UserMapper userMapper;

    @PostMapping("login")
    @Limit(key = "login", period = 60, count = 10, name = "登录接口", prefix = "limit")
    public SchoolResponse login(
            @NotBlank(message = "{required}") String username,
            @NotBlank(message = "{required}") String password,
            @NotBlank(message = "{required}") String verifyCode,
            boolean rememberMe, HttpServletRequest request) throws SchoolException {
        validateCodeService.check(request.getSession().getId(), verifyCode);
        User user = userMapper.findByName(username);
        UsernamePasswordToken token = new UsernamePasswordToken(user.getMobile(),
                Md5Util.encrypt(username.toLowerCase(), password), rememberMe);
        super.login(token);
        // 保存登录日志
        loginLogService.saveLoginLog(username);
        return new SchoolResponse().success().data(properties.getShiro().getSuccessUrl());
    }

    @PostMapping("validate")
    @Limit(key = "login", period = 60, count = 10, name = "证书验证接口", prefix = "limit")
    public SchoolResponse validate(
            @NotBlank(message = "{required}") String verifyCode, HttpServletRequest request) throws SchoolException {
        validateCodeService.check(request.getSession().getId(), verifyCode);
        System.out.println(verifyCode);
        return new SchoolResponse().success();
    }

    @PostMapping("register")
    public SchoolResponse register(
            @NotBlank(message = "{required}") String username,
            @NotBlank(message = "{required}") String password) throws SchoolException {
        User user = userService.findByName(username);
        if (user != null) {
            throw new SchoolException("该用户名已存在");
        }
        userService.register(username, password);
        return new SchoolResponse().success();
    }

    @GetMapping("index/{username}")
    public SchoolResponse index(@NotBlank(message = "{required}") @PathVariable String username) {
        // 更新登录时间
        userService.updateLoginTime(username);
        // 获取首页数据
        Map<String, Object> data = loginLogService.retrieveIndexPageData(username);
        return new SchoolResponse().success().data(data);
    }

    @GetMapping("images/captcha")
    @Limit(key = "get_captcha", period = 60, count = 10, name = "获取验证码", prefix = "limit")
    public void captcha(HttpServletRequest request, HttpServletResponse response) throws IOException, SchoolException {
        validateCodeService.create(request, response);
    }
}
