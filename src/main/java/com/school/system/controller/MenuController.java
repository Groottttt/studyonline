package com.school.system.controller;


import com.school.common.annotation.ControllerEndpoint;
import com.school.common.controller.BaseController;
import com.school.common.entity.SchoolResponse;
import com.school.common.exception.SchoolException;
import com.school.system.entity.Menu;
import com.school.system.service.IMenuService;
import com.wuwenze.poi.ExcelKit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * @author MrBird
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("menu")
public class MenuController extends BaseController {

    private final IMenuService menuService;

    @GetMapping("{username}")
    public SchoolResponse getUserMenus(@NotBlank(message = "{required}") @PathVariable String username) throws SchoolException {
        if (!StringUtils.equalsIgnoreCase(username, getCurrentUser().getUsername())) {
            throw new SchoolException("您无权获取别人的菜单");
        }
        return new SchoolResponse().data(menuService.findUserMenus(username));
    }

    @GetMapping("tree")
    @ControllerEndpoint(exceptionMessage = "获取菜单树失败")
    public SchoolResponse getMenuTree(Menu menu) {
        return new SchoolResponse().success()
                .data(menuService.findMenus(menu).getChilds());
    }

    @PostMapping
    @RequiresPermissions("menu:add")
    @ControllerEndpoint(operation = "新增菜单/按钮", exceptionMessage = "新增菜单/按钮失败")
    public SchoolResponse addMenu(@Valid Menu menu) {
        menuService.createMenu(menu);
        return new SchoolResponse().success();
    }

    @GetMapping("delete/{menuIds}")
    @RequiresPermissions("menu:delete")
    @ControllerEndpoint(operation = "删除菜单/按钮", exceptionMessage = "删除菜单/按钮失败")
    public SchoolResponse deleteMenus(@NotBlank(message = "{required}") @PathVariable String menuIds) {
        menuService.deleteMenus(menuIds);
        return new SchoolResponse().success();
    }

    @PostMapping("update")
    @RequiresPermissions("menu:update")
    @ControllerEndpoint(operation = "修改菜单/按钮", exceptionMessage = "修改菜单/按钮失败")
    public SchoolResponse updateMenu(@Valid Menu menu) {
        menuService.updateMenu(menu);
        return new SchoolResponse().success();
    }

    @GetMapping("excel")
    @RequiresPermissions("menu:export")
    @ControllerEndpoint(exceptionMessage = "导出Excel失败")
    public void export(Menu menu, HttpServletResponse response) {
        ExcelKit.$Export(Menu.class, response).downXlsx(menuService.findMenuList(menu), false);
    }
}
