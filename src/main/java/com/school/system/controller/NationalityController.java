package com.school.system.controller;


import com.school.common.annotation.ControllerEndpoint;
import com.school.common.entity.SchoolResponse;
import com.school.common.exception.SchoolException;
import com.school.system.entity.Nationality;
import com.school.system.service.INationalityService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author MrBird
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("nationality")
public class NationalityController {

    private final INationalityService nationalityService;

    @GetMapping("select/tree")
    @ControllerEndpoint(exceptionMessage = "获取民族树失败")
    public SchoolResponse getNationalityTree() throws SchoolException {
        return new SchoolResponse().success().data(nationalityService.findNationality());
    }

    @GetMapping("tree")
    @ControllerEndpoint(exceptionMessage = "获取民族树失败")
    public SchoolResponse getNationalityTree(Nationality nationality) throws SchoolException {
        return new SchoolResponse().success().data(nationalityService.findNationality(nationality));
    }
}
