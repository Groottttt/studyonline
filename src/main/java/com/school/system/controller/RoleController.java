package com.school.system.controller;


import com.school.common.annotation.ControllerEndpoint;
import com.school.common.controller.BaseController;
import com.school.common.entity.SchoolResponse;
import com.school.common.entity.QueryRequest;
import com.school.common.exception.SchoolException;
import com.school.system.entity.Role;
import com.school.system.service.IRoleService;
import com.wuwenze.poi.ExcelKit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * @author MrBird
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("role")
public class RoleController extends BaseController {

    private final IRoleService roleService;

    @GetMapping
    public SchoolResponse getAllRoles(Role role) {
        return new SchoolResponse().success().data(roleService.findRoles(role));
    }

    @GetMapping("list")
    @RequiresPermissions("role:view")
    public SchoolResponse roleList(Role role, QueryRequest request) {
        return new SchoolResponse().success()
                .data(getDataTable(roleService.findRoles(role, request)));
    }

    @PostMapping
    @RequiresPermissions("role:add")
    @ControllerEndpoint(operation = "新增角色", exceptionMessage = "新增角色失败")
    public SchoolResponse addRole(@Valid Role role) {
        roleService.createRole(role);
        return new SchoolResponse().success();
    }

    @GetMapping("delete/{roleIds}")
    @RequiresPermissions("role:delete")
    @ControllerEndpoint(operation = "删除角色", exceptionMessage = "删除角色失败")
    public SchoolResponse deleteRoles(@NotBlank(message = "{required}") @PathVariable String roleIds) {
        roleService.deleteRoles(roleIds);
        return new SchoolResponse().success();
    }

    @PostMapping("update")
    @RequiresPermissions("role:update")
    @ControllerEndpoint(operation = "修改角色", exceptionMessage = "修改角色失败")
    public SchoolResponse updateRole(Role role) {
        roleService.updateRole(role);
        return new SchoolResponse().success();
    }

    @GetMapping("excel")
    @RequiresPermissions("role:export")
    @ControllerEndpoint(exceptionMessage = "导出Excel失败")
    public void export(QueryRequest queryRequest, Role role, HttpServletResponse response) throws SchoolException {
        ExcelKit.$Export(Role.class, response)
                .downXlsx(roleService.findRoles(role, queryRequest).getRecords(), false);
    }

}
