package com.school.system.controller;

import com.school.common.annotation.ControllerEndpoint;
import com.school.common.controller.BaseController;
import com.school.common.entity.SchoolResponse;
import com.school.common.entity.QueryRequest;
import com.school.common.entity.Strings;
import com.school.common.exception.SchoolException;
import com.school.common.utils.DateUtil;
import com.school.common.utils.FileConfiguration;
import com.school.common.utils.Md5Util;
import com.school.gen.entity.Exam;
import com.school.gen.mapper.SubjectMapper;
import com.school.gen.service.impl.ExamServiceImpl;
import com.school.system.entity.User;
import com.school.system.service.IUserService;
import com.google.common.collect.ImmutableMap;
import com.google.zxing.WriterException;
import com.wuwenze.poi.ExcelKit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.*;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * @author MrBird
 */
@Slf4j
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("user")
public class UserController extends BaseController {

    private final IUserService userService;
    private final FileConfiguration fileConfiguration;
    private final SubjectMapper subjectMapper;
    private final ExamServiceImpl examService;

    @GetMapping("{username}")
    public User getUser(@NotBlank(message = "{required}") @PathVariable String username) {
        return userService.findUserDetailList(username);
    }

    @GetMapping("check/{username}")
    public boolean checkUserName(@NotBlank(message = "{required}") @PathVariable String username, String userId) {
        return userService.findByName(username) == null || StringUtils.isNotBlank(userId);
    }

    @GetMapping("list")
    @RequiresPermissions("user:view")
    public SchoolResponse userList(User user, QueryRequest request) {
        return new SchoolResponse().success().data(getDataTable(userService.findUserDetailList(user, request)));
    }

    @PostMapping
    @RequiresPermissions("user:add")
    @ControllerEndpoint(operation = "新增用户", exceptionMessage = "新增用户失败")
    public SchoolResponse addUser(@Valid User user) {
        User userTmp = userService.findByMobile(user.getMobile());
        if (userTmp != null) {
            return new SchoolResponse().fail().data("该手机号已存在");
        }
        userService.createUser(user);
        return new SchoolResponse().success();
    }

    @GetMapping("delete/{userIds}")
    @RequiresPermissions("user:delete")
    @ControllerEndpoint(operation = "删除用户", exceptionMessage = "删除用户失败")
    public SchoolResponse deleteUsers(@NotBlank(message = "{required}") @PathVariable String userIds) {
        userService.deleteUsers(StringUtils.split(userIds, Strings.COMMA));
        return new SchoolResponse().success();
    }

    @PostMapping("update")
    @RequiresPermissions("user:update")
    @ControllerEndpoint(operation = "修改用户", exceptionMessage = "修改用户失败")
    public SchoolResponse updateUser(@Valid User user) {
        if (user.getUserId() == null) {
            throw new SchoolException("用户ID为空");
        }

        User userTmp = userService.findByMobile(user.getMobile());
        if (userTmp != null) {
            if (user.getUserId().longValue() != userTmp.getUserId().longValue()) {
                return new SchoolResponse().fail().data("该手机号已存在");
            }
            userService.updateUser(user);
            return new SchoolResponse().success();
        } else {
            userService.updateUser(user);
            return new SchoolResponse().success();
        }
    }

    @PostMapping("password/reset/{usernames}")
    @RequiresPermissions("user:password:reset")
    @ControllerEndpoint(exceptionMessage = "重置用户密码失败")
    public SchoolResponse resetPassword(@NotBlank(message = "{required}") @PathVariable String usernames) {
        userService.resetPassword(StringUtils.split(usernames, Strings.COMMA));
        return new SchoolResponse().success();
    }

    @PostMapping("password/reset_id/{userIds}")
    @RequiresPermissions("user:password:reset")
    @ControllerEndpoint(exceptionMessage = "重置用户密码失败")
    public SchoolResponse resetPasswordById(@NotBlank(message = "{required}") @PathVariable String userIds) {
        userService.resetPasswordByIds(StringUtils.split(userIds, Strings.COMMA));
        return new SchoolResponse().success();
    }

    @PostMapping("password/update")
    @ControllerEndpoint(exceptionMessage = "修改密码失败")
    public SchoolResponse updatePassword(@NotBlank(message = "{required}") String oldPassword,
                                         @NotBlank(message = "{required}") String newPassword) {
        User user = getCurrentUser();
        if (!StringUtils.equals(user.getPassword(), Md5Util.encrypt(user.getUsername(), oldPassword))) {
            throw new SchoolException("原密码不正确");
        }
        userService.updatePassword(user.getUsername(), newPassword);
        return new SchoolResponse().success();
    }

    @GetMapping("avatar/{image}")
    @ControllerEndpoint(exceptionMessage = "修改头像失败")
    public SchoolResponse updateAvatar(@NotBlank(message = "{required}") @PathVariable String image) {
        userService.updateAvatar(getCurrentUser().getUsername(), image);
        return new SchoolResponse().success();
    }



    @PostMapping("theme/update")
    @ControllerEndpoint(exceptionMessage = "修改系统配置失败")
    public SchoolResponse updateTheme(String theme, String isTab) {
        userService.updateTheme(getCurrentUser().getUsername(), theme, isTab);
        return new SchoolResponse().success();
    }

    @PostMapping("profile/update")
    @ControllerEndpoint(exceptionMessage = "修改个人信息失败")
    public SchoolResponse updateProfile(User user) throws SchoolException {
        user.setUserId(getCurrentUser().getUserId());
        userService.updateProfile(user);
        return new SchoolResponse().success();
    }

    @GetMapping("excel/{subjectId}")
    @RequiresPermissions("user:export")
    @ControllerEndpoint(exceptionMessage = "导出Excel失败")
    public void export(@PathVariable long subjectId, HttpServletResponse response) {
        List<User> users = userService.findUserList(subjectId);
        ExcelKit.$Export(com.school.excel.entity.UserExcel.class, response)
                .downXlsx(users, false);
    }




    // 图片上传测试
    @ResponseBody
    @PostMapping("upload/{userId}")
    @ControllerEndpoint(exceptionMessage = "上传文件失败")
    public Map upload(MultipartFile file, @NotBlank(message = "{required}") @PathVariable String userId,
                      HttpServletRequest request) {
        String prefix = "";
        // 保存上传
        OutputStream out = null;
        InputStream fileInput = null;
        try {
            if (file != null) {
                String originalName = file.getOriginalFilename();
                prefix = originalName.substring(originalName.lastIndexOf(".") + 1);
                String uuid = UUID.randomUUID() + "";
                String filepath = File.separator + "data" + File.separator + "SCHOOL" + File.separator + "upload"
                        + File.separator + "certificate" + File.separator + userId + File.separator + uuid + "."
                        + prefix;

                File files = new File(filepath);
                // 打印查看上传路径
                System.out.println(filepath);
                if (!files.getParentFile().exists()) {
                    files.getParentFile().mkdirs();
                }
                file.transferTo(files);
                Map<String, Object> map2 = new HashMap<>();
                Map<String, Object> map = new HashMap<>();
                map.put("code", 0);
                map.put("msg", "");
                map.put("data", map2);
                map2.put("src", "/upload/certificate/" + userId + "/" + uuid + "." + prefix);
                return map;
            }

        } catch (Exception e) {
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (fileInput != null) {
                    fileInput.close();
                }
            } catch (IOException e) {
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("code", 1);
        map.put("msg", "");
        return map;

    }

    //生成证书
    @PostMapping("generate_certificate")
    public SchoolResponse generateCertificate(@RequestParam(value = "userId") Long userId, @RequestParam(value = "subjectId") Long subjectId) {
        return userService.generateCertificate(userId, subjectId);
    }
    //删除证书
    @PostMapping("delete_certificate")
    public SchoolResponse deleteCertificate(@RequestParam(value = "userId") Long userId,@RequestParam(value = "subjectId") Long subjectId) {
        return userService.deleteCertificate(userId,subjectId);
    }


}

