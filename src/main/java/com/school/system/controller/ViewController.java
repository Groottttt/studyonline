package com.school.system.controller;

import com.school.common.controller.BaseController;
import com.school.common.entity.SchoolConstant;
import com.school.common.utils.DateUtil;
import com.school.common.utils.SchoolUtil;
import com.school.common.utils.HumanSizeUtil;
import com.school.gen.entity.*;
import com.school.gen.mapper.CourseTypeMapper;
import com.school.gen.mapper.SubjectMapper;
import com.school.gen.service.ICourseService;
import com.school.gen.service.IFaceDetectService;
import com.school.gen.service.IQuestionService;
import com.school.gen.service.ISubjectService;
import com.school.system.entity.Certificate;
import com.school.system.entity.ClassPojo;
import com.school.system.entity.User;
import com.school.system.mapper.CertificateMapper;
import com.school.system.mapper.ClassMapper;
import com.school.system.mapper.UserMapper;
import com.school.system.service.IUserDataPermissionService;
import com.school.system.service.IUserService;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.session.ExpiredSessionException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * @author MrBird
 */
@Controller("systemView")
@RequiredArgsConstructor
public class ViewController extends BaseController {

    private final IUserService userService;
    private final ICourseService courseService;
    private final CourseTypeMapper courseTypeMapper;
    private final ISubjectService subjectService;
    private final IUserDataPermissionService userDataPermissionService;
    private final IFaceDetectService faceDetectService;
    private final IQuestionService questionService;
    private final CertificateMapper certificateMapper;
    private final SubjectMapper subjectMapper;
    private final ClassMapper classMapper;

    @GetMapping("login")
    @ResponseBody
    public Object login(HttpServletRequest request) {
        if (SchoolUtil.isAjaxRequest(request)) {
            throw new ExpiredSessionException();
        } else {
            ModelAndView mav = new ModelAndView();
            mav.setViewName(SchoolUtil.view("login"));
            return mav;
        }
    }

    @GetMapping("qrcode")
    @ResponseBody
    public Object qrcode(HttpServletRequest request,long userId,Model model) {
        if (SchoolUtil.isAjaxRequest(request)) {
            throw new ExpiredSessionException();
        } else {
            ModelAndView mav = new ModelAndView();
            model.addAttribute("userId",userId);
            mav.setViewName(SchoolUtil.view("qrcode"));
            return mav;
        }
    }

    @GetMapping("unauthorized")
    public String unauthorized() {
        return SchoolUtil.view("error/403");
    }

    @GetMapping("/")
    public String redirectIndex() {
        return "redirect:/index";
    }

    @GetMapping("index")
    public String index(Model model) {
        User principal = userService.findByName(getCurrentUser().getUsername());
        userService.doGetUserAuthorizationInfo(principal);
        principal.setPassword("It's a secret");
        model.addAttribute("user", principal);
        model.addAttribute("permissions", principal.getStringPermissions());
        model.addAttribute("roles", principal.getRoles());
        return "index";
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "layout")
    public String layout() {
        return SchoolUtil.view("layout");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "user/avatar")
    public String userAvatar() {
        return SchoolUtil.view("system/user/avatar");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "password/update")
    public String passwordUpdate() {
        return SchoolUtil.view("system/user/passwordUpdate");
    }


    @GetMapping(SchoolConstant.VIEW_PREFIX + "/classManagement")
    public String ClassManagement() {
        return SchoolUtil.view("classManagement/classManagement");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "/classManagement/classAdd")
    public String classAdd() {
        return SchoolUtil.view("classManagement/classAdd");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "/classManagement/classUpdate/{classesId}")
    public String classUpdate(@PathVariable Long classesId,Model model) {
        ClassPojo classPojo = classMapper.selectById(classesId);
        model.addAttribute("classes",classPojo);
        return SchoolUtil.view("classManagement/classUpdate");
    }
    @GetMapping(SchoolConstant.VIEW_PREFIX + "/classManagement/studyFollow/{classesName}")
    public String studyFollow(@PathVariable String classesName,Model model) {
        model.addAttribute("classesName",classesName);
        return SchoolUtil.view("classManagement/studyFollow");
    }
    @GetMapping(SchoolConstant.VIEW_PREFIX + "/classManagement/studyRecord/{userId}")
    public String studyRecord(@PathVariable Long userId,Model model) {
        model.addAttribute("userId",userId);
        return SchoolUtil.view("classManagement/studyRecord");
    }
    @GetMapping(SchoolConstant.VIEW_PREFIX + "/classManagement/examRecord/{userId}")
    public String examRecord(@PathVariable Long userId,Model model) {
        model.addAttribute("userId",userId);
        return SchoolUtil.view("classManagement/examRecord");
    }
    @GetMapping(SchoolConstant.VIEW_PREFIX + "/courseType")
    public String coursetype() {
        return SchoolUtil.view("coursetype/coursetype");
    }
    @GetMapping(SchoolConstant.VIEW_PREFIX + "/courseType/add")
    public String courseTypeAdd() {
        return SchoolUtil.view("coursetype/courseTypeAdd");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "courseType/update/{ctypeId}")
    public String courseTypeUpdate(@PathVariable Long ctypeId,Model model) {
        CourseType ctype = courseTypeMapper.selectById(ctypeId);
        model.addAttribute("ctype",ctype);
        return SchoolUtil.view("coursetype/courseTypeUpdate");
    }



    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/user/addExamFrequency/{userId}")
    public String addExamFrequency(@PathVariable Long userId,Model model) {
        model.addAttribute("userId",userId);
        return SchoolUtil.view("system/user/addExamFrequency");
    }
    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/user/generateCertificate/{userId}")
    public String generateCertificate(@PathVariable Long userId,Model model) {
        model.addAttribute("userId",userId);
        return SchoolUtil.view("system/user/generateCertificate");
    }
    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/user/deleteCertificate/{userId}")
    public String deleteCertificate(@PathVariable Long userId,Model model) {
        model.addAttribute("userId",userId);
        return SchoolUtil.view("system/user/deleteCertificate");
    }


    @GetMapping(SchoolConstant.VIEW_PREFIX + "user/profile/update")
    public String profileUpdate() {
        return SchoolUtil.view("system/user/profileUpdate");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/user")
    @RequiresPermissions("user:view")
    public String systemUser() {
        return SchoolUtil.view("system/user/user");
    }
    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/user/add")
    @RequiresPermissions("user:add")
    public String systemUserAdd() {
        return SchoolUtil.view("system/user/userAdd");
    }


    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/user/detail/{userId}")
    @RequiresPermissions("user:view")
    public String systemUserDetail(@PathVariable Long userId, Model model) {
        resolveUserModel(userId, model, true);
        return SchoolUtil.view("system/user/userDetail");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "user/profile")
    public String userProfile() {
        return SchoolUtil.view("system/user/userProfile");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/user/update/{userId}")
    @RequiresPermissions("user:update")
    public String systemUserUpdate(@PathVariable Long userId, Model model) {
        resolveUserModel(userId, model, false);
        return SchoolUtil.view("system/user/userUpdate");
    }



    @GetMapping(SchoolConstant.VIEW_PREFIX + "admin/avatar")
    public String adminAvatar() {
        return SchoolUtil.view("system/admin/avatar");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "admin/password/update")
    @RequiresPermissions("admin:password:reset")
    public String adminPasswordUpdate() {
        return SchoolUtil.view("system/admin/passwordUpdate");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "admin/profile/update")
    public String adminProfileUpdate() {
        return SchoolUtil.view("system/admin/profileUpdate");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/admin")
    @RequiresPermissions("admin:view")
    public String systemAdmin() {
        return SchoolUtil.view("system/admin/user");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/admin/add")
    @RequiresPermissions("admin:add")
    public String systemAdminAdd() {
        return SchoolUtil.view("system/admin/userAdd");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/admin/detail/{username}")
    @RequiresPermissions("admin:view")
    public String systemAdminDetail(@PathVariable String username, Model model) {
        resolveUserModel(username, model, true);
        return SchoolUtil.view("system/admin/userDetail");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "admin/profile")
    public String adminProfile() {
        return SchoolUtil.view("system/admin/userProfile");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/admin/update/{username}")
    @RequiresPermissions("admin:update")
    public String systemAdminUpdate(@PathVariable String username, Model model) {
        resolveUserModel(username, model, false);
        return SchoolUtil.view("system/admin/userUpdate");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/role")
    @RequiresPermissions("role:view")
    public String systemRole() {
        return SchoolUtil.view("system/role/role");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/menu")
    @RequiresPermissions("menu:view")
    public String systemMenu() {
        return SchoolUtil.view("system/menu/menu");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/dept")
    @RequiresPermissions("dept:view")
    public String systemDept() {
        return SchoolUtil.view("system/dept/dept");
    }

    @RequestMapping(SchoolConstant.VIEW_PREFIX + "index")
    public String pageIndex() {
        return SchoolUtil.view("index");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "404")
    public String error404() {
        return SchoolUtil.view("error/404");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "403")
    public String error403() {
        return SchoolUtil.view("error/403");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "500")
    public String error500() {
        return SchoolUtil.view("error/500");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "course/add")
    @RequiresPermissions("course:add")
    public String CourseAdd() {
        return SchoolUtil.view("course/courseAdd");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "course")
    @RequiresPermissions("course:view")
    public String courseIndex() {
        return SchoolUtil.view("course/course");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "course/detail/{courseId}")
    @RequiresPermissions("course:view")
    public String CourseDetail(@PathVariable Long courseId, Model model) {
        resolveCourseModel(courseId, model, true);
        return SchoolUtil.view("course/courseDetail");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "course/update/{courseId}")
    @RequiresPermissions("course:update")
    public String CourseUpdate(@PathVariable Long courseId, Model model) {
        resolveCourseModel(courseId, model, false);
        return SchoolUtil.view("course/courseUpdate");
    }


    private void resolveUserModel(String username, Model model, Boolean transform) {
        User user = userService.findByName(username);
        String deptIds = userDataPermissionService.findByUserId(String.valueOf(user.getUserId()));
        user.setDeptIds(deptIds);
        model.addAttribute("user", user);

        if (transform) {
            String sex = user.getSex();
            switch (sex) {
                case User.SEX_MALE:
                    user.setSex("男");
                    break;
                case User.SEX_FEMALE:
                    user.setSex("女");
                    break;
                default:
                    user.setSex("保密");
                    break;
            }
        }
        if (user.getLastLoginTime() != null) {
            model.addAttribute("lastLoginTime",
                    DateUtil.getDateFormat(user.getLastLoginTime(), DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
    }


    private void resolveUserModel(Long userId, Model model, Boolean transform) {
        List<Certificate> certificates = certificateMapper.getById(userId);
        List<Certificate> certificateUrls = new ArrayList<>();
        for (Certificate c : certificates) {
            certificateUrls.add(c);
        }
        User user = userService.getById(userId);
        String gradeUrl = File.separator + "upload" + File.separator + "certificate" + File.separator
                + user.getUserId() + File.separator + "grade" + ".png";
        user.setGradeUrl(gradeUrl);
        user.setCertificateUrls(certificateUrls);
        String deptIds = userDataPermissionService.findByUserId(String.valueOf(user.getUserId()));
        user.setDeptIds(deptIds);
        model.addAttribute("user", user);
        if (transform) {
            String sex = user.getSex();
            switch (sex) {
                case User.SEX_MALE:
                    user.setSex("男");
                    break;
                case User.SEX_FEMALE:
                    user.setSex("女");
                    break;
                default:
                    user.setSex("保密");
                    break;
            }
        }
        if (user.getLastLoginTime() != null) {
            model.addAttribute("lastLoginTime",
                    DateUtil.getDateFormat(user.getLastLoginTime(), DateUtil.FULL_TIME_SPLIT_PATTERN));
        }
    }

    private void resolveCourseModel(Long courseId, Model model, Boolean transform) {
        Course course = courseService.findById(courseId);
        String videosize = HumanSizeUtil.humanReadableByteCountBin(course.getVideosize());
        course.setVideoSizeHuman(String.valueOf(videosize));
        model.addAttribute("course", course);
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "subject")
    public String subjectIndex() {
        return SchoolUtil.view("subject/subject");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "subject/add")
    @RequiresPermissions("subject:add")
    public String SubjectAdd() {
        return SchoolUtil.view("subject/subjectAdd");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "subject/detail/{subjectId}")
    @RequiresPermissions("subject:view")
    public String SubjectDetail(@PathVariable Long subjectId, Model model) {
        resolveSubjectModel(subjectId, model, true);
        return SchoolUtil.view("subject/subjectDetail");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "subject/update/{subjectId}")
    @RequiresPermissions("subject:update")
    public String SubjectUpdate(@PathVariable Long subjectId, Model model) {
        resolveSubjectModel(subjectId, model, false);
        return SchoolUtil.view("subject/subjectUpdate");
    }

    private void resolveSubjectModel(Long subjectId, Model model, Boolean transform) {
        Subject subject = subjectService.findById(subjectId);
        if (transform) {
            switch (subject.getIsPublic()) {
                case Subject.PUBLIC_YES:
                    subject.setIsPublic("是");
                    break;
                case Subject.PUBLIC_NO:
                    subject.setIsPublic("否");
                    break;
            }
        }
        model.addAttribute("subject", subject);
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "face_detect")
    @RequiresPermissions("face_detect:view")
    public String faceDetectIndex() {
        return SchoolUtil.view("face_detect/face_detect");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "face_detect/detail/{faceDetectId}")
    @RequiresPermissions("subject:view")
    public String FaceDetectDetail(@PathVariable Long faceDetectId, Model model) {
        resolveFaceDetectModel(faceDetectId, model, true);
        return SchoolUtil.view("face_detect/face_detect_detail");
    }

    private void resolveFaceDetectModel(Long faceDetectId, Model model, Boolean transform) {
        FaceDetect faceDetect = faceDetectService.findById(faceDetectId);
        model.addAttribute("faceDetect", faceDetect);
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/backups")
    @RequiresPermissions("backups:view")
    public String systemBackups() {
        return SchoolUtil.view("system/backups/backups");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "question")
    @RequiresPermissions("question:view")
    public String questionIndex() {
        return SchoolUtil.view("question/question");
    }


    @GetMapping(SchoolConstant.VIEW_PREFIX + "question/detail/{param}")
    @RequiresPermissions("question:view")
    public String questionDetail(@PathVariable Long param, Model model) {
        Question question = new Question();
        String name = "三类人员继续教育";
        Long subjectId = subjectService.findIdByTitle(name);
        Subject san = subjectMapper.findById(param);
        if (param.equals(subjectId)) {
            question.setSubject(subjectId);
            List<Question> questions = questionService.findQuestions(question);
            model.addAttribute("questions", questions);
            model.addAttribute("subjectId", subjectId);
            return SchoolUtil.view("question/questionSubDetail");
        } else {
            question.setCourseId(Long.valueOf(param));
            List<Question> questions = questionService.findQuestions(question);
            model.addAttribute("questions", questions);
            model.addAttribute("courseId", param);
            return SchoolUtil.view("question/questionDetail");
        }

    }


    @GetMapping(SchoolConstant.VIEW_PREFIX + "question/add")
    @RequiresPermissions("question:add")
    public String questionAdd() {
        return SchoolUtil.view("question/questionAdd");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "question/upload")
    @RequiresPermissions("question:upload")
    public String questionUpload() {
        return SchoolUtil.view("question/questionUpload");
    }


    @GetMapping(SchoolConstant.VIEW_PREFIX + "question/update/{questionId}")
    @RequiresPermissions("question:update")
    public String questionUpdate(@PathVariable Long questionId, Model model) {
        Question question = questionService.findById(questionId);
        model.addAttribute("question", question);
        return SchoolUtil.view("question/questionUpdate");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "question/updateS/{questionId}")
    @RequiresPermissions("question:update")
    public String questionUpdateS(@PathVariable Long questionId, Model model) {
        Question question = questionService.findSById(questionId);
        model.addAttribute("question", question);
        return SchoolUtil.view("question/questionUpdateS");
    }

    @GetMapping(SchoolConstant.VIEW_PREFIX + "system/user/export")
    @RequiresPermissions("user:export")
    public String systemUserExport() {
        return SchoolUtil.view("system/user/export");
    }
}
