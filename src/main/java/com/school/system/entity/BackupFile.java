package com.school.system.entity;

import java.io.Serializable;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.wuwenze.poi.annotation.Excel;

/**
 * 课程表 Entity
 *
 * @author MrBird
 * @date 2021-08-22 00:23:17
 */
@Data
@Excel("备份文件信息")
public class BackupFile implements Serializable {
    
	private static final long serialVersionUID = 4352868070865765001L;
    
    @TableField(exist = false)
    private String filePath;
    
    @TableField(exist = false)
    private String fileName;

    @TableField(exist = false)
    private String extName;
    
    @TableField(exist = false)
    private String fileSize;
    
    @TableField(exist = false)
    private String type;
    
    @TableField(exist = false)
    private Long createTime;
}
