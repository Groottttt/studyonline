package com.school.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("t_certificate")
public class Certificate implements Serializable {

    /**
     * 证书 ID
     */
    @TableId(value = "ID",type = IdType.AUTO)
    private Long id;

    /**
     * 用户 ID
     */
    @TableField("USER_ID")
    private Long userId;

    /**
     * 课程 标题
     */
    @TableField("SUBJECT_NAME")
    private String subjectName;

    /**
     * 创建 时间
     */
    @TableField("C_CREATE_TIME")
    private Date cCreateTime;

    /**
     * 证书 路径
     */
    @TableField("CERTIFICATE_URL")
    private String certificateUrl;

    /**
     * 考试 成绩
     */
    @TableField("GRADE")
    private Integer grade;



}
