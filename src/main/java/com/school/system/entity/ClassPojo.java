package com.school.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @author Mr.Z
 * @title: ClassPojo
 * @projectName studyonline
 * @description: TODO
 * @date 2022/9/216:14
 */
@SuppressWarnings({"all"})
@Data
@TableName("t_class")
@Excel("班级信息表")
public class ClassPojo {
    /**
     * 班级ID
     */
    @TableId(value = "class_id", type = IdType.AUTO)
    private Long CLASS_ID;
    /**
     * 班级名称
     */
    private String NAME;
    /**
     * 专业id
     */
    private String SUBJECT_ID;
    /**
     * 创建时间
     */
    private Date CREATE_TIME;
    /**
     * 状态
     */
    private Integer PUBLIC;
    /**
     * 专业名称
     */
    @TableField(exist = false)
    private String MAJOR_NAME;
    /**
     * 班级人数
     */
    @TableField(exist = false)
    private String SUM;
    /**
     * 专业名称
     */
    @TableField(exist = false)
    private String TITLE;

}
