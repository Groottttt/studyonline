package com.school.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author MrBird
 */
@Data
@TableName("t_nationality")
@Excel("民族信息表")
public class Nationality implements Serializable {

    public static final Long TOP_NODE = 0L;
    private static final long serialVersionUID = 5702271346763798328L;
    /**
     * 民族 ID
     */
    @TableId(value = "NATIONALITY_ID", type = IdType.AUTO)
    private Long nationalityId;

    /**
     * 民族名称
     */
    @TableField("NATIONALITY_NAME")
    private String nationalityName;

    /**
     * 排序
     */
    @TableField("ORDER_NUM")
    private Long orderNum;

    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    @TableField("MODIFY_TIME")
    private Date modifyTime;

}
