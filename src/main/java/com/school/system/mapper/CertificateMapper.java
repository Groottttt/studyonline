package com.school.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.school.system.entity.Certificate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
@Mapper
public interface CertificateMapper extends BaseMapper<Certificate> {

    List<Certificate> getById(@Param("userId") long userId);

    List<Integer> findCourseId(@Param("subjectId") long subjectId);

    Integer findMaxCourseGrade(@Param("courseId") long courseId,@Param("userId") long userId);

    Integer findSubjectGrade(@Param("userId") long userId,@Param("subjectId") long subjectId);

    List<Certificate> getBySubjectIdAndUserId(@Param("userId") long userId,@Param("subjectId") long subjectId);

    Integer deleteCertificate(@Param("userId") long userId,@Param("subjectName") String subjectName);

    Integer countExams(@Param("userId") long userId,@Param("subjectId") long subjectId);

    List<Certificate> exportCertificates(@Param("subjectName") String subjectName);

    List<Certificate> exportCertificate(@Param("subjectId") Long subjectId);

    Certificate findByUrl(String certificateUrl);
}
