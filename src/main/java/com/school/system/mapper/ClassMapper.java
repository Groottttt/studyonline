package com.school.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.school.gen.entity.Exam;
import com.school.gen.entity.PlayRecord;
import com.school.system.entity.ClassPojo;
import com.school.system.entity.User;
import org.apache.ibatis.annotations.Param;

/**
 * @author Mr.Z
 * @title: ClassMapper
 * @projectName studyonline
 * @description: TODO
 * @date 2022/9/216:33
 */
@SuppressWarnings({"all"})
public interface ClassMapper extends BaseMapper<ClassPojo> {

    IPage<ClassPojo> getAllClass(Page<ClassPojo> page, @Param("classPojo") ClassPojo classPojo);

    IPage<User> findStudents(Page<User> page, @Param("user") User user,@Param("classesName") String classesName);

    IPage<PlayRecord> findStudy(Page<PlayRecord> page, @Param("playRecord") PlayRecord playRecord, @Param("userId") long userId);

    IPage<Exam> findExam(Page<Exam> page, @Param("exam") Exam exam, @Param("userId") long userId);
}
