package com.school.system.mapper;

import com.school.system.entity.Nationality;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author MrBird
 */
public interface NationalityMapper extends BaseMapper<Nationality> {
}
