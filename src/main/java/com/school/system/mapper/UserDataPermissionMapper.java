package com.school.system.mapper;

import com.school.system.entity.UserDataPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author MrBird
 */
public interface UserDataPermissionMapper extends BaseMapper<UserDataPermission> {

}
