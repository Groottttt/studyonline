package com.school.system.mapper;

import com.school.system.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author MrBird
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
