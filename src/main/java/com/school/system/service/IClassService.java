package com.school.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.school.common.entity.QueryRequest;
import com.school.common.entity.SchoolResponse;
import com.school.gen.entity.Exam;
import com.school.gen.entity.PlayRecord;
import com.school.system.entity.ClassPojo;
import com.school.system.entity.User;

import javax.validation.constraints.NotBlank;

/**
 * @author Mr.Z
 * @title: IClassService
 * @projectName studyonline
 * @description: TODO
 * @date 2022/9/216:28
 */
@SuppressWarnings({"all"})
public interface IClassService extends IService<ClassPojo> {

    IPage<ClassPojo> findClassDetailList(ClassPojo classPojo, QueryRequest request);

    SchoolResponse classAdd(ClassPojo classPojo);

    SchoolResponse classUpdata(ClassPojo classPojo);

    SchoolResponse classDelete( Integer[] classIds);

    IPage<User> findStudents(String classesName, User user, QueryRequest request);

    IPage<PlayRecord> findStudy(long userId, PlayRecord playRecord, QueryRequest request);

    IPage<Exam> findExam(long userId, Exam exam, QueryRequest request);
}
