package com.school.system.service;

import com.school.common.entity.DeptTree;
import com.school.system.entity.Nationality;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author MrBird
 */
public interface INationalityService extends IService<Nationality> {

    /**
     * 获取民族树（下拉选使用）
     *
     * @return 民族树集合
     */
    List<DeptTree<Nationality>> findNationality();

    /**
     * 获取民族列表（树形列表）
     *
     * @param nationality 民族对象（传递查询参数）
     * @return 民族树
     */
    List<DeptTree<Nationality>> findNationality(Nationality nationality);

}
