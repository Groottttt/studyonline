package com.school.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.school.common.entity.QueryRequest;
import com.school.common.entity.SchoolResponse;
import com.school.gen.entity.Exam;
import com.school.gen.entity.PlayRecord;
import com.school.system.entity.ClassPojo;
import com.school.system.entity.Menu;
import com.school.system.entity.User;
import com.school.system.mapper.ClassMapper;
import com.school.system.mapper.MenuMapper;
import com.school.system.service.IClassService;
import com.school.system.service.IMenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Mr.Z
 * @title: ClassServiceImpl
 * @projectName studyonline
 * @description: TODO
 * @date 2022/9/216:31
 */
@SuppressWarnings({"all"})

@Service
@RequiredArgsConstructor
public class ClassServiceImpl extends ServiceImpl<ClassMapper, ClassPojo> implements IClassService {

    @Override
    public IPage<ClassPojo> findClassDetailList(ClassPojo classPojo, QueryRequest request) {
        Page<ClassPojo> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<ClassPojo> allClass = baseMapper.getAllClass(page, classPojo);
        return allClass;
    }

    @Override
    public SchoolResponse classAdd(ClassPojo classPojo) {
        classPojo.setCREATE_TIME(new Date());
        if (baseMapper.insert(classPojo) == 1) {
            return new SchoolResponse().success();
        }
        return new SchoolResponse().fail().data("添加失败");
    }

    @Override
    public SchoolResponse classUpdata(ClassPojo classPojo) {
        if (baseMapper.updateById(classPojo) == 1) {
            return new SchoolResponse().success();
        }
        return new SchoolResponse().fail().data("更新失败");
    }

    @Override
    public SchoolResponse classDelete(Integer[] classIds) {
        if (classIds.length < 1) {
            return new SchoolResponse().fail().data("删除失败");
        }
        baseMapper.deleteBatchIds(Arrays.asList(classIds));
        return new SchoolResponse().success();
    }

    @Override
    public IPage<User> findStudents(String classesName, User user, QueryRequest request) {
        Page<User> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<User> allUser = baseMapper.findStudents(page, user, classesName);
        return allUser;
    }

    @Override
    public IPage<PlayRecord> findStudy(long userId, PlayRecord playRecord, QueryRequest request) {

        Page<PlayRecord> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<PlayRecord> allPlayRecord = baseMapper.findStudy(page, playRecord, userId);
        List<PlayRecord> playRecordList = allPlayRecord.getRecords();
        //格式处理
        playRecordList.forEach(e -> {
            //学习进度
            e.setPercent(getPercentage(Integer.valueOf(e.getCourseDuration()), Integer.valueOf(e.getPlayTime())));
            e.setCourseDuration(timeFormatConversion(e.getCourseDuration()));
            e.setPlayTime(timeFormatConversion(e.getPlayTime()));

        });
        return allPlayRecord;

    }

    @Override
    public IPage<Exam> findExam(long userId, Exam exam, QueryRequest request) {
        Page<Exam> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<Exam> allExam = baseMapper.findExam(page, exam, userId);
        return allExam;
    }


    /**
     * 获取百分比处理
     */
    private String getPercentage(Integer courseDuration, Integer playTime) {
        if (courseDuration > playTime) {
            return String.format("%.2f", ((double) playTime / courseDuration) * 100) + "%";
        }
        return "100.00%";
    }

    /**
     * /时间格式处理
     */

    private String timeFormatConversion(String courseDuration) {
        int time = Integer.valueOf(courseDuration);
        int[] hh = {time / 60 / 60, time % (60 * 60) / 60, time % 60};
        String t = "";
        for (int i = 0; i < hh.length; i++) {
            if (hh[i] < 10) {
                t = t + "0" + hh[i] + ":";
            } else {
                t = t + hh[i] + ":";
            }
        }
        StringBuilder sb = new StringBuilder(t);
        sb.replace(t.length() - 1, t.length(), String.valueOf(""));
        return sb.toString();
    }
}
