package com.school.system.service.impl;

import com.school.common.entity.DeptTree;
import com.school.common.utils.TreeUtil;
import com.school.system.entity.Nationality;
import com.school.system.mapper.NationalityMapper;
import com.school.system.service.INationalityService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author MrBird
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class NationalityServiceImpl extends ServiceImpl<NationalityMapper, Nationality> implements INationalityService {

    @Override
    public List<DeptTree<Nationality>> findNationality() {
        List<Nationality> nationalityList = baseMapper.selectList(new QueryWrapper<>());
        List<DeptTree<Nationality>> trees = convertNationality(nationalityList);
        return TreeUtil.buildDeptTree(trees);
    }

    @Override
    public List<DeptTree<Nationality>> findNationality(Nationality nationality) {
        QueryWrapper<Nationality> queryWrapper = new QueryWrapper<>();

        if (StringUtils.isNotBlank(nationality.getNationalityName())) {
            queryWrapper.lambda().eq(Nationality::getNationalityName, nationality.getNationalityName());
        }
        queryWrapper.lambda().orderByAsc(Nationality::getOrderNum);

        List<Nationality> nationalityList = baseMapper.selectList(queryWrapper);
        List<DeptTree<Nationality>> trees = convertNationality(nationalityList);
        return TreeUtil.buildDeptTree(trees);
    }

    private List<DeptTree<Nationality>> convertNationality(List<Nationality> Nationality) {
        List<DeptTree<Nationality>> trees = new ArrayList<>();
        Nationality.forEach(nationality -> {
            DeptTree<Nationality> tree = new DeptTree<>();
            tree.setId(String.valueOf(nationality.getNationalityId()));
            tree.setName(nationality.getNationalityName());
            trees.add(tree);
        });
        return trees;
    }
}
