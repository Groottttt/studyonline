package com.school.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.zxing.WriterException;
import com.school.common.entity.SchoolConstant;
import com.school.common.entity.QueryRequest;
import com.school.common.entity.SchoolResponse;
import com.school.common.entity.Strings;
import com.school.common.event.UserAuthenticationUpdatedEventPublisher;
import com.school.common.exception.SchoolException;
import com.school.common.utils.SchoolUtil;
import com.school.common.utils.Md5Util;
import com.school.common.utils.SortUtil;
import com.school.gen.entity.*;
import com.school.gen.mapper.*;
import com.school.gen.service.ICourseService;
import com.school.gen.service.IFaceDetectService;
import com.school.gen.service.IPlayRecordService;
import com.school.system.entity.*;
import com.school.system.entity.Menu;
import com.school.system.mapper.CertificateMapper;
import com.school.system.mapper.UserMapper;
import com.school.system.service.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Sets;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author MrBird
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    private final IMenuService menuService;
    private final IRoleService roleService;
    private final IUserRoleService userRoleService;
    private final UserAuthenticationUpdatedEventPublisher publisher;
    private final IUserDataPermissionService userDataPermissionService;
    private final IFaceDetectService faceDetectService;
    private final IPlayRecordService playRecordService;
    private final PlayFinishRecordMapper playFinishRecordMapper;
    private final ICourseService courseService;
    private final UserMapper userMapper;
    private final CertificateMapper certificateMapper;
    private final ExamMapper examMapper;
    private final SubjectMapper subjectMapper;
    private final CourseMapper courseMapper;
    private final PlayRecordMapper playRecordMapper;


    @Override
    public User findByName(String username) {
        return baseMapper.findByName(username);
    }

    @Override
    public IPage<User> findUserDetailList(User user, QueryRequest request) {
        if (StringUtils.isNotBlank(user.getCreateTimeFrom())
                && StringUtils.equals(user.getCreateTimeFrom(), user.getCreateTimeTo())) {
            user.setCreateTimeFrom(user.getCreateTimeFrom() + SchoolConstant.DAY_START_PATTERN_SUFFIX);
            user.setCreateTimeTo(user.getCreateTimeTo() + SchoolConstant.DAY_END_PATTERN_SUFFIX);
        }
        Page<User> page = new Page<>(request.getPageNum(), request.getPageSize());
        page.setSearchCount(false);
        page.setTotal(baseMapper.countUserDetail(user));
        SortUtil.handlePageSort(request, page, "userId", SchoolConstant.ORDER_ASC, false);
        IPage<User> userIPage = baseMapper.findUserDetailPage(page, user);
        for (User user1 : userIPage.getRecords()) {
            if (StringUtils.isBlank(user1.getIdcard()) || StringUtils.isBlank(user1.getSubjectId()) || StringUtils.isBlank(user1.getOneInchPhoto()) || StringUtils.isBlank(user1.getIdcardFront()) || StringUtils.isBlank(user1.getIdcardNegative())) {
                user1.setHasDetected("否");
            } else {
                user1.setHasDetected("是");
            }
            if (user1.getCertificateUrl() == null) {
                user1.setHasCertificate("否");
            } else {
                user1.setHasCertificate("是");
            }
        }
        return userIPage;
    }

    @Override
    public User findUserDetailList(String username) {
        User param = new User();
        param.setUsername(username);
        List<User> users = baseMapper.findUserDetail(param);
        return CollectionUtils.isNotEmpty(users) ? users.get(0) : null;
    }


    @Override
    public User getById(Long userId) {
        User user = baseMapper.getById(userId);
        if (StringUtils.isNoneBlank(user.getSubjectId())) {
            PlayFinishRecord playFinishRecord = new PlayFinishRecord();
            playFinishRecord.setUserId(userId);
            List<PlayFinishRecord> playFinishRecords = playFinishRecordMapper.listPlayFinishRecords(playFinishRecord);
            Course course = new Course();
            course.setSubject(Long.valueOf(user.getSubjectId()));
            List<Course> courses = courseService.findCourses(course);
            List<Course> playFinishCourses = new ArrayList<>();
            for (Course course2 : courses) {
                for (PlayFinishRecord playFinishRecord1 : playFinishRecords) {
                    if (playFinishRecord1.getCourseId().longValue() == course2.getCourseId().longValue()) {
                        playFinishCourses.add(course2);
                    }
                }
            }


            user.setPlayFinishedCourses(playFinishCourses);
        }
        return user;
    }

    @Override
    public User findByMobile(String mobile) {
        User user = baseMapper.findByMobile(mobile);
        return user;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateLoginTime(String username) {
        User user = new User();
        user.setLastLoginTime(new Date());
        baseMapper.update(user, new LambdaQueryWrapper<User>().eq(User::getUsername, username));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createUser(User user) {
        user.setCreateTime(new Date());
        user.setStatus(User.STATUS_VALID);
        user.setAvatar(User.DEFAULT_AVATAR);
        user.setTheme(User.THEME_BLACK);
        user.setIsTab(User.TAB_OPEN);
        user.setPassword(Md5Util.encrypt(user.getUsername(), User.DEFAULT_PASSWORD));
        save(user);
        // 保存用户角色
        String[] roles = user.getRoleId().split(Strings.COMMA);
        setUserRoles(user, roles);
        // 保存用户数据权限关联关系
        String[] deptIds = StringUtils.splitByWholeSeparatorPreserveAllTokens(user.getDeptIds(), Strings.COMMA);
        if (ArrayUtils.isNotEmpty(deptIds)) {
            setUserDataPermissions(user, deptIds);
        }
        File file1 = new File(File.separator + "data" + File.separator + "SCHOOL" + File.separator + "upload" + File.separator + "certificate" + File.separator + user.getUserId());
        if (!file1.exists()) {
            file1.mkdirs();
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteUsers(String[] userIds) {
        List<String> list = Arrays.asList(userIds);
        // 删除用户
        removeByIds(list);
        // 删除关联角色
        userRoleService.deleteUserRolesByUserId(list);
        // 删除关联数据权限
        userDataPermissionService.deleteByUserIds(userIds);
        // 删除人脸检测记录
        faceDetectService.deleteByUserIds(userIds);
        // 删除播放记录
        playRecordService.deleteByUserIds(userIds);
        //删除合格证书
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateUser(User user) {
        // 更新用户
        user.setPassword(null);
        user.setUsername(null);
        user.setModifyTime(new Date());
        updateById(user);

        String[] userId = {String.valueOf(user.getUserId())};
        userRoleService.deleteUserRolesByUserId(Arrays.asList(userId));
        String[] roles = StringUtils.splitByWholeSeparatorPreserveAllTokens(user.getRoleId(), Strings.COMMA);
        if (ArrayUtils.isNotEmpty(roles)) {
            setUserRoles(user, roles);
        }

        userDataPermissionService.deleteByUserIds(userId);
        String[] deptIds = StringUtils.splitByWholeSeparatorPreserveAllTokens(user.getDeptIds(), Strings.COMMA);
        if (ArrayUtils.isNotEmpty(deptIds)) {
            setUserDataPermissions(user, deptIds);
        }
        publisher.publishEvent(Sets.newHashSet(user.getUserId()));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void resetPassword(String[] usernames) {
        Arrays.stream(usernames).forEach(username -> {
            User user = new User();
            user.setPassword(Md5Util.encrypt(username, User.DEFAULT_PASSWORD));
            baseMapper.update(user, new LambdaQueryWrapper<User>().eq(User::getUsername, username));
        });
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void resetPasswordByIds(String[] userIds) {
        Arrays.stream(userIds).forEach(userId -> {
            User user = baseMapper.selectById(userId);
            user.setPassword(Md5Util.encrypt(user.getUsername(), User.DEFAULT_PASSWORD));
            baseMapper.update(user, new LambdaQueryWrapper<User>().eq(User::getUsername, user.getUsername()));
        });
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void register(String username, String password) {
        User user = new User();
        user.setPassword(Md5Util.encrypt(username, password));
        user.setUsername(username);
        user.setCreateTime(new Date());
        user.setStatus(User.STATUS_VALID);
        user.setSex(User.SEX_UNKNOW);
        user.setAvatar(User.DEFAULT_AVATAR);
        user.setTheme(User.THEME_BLACK);
        user.setIsTab(User.TAB_OPEN);
        user.setDescription("注册用户");
        save(user);

        UserRole ur = new UserRole();
        ur.setUserId(user.getUserId());
        ur.setRoleId(SchoolConstant.REGISTER_ROLE_ID);
        userRoleService.save(ur);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updatePassword(String username, String password) {
        User user = new User();
        user.setPassword(Md5Util.encrypt(username, password));
        user.setModifyTime(new Date());
        baseMapper.update(user, new LambdaQueryWrapper<User>().eq(User::getUsername, username));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateAvatar(String username, String avatar) {
        User user = new User();
        user.setAvatar(avatar);
        user.setModifyTime(new Date());
        baseMapper.update(user, new LambdaQueryWrapper<User>().eq(User::getUsername, username));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateTheme(String username, String theme, String isTab) {
        User user = new User();
        user.setTheme(theme);
        user.setIsTab(isTab);
        user.setModifyTime(new Date());
        baseMapper.update(user, new LambdaQueryWrapper<User>().eq(User::getUsername, username));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateProfile(User user) {
        user.setUsername(null);
        user.setRoleId(null);
        user.setPassword(null);
        if (isCurrentUser(user.getUserId())) {
            updateById(user);
        } else {
            throw new SchoolException("您无权修改别人的账号信息！");
        }
    }

    @Override
    public void doGetUserAuthorizationInfo(User user) {
        // 获取用户角色集
        List<Role> roleList = roleService.findUserRole(user.getUsername());
        Set<String> roleSet = roleList.stream().map(Role::getRoleName).collect(Collectors.toSet());
        user.setRoles(roleSet);

        // 获取用户权限集
        List<Menu> permissionList = menuService.findUserPermissions(user.getUsername());
        Set<String> permissionSet = permissionList.stream().map(Menu::getPerms).collect(Collectors.toSet());
        user.setStringPermissions(permissionSet);
    }

    private void setUserRoles(User user, String[] roles) {
        List<UserRole> userRoles = new ArrayList<>();
        Arrays.stream(roles).forEach(roleId -> {
            UserRole userRole = new UserRole();
            userRole.setUserId(user.getUserId());
            userRole.setRoleId(Long.valueOf(roleId));
            userRoles.add(userRole);
        });
        userRoleService.saveBatch(userRoles);
    }

    private void setUserDataPermissions(User user, String[] deptIds) {
        List<UserDataPermission> userDataPermissions = new ArrayList<>();
        Arrays.stream(deptIds).forEach(deptId -> {
            UserDataPermission permission = new UserDataPermission();
            permission.setDeptId(Long.valueOf(deptId));
            permission.setUserId(user.getUserId());
            userDataPermissions.add(permission);
        });
        userDataPermissionService.saveBatch(userDataPermissions);
    }

    private boolean isCurrentUser(Long id) {
        User currentUser = SchoolUtil.getCurrentUser();
        return currentUser.getUserId().equals(id);
    }





    @Override
    public List<User> findUserList(long id) {
        List<User> users = userMapper.findExcel(id);
        for (User user1 : users) {
            Course course = new Course();
            course.setSubject(Long.valueOf(user1.getSubjectId()));
            List<Course> courses = courseService.findCourses(course);
            PlayFinishRecord playFinishRecord = new PlayFinishRecord();
            playFinishRecord.setUserId(user1.getUserId());
            List<PlayFinishRecord> num = playFinishRecordMapper.countFinishRecords(user1.getUserId(), Long.valueOf(user1.getSubjectId()));
            user1.setFinishCourseNum(num.size() + "/" + courses.size());
        }
        return users;
    }



    @Override
    public User findByIdcard(String idcard) {
        User user = baseMapper.findByIdCard(idcard);
        return user;
    }

    @Override
    public SchoolResponse generateCertificate(Long userId, Long subjectId)  {
        List<Certificate> cc = certificateMapper.getBySubjectIdAndUserId(userId, subjectId);
        List<Certificate> certificates = certificateMapper.getById(userId);
        User user = baseMapper.getById(userId);
        Certificate c1 = new Certificate();
        if (cc.size()>=1){
            c1 = cc.get(0);
        }

            //判断有没有合格证
            c1.setGrade(null);
            if (certificates.contains(c1)) {
                return new SchoolResponse().message("该用户已有合格证书!");
            }
            //判断有没有相应的考试 返回成绩
            Exam exam =baseMapper.getBySubjectIdAndUserId(userId, subjectId);
            if (exam==null){
                //没有考试 判断课程看完没
                int playNum = playFinishRecordMapper.countFinishRecords(userId, subjectId).size();
                Course c = new Course();
                c.setSubject(subjectId);
                int sumNum=courseService.findCourses(c).size();
                if (playNum<sumNum){
                    //没看完
                    return new SchoolResponse().message("该用户有课程未看完!");
                }else {
                    //看完了
                    return  newCertificate(user, subjectId,60);
                }
            }else {
                //有考试
                if (exam.getScore()>60){
                    //及格
                    return  newCertificate(user, subjectId,exam.getScore());
                }else{
                    //不及格
                    return new SchoolResponse().message("该课程考试未及格!");
                }
            }



    }

    @Override
    public SchoolResponse deleteCertificate(Long userId, Long subjectId) {
        Subject subject =subjectMapper.selectById(subjectId);
        Certificate certificate= certificateMapper.selectOne(new UpdateWrapper<Certificate>().eq("user_id",userId).eq("subject_name",subject.getTitle()));
        if (certificateMapper.deleteById(certificate.getId())==1){
            return new SchoolResponse().success();
        }
        return new SchoolResponse().message("操作失败请重试!");
    }

    /**
     * 生成合格证
     * @param
     * @param subjectId
     * @param score
     * @return
     */
    private SchoolResponse newCertificate(User user, Long subjectId, Integer score) {
        Certificate certificate = new Certificate();
        Subject subject=subjectMapper.findById(subjectId);
        String certificateUrl=null;
        try {
             certificateUrl = drawCertificate(user, subject);
        } catch (WriterException | IOException e) {
            e.printStackTrace();
        }

        certificate.setUserId(user.getUserId());
        certificate.setSubjectName(subject.getTitle());
        certificate.setCCreateTime(new Date());
        certificate.setCertificateUrl(certificateUrl);
        certificate.setGrade(score);
        if ( certificateMapper.insert(certificate)==1){
            return new SchoolResponse().success();
        }
        return new SchoolResponse().message("生成失败，请稍后重试");

    }
    /**
     *     拼接路径
     */
    private String drawCertificate(User user, Subject subject) throws WriterException, IOException {
        return File.separator + "upload" + File.separator + "certificate" + File.separator
                + user.getUserId() + File.separator + "certificate" + subject.getSubjectId() + ".png";
    }

}
