layui.define(function(exports) {
  exports('conf', {
    container: 'school',
    containerBody: 'school-body',
    v: '2.0',
    base: layui.cache.base,
    css: layui.cache.base + 'css/',
    views: layui.cache.base + 'views/',
    viewLoadBar: true,
    debug: layui.cache.debug,
    name: 'school',
    entry: '/index',
    engine: '',
    eventName: 'school-event',
    tableName: 'school',
    requestUrl: './'
  })
});
